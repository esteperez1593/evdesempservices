//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Desempeño.DataConect
{
    using System;
    using System.Collections.Generic;
    
    public partial class PZVPERM
    {
        public int PZVPERM_CODE { get; set; }
        public string PZVPERM_POST_CODE { get; set; }
        public string PZVPERM_DNCY_CODE { get; set; }
        public string PZVPERM_PFLE_CODE { get; set; }
        public Nullable<int> PZVPERM_PIDM { get; set; }
        public string PZVPERM_RGHT_CODE { get; set; }
        public string PZVPERM_USER { get; set; }
        public System.DateTime PZVPERM_ACTIVITY_DATE { get; set; }
        public string PZVPERM_DATA_ORIGIN { get; set; }
    
        public virtual PZRASPE PZRASPE { get; set; }
        public virtual PZVPFLE PZVPFLE { get; set; }
        public virtual PZVRGHT PZVRGHT { get; set; }
    }
}
