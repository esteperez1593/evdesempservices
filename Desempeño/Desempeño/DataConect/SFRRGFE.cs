//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Desempeño.DataConect
{
    using System;
    using System.Collections.Generic;
    
    public partial class SFRRGFE
    {
        public string SFRRGFE_TERM_CODE { get; set; }
        public string SFRRGFE_DETL_CODE { get; set; }
        public System.DateTime SFRRGFE_ACTIVITY_DATE { get; set; }
        public decimal SFRRGFE_PER_CRED_CHARGE { get; set; }
        public decimal SFRRGFE_MIN_CHARGE { get; set; }
        public decimal SFRRGFE_MAX_CHARGE { get; set; }
        public string SFRRGFE_PTRM_CODE { get; set; }
        public string SFRRGFE_RESD_CODE { get; set; }
        public string SFRRGFE_LEVL_CODE { get; set; }
        public string SFRRGFE_COLL_CODE { get; set; }
        public string SFRRGFE_MAJR_CODE { get; set; }
        public string SFRRGFE_CLAS_CODE { get; set; }
        public string SFRRGFE_RATE_CODE { get; set; }
        public string SFRRGFE_CRSE_WAIV_IND { get; set; }
        public Nullable<decimal> SFRRGFE_FROM_CRED_HRS { get; set; }
        public Nullable<decimal> SFRRGFE_TO_CRED_HRS { get; set; }
        public Nullable<System.DateTime> SFRRGFE_FROM_ADD_DATE { get; set; }
        public Nullable<System.DateTime> SFRRGFE_TO_ADD_DATE { get; set; }
        public string SFRRGFE_TYPE { get; set; }
        public string SFRRGFE_CAMP_CODE { get; set; }
        public string SFRRGFE_LEVL_CODE_CRSE { get; set; }
        public string SFRRGFE_CAMP_CODE_CRSE { get; set; }
        public string SFRRGFE_ENTRY_TYPE { get; set; }
        public Nullable<decimal> SFRRGFE_FROM_STUD_HRS { get; set; }
        public Nullable<decimal> SFRRGFE_TO_STUD_HRS { get; set; }
        public string SFRRGFE_CRED_IND { get; set; }
        public Nullable<decimal> SFRRGFE_FLAT_HRS { get; set; }
        public string SFRRGFE_COPY_IND { get; set; }
        public int SFRRGFE_SEQNO { get; set; }
        public string SFRRGFE_PROGRAM { get; set; }
        public string SFRRGFE_DEPT_CODE { get; set; }
        public string SFRRGFE_DEGC_CODE { get; set; }
        public string SFRRGFE_STYP_CODE { get; set; }
        public string SFRRGFE_TERM_CODE_ADMIT { get; set; }
        public string SFRRGFE_ATTR_CODE_CRSE { get; set; }
        public string SFRRGFE_ATTS_CODE { get; set; }
        public string SFRRGFE_GMOD_CODE { get; set; }
        public string SFRRGFE_ASSESS_BY_COURSE_IND { get; set; }
        public string SFRRGFE_USER_ID { get; set; }
        public Nullable<decimal> SFRRGFE_FROM_FLAT_HRS { get; set; }
        public Nullable<decimal> SFRRGFE_TO_FLAT_HRS { get; set; }
        public Nullable<decimal> SFRRGFE_FLAT_FEE_AMOUNT { get; set; }
        public Nullable<decimal> SFRRGFE_CRSE_OVERLOAD_START_HR { get; set; }
        public string SFRRGFE_INSM_CODE { get; set; }
        public string SFRRGFE_SCHD_CODE { get; set; }
        public string SFRRGFE_LFST_CODE { get; set; }
        public string SFRRGFE_PRIM_SEC_CDE { get; set; }
        public string SFRRGFE_RATE_CODE_CURRIC { get; set; }
        public string SFRRGFE_STYP_CODE_CURRIC { get; set; }
        public string SFRRGFE_CHRT_CODE { get; set; }
        public string SFRRGFE_VTYP_CODE { get; set; }
        public string SFRRGFE_LFST_PRIM_SEC_CDE { get; set; }
        public string SFRRGFE_STSP_RESD_CODE { get; set; }
        public string SFRRGFE_STSP_SESS_CODE { get; set; }
    }
}
