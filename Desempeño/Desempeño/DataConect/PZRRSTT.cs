//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Desempeño.DataConect
{
    using System;
    using System.Collections.Generic;
    
    public partial class PZRRSTT
    {
        public int PZRRSTT_ID { get; set; }
        public int PZRRSTT_PLAP_SEQ_NUMBER { get; set; }
        public string PZRRSTT_DESCRIPTION { get; set; }
        public Nullable<int> PZRRSTT_RESULT_VALUE { get; set; }
        public string PZRRSTT_USER { get; set; }
        public System.DateTime PZRRSTT_ACTIVITY_DATE { get; set; }
        public string PZRRSTT_DATA_ORIGIN { get; set; }
    
        public virtual PZRPLAP PZRPLAP { get; set; }
    }
}
