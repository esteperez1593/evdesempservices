﻿using Desempeño.DataConect;
using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;

namespace Desempeño.Repository
{
    public class FunctionsRepository
    {
        private Entities db = new Entities();

        /// <summary>
        ///  
        /// Calcula el indice de desempeño para un usuario en funcion del 
        /// valor porcentual que representa para el perfil que tiene asignado
        ///  
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <param name="_term"></param>
        /// <returns></returns>

        public decimal GetIndPerc(string _perfil, string _calendario, string _instrumento, string _pidm, string _term)
        {


            try
            {

                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_RESULT_COMP('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "','" + _term + "') FROM DUAL");
                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }


        }

        /// <summary>
        /// 
        /// Obtiene el porcentaje asignado a un instrumento para un
        /// oerfil dado
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_term"></param>
        /// <returns></returns>

        public decimal GetPercProf(string _perfil, string _calendario, string _instrumento, string _term)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_PERC_EVAL('" + _perfil + "','" + _term + "','" + _instrumento + "','" + _calendario + "') FROM DUAL");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return 0;

            }



        }

        public decimal GetPercPoll(String profile_code,
                                     String term_code,
                                     String poll_code,
                                     String calr_code,
                                     String pidm)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval.F_GET_PERC_POLL ('" + profile_code + "','" + term_code + "','" + poll_code + "','" + calr_code + "','" + pidm + "') from dual ");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return 0;

            }
        }

        /// <summary>
        /// 
        /// Obtiene la cantidad de preguntas a totalizar
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        public decimal GetCountItem(string _perfil, string _calendario, string _instrumento, string _pidm)
        {

            try
            {

                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_RESULT_COUNT('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return 0;

            }



        }

        /// <summary>
        ///
        /// Obtiene el valor correspondiente al diferencial 
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        public decimal GetDifItem(string _perfil, string _calendario, string _instrumento, string _pidm)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_RESULT_DIF('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }
        }

        /// <summary>
        /// 
        /// Obtiene la media de las respuestas de un Instrumento
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>

        public decimal GetMedItem(string _perfil, string _calendario, string _instrumento, string _pidm)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_RESULT_MED('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }
        }

        /// <summary>
        /// 
        /// Calculo de la sumatoria correspondiente a un instrumento
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        public decimal GetSumAns(string _perfil, string _calendario, string _instrumento, string _pidm)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_RESULT_SUM('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }
        }

        /// <summary>
        ///
        /// Calcula el porcentaje de satisfacion en funcion de las respuestas registradas
        /// 
        /// </summary>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        public decimal GetPercObjT(string _instrumento, string _pidm, string _calr, string _pfle)
        {

            try
            {
                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval.F_GET_TOTAL_OBJ_PERC ('" + _pidm + "','" + _instrumento + ",'" + _calr + "','" + _pfle + "') from dual ");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }
        }

        /// <summary>
        /// 
        /// Calcula el valor porcentual para un objetivo aun no registrado
        /// 
        /// </summary>
        /// <param name="_resp_value"></param>
        /// <param name="_perc_value"></param>
        /// <returns></returns>

        public decimal GetPercObj(decimal _resp_value, decimal _perc_value)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval.F_GET_OBJ_PERC ('" + _resp_value + "','" + _perc_value + "') from dual ");
                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }
        }

        /*
         * 
         * Instrumento de Extension
         * 
         */

        /// <summary>
        /// 
        /// Suma las horas correspondientes a los proyectos de extension
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>

        public decimal GetSumProyExt(string _perfil, string _calendario, string _instrumento, string _pidm)
        {


            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_POLL_EXT('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }


        }

        /// <summary>
        /// 
        /// Devuelve el monto actual de la UC 
        /// para el calculo de los instrumentos de extension
        ///  
        /// </summary>
        /// <param name="_term"></param>
        /// <param name="_attr"></param>
        /// <returns></returns>

        public decimal GetUCAmount(string _term)
        {

            try
            {

                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval.F_GET_AMOUNT_UC ('" + _term + "') from dual ");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }



        }

        /// <summary>
        ///  
        ///  Devuelve expresado en Unidades Credito el Valor correspondiente 
        ///  a los ingresos generados a la UCAB por concepto de asesoria y/o
        ///  Consultoria
        ///  
        /// </summary>
        /// <param name="_term"></param>
        /// <param name="_amount"></param>
        /// <returns></returns>

        public decimal GetUCValue(string _term, string _amount)
        {

            try
            {

                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval.F_GET_VALUE_UC_EXT ('" + _term + "','" + _amount + "') from dual ");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }



        }

        /// <summary>
        /// 
        /// Calcula los puntos obtenidos en funcion del costo de la UC y 
        /// del monto ingresado por concepto de asesorias o consultorias
        /// 
        /// </summary>
        /// <param name="_term"></param>
        /// <param name="_amount"></param>
        /// <returns></returns>

        public decimal GetUCPoints(string _term, string _amount)
        {

            try
            {

                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval.F_GET_POINTS_UC_EXT ('" + _term + "','" + _amount + "') from dual ");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }



        }

        /// <summary>
        /// 
        /// Calculo de la sumatoria correspondiente a un instrumento 
        /// por areas
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>

        public decimal GetSumAns(string _perfil, string _calendario, string _instrumento, string _pidm, string _area)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_RESULT_SUM('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "','" + _area + "') FROM DUAL");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }
        }

        /*
         * 
         * Instrumento de Investigadores
         * 
         */

        /// <summary>
        /// 
        /// Calcula los puntos por publicaciones
        /// 
        /// </summary>
        /// <param name="_pregunta"></param>
        /// <param name="_cantidad"></param>
        /// <returns></returns>
        public decimal GetInvPub(string _pregunta, string _cantidad)
        {

            try
            {

                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval. F_CALC_INVE_PUBL('" + _pregunta + "','" + _cantidad + "') from dual ");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }



        }

        /// <summary>
        /// 
        /// Calcula los puntos por comite
        /// 
        /// </summary>
        /// <param name="_pregunta"></param>
        /// <param name="_cantidad"></param>
        /// <returns></returns>
        public decimal GetInvCom(string _pregunta, string _cantidad)
        {

            try
            {

                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval. F_CALC_INVE_CONS('" + _pregunta + "','" + _cantidad + "') from dual ");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }



        }

        /*
         * 
         * General de funciones
         * 
         */

        /// <summary>
        ///  
        /// Obtiene el total de areas que componen un instrumento
        ///  
        /// </summary>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <returns></returns>

        public decimal GetAreaIns(string _calendario, string _instrumento)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_AREA_COUNT('" + _calendario + "','" + _instrumento + "') FROM DUAL");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }
        }


        /// <summary>
        /// 
        /// Devuelve el total de preguntas que componen un Area
        /// 
        /// </summary>
        /// <param name="_term"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_perfil"></param>
        /// <returns></returns>

        public decimal GetPregCont(string _term, string _calendario, string _instrumento, string _perfil)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_COUNT_QSTN('" + _term + "', '" + _calendario + "','" + _instrumento + "','" + _perfil + "') FROM DUAL");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }
        }


        /// <summary>
        /// 
        /// Obtiene el codigo de los instrumentos
        /// 
        /// </summary>
        /// <param name="_instrumento"></param>
        /// <returns></returns>

        public String GetInsCod(string _instrumento)
        {
            try
            {
                var res = db.Database.SqlQuery<String>(@"SELECT BZPKEVAL.F_GET_POLL_CODE('" + _instrumento + "') FROM DUAL");

                foreach (string resp in res)
                {
                    return resp;
                }
                return "No encontrado";
            }

            catch (Exception e)
            {

                return ("Error");

            }
        }



        /// <summary>
        /// 
        /// Obtiene el codigo de un perfil
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <returns></returns>
        public String GetPerCod(string _perfil)
        {
            try
            {
                var res = db.Database.SqlQuery<String>(@"SELECT BZPKEVAL.F_GET_PFLE_CODE('" + _perfil + "') FROM DUAL");

                foreach (String resp in res)
                {
                    return resp;
                }
                return "No encontrado";
            }

            catch (Exception e)
            {

                return ("Error");

            }
        }


        /// <summary>
        /// 
        /// Obtiene el codigo del calendario
        /// 
        /// </summary>
        /// <param name="_calendario"></param>
        /// <returns></returns>

        public String GetCalCod(string _calendario)
        {
            try
            {
                var res = db.Database.SqlQuery<String>(@"SELECT BZPKEVAL.F_GET_CALR_CODE('" + _calendario + "') FROM DUAL");

                foreach (string resp in res)
                {
                    return resp;
                }
                return "No encontrado";
            }

            catch (Exception e)
            {

                return (("Error"));

            }
        }

        public decimal GetIndPoll(string _perfil,string _term, string _calendario, string _instrumento, string _pidm)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_PERC_POLL('" + _perfil + "','"+_term+"','"+_instrumento+"','"+_calendario+"','"+_pidm+"') FROM DUAL");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }
        }

        public decimal GetIndTot(string _perfil, string _term,  string _pidm)
        {
            try
            {

                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_TOTAL_INDEX('" + _perfil + "','" + _term + "','" + _pidm + "') FROM DUAL");

                foreach (decimal resp in res)
                {
                    return resp;
                }
                return 0;
            }

            catch (Exception e)
            {

                return (0);

            }
        }



        public void PutPZRPLAP(int id, string user, string status)//Models.AsignacionInstrumentoPersona _PZRPLAP)
        {


            List<Models.AsignacionInstrumentoPersona> re = new List<Models.AsignacionInstrumentoPersona>();
            int _pidm = 0;
            string code ="";
            string post = "";
            string dncy = "";
            string calr = "";
            string pfle = "";
          
            int _id_plap = 0;

            

            try
            {

                var es = db.PZRPLAP.SqlQuery(@"select * FROM PZRPLAP WHERE pzrplap.PZRPLAP_SEQ_NUMBER = '" + id + "' ").ToList();


                foreach (var result in es)
                {
                    DateTime fecha = DateTime.Today;
                    _pidm = result.PZRPLAP_PIDM;
                    result.PZRPLAP_DATE = fecha;
                    result.PZRPLAP_ACTIVITY_DATE = fecha;
                    result.PZRPLAP_STATUS = status;
                    result.PZRPLAP_USER = user;
                    code = result.PZRPLAP_POLL_CODE ;
                    _id_plap = result.PZRPLAP_APPL_SEQ_NUMBER;
                    post = result.PZRPLAP_POST_CODE;
                    dncy = result.PZRPLAP_DNCY_CODE;
                    pfle = result.PZRPLAP_PFLE_CODE;
                    calr = result.PZRPLAP_CALR_CODE;
                   

                    db.PZRPLAP.Attach(result);
                    System.Diagnostics.Debug.WriteLine(result);
                    db.Entry(result).State = EntityState.Modified;
                    System.Diagnostics.Debug.WriteLine((db.Entry(result).State = EntityState.Modified));
                    db.SaveChanges();
                   
           
                   
                }
                if (status == "F")
                {

                    String _eval = IdEvaluador(_pidm, pfle,post,dncy,code,calr);
                    int _pidm_evaluador = Convert.ToInt32(_eval);

                    String respuest2 = GetNofifier(_pidm, "Se ha cargado su evaluación ", "Estimado(a) " + NameUser(_pidm) + ": \n  El usuario " + NameUser(_pidm_evaluador)
                                                               + "ha realizado exitosamete su evaluacion " + NamePoll(code) + ". ");
                }
                else
                {
                    if (status == "V" || status == "T")
                    {
                        String respuest2 = GetNofifier(_pidm, "Se ha cargado su evaluación ", "Estimado(a) " + NameUser(_pidm) + " se ha modificado exitosamente su evaluacion " + NamePoll(code) + ". ");
                    }

                    if (status == "C")
                    {
                        String respuest2 = GetNofifier(_pidm, "Se han cargado sus objetivos", "Estimado(a) " + NameUser(_pidm) + " : {0}  se ha modificado exitosamente su evaluacion " + NamePoll(code) + ". ");
                    }
                }
                String eval_respuest = GetNofifierE(_pidm, pfle, post, dncy, code, calr,"Se ha completado una evaluación"," La evaluacion " + NamePoll(code) + " asignada a " + NameUser(_pidm) + " se ha modiificada exitosamente");

            }
            catch (Exception e)
            {

            }

        }

        public string PostPZRRSPL(string _status, int _plap, List<Models.ResultadosParciales> _PZRRSPL)
        {



            try
            {
                foreach (var i in _PZRRSPL)
                {
                    int es;
                    int num_reg = (int)db.PZRRSPL.SqlQuery(@"select * FROM PZRRSPL").Count();

                    if (num_reg == 0)
                    {

                        es = 1;
                    }

                    else
                    {
                        es = (int)db.PZRRSPL.Max(x => x.PZRRSPL_SEQ_NUMBER);


                        es = es + 1;
                    }



                    DateTime fecha = DateTime.Today;

                    i._PZRRSPL_PLAP_SEQ_NUMBER = _plap;

                    i._PZRRSPL_ACTIVITY_DATE = fecha;


                    PZRRSPL tr = new PZRRSPL();
                    tr.PZRRSPL_SEQ_NUMBER = es;
                    tr.PZRRSPL_APRS_SEQ_NUMBER = i._PZRRSPL_APRS_SEQ_NUMBER;
                    tr.PZRRSPL_PLAP_SEQ_NUMBER = i._PZRRSPL_PLAP_SEQ_NUMBER;
                    tr.PZRRSPL_NUMB_VALUE = (short)i._PZRRSPL_NUMB_VALUE;
                    tr.PZRRSPL_STR_VALUE = i._PZRRSPL_STR_VALUE;
                    tr.PZRRSPL_ACTIVITY_DATE = i._PZRRSPL_ACTIVITY_DATE;
                    tr.PZRRSPL_DATA_ORIGIN = i._PZRRSPL_DATA_ORIGIN;
                    tr.PZRRSPL_USER = i._PZRRSPL_USER;
                    



                    db.PZRRSPL.Add(tr);

                    db.SaveChanges();

                    PutPZRPLAP(tr.PZRRSPL_PLAP_SEQ_NUMBER, tr.PZRRSPL_USER, _status);

                }
                return ("Registro Guardado Exitosamente");
            }

            catch (Exception e)
            {
                return ("Error");
            }
            return ("Registro Guardado Exitosamente");
        }

        public String PutPZRRSPL(string _status, int _plap,List<ResultadosParciales> _PZRRSPL)
        {


          

            foreach (var i in _PZRRSPL)
            {
                int es = db.Database.SqlQuery<int>(@"select PZRRSPL_SEQ_NUMBER 
                                                    FROM PZRRSPL 
                                                    WHERE pzrrspl_plap_seq_number = '"+_plap+"' "+
                                                    " AND pzrrspl_aprs_seq_number = '"+i._PZRRSPL_APRS_SEQ_NUMBER+"' ").First();
                try
                {

                    i._PZRRSPL_SEQ_NUMBER = es;

                   DateTime fecha = DateTime.Today;

                    i._PZRRSPL_ACTIVITY_DATE = fecha;



                    PZRRSPL tr = new PZRRSPL();
                    tr.PZRRSPL_SEQ_NUMBER = i._PZRRSPL_SEQ_NUMBER;
                    tr.PZRRSPL_APRS_SEQ_NUMBER = i._PZRRSPL_APRS_SEQ_NUMBER;
                    tr.PZRRSPL_PLAP_SEQ_NUMBER = i._PZRRSPL_PLAP_SEQ_NUMBER;
                    tr.PZRRSPL_NUMB_VALUE = (short)i._PZRRSPL_NUMB_VALUE;
                    tr.PZRRSPL_STR_VALUE = i._PZRRSPL_STR_VALUE;
                    tr.PZRRSPL_ACTIVITY_DATE = i._PZRRSPL_ACTIVITY_DATE;
                    tr.PZRRSPL_DATA_ORIGIN = i._PZRRSPL_DATA_ORIGIN;
                    tr.PZRRSPL_USER = i._PZRRSPL_USER;

                    db.PZRRSPL.Attach(tr);
                    System.Diagnostics.Debug.WriteLine(tr);
                    db.Entry(tr).State = EntityState.Modified;
                    System.Diagnostics.Debug.WriteLine((db.Entry(tr).State = EntityState.Modified));
                    db.SaveChanges();

                  
                }
                catch (DbUpdateConcurrencyException)
                {
                    
                        return "Error";
                    
                }
            }

            return ("Resultado Modificado  Modificado");
        }



        /// <summary>
        /// Procedimiento que envia 1 correo a  1 Usuario
        /// 
        /// </summary>
        /// <param name="_pidm"></param>
        /// <param name="_titulo"></param>
        /// <param name="_contenido"></param>
        /// <returns></returns>
        public string GetNofifier(int _pidm, String _titulo, String _contenido)
        {
          

            try
            {
                var res = db.Database.SqlQuery<String>(@" SELECT baninst1.f_sendemail(
                                                                 baninst1.f_bus_email ('" + _pidm + "',NULL,'A','Y'), null, 'evaluacionRRHH@ucab.edu.ve', 'Sistema de Evaluacion de Desempeño ', 'smtp2.ucab.edu.ve', 'Notificacion de Evaluacion', '" + _contenido + "') " +
                                                                " FROM dual"
                                                                 ).ToList();
                if (res.Equals("T - Mail Sent"))
                {

                    return ("Se ha enviado el correo");

                }
                else
                {
                    return ("No se pudo enviar. Intente Nuevamente. ");
                }

            }

            catch (Exception e)
            {

                return ("Error.");

            }


        }
        public string NameUser(int _pidm) {

            //select f_format_name(71285, 'LFM') from dual;

            var res = db.Database.SqlQuery<String>(@"select f_format_name('"+_pidm+"', 'LFM') from dual");

            foreach (string rp in res)
            {
                return rp;
            }

            return "null";


        }

        public string NamePoll(string _code)
        {

            //select f_format_name(71285, 'LFM') from dual;

            var res = db.Database.SqlQuery<String>(@" select pzbpoll.pzbpoll_name
                                                     from pzbpoll where pzbpoll_code='"+_code+"'");

            foreach (string rp in res)
            {
                return rp;
            }

            return "null";


        }

        public string IdEvaluador(int _pidm,string _pfle, string _post,
                                  string _dncy, string _poll,string _calr) {

            string resp;
            var res = db.Database.SqlQuery<String>(@" select prso.pzbprso_pidm

                                                                from pzrplap,
                                                                     pzraspe,pzbprso,
                                                                     pzrdypt,pzvpost,
                                                                     pzvdncy,pzvpfle,
                                                                --evaluador
                                                                     pzbprso prso,
                                                                     pzrdypt dypt,
                                                                     pzvpost post,
                                                                     pzvdncy dncy,
                                                                     pzraspe aspe

                                                                where pzrplap.pzrplap_pidm ='" + _pidm+"'"+
                                                               "and pzrplap_pfle_code='"+_pfle+"'"+
                                                               "and pzrplap_post_code = '"+_post+"'"+
                                                               " and pzrplap_dncy_code='"+_dncy+"'"+
                                                               " AND pzrplap_calr_code= '"+_calr+"'"+
                                                               " and pzrplap_poll_code= '"+_poll+"'"+
                                                               " AND pzraspe.pzraspe_pidm = pzrplap.pzrplap_pidm"+
                                                               " and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm"+
                                                               " and pzraspe.pzraspe_pfle_code = pzrplap.pzrplap_pfle_code"+
                                                               " and pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code"+
                                                               " and pzraspe.pzraspe_post_code = pzrplap.pzrplap_post_code"+
                                                               " and pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code"+
                                                               " and pzrdypt.pzrdypt_post_code = pzvpost.pzvpost_code"+
                                                               " and pzvpost.pzvpost_code_sup = post.pzvpost_code  "+
                                                               " and post.pzvpost_code = dypt.pzrdypt_post_code"+
                                                               " and dypt.pzrdypt_post_code = aspe.pzraspe_post_code"+
                                                               " and aspe.pzraspe_pidm = prso.pzbprso_pidm"+
                                                               " and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code"+ 
                                                               " and pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code"+
                                                               " and pzrdypt.pzrdypt_dncy_code = pzvdncy.pzvdncy_code"+
                                                               " and pzvdncy.pzvdncy_code_sup =  dncy.pzvdncy_code "
                                                                 ).ToList();
            foreach (string rp in res)
            {
                return rp;
            }

            return "null";

        }


        public List<AsignacionInstrumentoPregunta> NoAnswerQstn(int id, string idins, string idcal, string _plap) {

            List<Models.AsignacionInstrumentoPregunta> result = new List<Models.AsignacionInstrumentoPregunta>();

            
            var res = db.PZBPLQS.SqlQuery(@"SELECT  distinct pzbqstn.*,pzbplqs.* 
                                              FROM PZBPOLL,pzbappl,pzbplqs,pzbqstn,
                                                          pzrplap,PZBCALR,PZRASPE,PZBPRSO,
                                                             PZVPFLE,pzrdypt,pzvpost,pzvdncy,
                                                             pzbarea,pzbcrit,pzrawap
                                                 WHERE  PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code
                                                         AND pzbappl.pzbappl_poll_code = pzbplqs.pzbplqs_poll_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzrplap.pzrplap_status not in ('F','C')
                                                         and pzbappl.pzbappl_seq_number = pzbplqs.pzbplqs_appl_seq_number
                                                         and pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number  
                                                         and pzbcalr.pzbcalr_code = pzbappl.pzbappl_calr_code
                                                         and pzbappl.pzbappl_calr_code = pzbplqs.pzbplqs_calr_code
                                                         and pzvpfle.pzvpfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzbqstn.pzbqstn_code = pzbplqs.pzbplqs_qstn_code
                                                         and pzbcrit.pzbcrit_code = pzbplqs.pzbplqs_crit_code
                                                         and pzbarea.pzbarea_code = pzbplqs.pzbplqs_area_code
                                                         and pzbplqs.pzbplqs_seq_number not in(Select  pzrawap.pzrawap_plqs_seq_number from pzrawap where pzrawap.PZRAWAP_PLAP_SEQ_NUMBER = '"+_plap+"' )"+
                                                         "and pzbplqs.pzbplqs_status = 'E' " +
                                                         "and pzrplap.pzrplap_seq_number = pzrawap.pzrawap_plap_seq_number "+
                                                         "and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code "+
                                                         "and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code "+
                                                         "and pzrplap.pzrplap_pfle_code = pzbappl.pzbappl_pfle_code "+
                                                         "and pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code "+
                                                         "and pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code "+
                                                         "and pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code "+
                                                         "and pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code "+
                                                         "and pzrdypt.pzrdypt_post_code =  pzvpost.pzvpost_code "+
                                                         "and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code "+
                                                         "and pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code "+
                                                         "and pzrdypt.pzrdypt_dncy_code =  pzvdncy.pzvdncy_code "+
                                                         "and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm "+
                                                         "and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm "+
                                                         "AND PZBPOLL.PZBPOLL_code='" + idins+"'" +
                                                         "and pzbprso.pzbprso_pidm = '"+id+"'"+
                                                         "and pzbcalr.pzbcalr_code ='"+idcal+"'"+
                                                         "and pzrplap.pzrplap_seq_number='"+_plap+"' " +
                                                        "ORDER BY PZBPLQS.PZBPLQS_ORDER , pzbplqs.pzbplqs_area_code "
                                                                ).ToList();
            foreach (var x in res)
            {
                Models.AsignacionInstrumentoPregunta _insPre = new Models.AsignacionInstrumentoPregunta();

                _insPre._PZBPLQS_SEQ_NUMBER = x.PZBPLQS_SEQ_NUMBER;
                _insPre._PZBPLQS_USER = x.PZBPLQS_USER;
                _insPre._PZBPLQS_DATA_ORIGIN = x.PZBPLQS_DATA_ORIGIN;
                _insPre._PZBPLQS_ACTIVITY_DATE = x.PZBPLQS_ACTIVITY_DATE;
                _insPre._PZBPLQS_QSTN_CODE = x.PZBPLQS_QSTN_CODE;
                _insPre._PZBPLQS_POLL_CODE = x.PZBPLQS_POLL_CODE;
                _insPre._PZBPLQS_CALR_CODE = x.PZBPLQS_CALR_CODE;
                _insPre._PZBPLQS_APPL_SEQ_NUMBER = x.PZBPLQS_APPL_SEQ_NUMBER;
                _insPre._PZBPLQS_AREA_CODE = x.PZBPLQS_AREA_CODE;
                _insPre._PZBPLQS_CRIT_CODE = x.PZBPLQS_CRIT_CODE;
                _insPre._PZBPLQS_TYAW_CODE = x.PZBPLQS_TYAW_CODE;
                _insPre._PZBPLQS_MAX_OVAL = x.PZBPLQS_MAX_OVAL;
                _insPre._PZBPLQS_MAX_VALUE = x.PZBPLQS_MAX_VALUE;
                _insPre._PZBPLQS_MIN_OVAL = x.PZBPLQS_MIN_OVAL;
                _insPre._PZBPLQS_MIN_VALUE = x.PZBPLQS_MIN_VALUE;
                _insPre._PZBPLQS_ORDER = x.PZBPLQS_ORDER;
                _insPre._PZBPLQS_STATUS = x.PZBPLQS_STATUS;


                //DATOS DEL criterio de Evaluacion
                Models.Criterio _cri = new Models.Criterio();
                _cri._PZBCRIT_CODE = x.PZBCRIT.PZBCRIT_CODE;
                _cri._PZBCRIT_USER = x.PZBCRIT.PZBCRIT_USER;
                _cri._PZBCRIT_DESCRIPTION = x.PZBCRIT.PZBCRIT_DESCRIPTION;
                _cri._PZBCRIT_DATA_ORIGIN = x.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                _cri._PZBCRIT_ACTIVITY_DATE = x.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                _insPre._PZBCRIT = _cri;

                //Datos del area a la cual pertenece la pregunta
                Models.Area _area = new Models.Area();
                _area._PZBAREA_CODE = x.PZBAREA.PZBAREA_CODE;
                _area._PZBAREA_USER = x.PZBAREA.PZBAREA_USER;
                _area._PZBAREA_DESCRIPTION = x.PZBAREA.PZBAREA_DESCRIPTION;
                _area._PZBAREA_DATA_ORIGIN = x.PZBAREA.PZBAREA_DATA_ORIGIN;
                _area._PZBAREA_ACTIVITY_DATE = x.PZBAREA.PZBAREA_ACTIVITY_DATE;
                _insPre._PZBAREA = _area;

                Models.Pregunta item = new Models.Pregunta();

                item._PZBQSTN_CODE = x.PZBQSTN.PZBQSTN_CODE;
                item._PZBQSTN_NAME = x.PZBQSTN.PZBQSTN_NAME;
                item._PZBQSTN_USER = x.PZBQSTN.PZBQSTN_USER;
                item._PZBQSTN_DESCRIPTION = x.PZBQSTN.PZBQSTN_DESCRIPTION;
                item._PZBQSTN_DATA_ORIGIN = x.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                item._PZBQSTN_ACTIVITY_DATE = x.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                _insPre._PZBQSTN = item;

                result.Add(_insPre);
            }
            return result;
        }

        

        /// <summary>
        /// Enviar correo al evaluador
        /// </summary>
        /// <param name="_pidm"></param>
        /// <param name="_pfle"></param>
        /// <param name="_post"></param>
        /// <param name="_dncy"></param>
        /// <param name="_poll"></param>
        /// <param name="_calr"></param>
        /// <param name="_titulo"></param>
        /// <param name="_contenido"></param>
        /// <returns></returns>
        public string GetNofifierE(int _pidm, string _pfle, string _post,
                                   string _dncy, string _poll, string _calr,
                                   String _titulo, String _contenido)
        {
            
            try
            {
                String pidm_evaluador = IdEvaluador(_pidm, _pfle, _post,
                                                    _dncy,  _poll,_calr);

                var res = db.Database.SqlQuery<String>(@" SELECT baninst1.f_sendemail(
                                                                 baninst1.f_bus_email ('" + pidm_evaluador + "',NULL,'A','Y'), null, 'evaluacionRRHH@ucab.edu.ve', 'Evaluación de Desempeño', 'smtp2.ucab.edu.ve', '" + _titulo + "', '" + _contenido + "') " +
                                                                " FROM dual"
                                                                 ).ToList();
                if (res.Equals("T - Mail Sent"))
                {

                    return ("Se ha enviado el correo");

                }
                else
                {
                    return ("No se pudo enviar. Intente Nuevamente. ");
                }

            }

            catch (Exception e)
            {

                return ("Error.");

            }


        }

        

        /// <summary>
        /// Procedimiento que envia correos a un grupo 
        /// </summary>
        /// <param name="_pidm"></param>
        /// <param name="_titulo"></param>
        /// <param name="_contenido"></param>
        /// <returns></returns>
        public string GetNofifierG(int[] _pidm, String _titulo, String _contenido)
        {

            try
            {
                for (int dimension = 1; dimension <= _pidm.Length; dimension++)
                {

                    var res = db.Database.SqlQuery<String>(@" SELECT baninst1.f_sendemail(
                                                                 baninst1.f_bus_email ('" + _pidm[dimension] + "',NULL,'A','Y'), null, 'evaluacionRRHH@ucab.edu.ve', 'Evaluación de Desempeño', 'smtp2.ucab.edu.ve', '" + _titulo + "', '" + _contenido + "') " +
                                                                " FROM dual"
                                                                 ).ToList();
                }

                return ("Enviados");

            }

            catch (Exception e)
            {

                return ("Error");

            }



       

        }



    }

}


