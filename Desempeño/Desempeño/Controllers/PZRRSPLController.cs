﻿using Desempeño.DataConect;
using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Desempeño.Controllers
{
    public class PZRRSPLController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/PZRRSPL
        /// <summary>
        /// 
        /// Obtiene los tipos de Resultado Disponible
        /// Ruta api/PZRRSPL/GetPZRRSPL
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/PZRRSPL/GetPZRRSPLs/")]
        [ResponseType(typeof(ResultadosParciales))]
        public IHttpActionResult GetPZRRSPLs()
        {

            List<Models.ResultadosParciales> result = new List<Models.ResultadosParciales>();
            try
            {

              
                var res = db.PZRRSPL.SqlQuery(@" SELECT * from PZRRSPL ").ToList();

                foreach (var x in res)
                {

                    //Datos del tipo de Respuesta
                    Models.ResultadosParciales tr = new Models.ResultadosParciales();
                    tr._PZRRSPL_SEQ_NUMBER = x.PZRRSPL_SEQ_NUMBER;
                    tr._PZRRSPL_APRS_SEQ_NUMBER = x.PZRRSPL_APRS_SEQ_NUMBER;
                    tr._PZRRSPL_PLAP_SEQ_NUMBER = x.PZRRSPL_PLAP_SEQ_NUMBER;
                    tr._PZRRSPL_NUMB_VALUE = x.PZRRSPL_NUMB_VALUE;
                    tr._PZRRSPL_STR_VALUE = x.PZRRSPL_STR_VALUE;
                    tr._PZRRSPL_ACTIVITY_DATE = x.PZRRSPL_ACTIVITY_DATE;
                    tr._PZRRSPL_DATA_ORIGIN = x.PZRRSPL_DATA_ORIGIN;
                    tr._PZRRSPL_USER = x.PZRRSPL_USER;

                    result.Add(tr);

                }

                return (Ok(result));

            }

            catch (Exception e)
            {

                return (NotFound());

            }


        }


        // GET: api/PZRRSPL
        /// <summary>
        /// 
        /// Obtiene los tipos de Resultado Disponible
        /// Ruta api/PZRRSPL/GetPZRRSPL
        /// <param name="_calr">Calendario al que estan asignados</param>
        /// <param name="_pfle">Perfil al que estan asignados</param>
        /// <param name="_pidm"></param>
        /// <param name="_poll">Instrumento al que estan asignados</param>
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/PZRRSPL/GetPZRRSPL/{_calr}/{_poll}/{_pfle}/{_pidm}")]
        [ResponseType(typeof(ResultadosParciales))]
        public IHttpActionResult GetPZRRSPL(string _calr, string _poll,string _pfle, string _pidm)
        {

            List<Models.ResultadosParciales> result = new List<Models.ResultadosParciales>();
            try
            {
             

                var res = db.PZRRSPL.SqlQuery(@" select pzrrspl.*,pzvtyrs.*
                                                     from pzrrspl,pzrplap,PZBAPRS,PZVTYRS
                                                     where pzrrspl.pzrrspl_plap_seq_number = pzrplap.pzrplap_seq_number
                                                     AND pzrrspl.pzrrspl_aprs_seq_number = pzbaprs.pzbaprs_seq_number 
                                                     AND pzbaprs.pzbaprs_tyrs_code = pzvtyrs.pzvtyrs_code
                                                            and  pzrplap.pzrplap_calr_code ='" + _calr+"'"+
                                                            "and pzrplap.pzrplap_poll_code = '"+_poll+"'"+
                                                            "and pzrplap.pzrplap_pfle_code ='"+_pfle+"'"+
                                                            "and pzrplap.pzrplap_pidm ='"+_pidm+"'").ToList();

                foreach (var x in res)
                {

                    //Datos del tipo de Respuesta
                    Models.ResultadosParciales tr = new Models.ResultadosParciales();
                    tr._PZRRSPL_SEQ_NUMBER = x.PZRRSPL_SEQ_NUMBER;
                    tr._PZRRSPL_APRS_SEQ_NUMBER = x.PZRRSPL_APRS_SEQ_NUMBER;
                    tr._PZRRSPL_PLAP_SEQ_NUMBER = x.PZRRSPL_PLAP_SEQ_NUMBER;
                    tr._PZRRSPL_NUMB_VALUE = x.PZRRSPL_NUMB_VALUE;
                    tr._PZRRSPL_STR_VALUE = x.PZRRSPL_STR_VALUE;
                    tr._PZRRSPL_ACTIVITY_DATE = x.PZRRSPL_ACTIVITY_DATE;
                    tr._PZRRSPL_DATA_ORIGIN = x.PZRRSPL_DATA_ORIGIN;
                    tr._PZRRSPL_USER = x.PZRRSPL_USER;

                   Models.ResultadoAplicable _resApl = new Models.ResultadoAplicable();
                    _resApl._PZBAPRS_CALR_CODE = x.PZBAPRS.PZBAPRS_CALR_CODE;
                    _resApl._PZBAPRS_POLL_CODE = x.PZBAPRS.PZBAPRS_POLL_CODE;
                    _resApl._PZBAPRS_SEQ_NUMBER = x.PZBAPRS.PZBAPRS_SEQ_NUMBER;
                    _resApl._PZBAPRS_USER = x.PZBAPRS.PZBAPRS_USER;
                    _resApl._PZBAPRS_DATA_ORIGIN = x.PZBAPRS.PZBAPRS_DATA_ORIGIN;
                    _resApl._PZBAPRS_ACTIVITY_DATE = x.PZBAPRS.PZBAPRS_ACTIVITY_DATE;
                    _resApl._PZBAPRS_ORDER = x.PZBAPRS.PZBAPRS_ORDER;
                    _resApl._PZBAPRS_APPL_SEQ_NUMBER = x.PZBAPRS.PZBAPRS_APPL_SEQ_NUMBER;
                    _resApl._PZBAPRS_TYRS_CODE = x.PZBAPRS.PZBAPRS_TYRS_CODE;
                    tr._PZBAPRS = _resApl;
                    

                    Models.TipoResultado trn = new Models.TipoResultado();
                    trn._PZVTYRS_CODE = x.PZBAPRS.PZVTYRS.PZVTYRS_CODE;
                    trn._PZVTYRS_DESCRIPTION =  x.PZBAPRS.PZVTYRS.PZVTYRS_DESCRIPTION;
                    trn._PZVTYRS_ACTIVITY_DATE  = x.PZBAPRS.PZVTYRS.PZVTYRS_ACTIVITY_DATE;
                    trn._PZVTYRS_DATA_ORIGIN =  x.PZBAPRS.PZVTYRS.PZVTYRS_DATA_ORIGIN;
                    trn._PZVTYRS_USER = x.PZBAPRS.PZVTYRS.PZVTYRS_USER;

                    tr._PZBAPRS._PZVTYRS = trn;

                    result.Add(tr);

                }

                return (Ok(result));

            }

            catch (Exception e)
            {

                return (NotFound());

            }


        }


        // PUT: api/PZRRSPL/5
        /// <summary>
        /// 
        /// Modifica los registros de los tipos de Resultado
        /// 
        /// </summary>
        /// 
        /// <param name="_PZRRSPL"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/PZRRSPL/PutPZRRSPL/{_PZRRSPL}/")]
        [ResponseType(typeof(ResultadosParciales))]
        public IHttpActionResult PutPZRRSPL( List<ResultadosParciales> _PZRRSPL)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            foreach (var i in _PZRRSPL)
            {

             



                try
                {
                    DateTime fecha = DateTime.Today;

                    i._PZRRSPL_ACTIVITY_DATE = fecha;



                    PZRRSPL tr = new PZRRSPL();
                    tr.PZRRSPL_SEQ_NUMBER = i._PZRRSPL_SEQ_NUMBER;
                    tr.PZRRSPL_APRS_SEQ_NUMBER = i._PZRRSPL_APRS_SEQ_NUMBER;
                    tr.PZRRSPL_PLAP_SEQ_NUMBER = i._PZRRSPL_PLAP_SEQ_NUMBER;
                    tr.PZRRSPL_NUMB_VALUE = (short)i._PZRRSPL_NUMB_VALUE;
                    tr.PZRRSPL_STR_VALUE = i._PZRRSPL_STR_VALUE;
                    tr.PZRRSPL_ACTIVITY_DATE = i._PZRRSPL_ACTIVITY_DATE;
                    tr.PZRRSPL_DATA_ORIGIN = i._PZRRSPL_DATA_ORIGIN;
                    tr.PZRRSPL_USER = i._PZRRSPL_USER;

                    db.PZRRSPL.Attach(tr);
                    System.Diagnostics.Debug.WriteLine(tr);
                    db.Entry(tr).State = EntityState.Modified;
                    System.Diagnostics.Debug.WriteLine((db.Entry(tr).State = EntityState.Modified));
                    db.SaveChanges();



                    return Ok("Resultado Modificado " + tr.PZRRSPL_STR_VALUE + " Modificado");

                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PZRRSPLExists(i._PZRRSPL_SEQ_NUMBER))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        // POST: api/PZRRSPL
        /// <summary>
        /// 
        /// Agrega los Tipo de Resultado Posibles
        /// RUTA: api/PZRRSPL/PostPZRRSPL
        /// 
        /// </summary>
        /// <param name="_status"></param>
        /// <param name="_PZRRSPL"></param>
        /// <returns>Tipo de Respuesta</returns>
        [HttpPost]
        [ResponseType(typeof(Models.ResultadosParciales))]
        [Route("api/PZRRSPL/PostPZRRSPL/{_status}")]
        public IHttpActionResult PostPZRRSPL(String _status, Models.ResultadosParciales _PZRRSPL)
        {



            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (db.PZRRSPL.Count(e => e.PZRRSPL_SEQ_NUMBER == _PZRRSPL._PZRRSPL_SEQ_NUMBER) > 0)
            {

                return BadRequest("El codigo " + _PZRRSPL._PZRRSPL_SEQ_NUMBER + " ya existe.");
            }

            try
            {
                int es;
                
                DateTime fecha = DateTime.Today;


                _PZRRSPL._PZRRSPL_ACTIVITY_DATE = fecha;


                PZRRSPL tr = new PZRRSPL();
                tr.PZRRSPL_SEQ_NUMBER = _PZRRSPL._PZRRSPL_SEQ_NUMBER;
                tr.PZRRSPL_APRS_SEQ_NUMBER = _PZRRSPL._PZRRSPL_APRS_SEQ_NUMBER;
                tr.PZRRSPL_PLAP_SEQ_NUMBER = _PZRRSPL._PZRRSPL_PLAP_SEQ_NUMBER;
                tr.PZRRSPL_NUMB_VALUE = (short)_PZRRSPL._PZRRSPL_NUMB_VALUE;
                tr.PZRRSPL_STR_VALUE = _PZRRSPL._PZRRSPL_STR_VALUE;
                tr.PZRRSPL_ACTIVITY_DATE = _PZRRSPL._PZRRSPL_ACTIVITY_DATE;
                tr.PZRRSPL_DATA_ORIGIN = _PZRRSPL._PZRRSPL_DATA_ORIGIN;
                tr.PZRRSPL_USER = _PZRRSPL._PZRRSPL_USER;
                


                db.PZRRSPL.Add(tr);

                db.SaveChanges();

                PutPZRPLAP(tr.PZRRSPL_PLAP_SEQ_NUMBER, tr.PZRRSPL_USER, _status);

            }
            catch (DbUpdateException)
            {
                if (PZRRSPLExists(_PZRRSPL._PZRRSPL_SEQ_NUMBER))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = _PZRRSPL._PZRRSPL_SEQ_NUMBER  }, _PZRRSPL);
        }

 



        // DELETE: api/PZRRSPL/5
        /// <summary>
        /// Elimina un tipo de resultado segun el id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(PZRRSPL))]
        public IHttpActionResult DeletePZRRSPL(decimal id)
        {
            PZRRSPL PZRRSPL = db.PZRRSPL.Find(id);
            if (PZRRSPL == null)
            {
                return NotFound();
            }

            db.PZRRSPL.Remove(PZRRSPL);
            db.SaveChanges();

            return Ok(PZRRSPL);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZRRSPLExists(int id)
        {
            return db.PZRRSPL.Count(e => e.PZRRSPL_SEQ_NUMBER == id) > 0;
        }
        public void PutPZRPLAP(int id, string user, string status)//Models.AsignacionInstrumentoPersona _PZRPLAP)
        {


            List<Models.AsignacionInstrumentoPersona> re = new List<Models.AsignacionInstrumentoPersona>();
            int _pidm = 0;
            try
            {

                var es = db.PZRPLAP.SqlQuery(@"select * FROM PZRPLAP WHERE pzrplap.PZRPLAP_SEQ_NUMBER = '" + id + "' ").ToList();

                foreach (var result in es)
                {
                    DateTime fecha = DateTime.Today;
                    _pidm = result.PZRPLAP_PIDM;
                    result.PZRPLAP_DATE = fecha;
                    result.PZRPLAP_ACTIVITY_DATE = fecha;
                    result.PZRPLAP_STATUS = status;
                    result.PZRPLAP_USER = user;


                    db.PZRPLAP.Attach(result);
                    System.Diagnostics.Debug.WriteLine(result);
                    db.Entry(result).State = EntityState.Modified;
                    System.Diagnostics.Debug.WriteLine((db.Entry(result).State = EntityState.Modified));
                    db.SaveChanges();


                }
                //  String respuest = GetNofifier(_pidm, "Se ha cargado su evaluación ", "Se ha modificado exitosamete su evaluacion ");
            }
            catch (Exception e)
            {

            }

        }

    }
}
