﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZBPLQSController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/PZBPLQSs
        /// <summary>
        /// 
        /// Carga todos los instrumentos y todas las _preguntas
        /// RUTA : api/PZBPLQSs/GetPZBPLQSs/
        /// 
        /// </summary>
        /// <returns>Lista todos los intrumentos existentes en la BD con sus preguntas</returns>
        [ResponseType(typeof(List<Models.AsignacionInstrumentoPregunta>))]
        [Route("api/PZBPLQSs/GetPZBPLQS/")]
        public IHttpActionResult GetPZBPLQS()
        {

            List<Models.AsignacionInstrumentoPregunta> result = new List<Models.AsignacionInstrumentoPregunta>();
            try
            {

                db.PZBPLQS.Include(i => i.PZBAPPL)
                          .Include(p => p.PZBQSTN)
                          .Include(poll => poll.PZBAPPL.PZBPOLL)
                          .Include(area => area.PZBAREA)
                          .Include(criterio => criterio.PZBCRIT);
                var res = db.PZBPLQS.SqlQuery(@" SELECT ins.pzbpoll_name,
                                                        pre.pzbqstn_name ,
                                                        conf_ins.*,
                                                        are.pzbarea_description,
                                                        cri.pzbcrit_description

                                                    from PZBPLQS conf_ins ,
                                                         PZBAPPL apl_ins ,
                                                         pzbqstn pre,
                                                         PZBPOLL ins,
                                                         PZBCALR cal,
                                                         pzvpfle pfl,
                                                         pzbcrit cri,
                                                         pzbarea are
                                                         
                                                    where ins.PZBPOLL_code = apl_ins.pzbappl_poll_code
                                                        and apl_ins.pzbappl_calr_code = cal.pzbcalr_code
                                                        and apl_ins.pzbappl_pfle_code = pfl.pzvpfle_code
                                                        and apl_ins.pzbappl_poll_code = conf_ins.PZBPLQS_POLL_code
                                                        and apl_ins.pzbappl_calr_code = conf_ins.pzbplqs_calr_code
                                                        and apl_ins.pzbappl_seq_number = conf_ins.pzbplqs_appl_seq_number
                                                        and conf_ins.PZBPLQS_qstn_CODE = pre.pzbqstn_CODE 
                                                        and conf_ins.PZBPLQS_CRIT_CODE = cri.pzbcrit_code
                                                        and conf_ins.pzbplqs_area_code = are.pzbarea_code
                                                         ").ToList();

                foreach (var x in res)
                {
                    
                    //Relacion Instrumento-Preguntas
                    Models.AsignacionInstrumentoPregunta _insPre = new Models.AsignacionInstrumentoPregunta();
                    _insPre._PZBPLQS_SEQ_NUMBER = x.PZBPLQS_SEQ_NUMBER;
                    _insPre._PZBPLQS_USER = x.PZBPLQS_USER;
                    _insPre._PZBPLQS_DATA_ORIGIN = x.PZBPLQS_DATA_ORIGIN;
                    _insPre._PZBPLQS_ACTIVITY_DATE = x.PZBPLQS_ACTIVITY_DATE;
                    _insPre._PZBPLQS_QSTN_CODE = x.PZBPLQS_QSTN_CODE;
                    _insPre._PZBPLQS_POLL_CODE = x.PZBPLQS_POLL_CODE;
                    _insPre._PZBPLQS_CALR_CODE = x.PZBPLQS_CALR_CODE;
                    _insPre._PZBPLQS_APPL_SEQ_NUMBER = x.PZBPLQS_APPL_SEQ_NUMBER;
                    _insPre._PZBPLQS_AREA_CODE = x.PZBPLQS_AREA_CODE;
                    _insPre._PZBPLQS_CRIT_CODE = x.PZBPLQS_CRIT_CODE;
                    _insPre._PZBPLQS_TYAW_CODE = x.PZBPLQS_TYAW_CODE;
                    _insPre._PZBPLQS_MAX_OVAL = x.PZBPLQS_MAX_OVAL;
                    _insPre._PZBPLQS_MAX_VALUE = x.PZBPLQS_MAX_VALUE;
                    _insPre._PZBPLQS_MIN_OVAL = x.PZBPLQS_MIN_OVAL;
                    _insPre._PZBPLQS_MIN_VALUE = x.PZBPLQS_MIN_VALUE;
                    _insPre._PZBPLQS_ORDER = x.PZBPLQS_ORDER;
                    _insPre._PZBPLQS_STATUS = x.PZBPLQS_STATUS;

                    //Datos del instrumento Aplicable
                    Models.InstrumentoAplicable _insApl = new Models.InstrumentoAplicable();
                    _insApl._PZBAPPL_CALR_CODE = x.PZBAPPL.PZBAPPL_CALR_CODE;
                    _insApl._PZBAPPL_PFLE_CODE = x.PZBAPPL.PZBAPPL_PFLE_CODE;
                    _insApl._PZBAPPL_PRCNT = x.PZBAPPL.PZBAPPL_PRCNT;
                    _insApl._PZBAPPL_POLL_CODE = x.PZBAPPL.PZBAPPL_POLL_CODE;
                    _insApl._PZBAPPL_SEQ_NUMBER = x.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                    _insApl._PZBAPPL_USER = x.PZBAPPL.PZBAPPL_USER;
                    _insApl._PZBAPPL_DATA_ORIGIN = x.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                    _insApl._PZBAPPL_ACTIVITY_DATE = x.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                    _insPre._PZBAPPL = _insApl;

                    //Carga Datos del instrumento
                    Models.Instrumento ins = new Models.Instrumento();
                  //  ins._PZBPOLL_CODE = x.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                    ins._PZBPOLL_NAME = x.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                    /*ins._PZBPOLL_USER = x.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = x.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = x.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;*/
                    _insPre._PZBAPPL._PZBPOLL = ins;

                    //Carga datos de las Preguntas 
                    Models.Pregunta _pre = new Models.Pregunta();
                    //_pre._PZBQSTN_CODE = x.PZBQSTN.PZBQSTN_CODE;
                    _pre._PZBQSTN_NAME = x.PZBQSTN.PZBQSTN_NAME;
                    _pre._PZBQSTN_USER = x.PZBQSTN.PZBQSTN_USER;
                    _pre._PZBQSTN_DATA_ORIGIN = x.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                    _pre._PZBQSTN_DESCRIPTION = x.PZBQSTN.PZBQSTN_DESCRIPTION;
                    _pre._PZBQSTN_ACTIVITY_DATE = x.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                    
                    _insPre._PZBQSTN = _pre;

                    //Area
                    Models.Area _area = new Models.Area();
                    _area._PZBAREA_CODE = x.PZBAREA.PZBAREA_CODE;
                    _area._PZBAREA_USER = x.PZBAREA.PZBAREA_USER;
                    _area._PZBAREA_DATA_ORIGIN = x.PZBAREA.PZBAREA_DATA_ORIGIN;
                    _area._PZBAREA_DESCRIPTION = x.PZBAREA.PZBAREA_DESCRIPTION;
                    _area._PZBAREA_ACTIVITY_DATE = x.PZBAREA.PZBAREA_ACTIVITY_DATE;
                    _insPre._PZBAREA = _area;

                    //Criterio
                    Models.Criterio _criterio = new Models.Criterio();
                     _criterio._PZBCRIT_CODE = x.PZBCRIT.PZBCRIT_CODE;
                    _criterio._PZBCRIT_USER = x.PZBCRIT.PZBCRIT_USER;
                    _criterio._PZBCRIT_DESCRIPTION = x.PZBCRIT.PZBCRIT_DESCRIPTION;
                    _insPre._PZBCRIT = _criterio;

                    result.Add(_insPre);

                }

                return (Ok(result));

            }

            catch (Exception e)
            {

                return (NotFound());

            }
        }


        /// <summary>
        /// 
        /// Dado el id del instrumento muestra las _preguntas que lo componen
        /// Ruta : api/PZBPLQSs/GetInsPre/{idins}
        /// 
        /// </summary>
        /// <param name="idins">Codigo del Instrumento</param>
        /// <returns>Lista de las preguntas asociadas a un instrumento</returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Pregunta>))]
        [Route("api/PZBPLQS/GetInsPre/{idins}")]
        public IHttpActionResult GetInsPre(String idins)
        {

            List<Models.Pregunta> result = new List<Models.Pregunta>();
            try
            {

                db.PZBPLQS.Include(i => i.PZBAPPL)
                         .Include(p => p.PZBQSTN)
                         .Include(poll => poll.PZBAPPL.PZBPOLL)
                         .Include(area => area.PZBAREA)
                         .Include(criterio => criterio.PZBCRIT);
                var res = db.PZBQSTN.SqlQuery(@"  SELECT  pre.* 

                                                    from PZBPLQS conf_ins ,
                                                         PZBAPPL apl_ins ,
                                                         pzbqstn pre,
                                                         PZBPOLL ins,
                                                         PZBCALR cal,
                                                         pzvpfle pfl,
                                                         pzbcrit cri,
                                                         pzbarea are
                                                         
                                                    where ins.PZBPOLL_code = apl_ins.pzbappl_poll_code
                                                        and apl_ins.pzbappl_calr_code = cal.pzbcalr_code
                                                        and apl_ins.pzbappl_pfle_code = pfl.pzvpfle_code
                                                        and apl_ins.pzbappl_poll_code = conf_ins.PZBPLQS_POLL_code
                                                        and apl_ins.pzbappl_calr_code = conf_ins.pzbplqs_calr_code
                                                        and apl_ins.pzbappl_seq_number = conf_ins.pzbplqs_appl_seq_number
                                                        and conf_ins.PZBPLQS_qstn_CODE = pre.pzbqstn_CODE 
                                                        and conf_ins.PZBPLQS_CRIT_CODE = cri.pzbcrit_code
                                                        and conf_ins.pzbplqs_area_code = are.pzbarea_code
                                                        and ins.PZBPOLL_CODE ='" + idins + "' ").ToList();

                foreach (var x in res)
                {

                    //Carga datos de las Preguntas 
                    Models.Pregunta _pre = new Models.Pregunta();
                    _pre._PZBQSTN_CODE = x.PZBQSTN_CODE;
                    _pre._PZBQSTN_USER = x.PZBQSTN_USER;
                    _pre._PZBQSTN_NAME = x.PZBQSTN_NAME;
                    _pre._PZBQSTN_DATA_ORIGIN = x.PZBQSTN_DATA_ORIGIN;
                    _pre._PZBQSTN_DESCRIPTION = x.PZBQSTN_DESCRIPTION;
                    _pre._PZBQSTN_ACTIVITY_DATE = x.PZBQSTN_ACTIVITY_DATE;
                   
                    result.Add(_pre);

                }

                return (Ok(result));

            }

            catch (Exception e)
            {

                return (NotFound());

            }

        }

        /// <summary>
        /// Lista las preguntas de acuerdo al codigo del instrumento
        /// y el codigo del calendario
        /// </summary>
        /// <param name="idins">Codigo del instrumento</param>
        /// <param name="idcal">Codigo del calendario</param>
        /// <returns>Lista de las preguntas de acuerdo a los parametros que se pasan</returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Pregunta>))]
        [Route("api/PZBPLQS/GetInsPre/{idins}/{idcal}")]
        public IHttpActionResult GetInsPre(String idins,String idcal)
        {

            List<Models.Pregunta> result = new List<Models.Pregunta>();
            try
            {

                db.PZBPLQS.Include(i => i.PZBAPPL)
                         .Include(p => p.PZBQSTN)
                         .Include(poll => poll.PZBAPPL.PZBPOLL)
                         .Include(area => area.PZBAREA)
                         .Include(criterio => criterio.PZBCRIT);
                var res = db.PZBQSTN.SqlQuery(@"  SELECT  pre.* 

                                                    from PZBPLQS conf_ins ,
                                                         PZBAPPL apl_ins ,
                                                         pzbqstn pre,
                                                         PZBPOLL ins,
                                                         PZBCALR cal,
                                                         pzvpfle pfl,
                                                         pzbcrit cri,
                                                         pzbarea are
                                                         
                                                    where ins.PZBPOLL_code = apl_ins.pzbappl_poll_code
                                                        and apl_ins.pzbappl_calr_code = cal.pzbcalr_code
                                                        and apl_ins.pzbappl_pfle_code = pfl.pzvpfle_code
                                                        and apl_ins.pzbappl_poll_code = conf_ins.PZBPLQS_POLL_code
                                                        and apl_ins.pzbappl_calr_code = conf_ins.pzbplqs_calr_code
                                                        and apl_ins.pzbappl_seq_number = conf_ins.pzbplqs_appl_seq_number
                                                        and conf_ins.PZBPLQS_qstn_CODE = pre.pzbqstn_CODE 
                                                        and conf_ins.PZBPLQS_CRIT_CODE = cri.pzbcrit_code
                                                        and conf_ins.pzbplqs_area_code = are.pzbarea_code
                                                        and ins.pzbpoll_code ='" + idins + " and and cal.pzbcalr_code ='"+idcal+"' ").ToList();

                foreach (var x in res)
                {

                    //Carga datos de las Preguntas 
                    Models.Pregunta _pre = new Models.Pregunta();
                    _pre._PZBQSTN_CODE = x.PZBQSTN_CODE;
                    _pre._PZBQSTN_USER = x.PZBQSTN_USER;
                    _pre._PZBQSTN_NAME = x.PZBQSTN_NAME;
                    _pre._PZBQSTN_DATA_ORIGIN = x.PZBQSTN_DATA_ORIGIN;
                    _pre._PZBQSTN_DESCRIPTION = x.PZBQSTN_DESCRIPTION;
                    _pre._PZBQSTN_ACTIVITY_DATE = x.PZBQSTN_ACTIVITY_DATE;

                    result.Add(_pre);

                }

                return (Ok(result));

            }

            catch (Exception e)
            {

                return (NotFound());

            }

        }



        /// <summary>
        /// 
        /// Modifica las _preguntas asociadas 
        /// a un instrumento
        /// 
        /// </summary>
        /// <param name="id">Codigo de la Pregunta</param>
        /// <param name="pZBPLQS">Modelo de la pregunta a modificar</param>
        /// <returns></returns>
        // PUT: api/PZBPLQSs/5
        [HttpPut]
        [Route("api/PZBPLQS/PutPZBPLQS/{id}/")]
        [ResponseType(typeof(Models.AsignacionInstrumentoPregunta))]
        public IHttpActionResult PutPZBPLQS(int id, AsignacionInstrumentoPregunta pZBPLQS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pZBPLQS._PZBPLQS_SEQ_NUMBER)
            {
                return BadRequest();
            }

            try
            {

                DateTime fecha = DateTime.Today;
                pZBPLQS._PZBPLQS_ACTIVITY_DATE = fecha;

                PZBPLQS ins__pre = new PZBPLQS();

                ins__pre.PZBPLQS_SEQ_NUMBER = pZBPLQS._PZBPLQS_SEQ_NUMBER;
                ins__pre.PZBPLQS_CALR_CODE = pZBPLQS._PZBPLQS_CALR_CODE;
                ins__pre.PZBPLQS_POLL_CODE = pZBPLQS._PZBPLQS_POLL_CODE;
                ins__pre.PZBPLQS_APPL_SEQ_NUMBER = pZBPLQS._PZBPLQS_APPL_SEQ_NUMBER;
                ins__pre.PZBPLQS_QSTN_CODE = pZBPLQS._PZBPLQS_QSTN_CODE;
                ins__pre.PZBPLQS_AREA_CODE = pZBPLQS._PZBPLQS_AREA_CODE;
                ins__pre.PZBPLQS_CRIT_CODE = pZBPLQS._PZBPLQS_CRIT_CODE;
                ins__pre.PZBPLQS_MIN_VALUE = pZBPLQS._PZBPLQS_MIN_VALUE;
                ins__pre.PZBPLQS_MAX_VALUE = pZBPLQS._PZBPLQS_MAX_VALUE;
                ins__pre.PZBPLQS_MAX_OVAL = pZBPLQS._PZBPLQS_MAX_OVAL;
                ins__pre.PZBPLQS_MIN_OVAL = pZBPLQS._PZBPLQS_MIN_OVAL;
                ins__pre.PZBPLQS_ORDER = pZBPLQS._PZBPLQS_ORDER;
                ins__pre.PZBPLQS_TYAW_CODE = pZBPLQS._PZBPLQS_TYAW_CODE;
                ins__pre.PZBPLQS_STATUS = pZBPLQS._PZBPLQS_STATUS;
                ins__pre.PZBPLQS_USER = pZBPLQS._PZBPLQS_USER;
                ins__pre.PZBPLQS_DATA_ORIGIN = pZBPLQS._PZBPLQS_DATA_ORIGIN;
                ins__pre.PZBPLQS_ACTIVITY_DATE = pZBPLQS._PZBPLQS_ACTIVITY_DATE;

                db.PZBPLQS.Attach(ins__pre);
                db.Entry(ins__pre).State = EntityState.Modified;
                db.SaveChanges();

                return Ok("Registro Modificado exitosamente");

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PZBPLQSExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PZBPLQSs
        /// <summary>
        /// 
        /// Agrega _preguntas a instrumentos
        /// Ruta:api/PZBPLQSs/PostPZBPLQS
        /// 
        /// </summary>
        /// <param name="_pZBPLQS"></param>
        /// <returns>201 Created</returns>
        [ResponseType(typeof(Models.AsignacionInstrumentoPregunta))]
        public IHttpActionResult PostPZBPLQS(Models.AsignacionInstrumentoPregunta _pZBPLQS)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {

                int es;
                int num_reg = (int)db.PZBPLQS.SqlQuery(@"select * FROM PZBPLQS ").Count();

                if (num_reg == 0)
                {

                    es = 1;
                }

                else
                {
                    es = (int)db.PZBPLQS.Max(x => x.PZBPLQS_SEQ_NUMBER);


                    es = es + 1;
                }


                DateTime fecha = DateTime.Today;

                //aqui tendria que enviar el usernaame ademas de lo que esta.

                _pZBPLQS._PZBPLQS_ACTIVITY_DATE = fecha;
                _pZBPLQS._PZBPLQS_SEQ_NUMBER = (int)es;


                PZBPLQS ins__pre = new PZBPLQS();


                ins__pre.PZBPLQS_SEQ_NUMBER = _pZBPLQS._PZBPLQS_SEQ_NUMBER;
                ins__pre.PZBPLQS_CALR_CODE = _pZBPLQS._PZBPLQS_CALR_CODE;
                ins__pre.PZBPLQS_POLL_CODE = _pZBPLQS._PZBPLQS_POLL_CODE;
                ins__pre.PZBPLQS_APPL_SEQ_NUMBER = _pZBPLQS._PZBPLQS_APPL_SEQ_NUMBER;
                ins__pre.PZBPLQS_QSTN_CODE = _pZBPLQS._PZBPLQS_QSTN_CODE;
                ins__pre.PZBPLQS_AREA_CODE = _pZBPLQS._PZBPLQS_AREA_CODE;
                ins__pre.PZBPLQS_CRIT_CODE = _pZBPLQS._PZBPLQS_CRIT_CODE;
                ins__pre.PZBPLQS_MIN_VALUE = _pZBPLQS._PZBPLQS_MIN_VALUE;
                ins__pre.PZBPLQS_MAX_VALUE = _pZBPLQS._PZBPLQS_MAX_VALUE;
                ins__pre.PZBPLQS_MAX_OVAL = _pZBPLQS._PZBPLQS_MAX_OVAL;
                ins__pre.PZBPLQS_MIN_OVAL = _pZBPLQS._PZBPLQS_MIN_OVAL;
                ins__pre.PZBPLQS_ORDER = _pZBPLQS._PZBPLQS_ORDER;
                ins__pre.PZBPLQS_TYAW_CODE = _pZBPLQS._PZBPLQS_TYAW_CODE;
                ins__pre.PZBPLQS_STATUS = _pZBPLQS._PZBPLQS_STATUS;
                ins__pre.PZBPLQS_USER = _pZBPLQS._PZBPLQS_USER;
                ins__pre.PZBPLQS_DATA_ORIGIN = _pZBPLQS._PZBPLQS_DATA_ORIGIN;
                ins__pre.PZBPLQS_ACTIVITY_DATE = _pZBPLQS._PZBPLQS_ACTIVITY_DATE;


                db.PZBPLQS.Add(ins__pre);

                db.SaveChanges();

            }
            catch (DbUpdateException)
            {
                if (PZBPLQSExists(_pZBPLQS._PZBPLQS_SEQ_NUMBER))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = _pZBPLQS._PZBPLQS_SEQ_NUMBER }, _pZBPLQS);
        }

        // DELETE: api/PZBPLQSs/5
        [ResponseType(typeof(PZBPLQS))]
        public IHttpActionResult DeletePZBPLQS(decimal id)
        {
            PZBPLQS PZBPLQS = db.PZBPLQS.Find(id);
            if (PZBPLQS == null)
            {
                return NotFound();
            }

            db.PZBPLQS.Remove(PZBPLQS);
            db.SaveChanges();

            return Ok(PZBPLQS);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZBPLQSExists(decimal id)
        {
            return db.PZBPLQS.Count(e => e.PZBPLQS_SEQ_NUMBER == id) > 0;
        }
    }
}