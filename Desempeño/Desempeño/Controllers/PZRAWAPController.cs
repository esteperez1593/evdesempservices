﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZRAWAPController : ApiController
    {
        private Entities db = new Entities();
        private Repository.FunctionsRepository _f = new Repository.FunctionsRepository();



        /// <summary>
        /// 
        /// Obtiene Todas las respuestas registradas
        /// Ruta : http://localhost:63665/api/PZRAWAP/GetPZRAWAP
        /// 
        /// </summary>
        /// <returns>List<Models.Resouesta>)</returns>

        [HttpGet]
        [ResponseType(typeof(List<Models.Respuesta>))]
        [Route("api/PZRAWAP/GetPZRAWAP/")]

        public IHttpActionResult GetPZRAWAP()
        {
            List<Models.Respuesta> re = new List<Models.Respuesta>();
            try {

                db.PZRAWAP.Include(tr => tr.PZRPLAP);
                var es = db.PZRAWAP.SqlQuery(@"select * FROM PZRAWAP ").ToList();



                foreach (var result in es)
                {
                    Models.Respuesta item = new Models.Respuesta();
                    item._PZRAWAP_SEQ_NUMBER = (int)result.PZRAWAP_SEQ_NUMBER;
                    item._PZRAWAP_ACTIVITY_DATE = result.PZRAWAP_ACTIVITY_DATE;
                    item._PZRAWAP_DESCRIPTION = result.PZRAWAP_DESCRIPTION;
                    item._PZRAWAP_PLAP_SEQ_NUMBER = result.PZRAWAP_PLAP_SEQ_NUMBER;
                    item._PZRAWAP_PLQS_SEQ_NUMBER = result.PZRAWAP_PLQS_SEQ_NUMBER;
                    item._PZRAWAP_PERC_IMP = result.PZRAWAP_PERC_IMP;
                    item._PZRAWAP_USER = result.PZRAWAP_USER;
                    item._PZRAWAP_VALUE = result.PZRAWAP_VALUE;
                    item._PZRAWAP_DATA_ORIGIN = result.PZRAWAP_DATA_ORIGIN;

                    // item.PZRASPE_ID = result.PZRASPE_ID;


                    re.Add(item);
                }


            }
            catch (Exception e)
            {

                return Ok("Error");

            }

            if (re.Count() == 0) {

                return Ok("No existen respuestas asociadas al los parametros enviados");


            }

            return Ok(re);


        }


        /// <summary>
        /// Dado el ide del evaluado y del isntrumento
        /// y calendario devuelve 
        /// las respuestas registradas
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idins"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Respuesta>))]
        [Route("api/PZRAWAP/GetPZRAWAP/{id}/{idins}/{idcal}/{_plap}")]
        public IHttpActionResult GetPZRAWAP(int id, string idins, string idcal, string _plap)
        {
            List<Models.Respuesta> re = new List<Models.Respuesta>();
            try
            {

                // db.PZRAWAP.Include(tr => tr.PZBPLQS);




                db.PZRAWAP.Include(appl => appl.PZBPLQS.PZBAPPL)
                          .Include(poll => poll.PZBPLQS.PZBAPPL.PZBPOLL)
                          .Include(area => area.PZBPLQS.PZBAREA)
                          .Include(crit => crit.PZBPLQS.PZBCRIT)
                          .Include(qstn => qstn.PZBPLQS.PZBQSTN)
                          .Include(tyaw => tyaw.PZBPLQS.PZVTYAW)
                          .Include(bpls => bpls.PZBPLQS);

                var es = db.PZRAWAP.SqlQuery(@"SELECT * FROM PZBPOLL,pzbappl,pzbplqs,pzbqstn,
                                                             pzrplap,PZBCALR,PZRASPE,PZBPRSO,
                                                             PZVPFLE,pzrdypt,pzvpost,pzvdncy,
                                                             pzbarea,pzbcrit,pzrawap
                                                 WHERE  PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code
                                                         AND pzbappl.pzbappl_poll_code = pzbplqs.pzbplqs_poll_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzbappl.pzbappl_seq_number = pzbplqs.pzbplqs_appl_seq_number
                                                         and pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number  
                                                         and pzbcalr.pzbcalr_code = pzbappl.pzbappl_calr_code
                                                         and pzbappl.pzbappl_calr_code = pzbplqs.pzbplqs_calr_code
                                                         and pzvpfle.pzvpfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzbqstn.pzbqstn_code = pzbplqs.pzbplqs_qstn_code
                                                         and pzbcrit.pzbcrit_code = pzbplqs.pzbplqs_crit_code
                                                         and pzbarea.pzbarea_code = pzbplqs.pzbplqs_area_code
                                                         and pzbplqs.pzbplqs_seq_number = pzrawap.pzrawap_plqs_seq_number
                                                         and pzrplap.pzrplap_seq_number = pzrawap.pzrawap_plap_seq_number
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                         and pzrplap.pzrplap_pfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         and pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                         and pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         and pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code =  pzvpost.pzvpost_code
                                                         and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                         and pzrdypt.pzrdypt_dncy_code =  pzvdncy.pzvdncy_code
                                                         and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                         and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm
                                                         AND PZBPOLL.PZBPOLL_code='" + idins + "' " +
                                                         "and pzbprso.pzbprso_pidm = '" + id + "' " +
                                                         "and pzbcalr.pzbcalr_code ='" + idcal + "' " +
                                                         " ORDER BY PZBPLQS.PZBPLQS_ORDER , pzbplqs.pzbplqs_area_code "
                                                         ).ToList();



                foreach (var x in es)
                {
                    Models.Respuesta resp = new Models.Respuesta();
                    resp._PZRAWAP_SEQ_NUMBER = (int)x.PZRAWAP_SEQ_NUMBER;
                    resp._PZRAWAP_ACTIVITY_DATE = x.PZRAWAP_ACTIVITY_DATE;
                    resp._PZRAWAP_DESCRIPTION = x.PZRAWAP_DESCRIPTION;
                    resp._PZRAWAP_PLAP_SEQ_NUMBER = x.PZRAWAP_PLAP_SEQ_NUMBER;
                    resp._PZRAWAP_PLQS_SEQ_NUMBER = x.PZRAWAP_PLQS_SEQ_NUMBER;
                    resp._PZRAWAP_PERC_IMP = x.PZRAWAP_PERC_IMP;
                    resp._PZRAWAP_USER = x.PZRAWAP_USER;
                    resp._PZRAWAP_VALUE = x.PZRAWAP_VALUE;
                    resp._PZRAWAP_DATA_ORIGIN = x.PZRAWAP_DATA_ORIGIN;



                    Models.AsignacionInstrumentoPregunta _insPre = new Models.AsignacionInstrumentoPregunta();
                    _insPre._PZBPLQS_SEQ_NUMBER = x.PZBPLQS.PZBPLQS_SEQ_NUMBER;
                    _insPre._PZBPLQS_USER = x.PZBPLQS.PZBPLQS_USER;
                    _insPre._PZBPLQS_DATA_ORIGIN = x.PZBPLQS.PZBPLQS_DATA_ORIGIN;
                    _insPre._PZBPLQS_ACTIVITY_DATE = x.PZBPLQS.PZBPLQS_ACTIVITY_DATE;
                    _insPre._PZBPLQS_QSTN_CODE = x.PZBPLQS.PZBPLQS_QSTN_CODE;
                    _insPre._PZBPLQS_POLL_CODE = x.PZBPLQS.PZBPLQS_POLL_CODE;
                    _insPre._PZBPLQS_CALR_CODE = x.PZBPLQS.PZBPLQS_CALR_CODE;
                    _insPre._PZBPLQS_APPL_SEQ_NUMBER = x.PZBPLQS.PZBPLQS_APPL_SEQ_NUMBER;
                    _insPre._PZBPLQS_AREA_CODE = x.PZBPLQS.PZBPLQS_AREA_CODE;
                    _insPre._PZBPLQS_CRIT_CODE = x.PZBPLQS.PZBPLQS_CRIT_CODE;
                    _insPre._PZBPLQS_TYAW_CODE = x.PZBPLQS.PZBPLQS_TYAW_CODE;
                    _insPre._PZBPLQS_MAX_OVAL = x.PZBPLQS.PZBPLQS_MAX_OVAL;
                    _insPre._PZBPLQS_MAX_VALUE = x.PZBPLQS.PZBPLQS_MAX_VALUE;
                    _insPre._PZBPLQS_MIN_OVAL = x.PZBPLQS.PZBPLQS_MIN_OVAL;
                    _insPre._PZBPLQS_MIN_VALUE = x.PZBPLQS.PZBPLQS_MIN_VALUE;
                    _insPre._PZBPLQS_ORDER = x.PZBPLQS.PZBPLQS_ORDER;
                    _insPre._PZBPLQS_STATUS = x.PZBPLQS.PZBPLQS_STATUS;


                    Models.InstrumentoAplicable apl = new Models.InstrumentoAplicable();
                    apl._PZBAPPL_USER = x.PZBPLQS.PZBAPPL.PZBAPPL_USER;
                    apl._PZBAPPL_PRCNT = x.PZBPLQS.PZBAPPL.PZBAPPL_PRCNT;
                    apl._PZBAPPL_SEQ_NUMBER = x.PZBPLQS.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                    apl._PZBAPPL_POLL_CODE = x.PZBPLQS.PZBAPPL.PZBAPPL_POLL_CODE;
                    apl._PZBAPPL_PFLE_CODE = x.PZBPLQS.PZBAPPL.PZBAPPL_PFLE_CODE;
                    apl._PZBAPPL_CALR_CODE = x.PZBPLQS.PZBAPPL.PZBAPPL_CALR_CODE;
                    _insPre._PZBAPPL = apl;

                    Models.Instrumento ins = new Models.Instrumento();
                    ins._PZBPOLL_NAME = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                    ins._PZBPOLL_CODE = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                    ins._PZBPOLL_USER = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                    _insPre._PZBAPPL._PZBPOLL = ins;

                    //DATOS DEL criterio de Evaluacion
                    Models.Criterio _cri = new Models.Criterio();
                    _cri._PZBCRIT_CODE = x.PZBPLQS.PZBCRIT.PZBCRIT_CODE;
                    _cri._PZBCRIT_USER = x.PZBPLQS.PZBCRIT.PZBCRIT_USER;
                    _cri._PZBCRIT_DESCRIPTION = x.PZBPLQS.PZBCRIT.PZBCRIT_DESCRIPTION;
                    _cri._PZBCRIT_DATA_ORIGIN = x.PZBPLQS.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                    _cri._PZBCRIT_ACTIVITY_DATE = x.PZBPLQS.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                    _insPre._PZBCRIT = _cri;

                    //Datos del area a la cual pertenece la pregunta
                    Models.Area _area = new Models.Area();
                    _area._PZBAREA_CODE = x.PZBPLQS.PZBAREA.PZBAREA_CODE;
                    _area._PZBAREA_USER = x.PZBPLQS.PZBAREA.PZBAREA_USER;
                    _area._PZBAREA_DESCRIPTION = x.PZBPLQS.PZBAREA.PZBAREA_DESCRIPTION;
                    _area._PZBAREA_DATA_ORIGIN = x.PZBPLQS.PZBAREA.PZBAREA_DATA_ORIGIN;
                    _area._PZBAREA_ACTIVITY_DATE = x.PZBPLQS.PZBAREA.PZBAREA_ACTIVITY_DATE;
                    _insPre._PZBAREA = _area;

                    Models.Pregunta item = new Models.Pregunta();

                    item._PZBQSTN_CODE = x.PZBPLQS.PZBQSTN.PZBQSTN_CODE;
                    item._PZBQSTN_NAME = x.PZBPLQS.PZBQSTN.PZBQSTN_NAME;
                    item._PZBQSTN_USER = x.PZBPLQS.PZBQSTN.PZBQSTN_USER;
                    item._PZBQSTN_DESCRIPTION = x.PZBPLQS.PZBQSTN.PZBQSTN_DESCRIPTION;
                    item._PZBQSTN_DATA_ORIGIN = x.PZBPLQS.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                    item._PZBQSTN_ACTIVITY_DATE = x.PZBPLQS.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                    _insPre._PZBQSTN = item;

                    resp._PZBPLQS = _insPre;

                
                    re.Add(resp);

                }


               
          

            }
            catch (Exception e)
            {

                return Ok("Respuestas no encontradas");

            }

            //Consulta las preguntas que no tienen respuesta
            List<Models.AsignacionInstrumentoPregunta> obj = _f.NoAnswerQstn(id, idins, idcal,_plap);

            int lon = obj.Count();

            if (lon > 0)
            {
                foreach (var x in obj)
                {

                    Models.Respuesta resp = new Models.Respuesta();
                    resp._PZRAWAP_SEQ_NUMBER = 0;
                    resp._PZRAWAP_ACTIVITY_DATE = DateTime.Today;
                    resp._PZRAWAP_DESCRIPTION = null;
                    resp._PZRAWAP_PLAP_SEQ_NUMBER = 0;
                    resp._PZRAWAP_PLQS_SEQ_NUMBER = 0;
                    resp._PZRAWAP_PERC_IMP = 0;
                    resp._PZRAWAP_USER = null;
                    resp._PZRAWAP_VALUE = 0;
                    resp._PZRAWAP_DATA_ORIGIN = null;

                    Models.AsignacionInstrumentoPregunta _insPre = new Models.AsignacionInstrumentoPregunta();

                    _insPre._PZBPLQS_SEQ_NUMBER = x._PZBPLQS_SEQ_NUMBER;
                    _insPre._PZBPLQS_USER = x._PZBPLQS_USER;
                    _insPre._PZBPLQS_DATA_ORIGIN = x._PZBPLQS_DATA_ORIGIN;
                    _insPre._PZBPLQS_ACTIVITY_DATE = x._PZBPLQS_ACTIVITY_DATE;
                    _insPre._PZBPLQS_QSTN_CODE = x._PZBPLQS_QSTN_CODE;
                    _insPre._PZBPLQS_POLL_CODE = x._PZBPLQS_POLL_CODE;
                    _insPre._PZBPLQS_CALR_CODE = x._PZBPLQS_CALR_CODE;
                    _insPre._PZBPLQS_APPL_SEQ_NUMBER = x._PZBPLQS_APPL_SEQ_NUMBER;
                    _insPre._PZBPLQS_AREA_CODE = x._PZBPLQS_AREA_CODE;
                    _insPre._PZBPLQS_CRIT_CODE = x._PZBPLQS_CRIT_CODE;
                    _insPre._PZBPLQS_TYAW_CODE = x._PZBPLQS_TYAW_CODE;
                    _insPre._PZBPLQS_MAX_OVAL = x._PZBPLQS_MAX_OVAL;
                    _insPre._PZBPLQS_MAX_VALUE = x._PZBPLQS_MAX_VALUE;
                    _insPre._PZBPLQS_MIN_OVAL = x._PZBPLQS_MIN_OVAL;
                    _insPre._PZBPLQS_MIN_VALUE = x._PZBPLQS_MIN_VALUE;
                    _insPre._PZBPLQS_ORDER = x._PZBPLQS_ORDER;
                    _insPre._PZBPLQS_STATUS = x._PZBPLQS_STATUS;
                    resp._PZBPLQS = _insPre;

                    //DATOS DEL criterio de Evaluacion
                    Models.Criterio _cri = new Models.Criterio();
                    _cri._PZBCRIT_CODE = x._PZBCRIT._PZBCRIT_CODE;
                    _cri._PZBCRIT_USER = x._PZBCRIT._PZBCRIT_USER;
                    _cri._PZBCRIT_DESCRIPTION = x._PZBCRIT._PZBCRIT_DESCRIPTION;
                    _cri._PZBCRIT_DATA_ORIGIN = x._PZBCRIT._PZBCRIT_DATA_ORIGIN;
                    _cri._PZBCRIT_ACTIVITY_DATE = x._PZBCRIT._PZBCRIT_ACTIVITY_DATE;
                    _insPre._PZBCRIT = _cri;
                    resp._PZBPLQS._PZBCRIT = _cri;

                    //Datos del area a la cual pertenece la pregunta
                    Models.Area _area = new Models.Area();
                    _area._PZBAREA_CODE = x._PZBAREA._PZBAREA_CODE;
                    _area._PZBAREA_USER = x._PZBAREA._PZBAREA_USER;
                    _area._PZBAREA_DESCRIPTION = x._PZBAREA._PZBAREA_DESCRIPTION;
                    _area._PZBAREA_DATA_ORIGIN = x._PZBAREA._PZBAREA_DATA_ORIGIN;
                    _area._PZBAREA_ACTIVITY_DATE = x._PZBAREA._PZBAREA_ACTIVITY_DATE;
                    _insPre._PZBAREA = _area;
                    resp._PZBPLQS._PZBAREA = _area;

                    Models.Pregunta item = new Models.Pregunta();

                    item._PZBQSTN_CODE = x._PZBQSTN._PZBQSTN_CODE;
                    item._PZBQSTN_NAME = x._PZBQSTN._PZBQSTN_NAME;
                    item._PZBQSTN_USER = x._PZBQSTN._PZBQSTN_USER;
                    item._PZBQSTN_DESCRIPTION = x._PZBQSTN._PZBQSTN_DESCRIPTION;
                    item._PZBQSTN_DATA_ORIGIN = x._PZBQSTN._PZBQSTN_DATA_ORIGIN;
                    item._PZBQSTN_ACTIVITY_DATE = x._PZBQSTN._PZBQSTN_ACTIVITY_DATE;
                    _insPre._PZBQSTN = item;
                    resp._PZBPLQS._PZBQSTN = item;

                    re.Add(resp);

                }
            }

            return Ok(re);


        }



        /// <summary>
        /// Dado el id del instrumento y del usuario carga 
        /// las respuestas registradas
        /// 
        /// </summary>
        /// <param name="pidm"></param>
        /// <param name="idins"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.InstrumentoAplicable>))]
        [Route("api/PZRAWAP/GetPZRAWAPI/{pidm}/{idins}/{idcal}")]
        public IHttpActionResult GetPZRAWAPI(String pidm, String idins, String idcal)
        {

            List<Models.Respuesta> result = new List<Models.Respuesta>();
            try
            {
                db.PZRAWAP.Include(appl => appl.PZBPLQS.PZBAPPL)
                          .Include(poll => poll.PZBPLQS.PZBAPPL.PZBPOLL)
                          .Include(area => area.PZBPLQS.PZBAREA)
                          .Include(crit => crit.PZBPLQS.PZBCRIT)
                          .Include(qstn => qstn.PZBPLQS.PZBQSTN)
                          .Include(tyaw => tyaw.PZBPLQS.PZVTYAW)
                          .Include(bpls => bpls.PZBPLQS);



                var res = db.PZRAWAP.SqlQuery(@"SELECT *
                                                 FROM    
                                                      PZBPOLL,pzbappl,
                                                      pzbplqs,pzbqstn,
                                                      pzrplap,PZBCALR,
                                                      PZRASPE,PZBPRSO,
                                                      PZVPFLE,pzrdypt,
                                                      pzvpost,pzvdncy,
                                                      pzbarea,pzbcrit,
                                                      pzrawap

                                                 WHERE  PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code
                                                         AND pzbappl.pzbappl_poll_code = pzbplqs.pzbplqs_poll_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzbappl.pzbappl_seq_number = pzbplqs.pzbplqs_appl_seq_number
                                                         and pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number  
                                                         and pzbcalr.pzbcalr_code = pzbappl.pzbappl_calr_code
                                                         and pzbappl.pzbappl_calr_code = pzbplqs.pzbplqs_calr_code
                                                         and pzvpfle.pzvpfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzbqstn.pzbqstn_code = pzbplqs.pzbplqs_qstn_code
                                                         and pzbcrit.pzbcrit_code = pzbplqs.pzbplqs_crit_code
                                                         and pzbarea.pzbarea_code = pzbplqs.pzbplqs_area_code
                                                         and pzbplqs.pzbplqs_seq_number = pzrawap.pzrawap_plqs_seq_number
                                                         and pzrplap.pzrplap_seq_number = pzrawap.pzrawap_plap_seq_number
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                         and pzrplap.pzrplap_pfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         and pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                         and pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         and pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code =  pzvpost.pzvpost_code
                                                         and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                         and pzrdypt.pzrdypt_dncy_code =  pzvdncy.pzvdncy_code
                                                         and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                         and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm
                                                         AND PZBPOLL.PZBPOLL_code='"+idins+"'"+
                                                         "AND PZBCALR.PZBCALR_CODE='"+idcal+"'"+
                                                         "and pzbprso.pzbprso_pidm = '"+pidm+"'").ToList();

                foreach (var x in res)
                {
                    Models.Respuesta resp = new Models.Respuesta();
                    resp._PZRAWAP_SEQ_NUMBER = (int)x.PZRAWAP_SEQ_NUMBER;
                    resp._PZRAWAP_ACTIVITY_DATE = x.PZRAWAP_ACTIVITY_DATE;
                    resp._PZRAWAP_DESCRIPTION = x.PZRAWAP_DESCRIPTION;
                    resp._PZRAWAP_PLAP_SEQ_NUMBER = x.PZRAWAP_PLAP_SEQ_NUMBER;
                    resp._PZRAWAP_PLQS_SEQ_NUMBER = x.PZRAWAP_PLQS_SEQ_NUMBER;
                    resp._PZRAWAP_PERC_IMP = x.PZRAWAP_PERC_IMP;
                    resp._PZRAWAP_USER = x.PZRAWAP_USER;
                    resp._PZRAWAP_VALUE = x.PZRAWAP_VALUE;
                    resp._PZRAWAP_DATA_ORIGIN = x.PZRAWAP_DATA_ORIGIN;



                    Models.AsignacionInstrumentoPregunta _insPre = new Models.AsignacionInstrumentoPregunta();
                    _insPre._PZBPLQS_SEQ_NUMBER = x.PZBPLQS.PZBPLQS_SEQ_NUMBER;
                    _insPre._PZBPLQS_USER = x.PZBPLQS.PZBPLQS_USER;
                    _insPre._PZBPLQS_DATA_ORIGIN = x.PZBPLQS.PZBPLQS_DATA_ORIGIN;
                    _insPre._PZBPLQS_ACTIVITY_DATE = x.PZBPLQS.PZBPLQS_ACTIVITY_DATE;
                    _insPre._PZBPLQS_QSTN_CODE = x.PZBPLQS.PZBPLQS_QSTN_CODE;
                    _insPre._PZBPLQS_POLL_CODE = x.PZBPLQS.PZBPLQS_POLL_CODE;
                    _insPre._PZBPLQS_CALR_CODE = x.PZBPLQS.PZBPLQS_CALR_CODE;
                    _insPre._PZBPLQS_APPL_SEQ_NUMBER = x.PZBPLQS.PZBPLQS_APPL_SEQ_NUMBER;
                    _insPre._PZBPLQS_AREA_CODE = x.PZBPLQS.PZBPLQS_AREA_CODE;
                    _insPre._PZBPLQS_CRIT_CODE = x.PZBPLQS.PZBPLQS_CRIT_CODE;
                    _insPre._PZBPLQS_TYAW_CODE = x.PZBPLQS.PZBPLQS_TYAW_CODE;
                    _insPre._PZBPLQS_MAX_OVAL = x.PZBPLQS.PZBPLQS_MAX_OVAL;
                    _insPre._PZBPLQS_MAX_VALUE = x.PZBPLQS.PZBPLQS_MAX_VALUE;
                    _insPre._PZBPLQS_MIN_OVAL = x.PZBPLQS.PZBPLQS_MIN_OVAL;
                    _insPre._PZBPLQS_MIN_VALUE = x.PZBPLQS.PZBPLQS_MIN_VALUE;
                    _insPre._PZBPLQS_ORDER = x.PZBPLQS.PZBPLQS_ORDER;
                    _insPre._PZBPLQS_STATUS = x.PZBPLQS.PZBPLQS_STATUS;


                    Models.InstrumentoAplicable apl = new Models.InstrumentoAplicable();
                    apl._PZBAPPL_USER = x.PZBPLQS.PZBAPPL.PZBAPPL_USER;
                    apl._PZBAPPL_PRCNT = x.PZBPLQS.PZBAPPL.PZBAPPL_PRCNT;
                    apl._PZBAPPL_SEQ_NUMBER = x.PZBPLQS.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                    apl._PZBAPPL_POLL_CODE = x.PZBPLQS.PZBAPPL.PZBAPPL_POLL_CODE;
                    apl._PZBAPPL_PFLE_CODE = x.PZBPLQS.PZBAPPL.PZBAPPL_PFLE_CODE;
                    apl._PZBAPPL_CALR_CODE = x.PZBPLQS.PZBAPPL.PZBAPPL_CALR_CODE;
                    _insPre._PZBAPPL = apl;

                    Models.Instrumento ins = new Models.Instrumento();
                    ins._PZBPOLL_NAME = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                    ins._PZBPOLL_CODE = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                    ins._PZBPOLL_USER = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;


                    //DATOS DEL criterio de Evaluacion
                    Models.Criterio _cri = new Models.Criterio();
                    _cri._PZBCRIT_CODE = x.PZBPLQS.PZBCRIT.PZBCRIT_CODE;
                    _cri._PZBCRIT_USER = x.PZBPLQS.PZBCRIT.PZBCRIT_USER;
                    _cri._PZBCRIT_DESCRIPTION = x.PZBPLQS.PZBCRIT.PZBCRIT_DESCRIPTION;
                    _cri._PZBCRIT_DATA_ORIGIN = x.PZBPLQS.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                    _cri._PZBCRIT_ACTIVITY_DATE = x.PZBPLQS.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                    _insPre._PZBCRIT = _cri;

                    //Datos del area a la cual pertenece la pregunta
                    Models.Area _area = new Models.Area();
                    _area._PZBAREA_CODE = x.PZBPLQS.PZBAREA.PZBAREA_CODE;
                    _area._PZBAREA_USER = x.PZBPLQS.PZBAREA.PZBAREA_USER;
                    _area._PZBAREA_DESCRIPTION = x.PZBPLQS.PZBAREA.PZBAREA_DESCRIPTION;
                    _area._PZBAREA_DATA_ORIGIN = x.PZBPLQS.PZBAREA.PZBAREA_DATA_ORIGIN;
                    _area._PZBAREA_ACTIVITY_DATE = x.PZBPLQS.PZBAREA.PZBAREA_ACTIVITY_DATE;
                    _insPre._PZBAREA = _area;

                    Models.Pregunta item = new Models.Pregunta();

                    item._PZBQSTN_CODE = x.PZBPLQS.PZBQSTN.PZBQSTN_CODE;
                    item._PZBQSTN_NAME = x.PZBPLQS.PZBQSTN.PZBQSTN_NAME;
                    item._PZBQSTN_USER = x.PZBPLQS.PZBQSTN.PZBQSTN_USER;
                    item._PZBQSTN_DESCRIPTION = x.PZBPLQS.PZBQSTN.PZBQSTN_DESCRIPTION;
                    item._PZBQSTN_DATA_ORIGIN = x.PZBPLQS.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                    item._PZBQSTN_ACTIVITY_DATE = x.PZBPLQS.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                    _insPre._PZBQSTN = item;

                    resp._PZBPLQS = _insPre;

                    result.Add(resp);
                }

                return (Ok(result));

            }

            catch (Exception e)
            {

                return (NotFound());

            }


        }



        /// <summary>
        ///  
        /// Dado el id del instrumento y del evaluado
        /// Devuelve las respuestas registradas con las
        /// preguntas asociadas
        /// 
        /// </summary>
        /// <param name="ide"></param>
        /// <param name="idi"></param>
        /// <returns></returns>

        [HttpGet]
        [ResponseType(typeof(List<Models.Respuesta>))]
        [Route("api/PZRAWAP/GetPZRAWAP/{id}/{idins}")]
        public IHttpActionResult GetPZRAWAP(int id, String idins)
        {
            List<Models.Respuesta> re = new List<Models.Respuesta>();
            try
            {
                db.PZRAWAP.Include(pregunta => pregunta.PZBPLQS.PZBQSTN);

                var es = db.PZRAWAP.SqlQuery(@"select *
                                                  from pzrawap,PZBPLQS,pzbqstn,PZBPOLL,
                                                       PZRPLAP,PZRASPE,pzbappl,pzbprso,
                                                       pzbcalr
                                                  where pzrawap.pzrawap_plqs_seq_number = pzbplqs.pzbplqs_seq_number
                                                        AND pzrawap.pzrawap_plap_seq_number = PZRPLAP.pzrplap_seq_number
                                                        and pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                        and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code 
                                                        and pzbappl.pzbappl_calr_code = pzbcalr.pzbcalr_code
                                                        AND PZBPLQS.PZBPLQS_qstn_code = pzbqstn.pzbqstn_code
                                                        and pzbappl.pzbappl_poll_code = pzbplqs.pzbplqs_poll_code
                                                        AND pzbappl.pzbappl_poll_code= PZBPOLL.PZBPOLL_code
                                                        AND pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                        and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm
                                                        AND pzbprso.pzbprso_pidm = '" + id + "'" +
                                                        "and pzbpoll.pzbpoll_code = '" + idins + "'" +
                                                        "ORDER BY pzrawap.pzrawap_seq_number ").ToList();
                foreach (var result in es)
                {
                    //Respuesta
                    Models.Respuesta resp = new Models.Respuesta();
                    resp._PZRAWAP_SEQ_NUMBER = (int)result.PZRAWAP_SEQ_NUMBER;
                    resp._PZRAWAP_ACTIVITY_DATE = result.PZRAWAP_ACTIVITY_DATE;
                    resp._PZRAWAP_DESCRIPTION = result.PZRAWAP_DESCRIPTION;
                    resp._PZRAWAP_PLAP_SEQ_NUMBER = result.PZRAWAP_PLAP_SEQ_NUMBER;
                    resp._PZRAWAP_PLQS_SEQ_NUMBER = result.PZRAWAP_PLQS_SEQ_NUMBER;
                    resp._PZRAWAP_PERC_IMP = result.PZRAWAP_PERC_IMP;
                    resp._PZRAWAP_USER = result.PZRAWAP_USER;
                    resp._PZRAWAP_VALUE = result.PZRAWAP_VALUE;
                    resp._PZRAWAP_DATA_ORIGIN = result.PZRAWAP_DATA_ORIGIN;

                    // PZBPLQS
                    Models.AsignacionInstrumentoPregunta i_c = new Models.AsignacionInstrumentoPregunta();
                    i_c._PZBPLQS_SEQ_NUMBER = result.PZBPLQS.PZBPLQS_SEQ_NUMBER;
                    i_c._PZBPLQS_USER = result.PZBPLQS.PZBPLQS_USER;
                    i_c._PZBPLQS_DATA_ORIGIN = result.PZBPLQS.PZBPLQS_DATA_ORIGIN;
                    i_c._PZBPLQS_ACTIVITY_DATE = result.PZBPLQS.PZBPLQS_ACTIVITY_DATE;
                    i_c._PZBPLQS_QSTN_CODE = result.PZBPLQS.PZBPLQS_QSTN_CODE;
                    i_c._PZBPLQS_POLL_CODE = result.PZBPLQS.PZBPLQS_POLL_CODE;
                    i_c._PZBPLQS_AREA_CODE = result.PZBPLQS.PZBPLQS_AREA_CODE;
                    i_c._PZBPLQS_CRIT_CODE = result.PZBPLQS.PZBPLQS_CRIT_CODE;
                    i_c._PZBPLQS_MAX_OVAL = result.PZBPLQS.PZBPLQS_MAX_OVAL;
                    i_c._PZBPLQS_MAX_VALUE = result.PZBPLQS.PZBPLQS_MAX_VALUE;
                    i_c._PZBPLQS_MIN_OVAL = result.PZBPLQS.PZBPLQS_MIN_OVAL;
                    i_c._PZBPLQS_MIN_VALUE = result.PZBPLQS.PZBPLQS_MIN_VALUE;
                    i_c._PZBPLQS_ORDER = result.PZBPLQS.PZBPLQS_ORDER;
                    i_c._PZBPLQS_STATUS = result.PZBPLQS.PZBPLQS_STATUS;
                    resp._PZBPLQS = i_c;

                    //Pregunta correspondiente
                    Models.Pregunta item = new Models.Pregunta();
                    item._PZBQSTN_CODE = result.PZBPLQS.PZBQSTN.PZBQSTN_CODE;
                    item._PZBQSTN_USER = result.PZBPLQS.PZBQSTN.PZBQSTN_USER;
                    item._PZBQSTN_NAME = result.PZBPLQS.PZBQSTN.PZBQSTN_NAME;
                    item._PZBQSTN_DESCRIPTION = result.PZBPLQS.PZBQSTN.PZBQSTN_DESCRIPTION;
                    item._PZBQSTN_DATA_ORIGIN = result.PZBPLQS.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                    item._PZBQSTN_ACTIVITY_DATE = result.PZBPLQS.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                    resp._PZBPLQS._PZBQSTN = item;


                    re.Add(resp);
                }


                if (re.Count() == 0)
                {

                    return Ok("No existen respuestas asociadas al los parametros enviados");


                }

            }
            catch (Exception e)
            {

                return Redirect("Respuesta no encontrada");

            }


            return Ok(re);

        }

        /// <summary>
        /// 
        /// Dado el id de la persona
        /// el instrumento  y el calendario
        /// busca las preguntas y respuestas asociadas
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idins"></param>
        /// <param name="idcal"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Respuesta>))]
        [Route("api/PZRAWAP/GetPZRAWAPO/{id}/{idins}/{idcal}")]
        public IHttpActionResult GetPZRAWAPO(int id, String idins, String idcal)
        {
            List<Models.Respuesta> re = new List<Models.Respuesta>();
            try
            {
                db.PZRAWAP.Include(pregunta => pregunta.PZBPLQS.PZBQSTN)
                          .Include(criterio => criterio.PZBPLQS.PZBCRIT)
                          .Include(instrumento => instrumento.PZBPLQS.PZBAPPL.PZBPOLL)
                          .Include(area => area.PZBPLQS.PZBAREA);

                var es = db.PZRAWAP.SqlQuery(@"select *
                                                    from pzrplap,pzrawap,pzbplqs,pzbcrit,
                                                         pzbarea,pzbpoll,pzbcalr,pzbqstn,
                                                         pzbphse,pzvtyaw,pzbappl
                                                    where pzrawap.pzrawap_plap_seq_number = pzrplap.pzrplap_seq_number
                                                    and pzrawap.pzrawap_plqs_seq_number = pzbplqs.pzbplqs_seq_number
                                                    and pzbplqs.pzbplqs_crit_code = pzbcrit.pzbcrit_code
                                                    AND   pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                    and   pzbappl.pzbappl_calr_code = PZRPLAP.PZRPLAP_CALR_code 
                                                    AND   pzbappl.pzbappl_calr_code = PZBCALR.PZBCALR_CODE
                                                    and   pzbplqs.pzbplqs_area_code = pzbarea.pzbarea_code
                                                    and   pzbplqs.pzbplqs_poll_code = pzbpoll.pzbpoll_code
                                                    and   pzbplqs.pzbplqs_qstn_code = pzbqstn.pzbqstn_code
                                                    and   pzbplqs.pzbplqs_tyaw_code = pzvtyaw.pzvtyaw_code
                                                    and   pzbplqs.pzbplqs_calr_code = pzbcalr.pzbcalr_code
                                                    AND   pzbcalr.pzbcalr_code = pzbphse.pzbphse_calr_code
                                                        AND SYSDATE >= pzbphse.pzbphse_open_date
                                                        and sysdate <=pzbphse.pzbphse_close_date 
                                                    and   pzbplqs.pzbplqs_status ='A'
                                                    and   pzrplap_pidm = '" + id + "'" +
                                                    "and   pzrplap.pzrplap_calr_code = '" + idcal + "'" +
                                                    "and   pzrplap.pzrplap_POLL_CODE = '" + idins + "'" +
                                                    "order by pzbplqs.pzbplqs_order asc ").ToList();
                foreach (var result in es)
                {
                    //Respuesta
                    Models.Respuesta resp = new Models.Respuesta();
                    resp._PZRAWAP_SEQ_NUMBER = (int)result.PZRAWAP_SEQ_NUMBER;
                    resp._PZRAWAP_ACTIVITY_DATE = result.PZRAWAP_ACTIVITY_DATE;
                    resp._PZRAWAP_DESCRIPTION = result.PZRAWAP_DESCRIPTION;
                    resp._PZRAWAP_PLAP_SEQ_NUMBER = result.PZRAWAP_PLAP_SEQ_NUMBER;
                    resp._PZRAWAP_PLQS_SEQ_NUMBER = result.PZRAWAP_PLQS_SEQ_NUMBER;
                    resp._PZRAWAP_PERC_IMP = result.PZRAWAP_PERC_IMP;
                    resp._PZRAWAP_USER = result.PZRAWAP_USER;
                    resp._PZRAWAP_VALUE = result.PZRAWAP_VALUE;
                    resp._PZRAWAP_DATA_ORIGIN = result.PZRAWAP_DATA_ORIGIN;

                    // PZBPLQS
                    Models.AsignacionInstrumentoPregunta i_c = new Models.AsignacionInstrumentoPregunta();
                    i_c._PZBPLQS_SEQ_NUMBER = result.PZBPLQS.PZBPLQS_SEQ_NUMBER;
                    i_c._PZBPLQS_USER = result.PZBPLQS.PZBPLQS_USER;
                    i_c._PZBPLQS_DATA_ORIGIN = result.PZBPLQS.PZBPLQS_DATA_ORIGIN;
                    i_c._PZBPLQS_ACTIVITY_DATE = result.PZBPLQS.PZBPLQS_ACTIVITY_DATE;
                    i_c._PZBPLQS_QSTN_CODE = result.PZBPLQS.PZBPLQS_QSTN_CODE;
                    i_c._PZBPLQS_POLL_CODE = result.PZBPLQS.PZBPLQS_POLL_CODE;
                    i_c._PZBPLQS_AREA_CODE = result.PZBPLQS.PZBPLQS_AREA_CODE;
                    i_c._PZBPLQS_CRIT_CODE = result.PZBPLQS.PZBPLQS_CRIT_CODE;
                    i_c._PZBPLQS_MAX_OVAL = result.PZBPLQS.PZBPLQS_MAX_OVAL;
                    i_c._PZBPLQS_MAX_VALUE = result.PZBPLQS.PZBPLQS_MAX_VALUE;
                    i_c._PZBPLQS_MIN_OVAL = result.PZBPLQS.PZBPLQS_MIN_OVAL;
                    i_c._PZBPLQS_MIN_VALUE = result.PZBPLQS.PZBPLQS_MIN_VALUE;
                    i_c._PZBPLQS_ORDER = result.PZBPLQS.PZBPLQS_ORDER;
                    i_c._PZBPLQS_STATUS = result.PZBPLQS.PZBPLQS_STATUS;
                    resp._PZBPLQS = i_c;

                    //Pregunta correspondiente
                    Models.Pregunta item = new Models.Pregunta();
                    item._PZBQSTN_CODE = result.PZBPLQS.PZBQSTN.PZBQSTN_CODE;
                    item._PZBQSTN_USER = result.PZBPLQS.PZBQSTN.PZBQSTN_USER;
                    item._PZBQSTN_NAME = result.PZBPLQS.PZBQSTN.PZBQSTN_NAME;
                    item._PZBQSTN_DESCRIPTION = result.PZBPLQS.PZBQSTN.PZBQSTN_DESCRIPTION;
                    item._PZBQSTN_DATA_ORIGIN = result.PZBPLQS.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                    item._PZBQSTN_ACTIVITY_DATE = result.PZBPLQS.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                    resp._PZBPLQS._PZBQSTN = item;

                    Models.Area _area = new Models.Area();
                    _area._PZBAREA_CODE = result.PZBPLQS.PZBAREA.PZBAREA_CODE;
                    _area._PZBAREA_DESCRIPTION = result.PZBPLQS.PZBAREA.PZBAREA_DESCRIPTION;
                    _area._PZBAREA_USER = result.PZBPLQS.PZBAREA.PZBAREA_USER;
                    _area._PZBAREA_DATA_ORIGIN = result.PZBPLQS.PZBAREA.PZBAREA_DATA_ORIGIN;
                    _area._PZBAREA_ACTIVITY_DATE = result.PZBPLQS.PZBAREA.PZBAREA_ACTIVITY_DATE;
                    resp._PZBPLQS._PZBAREA = _area;

                    Models.Criterio res = new Models.Criterio();
                    res._PZBCRIT_CODE = result.PZBPLQS.PZBCRIT.PZBCRIT_CODE;
                    res._PZBCRIT_USER = result.PZBPLQS.PZBCRIT.PZBCRIT_USER;
                    res._PZBCRIT_DESCRIPTION = result.PZBPLQS.PZBCRIT.PZBCRIT_DESCRIPTION;
                    res._PZBCRIT_DATA_ORIGIN = result.PZBPLQS.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                    res._PZBCRIT_ACTIVITY_DATE = result.PZBPLQS.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                    resp._PZBPLQS._PZBCRIT = res;

                    re.Add(resp);
                }


                if (re.Count() == 0)
                {

                    return Ok("No existen respuestas asociadas al los parametros enviados");


                }

            }
            catch (Exception e)
            {

                return Redirect("Respuesta no encontrada");

            }


            return Ok(re);

        }

        // PUT: api/PZRAWAP/5
        /// <summary>
        /// 
        /// Modifica las respuestas guardadas
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="_pZVADEF"></param>
        /// <returns></returns>

        [HttpPut]
        [Route("api/PZRAWAP/PutPZRAWAP/{id}/{_status}")]
        [ResponseType(typeof(Respuesta))]
        public IHttpActionResult PutPZRAWAP(decimal id, string _status, Respuesta _pZVADEF)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != _pZVADEF._PZRAWAP_SEQ_NUMBER)
            {
                return BadRequest();
            }



            try
            {
                DateTime fecha = DateTime.Today;

                //aqui tendria que enviar el username ademas de lo que esta.
                _pZVADEF._PZRAWAP_ACTIVITY_DATE = fecha;

                PZRAWAP respuesta = new PZRAWAP();

                respuesta.PZRAWAP_SEQ_NUMBER = _pZVADEF._PZRAWAP_SEQ_NUMBER;
                respuesta.PZRAWAP_PLQS_SEQ_NUMBER = _pZVADEF._PZRAWAP_PLQS_SEQ_NUMBER;
                respuesta.PZRAWAP_PLAP_SEQ_NUMBER = _pZVADEF._PZRAWAP_PLAP_SEQ_NUMBER;
                respuesta.PZRAWAP_ACTIVITY_DATE = fecha;
                respuesta.PZRAWAP_VALUE = _pZVADEF._PZRAWAP_VALUE;
                respuesta.PZRAWAP_DESCRIPTION = _pZVADEF._PZRAWAP_DESCRIPTION;
                respuesta.PZRAWAP_PERC_IMP = _pZVADEF._PZRAWAP_PERC_IMP;
                respuesta.PZRAWAP_USER = _pZVADEF._PZRAWAP_USER;
                respuesta.PZRAWAP_DATA_ORIGIN = _pZVADEF._PZRAWAP_DATA_ORIGIN;

                db.PZRAWAP.Attach(respuesta);
                System.Diagnostics.Debug.WriteLine(respuesta);
                db.Entry(respuesta).State = EntityState.Modified;
                System.Diagnostics.Debug.WriteLine((db.Entry(respuesta).State = EntityState.Modified));
                db.SaveChanges();

                _f.PutPZRPLAP(respuesta.PZRAWAP_PLAP_SEQ_NUMBER, respuesta.PZRAWAP_USER, _status);

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PZRAWAPExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(_pZVADEF);
        }


        /// <summary>
        /// Modifica dada una lista de respuestas los valores
        /// 
        /// </summary>
        /// <param name="__pZVADEF"></param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(List<Models.Respuesta>))]
        [Route("api/PZRAWAP/PutPZRAWAPO/{_status}")]
        public IHttpActionResult PutPZRAWAPO(string _status, List<Models.Respuesta> __pZVADEF)
        {
            String _idev;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            foreach (var i in __pZVADEF)
            {
                int id = i._PZRAWAP_SEQ_NUMBER;

                if (id != i._PZRAWAP_SEQ_NUMBER)
                {
                    return BadRequest();
                }

                try
                {
                    PZRAWAP respuesta = new PZRAWAP();
                    DateTime fecha = DateTime.Today;

                    //aqui tendria que enviar el username ademas de lo que esta.
                    i._PZRAWAP_ACTIVITY_DATE = fecha;
                    respuesta.PZRAWAP_ACTIVITY_DATE = fecha;
                    respuesta.PZRAWAP_USER = i._PZRAWAP_USER;
                    respuesta.PZRAWAP_VALUE = i._PZRAWAP_VALUE;
                    respuesta.PZRAWAP_PERC_IMP = i._PZRAWAP_PERC_IMP;
                    respuesta.PZRAWAP_SEQ_NUMBER = i._PZRAWAP_SEQ_NUMBER;
                    respuesta.PZRAWAP_DESCRIPTION = i._PZRAWAP_DESCRIPTION;
                    respuesta.PZRAWAP_DATA_ORIGIN = i._PZRAWAP_DATA_ORIGIN;
                    respuesta.PZRAWAP_PLQS_SEQ_NUMBER = i._PZRAWAP_PLQS_SEQ_NUMBER;
                    respuesta.PZRAWAP_PLAP_SEQ_NUMBER = i._PZRAWAP_PLAP_SEQ_NUMBER;


                    db.PZRAWAP.Attach(respuesta);
                    System.Diagnostics.Debug.WriteLine(respuesta);
                    db.Entry(respuesta).State = EntityState.Modified;
                    System.Diagnostics.Debug.WriteLine((db.Entry(respuesta).State = EntityState.Modified));
                    db.SaveChanges();

                    //ACTUALIZACION DEL STATUS de las respuestas en la asignacion
                    _f.PutPZRPLAP((int)respuesta.PZRAWAP_PLAP_SEQ_NUMBER, respuesta.PZRAWAP_USER, _status);

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PZRAWAPExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                // return Ok(i);

            }



            return Ok("Se registraron los valores exitosamente");
        }

        // POST: api/PZRAWAP

        /// <summary>
        /// 
        /// Inserta las respuestas del Usuario 
        /// recibiendo una lista
        /// 
        ///  Ruta : http://localhost:63665/api/PZRAWAP/PostPZRAWAP
        ///  
        /// </summary>
        /// <param name="_pZVADEF"></param>
        /// <returns>_pZVADEF</returns>
        [HttpPost]
        [ResponseType(typeof(List<Models.Respuesta>))]
        [Route("api/PZRAWAP/PostPZRAWAP/{_status}")]
        public IHttpActionResult PostPZRAWAP(string _status, List<Models.Respuesta> __pZVADEF)
        {
            int es = 0;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {


                foreach (var i in __pZVADEF) {
                    int num_reg = (int)db.PZRAWAP.SqlQuery(@"select * FROM PZRAWAP ").Count();

                    if (num_reg == 0)
                    {

                        es = 1;
                    }

                    else
                    {
                        es = (int)db.PZRAWAP.Max(x => x.PZRAWAP_SEQ_NUMBER);


                        es = es + 1;
                    }

                    DateTime fecha = DateTime.Today;

                    //aqui tendria que enviar el usernaame ademas de lo que esta.

                    i._PZRAWAP_ACTIVITY_DATE = fecha;
                    i._PZRAWAP_SEQ_NUMBER = (int)es;



                    PZRAWAP respuesta = new PZRAWAP();

                    respuesta.PZRAWAP_SEQ_NUMBER = (int)i._PZRAWAP_SEQ_NUMBER;
                    respuesta.PZRAWAP_PLQS_SEQ_NUMBER = (int)i._PZRAWAP_PLQS_SEQ_NUMBER;
                    respuesta.PZRAWAP_PLAP_SEQ_NUMBER = (int)i._PZRAWAP_PLAP_SEQ_NUMBER;
                    respuesta.PZRAWAP_PERC_IMP = i._PZRAWAP_PERC_IMP;
                    respuesta.PZRAWAP_DESCRIPTION = i._PZRAWAP_DESCRIPTION;
                    respuesta.PZRAWAP_VALUE = i._PZRAWAP_VALUE;
                    respuesta.PZRAWAP_ACTIVITY_DATE = i._PZRAWAP_ACTIVITY_DATE;
                    respuesta.PZRAWAP_USER = i._PZRAWAP_USER;
                    respuesta.PZRAWAP_DATA_ORIGIN = i._PZRAWAP_DATA_ORIGIN;

                    db.PZRAWAP.Add(respuesta);

                    db.SaveChanges();

                    _f.PutPZRPLAP((int)respuesta.PZRAWAP_PLAP_SEQ_NUMBER, respuesta.PZRAWAP_USER, _status);

                }
            }
            catch (Exception e)
            {

            }



            return CreatedAtRoute("DefaultApi", new { id = es }, __pZVADEF);
        }


        /// <summary>
        /// Registra Objetivos 
        /// </summary>
        /// <param name="_pZVADEF"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(Models.Respuesta))]
        [Route("api/PZRAWAP/PostPZRAWAPO/{_status}/")]
        public IHttpActionResult PostPZRAWAPO(string _status, Models.Respuesta _pZVADEF)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                int es;
                int num_reg = (int)db.PZRAWAP.SqlQuery(@"select * FROM PZRAWAP ").Count();

                if (num_reg == 0) {

                    es = 1;
                }

                else
                {
                    es = (int)db.PZRAWAP.Max(x => x.PZRAWAP_SEQ_NUMBER);


                    es = es + 1;
                }

                DateTime fecha = DateTime.Today;

                //aqui tendria que enviar el usernaame ademas de lo que esta.

                _pZVADEF._PZRAWAP_ACTIVITY_DATE = fecha;

                PZRAWAP respuesta = new PZRAWAP();


                respuesta.PZRAWAP_SEQ_NUMBER = (int)es;
                respuesta.PZRAWAP_PLQS_SEQ_NUMBER = (int)_pZVADEF._PZRAWAP_PLQS_SEQ_NUMBER;
                respuesta.PZRAWAP_PLAP_SEQ_NUMBER = (int)_pZVADEF._PZRAWAP_PLAP_SEQ_NUMBER;
                respuesta.PZRAWAP_PERC_IMP = _pZVADEF._PZRAWAP_PERC_IMP;
                respuesta.PZRAWAP_DESCRIPTION = _pZVADEF._PZRAWAP_DESCRIPTION;
                respuesta.PZRAWAP_VALUE = _pZVADEF._PZRAWAP_VALUE;
                respuesta.PZRAWAP_ACTIVITY_DATE = _pZVADEF._PZRAWAP_ACTIVITY_DATE;
                respuesta.PZRAWAP_USER = _pZVADEF._PZRAWAP_USER;
                respuesta.PZRAWAP_DATA_ORIGIN = _pZVADEF._PZRAWAP_DATA_ORIGIN;

                db.PZRAWAP.Add(respuesta);


                db.SaveChanges();
                
                //ACTUALIZACION DEL STATUS de las respuestas
                _f.PutPZRPLAP((int)respuesta.PZRAWAP_PLAP_SEQ_NUMBER, respuesta.PZRAWAP_USER, _status);
            }
            catch (DbUpdateException)
            {
                if (PZRAWAPExists(_pZVADEF._PZRAWAP_SEQ_NUMBER))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }



            return CreatedAtRoute("DefaultApi", new { id = _pZVADEF._PZRAWAP_SEQ_NUMBER }, _pZVADEF);
        }

        [ResponseType(typeof(PZRAWAP))]
        public IHttpActionResult PostPZRAWAP(PZRAWAP _pZVADEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PZRAWAP.Add(_pZVADEF);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PZRAWAPExists(_pZVADEF.PZRAWAP_SEQ_NUMBER))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = _pZVADEF.PZRAWAP_SEQ_NUMBER }, _pZVADEF);
        }







        // DELETE: api/PZRAWAP/5
        [ResponseType(typeof(PZRAWAP))]
        public IHttpActionResult DeletePZRAWAP(decimal id)
        {
            PZRAWAP _pZVADEF = db.PZRAWAP.Find(id);
            if (_pZVADEF == null)
            {
                return NotFound();
            }

            db.PZRAWAP.Remove(_pZVADEF);
            db.SaveChanges();

            return Ok(_pZVADEF);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZRAWAPExists(decimal id)
        {
            return db.PZRAWAP.Count(e => e.PZRAWAP_SEQ_NUMBER == id) > 0;
        }




        
    }
}


