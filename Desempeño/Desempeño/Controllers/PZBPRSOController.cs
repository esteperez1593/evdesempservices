﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZBPRSOController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/PZBPRSO
        [HttpGet]
        [ResponseTypeAttribute(typeof(List<Models.Persona>))]
        public IHttpActionResult GetPZBPRSO()
        {
            List<Models.Persona> re = new List<Models.Persona>();

            try
            {


                var es = db.PZBPRSO.SqlQuery(@"select * FROM PZBPRSO ").ToList();

                foreach (var result in es)
                {
                    Models.Persona item = new Models.Persona();

                   // item.PZBPRSO_ID = result.PZBPRSO_ID;
                    item._PZBPRSO_PIDM = result.PZBPRSO_PIDM;
                    //item.PZBPRSO_NAME = result.PZBPRSO_NAME;
                    item._PZBPRSO_ACTIVE = result.PZBPRSO_ACTIVE;

                  
                    re.Add(item);
                }

            }
            catch (Exception e)
            {

            }

            return Ok(re);
        }


        [HttpGet]
        [ResponseType(typeof(List<Models.Persona>))]
        [Route("api/PZBPRSO/GetPZBPRSO/{id}")]
        [ResponseType(typeof(PZBPRSO))]
        public IHttpActionResult GetPZBPRSO(int id)
        {
            List<Models.Persona> re = new List<Models.Persona>();

            try
            {


                db.PZBPRSO.Include(p => p.PZRASPE);      
                var es = db.PZBPRSO.SqlQuery(@"select *  
                                                   FROM PZBPRSO,PZRASPE  
                                                    where PZBPRSO_PIDM ='"+id+"' AND PZBPRSO_PIDM = PZRASPE_PIDM").ToList();
                
                foreach (var X in es)
                {

                    //Datos del Instrumento Aplicable
                    Models.Persona _prsn = new Models.Persona();
                    _prsn._PZBPRSO_PIDM = X.PZBPRSO_PIDM;
                    _prsn._PZBPRSO_ACTIVE = X.PZBPRSO_ACTIVE;
                    _prsn._PZBPRSO_SITE = X.PZBPRSO_SITE;
                    _prsn._PZBPRSO_USER = X.PZBPRSO_USER;
                    _prsn._PZBPRSO_PAYSHEET = X.PZBPRSO_PAYSHEET;
                    _prsn._PZBPRSO_DATA_ORIGIN = X.PZBPRSO_PAYSHEET;
                    _prsn._PZBPRSO_ACTIVITY_DATE = X.PZBPRSO_ACTIVITY_DATE;
                    _prsn._PZBPRSO_DATA_ORIGIN = X.PZBPRSO_DATA_ORIGIN;


                    foreach (var _raspe in X.PZRASPE) {

                        Models.AsignacionCargoPersona ppcd = new Models.AsignacionCargoPersona();
                        //   ppcd.PZRASPE_ID = x;
                        ppcd._PZRASPE_DNCY_CODE = _raspe.PZRASPE_DNCY_CODE;
                        ppcd._PZRASPE_POST_CODE = _raspe.PZRASPE_POST_CODE;
                        ppcd._PZRASPE_PIDM = _raspe.PZRASPE_PIDM;
                        ppcd._PZRASPE_PFLE_CODE = _raspe.PZRASPE_PFLE_CODE;
                        ppcd._PZRASPE_END_DATE = _raspe.PZRASPE_END_DATE;
                        ppcd._PZRASPE_START_DATE = _raspe.PZRASPE_START_DATE;
                        ppcd._PZRASPE_DATA_ORIGIN = _raspe.PZRASPE_DATA_ORIGIN;
                        ppcd._PZRASPE_ACTIVITY_DATE = _raspe.PZRASPE_ACTIVITY_DATE;
                        ppcd._PZRASPE_USER = _raspe.PZRASPE_USER;


                        Models.Perfil _pfle = new Models.Perfil();
                        _pfle._PZVPFLE_CODE = _raspe.PZVPFLE.PZVPFLE_CODE;
                        _pfle._PZVPFLE_NAME = _raspe.PZVPFLE.PZVPFLE_NAME;
                        _pfle._PZVPFLE_USER = _raspe.PZRASPE_USER;
                        _pfle._PZVPFLE_DATA_ORIGIN = _raspe.PZRASPE_DATA_ORIGIN;
                        _pfle._PZVPFLE_ACTIVITY_DATE = _raspe.PZVPFLE.PZVPFLE_ACTIVITY_DATE;
                        ppcd._PZVPFLE = _pfle;


                        _prsn._PZRASPE.Add(ppcd);
                        
                    }



                    re.Add(_prsn);


                }
                return (Ok(re));
            }
            catch (Exception e)
            {

            }

            return Ok(re);
        }

        // PUT: api/PZBPRSO/5
        [ResponseType(typeof(Persona))]
        public IHttpActionResult PutPZBPRSO(decimal id, Persona pZPERSO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pZPERSO._PZBPRSO_PIDM)
            {
                return BadRequest();
            }
            pZPERSO._PZBPRSO_ACTIVITY_DATE = DateTime.Today;
            db.Entry(pZPERSO).State = EntityState.Modified; ;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PZBPRSOExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// 
        /// Agrega Personas 
        /// Ruta: api/PZBPRSO
        /// 
        /// </summary>
        /// <param name="pZPERSO"></param>
        /// <returns></returns>
        [ResponseType(typeof(Persona))]
        public IHttpActionResult PostPZBPRSO(Persona pZPERSO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PZBPRSO dato = new PZBPRSO();

            //dato.PZBPRSO_ID = x.PZBPRSO_ID;
            //dato.PZBPRSO_NAME = x.PZBPRSO_NAME;
            dato.PZBPRSO_SITE = pZPERSO._PZBPRSO_SITE;
            dato.PZBPRSO_PAYSHEET = pZPERSO._PZBPRSO_PAYSHEET;
            dato.PZBPRSO_PIDM = pZPERSO._PZBPRSO_PIDM;
            //dato.PZBPRSO_PZVPOST = x.PZBPRSO_PZCRCOD;
            dato.PZBPRSO_USER = pZPERSO._PZBPRSO_USER;
            dato.PZBPRSO_ACTIVE = pZPERSO._PZBPRSO_ACTIVE;
            dato.PZBPRSO_DATA_ORIGIN = pZPERSO._PZBPRSO_DATA_ORIGIN;
            dato.PZBPRSO_ACTIVITY_DATE = pZPERSO._PZBPRSO_ACTIVITY_DATE;

            pZPERSO._PZBPRSO_ACTIVITY_DATE = DateTime.Today;
            


            db.PZBPRSO.Add(dato);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PZBPRSOExists(pZPERSO._PZBPRSO_PIDM))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pZPERSO._PZBPRSO_PIDM }, pZPERSO);
        }

        // DELETE: api/PZBPRSO/5
        [ResponseType(typeof(PZBPRSO))]
        public IHttpActionResult DeletePZBPRSO(decimal id)
        {
            PZBPRSO pZPERSO = db.PZBPRSO.Find(id);
            if (pZPERSO == null)
            {
                return NotFound();
            }

            db.PZBPRSO.Remove(pZPERSO);
            db.SaveChanges();

            return Ok(pZPERSO);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZBPRSOExists(decimal id)
        {
            return db.PZBPRSO.Count(e => e.PZBPRSO_PIDM == id) > 0;
        }
    }
}