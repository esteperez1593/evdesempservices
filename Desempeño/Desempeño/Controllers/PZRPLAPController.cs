﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZRPLAPController : ApiController
    {
        private Entities db = new Entities();
        private Repository.FunctionsRepository _f = new Repository.FunctionsRepository();

        // GET: api/PZRPLAP
        /// <summary>
        /// 
        /// Carga todos los registros de ins_cal_per_dep
        /// 
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [ResponseType(typeof(List<Models.AsignacionInstrumentoPersona>))]
        [Route("api/PZRPLAP/GetPZRPLAP/")]
        public IHttpActionResult GetPZRPLAP()
        {
            List<Models.AsignacionInstrumentoPersona> re = new List<Models.AsignacionInstrumentoPersona>();


            try
            {

                var es = db.PZRPLAP.SqlQuery(@"select * FROM PZRPLAP ").ToList();

                foreach (var result in es)
                {


                    Models.AsignacionInstrumentoPersona item = new Models.AsignacionInstrumentoPersona();
                    //item.PZRPLAP_ID = result.PZRPLAP_ID;
                    item._PZRPLAP_SEQ_NUMBER = result.PZRPLAP_SEQ_NUMBER;
                    item._PZRPLAP_APPL_SEQ_NUMBER = result.PZRPLAP_APPL_SEQ_NUMBER;
                    item._PZRPLAP_CALR_CODE = result.PZRPLAP_CALR_CODE;
                    item._PZRPLAP_POST_CODE = result.PZRPLAP_POST_CODE;
                    item._PZRPLAP_DNCY_CODE = result.PZRPLAP_DNCY_CODE;
                    item._PZRPLAP_PFLE_CODE = result.PZRPLAP_PFLE_CODE;
                    item._PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                    item._PZRPLAP_DATA_ORIGIN = result.PZRPLAP_DATA_ORIGIN;
                    item._PZRPLAP_ACTIVITY_DATE = result.PZRPLAP_ACTIVITY_DATE;
                    item._PZRPLAP_PIDM = result.PZRPLAP_PIDM;
                    item._PZRPLAP_USER = result.PZRPLAP_USER;



                    re.Add(item);

                }
            }
            catch (Exception e)
            {
                return Ok("Error");
            }

            return Ok(re);

        }

        [HttpGet]
        [ResponseType(typeof(List<Models.AsignacionInstrumentoPersona>))]
        [Route("api/PZRPLAP/GetPZRPLAPCalendar/")]
        public IHttpActionResult GetPZRPLAPCalendar()
        {
            List<Models.AsignacionInstrumentoPersona> re = new List<Models.AsignacionInstrumentoPersona>();

            try
            {

                var es = db.PZRPLAP.SqlQuery(@"select * FROM PZRPLAP ").ToList();

                foreach (var result in es)
                {


                    Models.AsignacionInstrumentoPersona item = new Models.AsignacionInstrumentoPersona();
                    //item.PZRPLAP_ID = result.PZRPLAP_ID;
                    item._PZRPLAP_SEQ_NUMBER = result.PZRPLAP_SEQ_NUMBER;
                    item._PZRPLAP_APPL_SEQ_NUMBER = result.PZRPLAP_APPL_SEQ_NUMBER;
                    item._PZRPLAP_CALR_CODE = result.PZRPLAP_CALR_CODE;
                    item._PZRPLAP_POST_CODE = result.PZRPLAP_POST_CODE;
                    item._PZRPLAP_DNCY_CODE = result.PZRPLAP_DNCY_CODE;
                    item._PZRPLAP_PFLE_CODE = result.PZRPLAP_PFLE_CODE;
                    item._PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                    item._PZRPLAP_DATA_ORIGIN = result.PZRPLAP_DATA_ORIGIN;
                    item._PZRPLAP_ACTIVITY_DATE = result.PZRPLAP_ACTIVITY_DATE;
                    item._PZRPLAP_PIDM = result.PZRPLAP_PIDM;
                    item._PZRPLAP_USER = result.PZRPLAP_USER;



                    re.Add(item);

                }
            }
            catch (Exception e)
            {
                return Ok("Error");
            }

            return Ok(re);

        }

        /// <summary>
        /// 
        /// El servicio dado un PIDM DEVUELVE LOS DATOS DEL EVALUADO Y
        /// ADEMAS LOS DATOS DEL EVALUADOR CON EL INSTRUMENTO ASIGNADO
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(Models.AsignacionInstrumentoPersona))]
        [Route("api/PZRPLAP/GetPZRPLAPId/{id}")]
        public IHttpActionResult GetPZRPLAPId(String id)
        {
            List<Models.AsignacionInstrumentoPersona> re = new List<Models.AsignacionInstrumentoPersona>();

            try
            {

                db.PZRPLAP.Include(insCal => insCal.PZBAPPL.PZBCALR)
                           .Include(perPer => perPer.PZRASPE)
                           .Include(dep => dep.PZRASPE.PZRDYPT.PZVDNCY)
                           .Include(per => per.PZRASPE.PZBPRSO)
                           .Include(cal => cal.PZBAPPL)
                           .Include(pef => pef.PZRASPE.PZVPFLE)
                           .Include(pe2 => pe2.PZRASPE.PZBPRSO)
                           .Include(carDep => carDep.PZRASPE.PZRDYPT)
                           .Include(car => car.PZRASPE.PZRDYPT)
                           .Include(ca2 => ca2.PZRASPE.PZRDYPT.PZVPOST)
                           .Include(de2 => de2.PZRASPE.PZRDYPT);

                var es = db.PZRPLAP.SqlQuery(@"SELECT *
                                                FROM PZRPLAP,
                                                PZBCALR,
                                                PZVPFLE, 
                                                PZRASPE,
                                                PZBPRSO,
                                                PZRDYPT,
                                                PZVDNCY,
                                                PZVPOST,
                                                PZBAPPL,
                                                pzbpoll,
                                                PZVPOST PZVPOST1,
                                                PZVDNCY PZVDNCY1
                                                WHERE pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                AND   pzbappl.pzbappl_pfle_code = PZRPLAP.PZRPLAP_PFLE_CODE
                                                AND   PZRPLAP.PZRPLAP_PFLE_code = pzraspe.pzraspe_pfle_code
                                                AND   pzraspe.pzraspe_pfle_code = PZVPFLE.PZVPFLE_code
                                                AND   pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                and   pzbappl.pzbappl_calr_code = PZRPLAP.PZRPLAP_CALR_code 
                                                AND   pzbappl.pzbappl_calr_code = PZBCALR.PZBCALR_CODE
                                                AND   pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                AND   pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                AND   PZRDYPT.PZRDYPT_POST_CODE = PZVPOST.PZVPOST_CODE
                                                AND   PZVPOST.PZVPOST_CODE_sup = PZVPOST1.PZVPOST_CODE 
                                                AND   pzrplap.pzrplap_dncy_code= pzraspe.pzraspe_dncy_code
                                                AND   pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                AND   PZRDYPT.PZRDYPT_DNCY_CODE = PZVDNCY.PZVDNCY_CODE
                                                AND   PZVDNCY.PZVDNCY_CODE_sup = PZVDNCY1.PZVDNCY_CODE
                                                AND   PZRPLAP.PZRPLAP_PIDM = PZRASPE.PZRASPE_pidm 
                                                AND   PZRASPE.PZRASPE_pidm = pzbprso.pzbprso_pidm
                                                AND   PZBPRSO.PZBPRSO_PIDM='" + id + "' ").ToList();

                foreach (var result in es)
                {

                    //Datos ICPD
                    Models.AsignacionInstrumentoPersona item = new Models.AsignacionInstrumentoPersona();
                    item._PZRPLAP_SEQ_NUMBER = result.PZRPLAP_SEQ_NUMBER;
                    item._PZRPLAP_APPL_SEQ_NUMBER = result.PZRPLAP_APPL_SEQ_NUMBER;
                    item._PZRPLAP_CALR_CODE = result.PZRPLAP_CALR_CODE;
                    item._PZRPLAP_POST_CODE = result.PZRPLAP_POST_CODE;
                    item._PZRPLAP_DNCY_CODE = result.PZRPLAP_DNCY_CODE;
                    item._PZRPLAP_PFLE_CODE = result.PZRPLAP_PFLE_CODE;
                    item._PZRPLAP_POLL_CODE = result.PZRPLAP_POLL_CODE;
                    item._PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                    item._PZRPLAP_DATA_ORIGIN = result.PZRPLAP_DATA_ORIGIN;
                    item._PZRPLAP_ACTIVITY_DATE = result.PZRPLAP_ACTIVITY_DATE;
                    item._PZRPLAP_PIDM = result.PZRPLAP_PIDM;
                    item._PZRPLAP_USER = result.PZRPLAP_USER;
                    item._PZRPLAP_DATE = result.PZRPLAP_DATE;

                    //InstrumentoAplicado
                    Models.InstrumentoAplicable _insApl = new Models.InstrumentoAplicable();
                    _insApl._PZBAPPL_CALR_CODE = result.PZBAPPL.PZBAPPL_CALR_CODE;
                    _insApl._PZBAPPL_PFLE_CODE = result.PZBAPPL.PZBAPPL_PFLE_CODE;
                    _insApl._PZBAPPL_PRCNT = result.PZBAPPL.PZBAPPL_PRCNT;
                    _insApl._PZBAPPL_POLL_CODE = result.PZBAPPL.PZBAPPL_POLL_CODE;
                    _insApl._PZBAPPL_SEQ_NUMBER = result.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                    _insApl._PZBAPPL_USER = result.PZBAPPL.PZBAPPL_USER;
                    _insApl._PZBAPPL_DATA_ORIGIN = result.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                    _insApl._PZBAPPL_ACTIVITY_DATE = result.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                    item._PZBAPPL = _insApl;

                    //Datos Instrumento
                    Models.Instrumento ins = new Models.Instrumento();
                    ins._PZBPOLL_CODE = result.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                    ins._PZBPOLL_NAME = result.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                    ins._PZBPOLL_USER = result.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = result.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = result.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                    item._PZBAPPL._PZBPOLL = ins;

                    //Datos Calendario
                    Models.Calendario cal = new Models.Calendario();
                    // cal.PZBCALR_ID = result.PZBCALR.PZBCALR_ID;
                    cal._PZBCALR_NAME = result.PZBAPPL.PZBCALR.PZBCALR_NAME;
                    cal._PZBCALR_USER = result.PZBAPPL.PZBCALR.PZBCALR_USER;
                    cal._PZBCALR_TERM = result.PZBAPPL.PZBCALR.PZBCALR_TERM;
                    cal._PZBCALR_END_DATE = result.PZBAPPL.PZBCALR.PZBCALR_END_DATE;
                    cal._PZBCALR_INIT_DATE = result.PZBAPPL.PZBCALR.PZBCALR_INIT_DATE;
                    cal._PZBCALR_DATA_ORIGIN = result.PZBAPPL.PZBCALR.PZBCALR_DATA_ORIGIN;
                    cal._PZBCALR_ACTIVITY_DATE = result.PZBAPPL.PZBCALR.PZBCALR_ACTIVITY_DATE;
                    item._PZBAPPL._PZBCALR = cal;


                    // Datos PER_PER
                    Models.AsignacionCargoPersona ppcd = new Models.AsignacionCargoPersona();
                    ppcd._PZRASPE_PIDM = result.PZRASPE.PZRASPE_PIDM;
                    ppcd._PZRASPE_PFLE_CODE = result.PZRASPE.PZRASPE_PFLE_CODE;
                    ppcd._PZRASPE_POST_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_POST_CODE;
                    item._PZRPLAP_DNCY_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_DNCY_CODE;
                    //ppcd.PZRASPE_ID = result.PZRASPE.PZRASPE_ID;
                    ppcd._PZRASPE_END_DATE = result.PZRASPE.PZRASPE_END_DATE;
                    ppcd._PZRASPE_START_DATE = result.PZRASPE.PZRASPE_START_DATE;

                    item._PZRASPE = ppcd;

                    //Datos Perfil
                    Models.Perfil per = new Models.Perfil();
                    //per.PZVPFLE_ID = result.PZRASPE.PZVPFLE.PZVPFLE_ID;
                    per._PZVPFLE_CODE = result.PZRASPE.PZVPFLE.PZVPFLE_CODE;
                    per._PZVPFLE_NAME = result.PZRASPE.PZVPFLE.PZVPFLE_NAME;
                    per._PZVPFLE_ACTIVITY_DATE = result.PZRASPE.PZVPFLE.PZVPFLE_ACTIVITY_DATE;

                    item._PZRASPE._PZVPFLE = per;

                    //Datos Persona
                    Models.Persona per2 = new Models.Persona();
                    //per2.PZBPRSO_ID = result.PZRASPE.PZBPRSO.PZBPRSO_ID;
                    per2._PZBPRSO_PIDM = result.PZRASPE.PZBPRSO.PZBPRSO_PIDM;
                    per2._PZBPRSO_SITE = result.PZRASPE.PZBPRSO.PZBPRSO_SITE;
                    //per2.PZBPRSO_NAME = result.PZRASPE.PZBPRSO.PZBPRSO_NAME;
                    per2._PZBPRSO_USER = result.PZRASPE.PZBPRSO.PZBPRSO_USER;
                    per2._PZBPRSO_ACTIVE = result.PZRASPE.PZBPRSO.PZBPRSO_ACTIVE;
                    per2._PZBPRSO_PAYSHEET = result.PZRASPE.PZBPRSO.PZBPRSO_PAYSHEET;
                    per2._PZBPRSO_ACTIVE = result.PZRASPE.PZBPRSO.PZBPRSO_ACTIVE;
                    //per2.PZBPRSO_PZVPOST = result.PZRASPE.PZBPRSO.PZBPRSO_PZCRCOD;
                    item._PZRASPE._PZBPRSO = per2;

                    //Datos Cardep
                    Models.AsignacionCargoDependencia carDep = new Models.AsignacionCargoDependencia();
                    //carDep.PZRDYPT_ID = result.PZRASPE.PZRDYPT.PZRDYPT_ID;
                    carDep._PZRDYPT_POST_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_POST_CODE;
                    carDep._PZRDYPT_DNCY_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_DNCY_CODE;
                    carDep._PZRDYPT_USER = result.PZRASPE.PZRDYPT.PZRDYPT_USER;
                    carDep._PZRDYPT_DATA_ORIGIN = result.PZRASPE.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                    carDep._PZRDYPT_ACTIVITY_DATE = result.PZRASPE.PZRDYPT.PZRDYPT_ACTIVITY_DATE;
                    item._PZRASPE._PZRDYPT = carDep;

                    //Datos Cargo
                    Models.Cargo car = new Models.Cargo();
                    //car.PZVPOST_ID = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_ID;
                    car._PZVPOST_CODE = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_CODE;
                    car._PZVPOST_NAME = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_NAME;
                    car._PZVPOST_CODE_SUP = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                    car._PZVPOST_USER = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_USER;
                    car._PZVPOST_DESCRIPTION = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                    car._PZVPOST_ACTIVITY_DATE = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;
                    item._PZRASPE._PZRDYPT._PZVPOST = car;

                    //Datos Departamento
                    Models.Dependencia dep = new Models.Dependencia();
                    //dep.PZVDNCY_ID = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_ID;
                    dep._PZVDNCY_CODE = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_CODE;
                    dep._PZVDNCY_CODE_SUP = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_CODE_SUP;
                    dep._PZVDNCY_KEY = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
                    dep._PZVDNCY_SITE = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
                    dep._PZVDNCY_LEVEL = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
                    dep._PZVDNCY_NAME = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                    dep._PZVDNCY_USER = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_USER;
                    item._PZRASPE._PZRDYPT._PZVDNCY = dep;


                    re.Add(item);

                }
            }
            catch (Exception e)
            {
                return Ok("Error");
            }

            return Ok(re);

        }
        /// <summary>
        /// Devuelde el total de instrumentos pendientes y/o completados
        /// </summary>
        /// <param name="id_calendario"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(decimal))]
        [Route("api/PZRPLAP/GetPZRPLAPCalendarCount/{id_calendario}/{status}")]
        public IHttpActionResult GetPZRPLAPCalendarCount(String id_calendario, String status)
        {

            int total_registros;
            try
            {

                var count = (from PZRPLAP in db.PZRPLAP
                             where PZRPLAP.PZRPLAP_CALR_CODE == (String)id_calendario
                             where PZRPLAP.PZRPLAP_STATUS == status
                             select PZRPLAP).Count();

                total_registros = (int)count;



            }
            catch (Exception e)
            {
                return Ok("Error");
            }
            if (status.Equals("P"))
            {
                return Ok("Total de evaluaciones pendientes: " + total_registros);
            }
            else
            {
                return Ok("Total de evaluaciones completadas: " + total_registros);
            }
        }

        /// <summary>
        /// 
        /// BUSQUEDA INTEELIGENTE
        /// Personas con instrumentos PENDIENTdES 
        /// dado un calendario,departamento y una sede
        /// 
        /// </summary>
        /// <param name="id_calendario"></param>
        /// <param name="sede"></param>
        /// <param name="depto"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Persona>))]
        [Route("api/PZRPLAP/GetPZRPLAPCalendarBusqueda/{id_calendario}/{sede}/{depto}")]
        public IHttpActionResult GetPZRPLAPCalendarBusqueda(String id_calendario, String sede, String depto)
        {

            List<Models.Persona> re = new List<Models.Persona>();
            try
            {

                var es = db.PZBPRSO.SqlQuery(@" SELECT PZBPRSO.* 
                                                FROM PZRPLAP,
                                                PZBAPPL,
                                                PZBCALR,
                                                PZBPRSO,
                                                PZVDNCY,
                                                PZRDYPT,
                                                PZRASPE
                                                WHERE PZRPLAP.PZRPLAP_STATUS='P'
                                                    and PZRPLAP.PZRPLAP_CALR_CODE = PZBAPPL.PZBAPPL_CALR_CODE
                                                    AND PZBAPPL.PZBAPPL_CALR_CODE = PZBCALR.PZBCALR_CODE
                                                    AND PZRPLAP.PZRPLAP_pidm = PZRASPE.PZRASPE_PIDM
                                                    AND PZRASPE.PZRASPE_PIDM = pzbprso.pzbprso_pidm
                                                    and PZRASPE.PZRASPE_DNCY_CODE = pzrdypt.pzrdypt_dncy_code
                                                    AND pzrdypt.pzrdypt_dncy_code = PZVDNCY.PZVDNCY_CODE
                                                    and PZBCALR.PZBCALR_CODE='" + id_calendario + "' " +
                                                   "and pzbprso.pzbprso_site ='" + sede + "' " +
                                                   "and PZVDNCY.PZVDNCY_CODE= '" + depto + "' ")
                         .Union(db.PZBPRSO.SqlQuery(@"SELECT PZBPRSO.* 
                                                        FROM  PZRPLAP,
                                                              PZBAPPL,
                                                              PZBCALR,
                                                              PZBPRSO,
                                                              PZVDNCY,
                                                              PZRDYPT,
                                                              PZRASPE
                                                        WHERE PZRPLAP.PZRPLAP_STATUS='P'
                                                        and PZRPLAP.PZRPLAP_CALR_CODE = PZBAPPL.PZBAPPL_CALR_CODE
                                                        AND PZBAPPL.PZBAPPL_CALR_CODE = PZBCALR.PZBCALR_CODE
                                                        AND PZRPLAP.PZRPLAP_pidm = PZRASPE.PZRASPE_PIDM
                                                        AND PZRASPE.PZRASPE_PIDM = pzbprso.pzbprso_pidm
                                                        and pzbprso.pzbprso_pidm = PZRASPE.PZRASPE_pidm
                                                        and PZRASPE.PZRASPE_DNCY_CODE = pzrdypt.pzrdypt_dncy_code
                                                        AND pzrdypt.pzrdypt_dncy_code = PZVDNCY.PZVDNCY_CODE
                                                        and PZBCALR.PZBCALR_CODE='" + id_calendario + "' " +
                                                        "and pzbprso.pzbprso_name like '%" + depto + "%' "))
                         .Union(db.PZBPRSO.SqlQuery(@" SELECT PZBPRSO.* 
                                                        FROM PZRPLAP ,PZBCALR,PZBPRSO,PZVDNCY,PZRASPE 
                                                        WHERE PZRPLAP.PZRPLAP_STATUS='P'
                                                        and PZRPLAP.PZRPLAP_CALR_CODE = PZBAPPL.PZBAPPL_CALR_CODE
                                                        AND PZBAPPL.PZBAPPL_CALR_CODE = PZBCALR.PZBCALR_CODE
                                                        AND PZRASPE.PZRASPE_PIDM = pzbprso.pzbprso_pidm
                                                        and pzbprso.pzbprso_pidm = PZRASPE.PZRASPE_pidm
                                                        and PZRASPE.PZRASPE_DNCY_CODE = pzrdypt.pzrdypt_dncy_code
                                                        AND pzrdypt.pzrdypt_dncy_code = PZVDNCY.PZVDNCY_CODE
                                                        and PZBCALR.PZBCALR_CODE='" + id_calendario + "' " +
                                                        "and pzbprso.pzbprso_id LIKE '" + depto + "' ")).ToList();

                foreach (var x in es)
                {
                    Persona dato = new Persona();

                    //dato.PZBPRSO_ID = x.PZBPRSO_ID;
                    //dato.PZBPRSO_NAME = x.PZBPRSO_NAME;
                    dato._PZBPRSO_SITE = x.PZBPRSO_SITE;
                    dato._PZBPRSO_PAYSHEET = x.PZBPRSO_PAYSHEET;
                    dato._PZBPRSO_PIDM = x.PZBPRSO_PIDM;
                    //dato.PZBPRSO_PZVPOST = x.PZBPRSO_PZCRCOD;
                    dato._PZBPRSO_USER = x.PZBPRSO_USER;
                    dato._PZBPRSO_ACTIVE = x.PZBPRSO_ACTIVE;
                    dato._PZBPRSO_DATA_ORIGIN = x.PZBPRSO_DATA_ORIGIN;
                    dato._PZBPRSO_ACTIVITY_DATE = x.PZBPRSO_ACTIVITY_DATE;
                    re.Add(dato);
                }


            }
            catch (Exception e)
            {
                return Ok("Error. Intente Nuevamente");
            }
            return Ok(re);
        }

        /// <summary>
        /// 
        /// Personas con instrumentos PENDIENTES 
        /// dado un calendario,departamento y una sede
        /// 
        /// </summary>
        /// <param name="id_calendario"></param>
        /// <param name="sede"></param>
        /// <param name="depto"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Persona>))]
        [Route("api/PZRPLAP/GetPZRPLAPCalendarSiteDep/{id_calendario}/{sede}/{depto}")]
        public IHttpActionResult GetPZRPLAPCalendarSiteDep(String id_calendario, String sede, String depto)
        {

            List<Models.Persona> re = new List<Models.Persona>();
            try
            {
                //db.PZRPLAP
                var es = db.PZBPRSO.SqlQuery(@"  
                                                    SELECT PZBPRSO.*
                                                       FROM PZRPLAP ,PZBCALR,PZBPRSO,PZVDNCY,PZRASPE
                                                        WHERE PZRPLAP.PZRPLAP_STATUS='P'
                                                         and PZRPLAP.PZRPLAP_CALR_CODE= PZBCALR.PZBCALR_CODE
                                                         AND PZRPLAP.pzrplap_pidm =pzbprso.pzbprso_pidm
                                                         and pzbprso.pzbprso_pidm = PZRASPE.pzraspe_pidm
                                                         and PZRASPE.PZraspe_DNCY_CODE = PZVDNCY.PZVDNCY_CODE
                                                         and PZBCALR.PZBCALR_CODE= '" + id_calendario + "'" +
                                                         "and pzbprso.pzbprso_site = '" + sede + "'" +
                                                         "and PZVDNCY.PZVDNCY_CODE = '" + depto + "' ").ToList();

                foreach (var x in es)
                {
                    Persona dato = new Persona();

                    //dato.PZBPRSO_ID = x.PZBPRSO_ID;
                    //dato.PZBPRSO_NAME = x.PZBPRSO_NAME;
                    dato._PZBPRSO_SITE = x.PZBPRSO_SITE;
                    dato._PZBPRSO_PAYSHEET = x.PZBPRSO_PAYSHEET;
                    dato._PZBPRSO_PIDM = x.PZBPRSO_PIDM;
                    //dato.PZBPRSO_PZVPOST = x.PZBPRSO_PZCRCOD;
                    dato._PZBPRSO_USER = x.PZBPRSO_USER;
                    dato._PZBPRSO_ACTIVE = x.PZBPRSO_ACTIVE;
                    dato._PZBPRSO_DATA_ORIGIN = x.PZBPRSO_DATA_ORIGIN;
                    dato._PZBPRSO_ACTIVITY_DATE = x.PZBPRSO_ACTIVITY_DATE;
                    re.Add(dato);
                }


            }
            catch (Exception e)
            {
                return Ok("Error. Intente Nuevamente");
            }
            return Ok(re);
        }

        /// <summary>
        /// 
        /// Dado un calendario y una cedula lista las personas con evaluaciones pendientes
        /// 
        /// </summary>
        /// <param name="id_calendario"></param>
        /// <param name="ci"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Persona>))]
        [Route("api/PZRPLAP/GetPZRPLAPCalendarCI/{id_calendario}/{ci}/")]
        public IHttpActionResult GetPZRPLAPCalendarCI(String id_calendario, String ci)
        {

            List<Models.Persona> re = new List<Models.Persona>();
            try
            {
                //db.PZRPLAP
                var es = db.PZBPRSO.SqlQuery(@"  SELECT PZBPRSO.*
                                                       FROM PZRPLAP ,PZBCALR,PZBPRSO,
	                                                         PZVDNCY,PZRASPE,PZBAPPL,
	                                                         pzbpoll,PZVPOST,PZVPFLE,
	                                                         pzrdypt
                                                        WHERE PZRPLAP.PZRPLAP_STATUS='P'
                                                         and PZRPLAP.PZRPLAP_CALR_CODE = pzbappl.pzbappl_calr_code
                                                         AND pzbappl.pzbappl_calr_code = PZBCALR.PZBCALR_CODE
                                                         AND PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code 
                                                         AND pzbappl.pzbappl_poll_code = pzrplap.pzrplap_poll_code
                                                         and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                         AND pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number
                                                         AND pzbappl.pzbappl_pfle_code = pzrplap.pzrplap_pfle_code
                                                         AND pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         AND pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                         AND PZRPLAP.PZRPLAP_pidm = PZRASPE.PZRASPE_PIDM
                                                         and PZRASPE.PZRASPE_pidm = pzbprso.pzbprso_pidm 
                                                         AND pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and PZRASPE.PZRASPE_DNCY_CODE = pzrdypt.pzrdypt_dncy_code
                                                         AND pzrdypt.pzrdypt_dncy_code = PZVDNCY.PZVDNCY_CODE
                                                         AND pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         AND pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code = PZVPOST.PZVPOST_CODE
                                                         and PZBCALR.PZBCALR_CODE='" + id_calendario + "'" +
                                                         "and pzbprso.pzbprso_pidm ='" + ci + "'  ").ToList();

                foreach (var x in es)
                {
                    Persona dato = new Persona();

                    //dato.PZBPRSO_ID = x.PZBPRSO_ID;
                    //dato.PZBPRSO_NAME = x.PZBPRSO_NAME;
                    dato._PZBPRSO_SITE = x.PZBPRSO_SITE;
                    dato._PZBPRSO_PAYSHEET = x.PZBPRSO_PAYSHEET;
                    dato._PZBPRSO_PIDM = x.PZBPRSO_PIDM;
                    //dato.PZBPRSO_PZVPOST = x.PZBPRSO_PZCRCOD;
                    dato._PZBPRSO_USER = x.PZBPRSO_USER;
                    dato._PZBPRSO_ACTIVE = x.PZBPRSO_ACTIVE;
                    dato._PZBPRSO_DATA_ORIGIN = x.PZBPRSO_DATA_ORIGIN;
                    dato._PZBPRSO_ACTIVITY_DATE = x.PZBPRSO_ACTIVITY_DATE;
                    re.Add(dato);
                }


            }
            catch (Exception e)
            {
                return Ok("Error. Intente Nuevamente");
            }
            return Ok(re);
        }

        /// <summary>
        /// 
        /// Dado un calendario y una sede muestra la lista de personas con evaluaciones pendientes
        /// 
        /// </summary>
        /// <param name="id_calendario"></param>
        /// <param name="sede"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Persona>))]
        [Route("api/PZRPLAP/GetPZRPLAPCalendarSede/{id_calendario}/{sede}/")]
        public IHttpActionResult GetPZRPLAPCalendarSede(String id_calendario, String sede)
        {

            List<Models.Persona> re = new List<Models.Persona>();
            try
            {
                //db.PZRPLAP
                var es = db.PZBPRSO.SqlQuery(@"  SELECT PZBPRSO.*
                                                      FROM PZRPLAP ,PZBCALR,PZBPRSO,
	                                                         PZVDNCY,PZRASPE,PZBAPPL,
	                                                         pzbpoll,PZVPOST,PZVPFLE,
	                                                         pzrdypt
                                                        WHERE PZRPLAP.PZRPLAP_STATUS='P'
 
                                                         and PZRPLAP.PZRPLAP_CALR_CODE = pzbappl.pzbappl_calr_code
                                                         AND pzbappl.pzbappl_calr_code = PZBCALR.PZBCALR_CODE
 
                                                         AND PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code 
                                                         AND pzbappl.pzbappl_poll_code = pzrplap.pzrplap_poll_code
                                                         and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
 
                                                         AND pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number
 
                                                         AND pzbappl.pzbappl_pfle_code = pzrplap.pzrplap_pfle_code
                                                         AND pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         AND pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
 
                                                         AND PZRPLAP.PZRPLAP_pidm = PZRASPE.PZRASPE_PIDM
                                                         and PZRASPE.PZRASPE_pidm = pzbprso.pzbprso_pidm 
 
                                                         AND pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and PZRASPE.PZRASPE_DNCY_CODE = pzrdypt.pzrdypt_dncy_code
                                                         AND pzrdypt.pzrdypt_dncy_code = PZVDNCY.PZVDNCY_CODE
 
                                                         AND pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         AND pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code = PZVPOST.PZVPOST_CODE
                                                         and PZBCALR.PZBCALR_CODE='" + id_calendario + "'" +
                                                         "and pzbprso.pzbprso_site ='" + sede + "'  ").ToList();

                foreach (var x in es)
                {
                    Persona dato = new Persona();

                    //dato.PZBPRSO_ID = x.PZBPRSO_ID;
                    //dato.PZBPRSO_NAME = x.PZBPRSO_NAME;
                    dato._PZBPRSO_SITE = x.PZBPRSO_SITE;
                    dato._PZBPRSO_PAYSHEET = x.PZBPRSO_PAYSHEET;
                    dato._PZBPRSO_PIDM = x.PZBPRSO_PIDM;
                    //dato.PZBPRSO_PZVPOST = x.PZBPRSO_PZCRCOD;
                    dato._PZBPRSO_USER = x.PZBPRSO_USER;
                    dato._PZBPRSO_ACTIVE = x.PZBPRSO_ACTIVE;
                    dato._PZBPRSO_DATA_ORIGIN = x.PZBPRSO_DATA_ORIGIN;
                    dato._PZBPRSO_ACTIVITY_DATE = x.PZBPRSO_ACTIVITY_DATE;
                    re.Add(dato);
                }


            }
            catch (Exception e)
            {
                return Ok("Error. Intente Nuevamente");
            }
            return Ok(re);
        }



        /// <summary>
        /// 
        /// Personas con instrumentos PENDIENTES 
        /// dado un calendario,departamento y una sede
        ///  CON LIKE
        /// </summary>
        /// <param name="id_calendario"></param>
        /// <param name="sede"></param>
        /// <param name="depto"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Persona>))]
        [Route("api/PZRPLAP/GetPZRPLAPCalendarSiteDep2/{id_calendario}/{sede}/{depto}")]
        public IHttpActionResult GetPZRPLAPCalendarSiteDep2(String id_calendario, String sede, String depto)
        {

            List<Models.Persona> re = new List<Models.Persona>();
            try
            {
                db.PZRPLAP.Include(a => a.PZRASPE.PZBPRSO);
                //db.PZRPLAP(per => per.);
                var es = db.PZBPRSO.SqlQuery(@"  SELECT PZBPRSO.*
                                                        FROM PZRPLAP ,PZBCALR,PZBPRSO,
	                                                         PZVDNCY,PZRASPE,PZBAPPL,
	                                                         pzbpoll,PZVPOST,PZVPFLE,
	                                                         pzrdypt
                                                        WHERE PZRPLAP.PZRPLAP_STATUS='P'
 
                                                         and PZRPLAP.PZRPLAP_CALR_CODE = pzbappl.pzbappl_calr_code
                                                         AND pzbappl.pzbappl_calr_code = PZBCALR.PZBCALR_CODE
 
                                                         AND PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code 
                                                         AND pzbappl.pzbappl_poll_code = pzrplap.pzrplap_poll_code
                                                         and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
 
                                                         AND pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number
 
                                                         AND pzbappl.pzbappl_pfle_code = pzrplap.pzrplap_pfle_code
                                                         AND pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         AND pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
 
                                                         AND PZRPLAP.PZRPLAP_pidm = PZRASPE.PZRASPE_PIDM
                                                         and PZRASPE.PZRASPE_pidm = pzbprso.pzbprso_pidm 
 
                                                         AND pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and PZRASPE.PZRASPE_DNCY_CODE = pzrdypt.pzrdypt_dncy_code
                                                         AND pzrdypt.pzrdypt_dncy_code = PZVDNCY.PZVDNCY_CODE
 
                                                         AND pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         AND pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code = PZVPOST.PZVPOST_CODE
                                                         or PZBCALR.PZBCALR_name LIKE %'" + id_calendario + "%'" +
                                                         "or pzbprso.pzbprso_site LIKE'%" + sede + "%'" +
                                                         "or PZVDNCY.PZVDNCY_name LIKE'%" + depto + "%' ").ToList();

                foreach (var x in es)
                {
                    Persona dato = new Persona();

                    //dato.PZBPRSO_ID = x.PZBPRSO_ID;
                    //dato.PZBPRSO_NAME = x.PZBPRSO_NAME;
                    dato._PZBPRSO_SITE = x.PZBPRSO_SITE;
                    dato._PZBPRSO_PAYSHEET = x.PZBPRSO_PAYSHEET;
                    dato._PZBPRSO_PIDM = x.PZBPRSO_PIDM;
                    //dato.PZBPRSO_PZVPOST = x.PZBPRSO_PZCRCOD;
                    dato._PZBPRSO_USER = x.PZBPRSO_USER;
                    dato._PZBPRSO_ACTIVE = x.PZBPRSO_ACTIVE;
                    dato._PZBPRSO_DATA_ORIGIN = x.PZBPRSO_DATA_ORIGIN;
                    dato._PZBPRSO_ACTIVITY_DATE = x.PZBPRSO_ACTIVITY_DATE;
                    re.Add(dato);
                }


            }
            catch (Exception e)
            {
                return Ok("Error. Intente Nuevamente");
            }
            return Ok(re);
        }


        /// <summary>
        /// Dado un calendario devuelve el listado personas
        /// con evaluaciones pendientes
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Persona>))]
        [Route("api/PZRPLAP/GetPZRPLAPCalendarP/{id_calendario}")]
        public IHttpActionResult GetPZRPLAPCalendarP(String id_calendario)
        {

            List<Models.Persona> re = new List<Models.Persona>();
            try
            {
                // db.PZRPLAP.Include(a => a.PZRASPE.PZBPRSO);
                var es = db.PZBPRSO.SqlQuery(@"  SELECT PZBPRSO.*
                                                        FROM PZRPLAP ,PZBCALR,PZBPRSO,
	                                                         PZVDNCY,PZRASPE,PZBAPPL,
	                                                         pzbpoll,PZVPOST,PZVPFLE,
	                                                         pzrdypt
                                                        WHERE PZRPLAP.PZRPLAP_STATUS='P'
 
                                                         and PZRPLAP.PZRPLAP_CALR_CODE = pzbappl.pzbappl_calr_code
                                                         AND pzbappl.pzbappl_calr_code = PZBCALR.PZBCALR_CODE
 
                                                         AND PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code 
                                                         AND pzbappl.pzbappl_poll_code = pzrplap.pzrplap_poll_code
                                                         and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
 
                                                         AND pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number
 
                                                         AND pzbappl.pzbappl_pfle_code = pzrplap.pzrplap_pfle_code
                                                         AND pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         AND pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
 
                                                         AND PZRPLAP.PZRPLAP_pidm = PZRASPE.PZRASPE_PIDM
                                                         and PZRASPE.PZRASPE_pidm = pzbprso.pzbprso_pidm 
 
                                                         AND pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and PZRASPE.PZRASPE_DNCY_CODE = pzrdypt.pzrdypt_dncy_code
                                                         AND pzrdypt.pzrdypt_dncy_code = PZVDNCY.PZVDNCY_CODE
 
                                                         AND pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         AND pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code = PZVPOST.PZVPOST_CODE
                                                         and PZBCALR.PZBCALR_CODE = '" + id_calendario + "' ").ToList();

                foreach (var x in es)
                {
                    Persona dato = new Persona();

                    //dato.PZBPRSO_ID = x.PZBPRSO_ID;
                    //dato.PZBPRSO_NAME = x.PZBPRSO_NAME;
                    dato._PZBPRSO_SITE = x.PZBPRSO_SITE;
                    dato._PZBPRSO_PAYSHEET = x.PZBPRSO_PAYSHEET;
                    dato._PZBPRSO_PIDM = x.PZBPRSO_PIDM;
                    //dato.PZBPRSO_PZVPOST = x.PZBPRSO_PZCRCOD;
                    dato._PZBPRSO_USER = x.PZBPRSO_USER;
                    dato._PZBPRSO_ACTIVE = x.PZBPRSO_ACTIVE;
                    dato._PZBPRSO_DATA_ORIGIN = x.PZBPRSO_DATA_ORIGIN;
                    dato._PZBPRSO_ACTIVITY_DATE = x.PZBPRSO_ACTIVITY_DATE;
                    re.Add(dato);
                }


            }
            catch (Exception e)
            {
                return Ok("Error");
            }
            return Ok(re);
        }

        /// <summary>
        /// Dado un calendario devuelve el listado personas
        /// con evaluaciones completadas
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Persona>))]
        [Route("api/PZRPLAP/GetPZRPLAPCalendarC/{id_calendario}")]
        public IHttpActionResult GetPZRPLAPCalendarC(String id_calendario)
        {

            List<Models.Persona> re = new List<Models.Persona>();
            try
            {
                // db.PZBCALR.Include(inscal => inscal.PZRPLAP).Include(calfas => calfas.PZRCRPH);

                var es = db.PZBPRSO.SqlQuery(@"  SELECT PZBPRSO.*
                                                       FROM PZRPLAP ,PZBCALR,PZBPRSO
                                                       WHERE PZRPLAP.PZRPLAP_STATUS = 'C'
                                                         and PZRPLAP.PZRPLAP_CALR_CODE = pzbappl.pzbappl_calr_code
                                                         AND pzbappl.pzbappl_calr_code = PZBCALR.PZBCALR_CODE
 
                                                         AND PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code 
                                                         AND pzbappl.pzbappl_poll_code = pzrplap.pzrplap_poll_code
                                                         and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
 
                                                         AND pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number
 
                                                         AND pzbappl.pzbappl_pfle_code = pzrplap.pzrplap_pfle_code
                                                         AND pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         AND pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
 
                                                         AND PZRPLAP.PZRPLAP_pidm = PZRASPE.PZRASPE_PIDM
                                                         and PZRASPE.PZRASPE_pidm = pzbprso.pzbprso_pidm 
 
                                                         AND pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and PZRASPE.PZRASPE_DNCY_CODE = pzrdypt.pzrdypt_dncy_code
                                                         AND pzrdypt.pzrdypt_dncy_code = PZVDNCY.PZVDNCY_CODE
 
                                                         AND pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         AND pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code = PZVPOST.PZVPOST_CODE
                                                        and PZBCALR.PZBCALR_CODE = '" + id_calendario + "' ").ToList();

                foreach (var x in es)
                {
                    Persona dato = new Persona();

                    //dato.PZBPRSO_ID = x.PZBPRSO_ID;
                    //dato.PZBPRSO_NAME = x.PZBPRSO_NAME;
                    dato._PZBPRSO_SITE = x.PZBPRSO_SITE;
                    dato._PZBPRSO_PAYSHEET = x.PZBPRSO_PAYSHEET;
                    dato._PZBPRSO_PIDM = x.PZBPRSO_PIDM;
                    //dato.PZBPRSO_PZVPOST = x.PZBPRSO_PZCRCOD;
                    dato._PZBPRSO_USER = x.PZBPRSO_USER;
                    dato._PZBPRSO_ACTIVE = x.PZBPRSO_ACTIVE;
                    dato._PZBPRSO_DATA_ORIGIN = x.PZBPRSO_DATA_ORIGIN;
                    dato._PZBPRSO_ACTIVITY_DATE = x.PZBPRSO_ACTIVITY_DATE;
                    re.Add(dato);
                }


            }
            catch (Exception e)
            {
                return Ok("Error");
            }
            return Ok(re);
        }


        /// <summary>
        ///  El servicio devuelve el listado de evaluados con 
        ///  los datos de : cargo,instrumento,nombre 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.AsignacionInstrumentoPersona>))]
        [Route("api/PZRPLAP/GetPZRPLAPIdE/{id}")]
        public IHttpActionResult GetPZRPLAPIdE(String id)
        {
            List<Models.AsignacionCargoPersona> re = new List<Models.AsignacionCargoPersona>();

            try
            {

                db.PZRASPE.Include(p => p.PZRDYPT.PZVPOST ).
                           Include(ins => ins.PZRDYPT.PZVDNCY)
                           .Include(f => f.PZVPFLE)
                           .Include(so => so.PZBPRSO);

                var es = db.PZRASPE.SqlQuery(@"   SELECT *
                                                FROM PZBPRSO, PZVPOST,PZVDNCY,
                                                     PZRDYPT,PZRASPE,pzvpfle,
                                                     PZBPRSO pevaluado,
                                                     PZVPOST cevaluado,
                                                     PZVDNCY devaluado,
                                                     pzrdypt cdevaluado,
                                                     pzraspe revaluado,
                                                     pzvpfle pfevaluado
                                                WHERE pzbprso.pzbprso_pidm = pzraspe.pzraspe_pidm
                                                AND pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                AND pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                AND pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                AND pzrdypt.pzrdypt_dncy_code = pzvdncy.pzvdncy_code
                                                AND pzrdypt.pzrdypt_post_code = pzvpost.pzvpost_code
                                                and pzvpost.pzvpost_code = cevaluado.pzvpost_code_sup
                                                and cevaluado.pzvpost_code = cdevaluado.pzrdypt_post_code
                                                and devaluado.pzvdncy_code = cdevaluado.pzrdypt_dncy_code
                                                and cdevaluado.pzrdypt_post_code = revaluado.pzraspe_post_code
                                                and cdevaluado.pzrdypt_dncy_code = revaluado.pzraspe_dncy_code
                                                and revaluado.pzraspe_pidm = pevaluado.pzbprso_pidm
                                                and revaluado.pzraspe_pfle_code = pfevaluado.pzvpfle_code
                                                and PZBPRSO.pzbprso_pidm= '" + id + "' ").ToList();

                foreach (var result in es)
                {

       

                    // Datos PER_PER
                    Models.AsignacionCargoPersona ppcd = new Models.AsignacionCargoPersona();
                    ppcd._PZRASPE_PIDM = result.PZRASPE_PIDM;
                    ppcd._PZRASPE_PFLE_CODE = result.PZRASPE_PFLE_CODE;
                    ppcd._PZRASPE_DNCY_CODE = result.PZRASPE_DNCY_CODE;
                    //ppcd.PZRASPE_ID = result.PZRASPE.PZRASPE_ID;
                    ppcd._PZRASPE_POST_CODE = result.PZRASPE_POST_CODE;
                    ppcd._PZRASPE_END_DATE = result.PZRASPE_END_DATE;
                    ppcd._PZRASPE_START_DATE = result.PZRASPE_START_DATE;
                    
                    //Datos Perfil
                    Models.Perfil per = new Models.Perfil();
                    //per.PZVPFLE_ID = result.PZRASPE.PZVPFLE.PZVPFLE_ID;
                    per._PZVPFLE_CODE = result.PZVPFLE.PZVPFLE_CODE;
                    per._PZVPFLE_NAME = result.PZVPFLE.PZVPFLE_NAME;
                    per._PZVPFLE_ACTIVITY_DATE = result.PZVPFLE.PZVPFLE_ACTIVITY_DATE;
                    ppcd._PZVPFLE = per;

                    //Datos Persona
                    Models.Persona per2 = new Models.Persona();
                    // per2.PZBPRSO_ID = result.PZRASPE.PZBPRSO.PZBPRSO_ID;
                    per2._PZBPRSO_PIDM = result.PZBPRSO.PZBPRSO_PIDM;
                    per2._PZBPRSO_SITE = result.PZBPRSO.PZBPRSO_SITE;
                    //per2.PZBPRSO_NAME = result.PZRASPE.PZBPRSO.PZBPRSO_NAME;
                    per2._PZBPRSO_USER = result.PZBPRSO.PZBPRSO_USER;
                    per2._PZBPRSO_ACTIVE = result.PZBPRSO.PZBPRSO_ACTIVE;
                    per2._PZBPRSO_PAYSHEET = result.PZBPRSO.PZBPRSO_PAYSHEET;
                    //per2.PZBPRSO_PZVPOST = result.PZRASPE.PZBPRSO.PZBPRSO_PZCRCOD;
                    ppcd._PZBPRSO = per2;

                    //Datos Cardep
                    Models.AsignacionCargoDependencia carDep = new Models.AsignacionCargoDependencia();
                    //carDep.PZRDYPT_ID = result.PZRASPE.PZRDYPT.PZRDYPT_ID;
                    carDep._PZRDYPT_DNCY_CODE = result.PZRDYPT.PZRDYPT_DNCY_CODE;
                    carDep._PZRDYPT_POST_CODE = result.PZRDYPT.PZRDYPT_POST_CODE;
                    carDep._PZRDYPT_USER = result.PZRDYPT.PZRDYPT_USER;
                    carDep._PZRDYPT_DATA_ORIGIN = result.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                    carDep._PZRDYPT_ACTIVITY_DATE = result.PZRDYPT.PZRDYPT_ACTIVITY_DATE;
                    ppcd._PZRDYPT = carDep;

                    //Datos Cargo
                    Models.Cargo car = new Models.Cargo();
                    //car.PZVPOST_ID = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_ID;
                    car._PZVPOST_CODE = result.PZRDYPT.PZVPOST.PZVPOST_CODE;
                    car._PZVPOST_NAME = result.PZRDYPT.PZVPOST.PZVPOST_NAME;
                    car._PZVPOST_CODE_SUP = result.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                    car._PZVPOST_USER = result.PZRDYPT.PZVPOST.PZVPOST_USER;
                    car._PZVPOST_DESCRIPTION = result.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                    car._PZVPOST_ACTIVITY_DATE = result.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;
                    ppcd._PZRDYPT._PZVPOST = car;

                    //Datos Departamento
                    Models.Dependencia dep = new Models.Dependencia();
                    //dep.PZVDNCY_ID = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_ID;
                    dep._PZVDNCY_CODE = result.PZRDYPT.PZVDNCY.PZVDNCY_CODE;
                    dep._PZVDNCY_CODE_SUP = result.PZRDYPT.PZVDNCY.PZVDNCY_CODE_SUP;
                    dep._PZVDNCY_KEY = result.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
                    dep._PZVDNCY_SITE = result.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
                    dep._PZVDNCY_LEVEL = result.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
                    dep._PZVDNCY_NAME = result.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                    dep._PZVDNCY_USER = result.PZRDYPT.PZVDNCY.PZVDNCY_USER;
                    ppcd._PZRDYPT._PZVDNCY = dep;


                    re.Add(ppcd);

                }
            }
            catch (Exception e)
            {
                return BadRequest("Error");
            }

            return Ok(re);

        }
        
        /// <summary>
        /// 
        /// Devuelve dado el PIDM DEL EVALUADOR 
        /// los evaluados con insrtrumentos pendientes
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.AsignacionInstrumentoPersona>))]
        [Route("api/PZRPLAP/GetPZRPLAPEvaPen/{id}")]
        public IHttpActionResult GetPZRPLAPEvaPen(String id)
        {
            List<Models.AsignacionInstrumentoPersona> re = new List<Models.AsignacionInstrumentoPersona>();

            try
            {

                db.PZRPLAP.Include(insCal => insCal.PZRASPE.PZBPRSO).Include(ins => ins.PZBAPPL.PZBPOLL);

                var es = db.PZRPLAP.SqlQuery(@"   SELECT *
                                                   FROM 
                                                        --INSTRUMENTO
                                                        PZRPLAP,PZBPOLL,pzbappl,PZBCALR,pzvpfle,
                                                        --EVALUADO
                                                        PZVPOST,PZRDYPT,PZVDNCY,PZRASPE,PZBPRSO,
                                                        --EVALUADOR
                                                        PZVPOST POST1,PZVDNCY DNCY1,PZRDYPT DYPT1,
                                                        PZRASPE ASPE1,PZBPRSO PRSO1
                                                   WHERE PZRPLAP.PZRPLAP_STATUS='P'     
                                                     and pzvpost.pzvpost_code = pzrdypt.pzrdypt_post_code
                                                     AND pzrdypt.pzrdypt_post_code = pzraspe.pzraspe_post_code
                                                     AND pzvdncy.pzvdncy_code = pzrdypt.pzrdypt_dncy_code
                                                     AND pzrdypt.pzrdypt_dncy_code = pzraspe.pzraspe_dncy_code
                                                     AND pzraspe.pzraspe_pidm = PZBPRSO.PZBPRSO_PIDM
                                                     AND PZVDNCY.PZVDNCY_CODE_sup = DNCY1.PZVDNCY_CODE
                                                     AND PZVPOST.PZVPOST_CODE_sup = POST1.PZVPOST_CODE 
                                                     AND POST1.PZVPOST_CODE = dypt1.pzrdypt_post_code 
                                                     AND dypt1.pzrdypt_dncy_code = DNCY1.PZVDNCY_CODE
                                                     AND aspe1.pzraspe_post_code = dypt1.pzrdypt_post_code
                                                     AND aspe1.pzraspe_dncy_code = dypt1.pzrdypt_dncy_code
                                                     and aspe1.pzraspe_pidm= prso1.pzbprso_pidm
                                                     and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                     AND pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                     and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                     AND pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                     and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                     and pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                     and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                     and pzbappl.pzbappl_calr_code = pzbcalr.pzbcalr_code
                                                     and pzbappl.pzbappl_calr_code = pzrplap.pzrplap_calr_code
                                                     and pzbappl.pzbappl_pfle_code = pzvpfle.pzvpfle_code
                                                     and pzbappl.pzbappl_pfle_code = pzrplap.pzrplap_pfle_code 
                                                     and prso1.pzbprso_pidm= '"+id+"' ").ToList();

                foreach (var result in es)
                {

                    //Datos ICPD
                    Models.AsignacionInstrumentoPersona item = new Models.AsignacionInstrumentoPersona();
                    item._PZRPLAP_CALR_CODE = result.PZRPLAP_CALR_CODE;
                    item._PZRPLAP_POST_CODE = result.PZRPLAP_POST_CODE;
                    item._PZRPLAP_DNCY_CODE = result.PZRPLAP_DNCY_CODE;
                    item._PZRPLAP_PFLE_CODE = result.PZRPLAP_PFLE_CODE;
                    item._PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                    item._PZRPLAP_DATA_ORIGIN = result.PZRPLAP_DATA_ORIGIN;
                    item._PZRPLAP_ACTIVITY_DATE = result.PZRPLAP_ACTIVITY_DATE;
                    item._PZRPLAP_PIDM = result.PZRPLAP_PIDM;
                    item._PZRPLAP_USER = result.PZRPLAP_USER;
                    item._PZRPLAP_APPL_SEQ_NUMBER = result.PZRPLAP_APPL_SEQ_NUMBER;
                    item._PZRPLAP_SEQ_NUMBER = result.PZRPLAP_SEQ_NUMBER;


                    //Datos Instrumento aplicado
                    Models.InstrumentoAplicable _insApl = new InstrumentoAplicable();
                    _insApl._PZBAPPL_CALR_CODE = result.PZBAPPL.PZBAPPL_CALR_CODE;
                    _insApl._PZBAPPL_PFLE_CODE = result.PZBAPPL.PZBAPPL_PFLE_CODE;
                    _insApl._PZBAPPL_PRCNT = result.PZBAPPL.PZBAPPL_PRCNT;
                    _insApl._PZBAPPL_POLL_CODE = result.PZBAPPL.PZBAPPL_POLL_CODE;
                    _insApl._PZBAPPL_SEQ_NUMBER = result.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                    _insApl._PZBAPPL_USER = result.PZBAPPL.PZBAPPL_USER;
                    _insApl._PZBAPPL_DATA_ORIGIN = result.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                    _insApl._PZBAPPL_ACTIVITY_DATE = result.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                    item._PZBAPPL = _insApl;

                    //Datos Instrumento
                    Models.Instrumento ins = new Models.Instrumento();
                    //ins.PZBPOLL_ID = result.PZBCALR.PZBPOLL.PZBPOLL_ID;
                    // ins._PZBPOLL_CODE = result.PZBCALR.PZBAPPL.;
                    //ins.PZBPRSO_ID = result.PZVICOD.PZBPOLL.PZBPRSO_ID;
                    ins._PZBPOLL_NAME = result.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                    ins._PZBPOLL_USER = result.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = result.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = result.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                    item._PZBAPPL._PZBPOLL = ins;

                    //Datos Calendario
                    Models.Calendario cal = new Models.Calendario();
                    // cal.PZBCALR_ID = result.PZBCALR.PZBCALR_ID;
                    cal._PZBCALR_NAME = result.PZBAPPL.PZBCALR.PZBCALR_NAME;
                    cal._PZBCALR_USER = result.PZBAPPL.PZBCALR.PZBCALR_USER;
                    cal._PZBCALR_TERM = result.PZBAPPL.PZBCALR.PZBCALR_TERM;
                    cal._PZBCALR_END_DATE = result.PZBAPPL.PZBCALR.PZBCALR_END_DATE;
                    cal._PZBCALR_INIT_DATE = result.PZBAPPL.PZBCALR.PZBCALR_INIT_DATE;
                    cal._PZBCALR_DATA_ORIGIN = result.PZBAPPL.PZBCALR.PZBCALR_DATA_ORIGIN;
                    cal._PZBCALR_ACTIVITY_DATE = result.PZBAPPL.PZBCALR.PZBCALR_ACTIVITY_DATE;
                    item._PZBAPPL._PZBCALR = cal;



                    // Datos PER_PER
                    Models.AsignacionCargoPersona ppcd = new Models.AsignacionCargoPersona();
                    ppcd._PZRASPE_PIDM = result.PZRASPE.PZRASPE_PIDM;
                    ppcd._PZRASPE_PFLE_CODE = result.PZRASPE.PZRASPE_PFLE_CODE;
                    ppcd._PZRASPE_DNCY_CODE = result.PZRASPE.PZRASPE_DNCY_CODE;
                    //ppcd.PZRASPE_ID = result.PZRASPE.PZRASPE_ID;
                    ppcd._PZRASPE_POST_CODE = result.PZRASPE.PZRASPE_POST_CODE;
                    ppcd._PZRASPE_END_DATE = result.PZRASPE.PZRASPE_END_DATE;
                    ppcd._PZRASPE_START_DATE = result.PZRASPE.PZRASPE_START_DATE;
                    item._PZRASPE = ppcd;

                    //Datos Perfil
                    Models.Perfil per = new Models.Perfil();
                    //per.PZVPFLE_ID = result.PZRASPE.PZVPFLE.PZVPFLE_ID;
                    per._PZVPFLE_CODE = result.PZRASPE.PZVPFLE.PZVPFLE_CODE;
                    per._PZVPFLE_NAME = result.PZRASPE.PZVPFLE.PZVPFLE_NAME;
                    per._PZVPFLE_ACTIVITY_DATE = result.PZRASPE.PZVPFLE.PZVPFLE_ACTIVITY_DATE;
                    item._PZRASPE._PZVPFLE = per;

                    //Datos Persona
                    Models.Persona per2 = new Models.Persona();
                    // per2.PZBPRSO_ID = result.PZRASPE.PZBPRSO.PZBPRSO_ID;
                    per2._PZBPRSO_PIDM = result.PZRASPE.PZBPRSO.PZBPRSO_PIDM;
                    per2._PZBPRSO_SITE = result.PZRASPE.PZBPRSO.PZBPRSO_SITE;
                    //per2.PZBPRSO_NAME = result.PZRASPE.PZBPRSO.PZBPRSO_NAME;
                    per2._PZBPRSO_USER = result.PZRASPE.PZBPRSO.PZBPRSO_USER;
                    per2._PZBPRSO_ACTIVE = result.PZRASPE.PZBPRSO.PZBPRSO_ACTIVE;
                    per2._PZBPRSO_PAYSHEET = result.PZRASPE.PZBPRSO.PZBPRSO_PAYSHEET;
                    //per2.PZBPRSO_PZVPOST = result.PZRASPE.PZBPRSO.PZBPRSO_PZCRCOD;
                    item._PZRASPE._PZBPRSO = per2;

                    //Datos Cardep
                    Models.AsignacionCargoDependencia carDep = new Models.AsignacionCargoDependencia();
                    //carDep.PZRDYPT_ID = result.PZRASPE.PZRDYPT.PZRDYPT_ID;
                    carDep._PZRDYPT_DNCY_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_DNCY_CODE;
                    carDep._PZRDYPT_POST_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_POST_CODE;
                    carDep._PZRDYPT_USER = result.PZRASPE.PZRDYPT.PZRDYPT_USER;
                    carDep._PZRDYPT_DATA_ORIGIN = result.PZRASPE.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                    carDep._PZRDYPT_ACTIVITY_DATE = result.PZRASPE.PZRDYPT.PZRDYPT_ACTIVITY_DATE;
                    item._PZRASPE._PZRDYPT = carDep;

                    //Datos Cargo
                    Models.Cargo car = new Models.Cargo();
                    //car.PZVPOST_ID = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_ID;
                    car._PZVPOST_CODE = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_CODE;
                    car._PZVPOST_NAME = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_NAME;
                    car._PZVPOST_CODE_SUP = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                    car._PZVPOST_USER = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_USER;
                    car._PZVPOST_DESCRIPTION = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                    car._PZVPOST_ACTIVITY_DATE = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;
                    item._PZRASPE._PZRDYPT._PZVPOST = car;

                    //Datos Departamento
                    Models.Dependencia dep = new Models.Dependencia();
                    //dep.PZVDNCY_ID = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_ID;
                    dep._PZVDNCY_CODE = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_CODE;
                    dep._PZVDNCY_CODE_SUP = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_CODE_SUP;
                    dep._PZVDNCY_KEY = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
                    dep._PZVDNCY_SITE = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
                    dep._PZVDNCY_LEVEL = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
                    dep._PZVDNCY_NAME = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                    dep._PZVDNCY_USER = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_USER;
                    item._PZRASPE._PZRDYPT._PZVDNCY = dep;


                    re.Add(item);

                }
            }
            catch (Exception e)
            {
                return BadRequest("Error");
            }

            return Ok(re);

        }

        [HttpGet]
        [ResponseType(typeof(List<Models.AsignacionInstrumentoPersona>))]
        [Route("api/PZRPLAP/GetPZRPLAPEvaPenDep/{id}")]
        public IHttpActionResult GetPZRPLAPEvaPenDep(String id)
        {
            List<Models.AsignacionInstrumentoPersona> re = new List<Models.AsignacionInstrumentoPersona>();

            try
            {

                db.PZRPLAP.Include(insCal => insCal.PZRASPE.PZBPRSO).Include(ins => ins.PZBAPPL.PZBPOLL);

                var es = db.PZRPLAP.SqlQuery(@"   SELECT *
                                                   FROM 
                                                        --INSTRUMENTO
                                                        PZRPLAP,PZBPOLL,pzbappl,PZBCALR,pzvpfle,
                                                        --EVALUADO
                                                        PZVPOST,PZRDYPT,PZVDNCY,PZRASPE,PZBPRSO,
                                                        --EVALUADOR
                                                        PZVPOST POST1,PZVDNCY DNCY1,PZRDYPT DYPT1,
                                                        PZRASPE ASPE1,PZBPRSO PRSO1
                                                   WHERE PZRPLAP.PZRPLAP_STATUS IN ('P','T')     
                                                     and pzvpost.pzvpost_code = pzrdypt.pzrdypt_post_code
                                                     AND pzrdypt.pzrdypt_post_code = pzraspe.pzraspe_post_code
                                                     AND pzvdncy.pzvdncy_code = pzrdypt.pzrdypt_dncy_code
                                                     AND pzrdypt.pzrdypt_dncy_code = pzraspe.pzraspe_dncy_code
                                                     AND pzraspe.pzraspe_pidm = PZBPRSO.PZBPRSO_PIDM
                                                     AND PZVDNCY.PZVDNCY_CODE_sup = DNCY1.PZVDNCY_CODE
                                                     AND PZVPOST.PZVPOST_CODE_sup = POST1.PZVPOST_CODE 
                                                     AND POST1.PZVPOST_CODE = dypt1.pzrdypt_post_code 
                                                     AND dypt1.pzrdypt_dncy_code = DNCY1.PZVDNCY_CODE
                                                     AND aspe1.pzraspe_post_code = dypt1.pzrdypt_post_code
                                                     AND aspe1.pzraspe_dncy_code = dypt1.pzrdypt_dncy_code
                                                     and aspe1.pzraspe_pidm= prso1.pzbprso_pidm
                                                     and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                     AND pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                     and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                     AND pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                     and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                     and pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                     and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                     and pzbappl.pzbappl_calr_code = pzbcalr.pzbcalr_code
                                                     and pzbappl.pzbappl_calr_code = pzrplap.pzrplap_calr_code
                                                     and pzbappl.pzbappl_pfle_code = pzvpfle.pzvpfle_code
                                                     and pzbappl.pzbappl_pfle_code = pzrplap.pzrplap_pfle_code 
                                                     and prso1.pzbprso_pidm= '" + id + "' ").ToList();

                foreach (var result in es)
                {

                    //Datos ICPD
                    Models.AsignacionInstrumentoPersona item = new Models.AsignacionInstrumentoPersona();
                    item._PZRPLAP_CALR_CODE = result.PZRPLAP_CALR_CODE;
                    item._PZRPLAP_POST_CODE = result.PZRPLAP_POST_CODE;
                    item._PZRPLAP_DNCY_CODE = result.PZRPLAP_DNCY_CODE;
                    item._PZRPLAP_PFLE_CODE = result.PZRPLAP_PFLE_CODE;
                    item._PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                    item._PZRPLAP_DATA_ORIGIN = result.PZRPLAP_DATA_ORIGIN;
                    item._PZRPLAP_ACTIVITY_DATE = result.PZRPLAP_ACTIVITY_DATE;
                    item._PZRPLAP_PIDM = result.PZRPLAP_PIDM;
                    item._PZRPLAP_USER = result.PZRPLAP_USER;
                    item._PZRPLAP_APPL_SEQ_NUMBER = result.PZRPLAP_APPL_SEQ_NUMBER;
                    item._PZRPLAP_SEQ_NUMBER = result.PZRPLAP_SEQ_NUMBER;


                    //Datos Instrumento aplicado
                    Models.InstrumentoAplicable _insApl = new InstrumentoAplicable();
                    _insApl._PZBAPPL_CALR_CODE = result.PZBAPPL.PZBAPPL_CALR_CODE;
                    _insApl._PZBAPPL_PFLE_CODE = result.PZBAPPL.PZBAPPL_PFLE_CODE;
                    _insApl._PZBAPPL_PRCNT = result.PZBAPPL.PZBAPPL_PRCNT;
                    _insApl._PZBAPPL_POLL_CODE = result.PZBAPPL.PZBAPPL_POLL_CODE;
                    _insApl._PZBAPPL_SEQ_NUMBER = result.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                    _insApl._PZBAPPL_USER = result.PZBAPPL.PZBAPPL_USER;
                    _insApl._PZBAPPL_DATA_ORIGIN = result.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                    _insApl._PZBAPPL_ACTIVITY_DATE = result.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                    item._PZBAPPL = _insApl;

                    //Datos Instrumento
                    Models.Instrumento ins = new Models.Instrumento();
                    //ins.PZBPOLL_ID = result.PZBCALR.PZBPOLL.PZBPOLL_ID;
                    // ins._PZBPOLL_CODE = result.PZBCALR.PZBAPPL.;
                    //ins.PZBPRSO_ID = result.PZVICOD.PZBPOLL.PZBPRSO_ID;
                    ins._PZBPOLL_NAME = result.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                    ins._PZBPOLL_USER = result.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = result.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = result.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                    item._PZBAPPL._PZBPOLL = ins;

                    //Datos Calendario
                    Models.Calendario cal = new Models.Calendario();
                    // cal.PZBCALR_ID = result.PZBCALR.PZBCALR_ID;
                    cal._PZBCALR_NAME = result.PZBAPPL.PZBCALR.PZBCALR_NAME;
                    cal._PZBCALR_USER = result.PZBAPPL.PZBCALR.PZBCALR_USER;
                    cal._PZBCALR_TERM = result.PZBAPPL.PZBCALR.PZBCALR_TERM;
                    cal._PZBCALR_END_DATE = result.PZBAPPL.PZBCALR.PZBCALR_END_DATE;
                    cal._PZBCALR_INIT_DATE = result.PZBAPPL.PZBCALR.PZBCALR_INIT_DATE;
                    cal._PZBCALR_DATA_ORIGIN = result.PZBAPPL.PZBCALR.PZBCALR_DATA_ORIGIN;
                    cal._PZBCALR_ACTIVITY_DATE = result.PZBAPPL.PZBCALR.PZBCALR_ACTIVITY_DATE;
                    item._PZBAPPL._PZBCALR = cal;



                    // Datos PER_PER
                    Models.AsignacionCargoPersona ppcd = new Models.AsignacionCargoPersona();
                    ppcd._PZRASPE_PIDM = result.PZRASPE.PZRASPE_PIDM;
                    ppcd._PZRASPE_PFLE_CODE = result.PZRASPE.PZRASPE_PFLE_CODE;
                    ppcd._PZRASPE_DNCY_CODE = result.PZRASPE.PZRASPE_DNCY_CODE;
                    //ppcd.PZRASPE_ID = result.PZRASPE.PZRASPE_ID;
                    ppcd._PZRASPE_POST_CODE = result.PZRASPE.PZRASPE_POST_CODE;
                    ppcd._PZRASPE_END_DATE = result.PZRASPE.PZRASPE_END_DATE;
                    ppcd._PZRASPE_START_DATE = result.PZRASPE.PZRASPE_START_DATE;
                    item._PZRASPE = ppcd;

                    //Datos Perfil
                    Models.Perfil per = new Models.Perfil();
                    //per.PZVPFLE_ID = result.PZRASPE.PZVPFLE.PZVPFLE_ID;
                    per._PZVPFLE_CODE = result.PZRASPE.PZVPFLE.PZVPFLE_CODE;
                    per._PZVPFLE_NAME = result.PZRASPE.PZVPFLE.PZVPFLE_NAME;
                    per._PZVPFLE_ACTIVITY_DATE = result.PZRASPE.PZVPFLE.PZVPFLE_ACTIVITY_DATE;
                    item._PZRASPE._PZVPFLE = per;

                    //Datos Persona
                    Models.Persona per2 = new Models.Persona();
                    // per2.PZBPRSO_ID = result.PZRASPE.PZBPRSO.PZBPRSO_ID;
                    per2._PZBPRSO_PIDM = result.PZRASPE.PZBPRSO.PZBPRSO_PIDM;
                    per2._PZBPRSO_SITE = result.PZRASPE.PZBPRSO.PZBPRSO_SITE;
                    //per2.PZBPRSO_NAME = result.PZRASPE.PZBPRSO.PZBPRSO_NAME;
                    per2._PZBPRSO_USER = result.PZRASPE.PZBPRSO.PZBPRSO_USER;
                    per2._PZBPRSO_ACTIVE = result.PZRASPE.PZBPRSO.PZBPRSO_ACTIVE;
                    per2._PZBPRSO_PAYSHEET = result.PZRASPE.PZBPRSO.PZBPRSO_PAYSHEET;
                    //per2.PZBPRSO_PZVPOST = result.PZRASPE.PZBPRSO.PZBPRSO_PZCRCOD;
                    item._PZRASPE._PZBPRSO = per2;

                    //Datos Cardep
                    Models.AsignacionCargoDependencia carDep = new Models.AsignacionCargoDependencia();
                    //carDep.PZRDYPT_ID = result.PZRASPE.PZRDYPT.PZRDYPT_ID;
                    carDep._PZRDYPT_DNCY_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_DNCY_CODE;
                    carDep._PZRDYPT_POST_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_POST_CODE;
                    carDep._PZRDYPT_USER = result.PZRASPE.PZRDYPT.PZRDYPT_USER;
                    carDep._PZRDYPT_DATA_ORIGIN = result.PZRASPE.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                    carDep._PZRDYPT_ACTIVITY_DATE = result.PZRASPE.PZRDYPT.PZRDYPT_ACTIVITY_DATE;
                    item._PZRASPE._PZRDYPT = carDep;

                    //Datos Cargo
                    Models.Cargo car = new Models.Cargo();
                    //car.PZVPOST_ID = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_ID;
                    car._PZVPOST_CODE = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_CODE;
                    car._PZVPOST_NAME = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_NAME;
                    car._PZVPOST_CODE_SUP = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                    car._PZVPOST_USER = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_USER;
                    car._PZVPOST_DESCRIPTION = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                    car._PZVPOST_ACTIVITY_DATE = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;
                    item._PZRASPE._PZRDYPT._PZVPOST = car;

                    //Datos Departamento
                    Models.Dependencia dep = new Models.Dependencia();
                    //dep.PZVDNCY_ID = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_ID;
                    dep._PZVDNCY_CODE = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_CODE;
                    dep._PZVDNCY_CODE_SUP = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_CODE_SUP;
                    dep._PZVDNCY_KEY = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
                    dep._PZVDNCY_SITE = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
                    dep._PZVDNCY_LEVEL = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
                    dep._PZVDNCY_NAME = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                    dep._PZVDNCY_USER = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_USER;
                    item._PZRASPE._PZRDYPT._PZVDNCY = dep;


                    re.Add(item);

                }
            }
            catch (Exception e)
            {
                return BadRequest("Error");
            }

            return Ok(re);

        }

        /// <summary>
        ///  
        /// Carga los instrumentos con las preguntas que tienen asignadas
        ///  
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.AsignacionInstrumentoPersona>))]
        [Route("api/PZRPLAP/GetPZRPLAPIdI/{id}")]
        public IHttpActionResult GetPZRPLAPIdI(String id)
        {
            List<Models.AsignacionInstrumentoPregunta> re = new List<Models.AsignacionInstrumentoPregunta>();

            try
            {

                db.PZBPLQS
                             .Include(a => a.PZBAREA)
                             .Include(y => y.PZBCRIT)
                             .Include(e => e.PZBQSTN)
                             .Include(p => p.PZBAPPL.PZBPOLL)
                             .Include(r => r.PZVTYAW)
                             .ToList();


                var es = db.PZBPLQS.SqlQuery(@"SELECT *
                                                 FROM --INSTRUMETNO   
                                                      PZBPOLL,pzbappl,
                                                      pzbplqs,pzbqstn,
                                                      pzrplap,PZBCALR,
                                                      PZRASPE,PZBPRSO,
                                                      PZVPFLE,pzrdypt,
                                                      pzvpost,pzvdncy,
                                                      pzbarea,pzbcrit

                                                 WHERE  PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code
                                                         AND pzbappl.pzbappl_poll_code = pzbplqs.pzbplqs_appl_poll_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzbappl.pzbappl_seq_number = pzbplqs.pzbplqs_appl_seq_number
                                                         and pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number  
                                                         and pzbcalr.pzbcalr_code = pzbappl.pzbappl_calr_code
                                                         and pzbappl.pzbappl_calr_code = pzbplqs.pzbplqs_appl_calr_code
                                                         and pzvpfle.pzvpfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzbqstn.pzbqstn_code = pzbplqs.pzbplqs_qstn_code
                                                         and pzbcrit.pzbcrit_code = pzbplqs.pzbplqs_crit_code
                                                         and pzbarea.pzbarea_code = pzbplqs.pzbplqs_area_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                         and pzrplap.pzrplap_pfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         and pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                         and pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         and pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code =  pzvpost.pzvpost_code
                                                         and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                         and pzrdypt.pzrdypt_dncy_code =  pzvdncy.pzvdncy_code
                                                         and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                         and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm 
                                                         and pzbprso.pzbprso_pidm=" + id + " ").ToList();

                foreach (var result in es)
                {
                    //Datos de I_c


                    Models.AsignacionInstrumentoPregunta i_c = new Models.AsignacionInstrumentoPregunta();
                    i_c._PZBPLQS_SEQ_NUMBER = result.PZBPLQS_SEQ_NUMBER;
                    i_c._PZBPLQS_APPL_SEQ_NUMBER = result.PZBPLQS_APPL_SEQ_NUMBER;
                    i_c._PZBPLQS_USER = result.PZBPLQS_USER;
                    i_c._PZBPLQS_DATA_ORIGIN = result.PZBPLQS_DATA_ORIGIN;
                    i_c._PZBPLQS_ACTIVITY_DATE = result.PZBPLQS_ACTIVITY_DATE;
                    i_c._PZBPLQS_QSTN_CODE = result.PZBPLQS_QSTN_CODE;
                    i_c._PZBPLQS_POLL_CODE = result.PZBPLQS_POLL_CODE;
                    i_c._PZBPLQS_AREA_CODE = result.PZBPLQS_AREA_CODE;
                    i_c._PZBPLQS_CRIT_CODE = result.PZBPLQS_CRIT_CODE;
                    i_c._PZBPLQS_MAX_OVAL = result.PZBPLQS_MAX_OVAL;
                    i_c._PZBPLQS_MAX_VALUE = result.PZBPLQS_MAX_VALUE;
                    i_c._PZBPLQS_MIN_OVAL = result.PZBPLQS_MIN_OVAL;
                    i_c._PZBPLQS_MIN_VALUE = result.PZBPLQS_MIN_VALUE;
                    i_c._PZBPLQS_ORDER = result.PZBPLQS_ORDER;
                    i_c._PZBPLQS_STATUS = result.PZBPLQS_STATUS;


                    //Datos de la pregunta
                    Models.Pregunta pre = new Models.Pregunta();
                    pre._PZBQSTN_CODE = result.PZBQSTN.PZBQSTN_CODE;
                    pre._PZBQSTN_USER = result.PZBQSTN.PZBQSTN_USER;
                    pre._PZBQSTN_DESCRIPTION = result.PZBQSTN.PZBQSTN_DESCRIPTION;
                    pre._PZBQSTN_ACTIVITY_DATE = result.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                    pre._PZBQSTN_DATA_ORIGIN = result.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                    pre._PZBQSTN_NAME = result.PZBQSTN.PZBQSTN_NAME;
                    i_c._PZBQSTN = pre;


                    //DATOS DEL criterio de Evaluacion
                    Models.Criterio _cri = new Models.Criterio();
                    _cri._PZBCRIT_CODE = result.PZBCRIT.PZBCRIT_CODE;
                    _cri._PZBCRIT_USER = result.PZBCRIT.PZBCRIT_USER;
                    _cri._PZBCRIT_DESCRIPTION = result.PZBCRIT.PZBCRIT_DESCRIPTION;
                    _cri._PZBCRIT_DATA_ORIGIN = result.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                    _cri._PZBCRIT_ACTIVITY_DATE = result.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                    i_c._PZBCRIT = _cri;

                    //Datos del area a la cual pertenece la pregunta
                    Models.Area _area = new Models.Area();
                    _area._PZBAREA_CODE = result.PZBAREA.PZBAREA_CODE;
                    _area._PZBAREA_USER = result.PZBAREA.PZBAREA_USER;
                    _area._PZBAREA_DESCRIPTION = result.PZBAREA.PZBAREA_DESCRIPTION;
                    _area._PZBAREA_DATA_ORIGIN = result.PZBAREA.PZBAREA_DATA_ORIGIN;
                    _area._PZBAREA_ACTIVITY_DATE = result.PZBAREA.PZBAREA_ACTIVITY_DATE;
                    i_c._PZBAREA = _area;


                    //DATOS instrumento aplicable
                    Models.InstrumentoAplicable _insApl = new Models.InstrumentoAplicable();
                    _insApl._PZBAPPL_CALR_CODE = result.PZBAPPL.PZBAPPL_CALR_CODE;
                    _insApl._PZBAPPL_PFLE_CODE = result.PZBAPPL.PZBAPPL_PFLE_CODE;
                    _insApl._PZBAPPL_PRCNT = result.PZBAPPL.PZBAPPL_PRCNT;
                    _insApl._PZBAPPL_POLL_CODE = result.PZBAPPL.PZBAPPL_POLL_CODE;
                    _insApl._PZBAPPL_SEQ_NUMBER = result.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                    _insApl._PZBAPPL_USER = result.PZBAPPL.PZBAPPL_USER;
                    _insApl._PZBAPPL_DATA_ORIGIN = result.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                    _insApl._PZBAPPL_ACTIVITY_DATE = result.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                    i_c._PZBAPPL = _insApl;

                    //Datos Instrumento
                    Models.Instrumento ins = new Models.Instrumento();
                    ins._PZBPOLL_CODE = result.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                    ins._PZBPOLL_NAME = result.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                    ins._PZBPOLL_USER = result.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = result.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = result.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                    i_c._PZBAPPL._PZBPOLL = ins;



                    re.Add(i_c);

                }
            }
            catch (Exception e)
            {
                return BadRequest("Error");
            }

            return Ok(re);

        }

        /// <summary>
        /// 
        /// Dada un instrumento Y PERSONA  carga las preguntas y criterios
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<AsignacionInstrumentoPersona>))]
        [Route("api/PZRPLAP/GetPZRPLAPIdC/{id}/{idi}/{idc}")]
        public IHttpActionResult GetPZRPLAPIdC(String id, String idi, String idc)
        {
            List<Models.AsignacionInstrumentoPregunta> re = new List<Models.AsignacionInstrumentoPregunta>();

            try
            {
                db.PZBPLQS
                             .Include(a => a.PZBAREA)
                             .Include(y => y.PZBCRIT)
                             .Include(e => e.PZBQSTN)
                             .Include(p => p.PZBAPPL.PZBPOLL)
                             .Include(r => r.PZVTYAW)
                             .ToList();

                var es = db.PZBPLQS.SqlQuery(@"SELECT *
                                                 FROM --INSTRUMETNO   
                                                      PZBPOLL,pzbappl,
                                                      pzbplqs,pzbqstn,
                                                      pzrplap,PZBCALR,
                                                      PZRASPE,PZBPRSO,
                                                      PZVPFLE,pzrdypt,
                                                      pzvpost,pzvdncy,
                                                      pzbarea,pzbcrit

                                                 WHERE  PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code
                                                         AND pzbappl.pzbappl_poll_code = pzbplqs.pzbplqs_poll_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzbappl.pzbappl_seq_number = pzbplqs.pzbplqs_appl_seq_number
                                                         and pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number  
                                                         and pzbcalr.pzbcalr_code = pzbappl.pzbappl_calr_code
                                                         and pzbappl.pzbappl_calr_code = pzbplqs.pzbplqs_calr_code
                                                         and pzvpfle.pzvpfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzbqstn.pzbqstn_code = pzbplqs.pzbplqs_qstn_code
                                                         and PZBPLQS_STATUS in ('A','E')
                                                         and pzbcrit.pzbcrit_code = pzbplqs.pzbplqs_crit_code
                                                         and pzbarea.pzbarea_code = pzbplqs.pzbplqs_area_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                         and pzrplap.pzrplap_pfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         and pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                         and pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         and pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code =  pzvpost.pzvpost_code
                                                         and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                         and pzrdypt.pzrdypt_dncy_code =  pzvdncy.pzvdncy_code
                                                         and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                         and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm
                                                         and pzbprso.pzbprso_pidm='" + id+"'"+
                                                         "and PZBPOLL.PZBPOLL_code='"+idi+"'"+
                                                         "and pzbcalr.pzbcalr_code ='"+idc+"'"+
                                                         " ORDER BY PZBPLQS.PZBPLQS_ORDER , pzbplqs.pzbplqs_area_code"
                                                         ).ToList();
               
                foreach (var result in es)
                {
                    //Datos de I_c


                    Models.AsignacionInstrumentoPregunta i_c = new Models.AsignacionInstrumentoPregunta();
                    i_c._PZBPLQS_SEQ_NUMBER = result.PZBPLQS_SEQ_NUMBER;
                    i_c._PZBPLQS_APPL_SEQ_NUMBER = result.PZBPLQS_APPL_SEQ_NUMBER;
                    i_c._PZBPLQS_USER = result.PZBPLQS_USER;
                    i_c._PZBPLQS_DATA_ORIGIN = result.PZBPLQS_DATA_ORIGIN;
                    i_c._PZBPLQS_ACTIVITY_DATE = result.PZBPLQS_ACTIVITY_DATE;
                    i_c._PZBPLQS_QSTN_CODE = result.PZBPLQS_QSTN_CODE;
                    i_c._PZBPLQS_POLL_CODE = result.PZBPLQS_POLL_CODE;
                    i_c._PZBPLQS_AREA_CODE = result.PZBPLQS_AREA_CODE;
                    i_c._PZBPLQS_TYAW_CODE = result.PZBPLQS_TYAW_CODE;
                    i_c._PZBPLQS_CRIT_CODE = result.PZBPLQS_CRIT_CODE;
                    i_c._PZBPLQS_MAX_OVAL = result.PZBPLQS_MAX_OVAL;
                    i_c._PZBPLQS_MAX_VALUE = result.PZBPLQS_MAX_VALUE;
                    i_c._PZBPLQS_MIN_OVAL = result.PZBPLQS_MIN_OVAL;
                    i_c._PZBPLQS_MIN_VALUE = result.PZBPLQS_MIN_VALUE;
                    i_c._PZBPLQS_ORDER = result.PZBPLQS_ORDER;
                    i_c._PZBPLQS_STATUS = result.PZBPLQS_STATUS;


                    //Datos de la pregunta
                    Models.Pregunta pre = new Models.Pregunta();
                    pre._PZBQSTN_CODE = result.PZBQSTN.PZBQSTN_CODE;
                    pre._PZBQSTN_USER = result.PZBQSTN.PZBQSTN_USER;
                    pre._PZBQSTN_DESCRIPTION = result.PZBQSTN.PZBQSTN_DESCRIPTION;
                    pre._PZBQSTN_ACTIVITY_DATE = result.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                    pre._PZBQSTN_DATA_ORIGIN = result.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                    pre._PZBQSTN_NAME = result.PZBQSTN.PZBQSTN_NAME;
                    i_c._PZBQSTN = pre;


                    //DATOS DEL criterio de Evaluacion
                    Models.Criterio _cri = new Models.Criterio();
                    _cri._PZBCRIT_CODE = result.PZBCRIT.PZBCRIT_CODE;
                    _cri._PZBCRIT_USER = result.PZBCRIT.PZBCRIT_USER;
                    _cri._PZBCRIT_DESCRIPTION = result.PZBCRIT.PZBCRIT_DESCRIPTION;
                    _cri._PZBCRIT_DATA_ORIGIN = result.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                    _cri._PZBCRIT_ACTIVITY_DATE = result.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                    i_c._PZBCRIT = _cri;

                    //Datos del area a la cual pertenece la pregunta
                    Models.Area _area = new Models.Area();
                    _area._PZBAREA_CODE = result.PZBAREA.PZBAREA_CODE;
                    _area._PZBAREA_USER = result.PZBAREA.PZBAREA_USER;
                    _area._PZBAREA_NAME = result.PZBAREA.PZBAREA_NAME;
                    _area._PZBAREA_DESCRIPTION = result.PZBAREA.PZBAREA_DESCRIPTION;
                    _area._PZBAREA_DATA_ORIGIN = result.PZBAREA.PZBAREA_DATA_ORIGIN;
                    _area._PZBAREA_ACTIVITY_DATE = result.PZBAREA.PZBAREA_ACTIVITY_DATE;
                    i_c._PZBAREA = _area;


                    //DATOS instrumento aplicable
                    Models.InstrumentoAplicable _insApl = new Models.InstrumentoAplicable();
                    _insApl._PZBAPPL_CALR_CODE = result.PZBAPPL.PZBAPPL_CALR_CODE;
                    _insApl._PZBAPPL_PFLE_CODE = result.PZBAPPL.PZBAPPL_PFLE_CODE;
                    _insApl._PZBAPPL_PRCNT = result.PZBAPPL.PZBAPPL_PRCNT;
                    _insApl._PZBAPPL_POLL_CODE = result.PZBAPPL.PZBAPPL_POLL_CODE;
                    _insApl._PZBAPPL_SEQ_NUMBER = result.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                    _insApl._PZBAPPL_USER = result.PZBAPPL.PZBAPPL_USER;
                    _insApl._PZBAPPL_DATA_ORIGIN = result.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                    _insApl._PZBAPPL_ACTIVITY_DATE = result.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                    i_c._PZBAPPL = _insApl;

                    //Datos Instrumento
                    Models.Instrumento ins = new Models.Instrumento();
                    ins._PZBPOLL_CODE = result.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                    ins._PZBPOLL_NAME = result.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                    ins._PZBPOLL_USER = result.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = result.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = result.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                    i_c._PZBAPPL._PZBPOLL = ins;



                    re.Add(i_c);
                    
                }
            }
            catch (Exception e)
            {
                return BadRequest("Error +" + idi);
            }

            return Ok(re);

        }


        /// <summary>
        ///  Dado el id del Usuario y el id del instrumento devuelve 
        ///  el instrumento con las preguntas que lo componen
        /// </summary>
        /// <param name="ide"></param>
        /// <param name="idi"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.AsignacionInstrumentoPersona>))]
        [Route("api/PZRPLAP/GetPZRPLAPIdI/{ide}/{idi}/{idc}")]
        public IHttpActionResult GetPZRPLAPIdI(String ide, String idi, String idc)
        {
            List<Models.AsignacionInstrumentoPregunta> re = new List<Models.AsignacionInstrumentoPregunta>();

            try
            {
                db.PZBPLQS
                               .Include(a => a.PZBAREA)
                               .Include(y => y.PZBCRIT)
                               .Include(e => e.PZBQSTN)
                               .Include(p => p.PZBAPPL.PZBPOLL)
                               .Include(r => r.PZVTYAW)
                               .ToList();

                var es = db.PZBPLQS.SqlQuery(@" SELECT *
                                                 FROM --INSTRUMETNO   
                                                      PZBPOLL,pzbappl,
                                                      pzbplqs,pzbqstn,
                                                      pzrplap,PZBCALR,
                                                      PZRASPE,PZBPRSO,
                                                      PZVPFLE,pzrdypt,
                                                      pzvpost,pzvdncy,
                                                      pzbarea,pzbcrit

                                                 WHERE  PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code
                                                         AND pzbappl.pzbappl_poll_code = pzbplqs.pzbplqs_poll_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzbappl.pzbappl_seq_number = pzbplqs.pzbplqs_appl_seq_number
                                                         and pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number  
                                                         and pzbcalr.pzbcalr_code = pzbappl.pzbappl_calr_code
                                                         and pzbappl.pzbappl_calr_code = pzbplqs.pzbplqs_calr_code
                                                         and pzvpfle.pzvpfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzbqstn.pzbqstn_code = pzbplqs.pzbplqs_qstn_code
                                                         and PZBPLQS_STATUS ='A'
                                                         and pzbcrit.pzbcrit_code = pzbplqs.pzbplqs_crit_code
                                                         and pzbarea.pzbarea_code = pzbplqs.pzbplqs_area_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                         and pzrplap.pzrplap_pfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         and pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                         and pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         and pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code =  pzvpost.pzvpost_code
                                                         and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                         and pzrdypt.pzrdypt_dncy_code =  pzvdncy.pzvdncy_code
                                                         and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                         and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm
                                                         AND PZBPOLL.PZBPOLL_code='" + ide+"'"+
                                                         "and pzbprso.pzbprso_pidm='"+idi+"'"+ 
                                                         "and pzbcalr.pzbcalr_code ='"+idc+"'" +
                                                         " ORDER BY pzbarea.pzbarea_code, pzbplqs.pzbplqs_order").ToList();
                
                foreach (var result in es)
                {
                    //Datos de I_c


                    Models.AsignacionInstrumentoPregunta i_c = new Models.AsignacionInstrumentoPregunta();
                    i_c._PZBPLQS_SEQ_NUMBER = result.PZBPLQS_SEQ_NUMBER;
                    i_c._PZBPLQS_APPL_SEQ_NUMBER = result.PZBPLQS_APPL_SEQ_NUMBER;
                    i_c._PZBPLQS_USER = result.PZBPLQS_USER;
                    i_c._PZBPLQS_DATA_ORIGIN = result.PZBPLQS_DATA_ORIGIN;
                    i_c._PZBPLQS_ACTIVITY_DATE = result.PZBPLQS_ACTIVITY_DATE;
                    i_c._PZBPLQS_QSTN_CODE = result.PZBPLQS_QSTN_CODE;
                    i_c._PZBPLQS_POLL_CODE = result.PZBPLQS_POLL_CODE;
                    i_c._PZBPLQS_AREA_CODE = result.PZBPLQS_AREA_CODE;
                    i_c._PZBPLQS_CRIT_CODE = result.PZBPLQS_CRIT_CODE;
                    i_c._PZBPLQS_MAX_OVAL = result.PZBPLQS_MAX_OVAL;
                    i_c._PZBPLQS_MAX_VALUE = result.PZBPLQS_MAX_VALUE;
                    i_c._PZBPLQS_MIN_OVAL = result.PZBPLQS_MIN_OVAL;
                    i_c._PZBPLQS_MIN_VALUE = result.PZBPLQS_MIN_VALUE;
                    i_c._PZBPLQS_ORDER = result.PZBPLQS_ORDER;
                    i_c._PZBPLQS_STATUS = result.PZBPLQS_STATUS;


                    //Datos de la pregunta
                    Models.Pregunta pre = new Models.Pregunta();
                    pre._PZBQSTN_CODE = result.PZBQSTN.PZBQSTN_CODE;
                    pre._PZBQSTN_USER = result.PZBQSTN.PZBQSTN_USER;
                    pre._PZBQSTN_DESCRIPTION = result.PZBQSTN.PZBQSTN_DESCRIPTION;
                    pre._PZBQSTN_ACTIVITY_DATE = result.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                    pre._PZBQSTN_DATA_ORIGIN = result.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                    pre._PZBQSTN_NAME = result.PZBQSTN.PZBQSTN_NAME;
                    i_c._PZBQSTN = pre;


                    //DATOS DEL criterio de Evaluacion
                    Models.Criterio _cri = new Models.Criterio();
                    _cri._PZBCRIT_CODE = result.PZBCRIT.PZBCRIT_CODE;
                    _cri._PZBCRIT_USER = result.PZBCRIT.PZBCRIT_USER;
                    _cri._PZBCRIT_DESCRIPTION = result.PZBCRIT.PZBCRIT_DESCRIPTION;
                    _cri._PZBCRIT_DATA_ORIGIN = result.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                    _cri._PZBCRIT_ACTIVITY_DATE = result.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                    i_c._PZBCRIT = _cri;

                    //Datos del area a la cual pertenece la pregunta
                    Models.Area _area = new Models.Area();
                    _area._PZBAREA_CODE = result.PZBAREA.PZBAREA_CODE;
                    _area._PZBAREA_USER = result.PZBAREA.PZBAREA_USER;
                    _area._PZBAREA_DESCRIPTION = result.PZBAREA.PZBAREA_DESCRIPTION;
                    _area._PZBAREA_DATA_ORIGIN = result.PZBAREA.PZBAREA_DATA_ORIGIN;
                    _area._PZBAREA_ACTIVITY_DATE = result.PZBAREA.PZBAREA_ACTIVITY_DATE;
                    i_c._PZBAREA = _area;


                    //DATOS instrumento aplicable
                    Models.InstrumentoAplicable _insApl = new Models.InstrumentoAplicable();
                    _insApl._PZBAPPL_CALR_CODE = result.PZBAPPL.PZBAPPL_CALR_CODE;
                    _insApl._PZBAPPL_PFLE_CODE = result.PZBAPPL.PZBAPPL_PFLE_CODE;
                    _insApl._PZBAPPL_PRCNT = result.PZBAPPL.PZBAPPL_PRCNT;
                    _insApl._PZBAPPL_POLL_CODE = result.PZBAPPL.PZBAPPL_POLL_CODE;
                    _insApl._PZBAPPL_SEQ_NUMBER = result.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                    _insApl._PZBAPPL_USER = result.PZBAPPL.PZBAPPL_USER;
                    _insApl._PZBAPPL_DATA_ORIGIN = result.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                    _insApl._PZBAPPL_ACTIVITY_DATE = result.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                    i_c._PZBAPPL = _insApl;

                    //Datos Instrumento
                    Models.Instrumento ins = new Models.Instrumento();
                    ins._PZBPOLL_CODE = result.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                    ins._PZBPOLL_NAME = result.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                    ins._PZBPOLL_USER = result.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = result.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = result.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                    i_c._PZBAPPL._PZBPOLL = ins;



                    re.Add(i_c);

                }
            }
            catch (Exception e)
            {
                return BadRequest("Error+" + idi);
            }

            return Ok(re);

        }
    

        [HttpGet]
        [ResponseType(typeof(List<Models.AsignacionInstrumentoPersona>))]
        [Route("api/PZRPLAP/GetPZRPLAPAll/")]
        public IHttpActionResult GetPZRPLAPAll()
        {
            List<Models.AsignacionInstrumentoPersona> re = new List<Models.AsignacionInstrumentoPersona>();

            try
            {
                //    db.PZRDYPT.Include(car => car.PZVPOST).Include(dep => dep.PZVDNCY).Include(rev => rev.PZVPOST.PZVPOST2).Include(sup => sup.PZVDNCY.PZVDNCY2);
                //  db.PZVPFLE.Include();
                db.PZRPLAP.Include(insCal => insCal.PZBAPPL)
                           .Include(perPer => perPer.PZRASPE)
                           .Include(dep => dep.PZRASPE.PZRDYPT.PZVDNCY)
                           .Include(per => per.PZRASPE.PZBPRSO)
                           .Include(ins => ins.PZBAPPL.PZBPOLL)
                           //.Include(cal => cal.PZVICOD.PZBCALR)
                           .Include(pef => pef.PZRASPE.PZVPFLE)
                           .Include(pe2 => pe2.PZRASPE.PZBPRSO)
                           .Include(carDep => carDep.PZRASPE.PZRDYPT)
                           .Include(ca2 => ca2.PZRASPE.PZRDYPT.PZVPOST)
                           .Include(de2 => de2.PZRASPE.PZRDYPT.PZVDNCY);

                var es = db.PZRPLAP.SqlQuery(@"select * FROM PZRPLAP ").ToList();

                foreach (var result in es)
                {

                    //Datos ICPD
                    Models.AsignacionInstrumentoPersona item = new Models.AsignacionInstrumentoPersona();
                    item._PZRPLAP_CALR_CODE = result.PZRPLAP_CALR_CODE;
                    item._PZRPLAP_POST_CODE = result.PZRPLAP_POST_CODE;
                    item._PZRPLAP_DNCY_CODE = result.PZRPLAP_DNCY_CODE;
                    item._PZRPLAP_PFLE_CODE = result.PZRPLAP_PFLE_CODE;
                    item._PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                    item._PZRPLAP_DATA_ORIGIN = result.PZRPLAP_DATA_ORIGIN;
                    item._PZRPLAP_ACTIVITY_DATE = result.PZRPLAP_ACTIVITY_DATE;
                    item._PZRPLAP_PIDM = result.PZRPLAP_PIDM;
                    item._PZRPLAP_USER = result.PZRPLAP_USER;


                    //DATOS instrumento aplicable
                    Models.InstrumentoAplicable _insApl = new Models.InstrumentoAplicable();
                    _insApl._PZBAPPL_CALR_CODE = result.PZBAPPL.PZBAPPL_CALR_CODE;
                    _insApl._PZBAPPL_PFLE_CODE = result.PZBAPPL.PZBAPPL_PFLE_CODE;
                    _insApl._PZBAPPL_PRCNT = result.PZBAPPL.PZBAPPL_PRCNT;
                    _insApl._PZBAPPL_POLL_CODE = result.PZBAPPL.PZBAPPL_POLL_CODE;
                    _insApl._PZBAPPL_SEQ_NUMBER = result.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                    _insApl._PZBAPPL_USER = result.PZBAPPL.PZBAPPL_USER;
                    _insApl._PZBAPPL_DATA_ORIGIN = result.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                    _insApl._PZBAPPL_ACTIVITY_DATE = result.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                    item._PZBAPPL = _insApl;

                    //Datos Instrumento
                    Models.Instrumento ins = new Models.Instrumento();
                    // ins.PZBPOLL_ID = result.PZBCALR.PZBPOLL.PZBPOLL_ID;
                    ins._PZBPOLL_CODE = result.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                    //ins.PZBPRSO_ID = result.PZVICOD.PZBPOLL.PZBPRSO_ID;
                    ins._PZBPOLL_NAME = result.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                    ins._PZBPOLL_USER = result.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = result.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = result.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                    item._PZBAPPL._PZBPOLL = ins;

                    //Datos Calendario
                    Models.Calendario cal = new Models.Calendario();
                    cal._PZBCALR_CODE = result.PZBAPPL.PZBCALR.PZBCALR_CODE;
                    cal._PZBCALR_NAME = result.PZBAPPL.PZBCALR.PZBCALR_NAME;
                    cal._PZBCALR_USER = result.PZBAPPL.PZBCALR.PZBCALR_USER;
                    cal._PZBCALR_TERM = result.PZBAPPL.PZBCALR.PZBCALR_TERM;
                    cal._PZBCALR_END_DATE = result.PZBAPPL.PZBCALR.PZBCALR_END_DATE;
                    cal._PZBCALR_INIT_DATE = result.PZBAPPL.PZBCALR.PZBCALR_INIT_DATE;
                    cal._PZBCALR_DATA_ORIGIN = result.PZBAPPL.PZBCALR.PZBCALR_DATA_ORIGIN;
                    cal._PZBCALR_ACTIVITY_DATE = result.PZBAPPL.PZBCALR.PZBCALR_ACTIVITY_DATE;
                    item._PZBAPPL._PZBCALR = cal;

                    // item.PZVICOD = insCal;

                    // Datos PER_PER
                    Models.AsignacionCargoPersona ppcd = new Models.AsignacionCargoPersona();
                    ppcd._PZRASPE_PIDM = result.PZRASPE.PZRASPE_PIDM;
                    ppcd._PZRASPE_PFLE_CODE = result.PZRASPE.PZRASPE_PFLE_CODE;
                    ppcd._PZRASPE_POST_CODE = result.PZRASPE.PZRASPE_POST_CODE;
                    ppcd._PZRASPE_POST_CODE = result.PZRASPE.PZRASPE_POST_CODE;
                    // ppcd.PZRASPE_ID = result.PZRASPE.PZRASPE_ID;
                    ppcd._PZRASPE_END_DATE = result.PZRASPE.PZRASPE_END_DATE;
                    ppcd._PZRASPE_START_DATE = result.PZRASPE.PZRASPE_START_DATE;
                    item._PZRASPE = ppcd;

                    //Datos Perfil
                    Models.Perfil per = new Models.Perfil();
                    //per.PZVPFLE_ID = result.PZRASPE.PZVPFLE.PZVPFLE_ID;
                    per._PZVPFLE_CODE = result.PZRASPE.PZVPFLE.PZVPFLE_CODE;
                    per._PZVPFLE_NAME = result.PZRASPE.PZVPFLE.PZVPFLE_NAME;
                    per._PZVPFLE_ACTIVITY_DATE = result.PZRASPE.PZVPFLE.PZVPFLE_ACTIVITY_DATE;
                    per._PZVPFLE_DATA_ORIGIN = result.PZRASPE.PZVPFLE.PZVPFLE_DATA_ORIGIN;
                    per._PZVPFLE_USER = result.PZRASPE.PZVPFLE.PZVPFLE_USER;
                    item._PZRASPE._PZVPFLE = per;

                    //Datos Persona
                    Models.Persona per2 = new Models.Persona();
                    // per2.PZBPRSO_ID = result.PZRASPE.PZBPRSO.PZBPRSO_ID;
                    per2._PZBPRSO_PIDM = result.PZRASPE.PZBPRSO.PZBPRSO_PIDM;
                    per2._PZBPRSO_SITE = result.PZRASPE.PZBPRSO.PZBPRSO_SITE;
                    //per2.PZBPRSO_NAME = result.PZRASPE.PZBPRSO.PZBPRSO_NAME;
                    per2._PZBPRSO_USER = result.PZRASPE.PZBPRSO.PZBPRSO_USER;
                    per2._PZBPRSO_ACTIVE = result.PZRASPE.PZBPRSO.PZBPRSO_ACTIVE;
                    per2._PZBPRSO_PAYSHEET = result.PZRASPE.PZBPRSO.PZBPRSO_PAYSHEET;
                    //per2.PZBPRSO_PZVPOST = result.PZRASPE.PZBPRSO.PZBPRSO_PZCRCOD;
                    item._PZRASPE._PZBPRSO = per2;

                    //Datos Cardep
                    Models.AsignacionCargoDependencia carDep = new Models.AsignacionCargoDependencia();
                    //carDep.PZRDYPT_ID = result.PZRASPE.PZRDYPT.PZRDYPT_ID;
                    carDep._PZRDYPT_POST_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_POST_CODE;
                    carDep._PZRDYPT_DNCY_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_DNCY_CODE;
                    carDep._PZRDYPT_USER = result.PZRASPE.PZRDYPT.PZRDYPT_USER;
                    carDep._PZRDYPT_DATA_ORIGIN = result.PZRASPE.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                    carDep._PZRDYPT_ACTIVITY_DATE = result.PZRASPE.PZRDYPT.PZRDYPT_ACTIVITY_DATE;
                    item._PZRASPE._PZRDYPT = carDep;

                    //Datos Cargo
                    Models.Cargo car = new Models.Cargo();
                    //car.PZVPOST_ID = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_ID;
                    car._PZVPOST_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_POST_CODE;
                    car._PZVPOST_NAME = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_NAME;
                    car._PZVPOST_CODE_SUP = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                    car._PZVPOST_USER = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_USER;
                    car._PZVPOST_DESCRIPTION = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                    car._PZVPOST_ACTIVITY_DATE = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;
                    item._PZRASPE._PZRDYPT._PZVPOST = car;

                    //Datos Departamento
                    Models.Dependencia dep = new Models.Dependencia();
                    //dep.PZVDNCY_ID = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_ID;
                    dep._PZVDNCY_CODE = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_CODE;
                    dep._PZVDNCY_KEY = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
                    dep._PZVDNCY_SITE = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
                    dep._PZVDNCY_LEVEL = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
                    dep._PZVDNCY_NAME = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                    dep._PZVDNCY_USER = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_USER;
                    item._PZRASPE._PZRDYPT._PZVDNCY = dep;


                    re.Add(item);

                }
            }
            catch (Exception e)
            {
                return BadRequest("Error");
            }

            return Ok(re);

        }

        // GET: api/PZRPLAP/5
        [ResponseType(typeof(PZRPLAP))]
        public IHttpActionResult GetPZRPLAP(decimal id)
        {
            PZRPLAP PZRPLAP = db.PZRPLAP.Find(id);
            if (PZRPLAP == null)
            {
                return NotFound();
            }

            return Ok(PZRPLAP);
        }

        [HttpPut]
        [Route("api/PZRPLAP/PutPZRPLAP/{id}/{user}/{status}")]
        [ResponseType(typeof(Models.AsignacionInstrumentoPersona))]
         public IHttpActionResult PutPZRPLAP(int id, string user, string status)//Models.AsignacionInstrumentoPersona _PZRPLAP)
          {
              Repository.FunctionsRepository _f = new Repository.FunctionsRepository();


              List<Models.AsignacionInstrumentoPersona> re = new List<Models.AsignacionInstrumentoPersona>();
              int _pidm = 0;
              string code = "";
              string post = "";
              string dncy = "";
              string calr = "";
              string pfle = "";
              string datao = "SYSTEM";
              int _id_plap = 0;
              DateTime fecha = DateTime.Today;

            try
            {
                var es = db.PZRPLAP.SqlQuery(@"select * FROM PZRPLAP 
                                                WHERE pzrplap.PZRPLAP_SEQ_NUMBER = '" + id + "' ");

                foreach (var result in es)
                {



                    result.PZRPLAP_STATUS = status;
                    result.PZRPLAP_ACTIVITY_DATE = fecha;
                    result.PZRPLAP_USER = user;

                    db.PZRPLAP.Attach(result);
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    

                }


                return Ok("Registro Modificado Exitosamente");


                /*    if (status == "F")
                    {

                        String _eval = _f.IdEvaluador(_pidm, pfle, post, dncy, code, calr);
                        int _pidm_evaluador = Convert.ToInt32(_eval);

                        String respuest2 = _f.GetNofifier(_pidm, "Se ha cargado su evaluación ", "Estimado(a) " + _f.NameUser(_pidm) + ": \n  El usuario " + _f.NameUser(_pidm_evaluador)
                                                                   + "ha realizado exitosamete su evaluacion " + _f.NamePoll(code) + ". ");
                    }
                    else
                    {
                        if (status == "V" || status == "T")
                        {
                            String respuest2 = _f.GetNofifier(_pidm, "Se ha cargado su evaluación ", "Estimado(a) " + _f.NameUser(_pidm) + " se ha modificado exitosamente su evaluacion " + _f.NamePoll(code) + ". ");
                        }

                        if (status == "C")
                        {
                            String respuest2 = _f.GetNofifier(_pidm, "Se han cargado sus objetivos  ", "Estimado(a) " + _f.NameUser(_pidm) + " : {0}  se ha modificado exitosamente su evaluacion " + _f.NamePoll(code) + ". ");
                        }
                    }
                    String eval_respuest = _f.GetNofifierE(_pidm, pfle, post, dncy, code, calr, "Se ha completado una evaluación", " La evaluacion " + _f.NamePoll(code) + " asignada a " + _f.NameUser(_pidm) + " se ha modiificada exitosamente");

                    */

                return Ok("Registro Modificado Exitosamente");

              }

              catch (Exception e) { }

              return NotFound();


          }
          

      
        /// <summary>
        /// 
        /// Modifica la asignacion de un instrumento
        /// en un calendario para una persona
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="PZRPLAP"></param>
        /// <returns></returns>
        // PUT: api/PZRPLAP/5
        [HttpPut]
        [Route("api/PZRPLAP/PutPZRPLAP/{id}/")]
        [ResponseType(typeof(Models.AsignacionInstrumentoPersona))]
        public IHttpActionResult PutPZRPLAP(String id, Models.AsignacionInstrumentoPersona _PZRPLAP)
        {
            int _pidm = 0;
            string code = "";

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                DateTime fecha = DateTime.Today;
                _PZRPLAP._PZRPLAP_ACTIVITY_DATE = fecha;


                PZRPLAP objIcpd = new PZRPLAP();

                objIcpd.PZRPLAP_PIDM = _PZRPLAP._PZRPLAP_PIDM;
                objIcpd.PZRPLAP_APPL_SEQ_NUMBER = _PZRPLAP._PZRPLAP_APPL_SEQ_NUMBER;
                objIcpd.PZRPLAP_SEQ_NUMBER = _PZRPLAP._PZRPLAP_SEQ_NUMBER;
                objIcpd.PZRPLAP_CALR_CODE = _PZRPLAP._PZRPLAP_CALR_CODE;
                objIcpd.PZRPLAP_DNCY_CODE = _PZRPLAP._PZRPLAP_DNCY_CODE;
                objIcpd.PZRPLAP_POST_CODE = _PZRPLAP._PZRPLAP_POST_CODE;
                objIcpd.PZRPLAP_PFLE_CODE = _PZRPLAP._PZRPLAP_PFLE_CODE;
                objIcpd.PZRPLAP_STATUS = _PZRPLAP._PZRPLAP_STATUS;
                objIcpd.PZRPLAP_USER = _PZRPLAP._PZRPLAP_USER;
                objIcpd.PZRPLAP_DATA_ORIGIN = _PZRPLAP._PZRPLAP_DATA_ORIGIN;
                objIcpd.PZRPLAP_ACTIVITY_DATE = fecha;
                objIcpd.PZRPLAP_POLL_CODE = _PZRPLAP._PZRPLAP_POLL_CODE;
                objIcpd.PZRPLAP_DATE = _PZRPLAP._PZRPLAP_DATE;

                db.PZRPLAP.Attach(objIcpd);
                db.Entry(objIcpd).State = EntityState.Modified;
                db.SaveChanges();



                return Ok("Registro Modificado Exitosamente");

            }
            catch (DbUpdateConcurrencyException)
            {


                return StatusCode(HttpStatusCode.NoContent);
            }
        }

        // POST: api/PZRPLAP
        [HttpPost]
        [Route("api/PZRPLAP/PostPZRPLAP/{_PZRPLAP}/")]
        [ResponseType(typeof(Models.AsignacionInstrumentoPersona))]
        public IHttpActionResult PostPZRPLAP(Models.AsignacionInstrumentoPersona _PZRPLAP)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                int es;
                var num_reg = db.PZRPLAP.SqlQuery(@"select * 
                                                            from pzrplap 
                                                            where PZRPLAP_PIDM = '"+ _PZRPLAP._PZRPLAP_PIDM+"'"+
                                                           " and PZRPLAP_APPL_SEQ_NUMBER = '"+_PZRPLAP._PZRPLAP_APPL_SEQ_NUMBER+"'"+
                                                           " and PZRPLAP_CALR_CODE = '"+_PZRPLAP._PZRPLAP_CALR_CODE+"'"+
                                                           " and PZRPLAP_DNCY_CODE = '"+_PZRPLAP._PZRPLAP_DNCY_CODE+"'"+
                                                           " and PZRPLAP_POST_CODE = '"+_PZRPLAP._PZRPLAP_POST_CODE+"'"+
                                                           " and PZRPLAP_PFLE_CODE = '"+_PZRPLAP._PZRPLAP_PFLE_CODE+"'").Count();
                 es = (int)num_reg;

                if (es > 0)
                {

                    return BadRequest("No se pudo registrar, el usuario ya tiene este instrumento "+_PZRPLAP._PZRPLAP_POLL_CODE+" asignado");
                }

                else
                {
                    DateTime fecha = DateTime.Today;

                    PZRPLAP objIcpd = new PZRPLAP();
                    // objIcpd.PZRPLAP_ID = es;
                    objIcpd.PZRPLAP_PIDM = _PZRPLAP._PZRPLAP_PIDM;
                    objIcpd.PZRPLAP_APPL_SEQ_NUMBER = _PZRPLAP._PZRPLAP_APPL_SEQ_NUMBER;
                    //objIcpd.PZRPLAP_SEQ_NUMBER = es;
                    objIcpd.PZRPLAP_CALR_CODE = _PZRPLAP._PZRPLAP_CALR_CODE;
                    objIcpd.PZRPLAP_DNCY_CODE = _PZRPLAP._PZRPLAP_DNCY_CODE;
                    objIcpd.PZRPLAP_POST_CODE = _PZRPLAP._PZRPLAP_POST_CODE;
                    objIcpd.PZRPLAP_PFLE_CODE = _PZRPLAP._PZRPLAP_PFLE_CODE;
                    objIcpd.PZRPLAP_STATUS = _PZRPLAP._PZRPLAP_STATUS;
                    objIcpd.PZRPLAP_USER = _PZRPLAP._PZRPLAP_USER;
                    objIcpd.PZRPLAP_DATA_ORIGIN = _PZRPLAP._PZRPLAP_DATA_ORIGIN;
                    objIcpd.PZRPLAP_ACTIVITY_DATE = fecha;
                    objIcpd.PZRPLAP_POLL_CODE = _PZRPLAP._PZRPLAP_POLL_CODE;
                    objIcpd.PZRPLAP_DATE = _PZRPLAP._PZRPLAP_DATE;


                    db.PZRPLAP.Add(objIcpd);

                    db.SaveChanges();

                    String respuest = _f.GetNofifier(objIcpd.PZRPLAP_PIDM, "Se le asignó una evaluación ", "Estimado(a) " + _f.NameUser(objIcpd.PZRPLAP_PIDM) + ": Se le ha asignado la evaluacion correspondiente a "+_f.NamePoll(objIcpd.PZRPLAP_POLL_CODE) +" ");
                }

         
            }
            catch (DbUpdateException)
            {

            }

            return CreatedAtRoute("DefaultApi", new { id = _PZRPLAP }, _PZRPLAP);
        }

        [HttpPost]
        [Route("api/PZRPLAP/PostPZRPLAP/{_PZRPLAP}/")]
        [ResponseType(typeof(Models.AsignacionInstrumentoPersona))]
        public IHttpActionResult PostPZRPLAP(List<Models.AsignacionInstrumentoPersona> _PZRPLAP)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                    foreach (var asig in _PZRPLAP) {


                    int es;
                    var num_reg = db.PZRPLAP.SqlQuery(@"select * 
                                                            from pzrplap 
                                                            where PZRPLAP_PIDM = '" + asig._PZRPLAP_PIDM + "'" +
                                                               " and PZRPLAP_APPL_SEQ_NUMBER = '" + asig._PZRPLAP_APPL_SEQ_NUMBER + "'" +
                                                               " and PZRPLAP_CALR_CODE = '" + asig._PZRPLAP_CALR_CODE + "'" +
                                                               " and PZRPLAP_DNCY_CODE = '" + asig._PZRPLAP_DNCY_CODE + "'" +
                                                               " and PZRPLAP_POST_CODE = '" + asig._PZRPLAP_POST_CODE + "'" +
                                                               " and PZRPLAP_PFLE_CODE = '" + asig._PZRPLAP_PFLE_CODE + "'").Count();
                    es = (int)num_reg;

                    if (es > 0)
                    {

                        return BadRequest("No se pudo registrar, el usuario ya tiene este instrumento " + asig._PZRPLAP_POLL_CODE + " asignado");
                    }


                        DateTime fecha = DateTime.Today;

                        PZRPLAP objIcpd = new PZRPLAP();
                        // objIcpd.PZRPLAP_ID = es;
                        objIcpd.PZRPLAP_PIDM = asig._PZRPLAP_PIDM;
                        objIcpd.PZRPLAP_APPL_SEQ_NUMBER = asig._PZRPLAP_APPL_SEQ_NUMBER;
                        //objIcpd.PZRPLAP_SEQ_NUMBER = es;
                        objIcpd.PZRPLAP_CALR_CODE = asig._PZRPLAP_CALR_CODE;
                        objIcpd.PZRPLAP_DNCY_CODE = asig._PZRPLAP_DNCY_CODE;
                        objIcpd.PZRPLAP_POST_CODE = asig._PZRPLAP_POST_CODE;
                        objIcpd.PZRPLAP_PFLE_CODE = asig._PZRPLAP_PFLE_CODE;
                        objIcpd.PZRPLAP_STATUS = asig._PZRPLAP_STATUS;
                        objIcpd.PZRPLAP_USER = asig._PZRPLAP_USER;
                        objIcpd.PZRPLAP_DATA_ORIGIN = asig._PZRPLAP_DATA_ORIGIN;
                        objIcpd.PZRPLAP_ACTIVITY_DATE = fecha;
                        objIcpd.PZRPLAP_POLL_CODE = asig._PZRPLAP_POLL_CODE;
                        objIcpd.PZRPLAP_DATE = asig._PZRPLAP_DATE;


                        db.PZRPLAP.Add(objIcpd);

                        db.SaveChanges();

                    String respuest = _f.GetNofifier(objIcpd.PZRPLAP_PIDM, "Se le ha asignado una evaluación ", "Estimado(a) " + _f.NameUser(objIcpd.PZRPLAP_PIDM) + ": Se le ha asignado la evaluacion correspondiente a " + _f.NamePoll(objIcpd.PZRPLAP_POLL_CODE) + " ");
                }

                


            }
            catch (DbUpdateException)
            {

            }

            return CreatedAtRoute("DefaultApi", new { id = _PZRPLAP }, _PZRPLAP);
        }


        // DELETE: api/PZRPLAP/5
        [ResponseType(typeof(PZRPLAP))]
        public IHttpActionResult DeletePZRPLAP(decimal id)
        {
            PZRPLAP PZRPLAP = db.PZRPLAP.Find(id);
            if (PZRPLAP == null)
            {
                return NotFound();
            }

            db.PZRPLAP.Remove(PZRPLAP);
            db.SaveChanges();

            return Ok(PZRPLAP);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZRPLAPExists(decimal id)
        {
            return db.PZRPLAP.Count(/*e => e.PZRPLAP_ID == id*/) > 0;
        }


        public AsignacionInstrumentoPersona registroPendiente(int id, string user, string status)
        {

            List<Models.AsignacionInstrumentoPersona> lresult = new List<Models.AsignacionInstrumentoPersona>();
            Models.AsignacionInstrumentoPersona item = new Models.AsignacionInstrumentoPersona();

            try
            {

                var es = db.PZRPLAP.SqlQuery(@"select * FROM PZRPLAP WHERE pzrplap.PZRPLAP_SEQ_NUMBER = '" + id + "' ");

                foreach (var result in es)
                {

                    //item.PZRPLAP_ID = result.PZRPLAP_ID;
                    item._PZRPLAP_SEQ_NUMBER = result.PZRPLAP_SEQ_NUMBER;
                    item._PZRPLAP_APPL_SEQ_NUMBER = result.PZRPLAP_APPL_SEQ_NUMBER;
                    item._PZRPLAP_CALR_CODE = result.PZRPLAP_CALR_CODE;
                    item._PZRPLAP_POST_CODE = result.PZRPLAP_POST_CODE;
                    item._PZRPLAP_DNCY_CODE = result.PZRPLAP_DNCY_CODE;
                    item._PZRPLAP_POLL_CODE = result.PZRPLAP_POLL_CODE;
                    item._PZRPLAP_PFLE_CODE = result.PZRPLAP_PFLE_CODE;
                    item._PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                    item._PZRPLAP_DATE = result.PZRPLAP_DATE;
                    item._PZRPLAP_DATA_ORIGIN = result.PZRPLAP_DATA_ORIGIN;
                    item._PZRPLAP_ACTIVITY_DATE = result.PZRPLAP_ACTIVITY_DATE;
                    item._PZRPLAP_PIDM = result.PZRPLAP_PIDM;
                    item._PZRPLAP_USER = result.PZRPLAP_USER;

                    lresult.Add(item);

                }


            }
            catch (Exception e)
            {

                return item;
            }

            return item;

        }
    }
}