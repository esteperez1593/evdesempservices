﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZVDNCYController : ApiController
    {
        private Entities db = new Entities();

        
        /// <summary>
        /// 
        /// Carga las dependencias existentes
        /// 
        /// RUTA: /api/PZVDNCYs/GetPZVDNCYs
        /// 
        /// </summary>
        /// <returns>Ok(Models.PZVDNCY)</returns>
        [HttpGet]
        [Route("api/PZVDNCY/GetPZVDNCYs/")]
        [ResponseTypeAttribute(typeof(List<Models.Dependencia>))]
        public IHttpActionResult GetPZVDNCYs()
        {
            List<Models.Dependencia> re = new List<Models.Dependencia>();

            try
            {
                db.PZVDNCY.Include(B => B.PZVDNCY1);
                var es = db.PZVDNCY.SqlQuery(@"select DEP.*,SUP.* FROM PZVDNCY DEP , PZVDNCY SUP
                                        WHERE DEP.PZVDNCY_CODE_SUP = SUP.PZVDNCY_CODE ").ToList();

                foreach (var result in es)
                {
                    Models.Dependencia item = new Models.Dependencia();

                    //Dependencia
                    //item.PZVDNCY_ID = result.PZVDNCY_ID;
                    item._PZVDNCY_CODE = result.PZVDNCY_CODE;
                    item._PZVDNCY_CODE_SUP = result.PZVDNCY_CODE_SUP;
                    item._PZVDNCY_NAME = result.PZVDNCY_NAME;
                    item._PZVDNCY_LEVEL = result.PZVDNCY_LEVEL;
                    item._PZVDNCY_KEY = result.PZVDNCY_KEY;
                    item._PZVDNCY_SITE = result.PZVDNCY_SITE;
                    item._PZVDNCY_ACTIVITY_DATE = result.PZVDNCY_ACTIVITY_DATE;
                    item._PZVDNCY_DATA_ORIGIN = result.PZVDNCY_DATA_ORIGIN;
                    item._PZVDNCY_USER = result.PZVDNCY_USER;
                    //item.PZVDNCY1 = result.PZVDNCY1;

                    //Dependencia Supervisora

                    Models.Dependencia itemSup = new Models.Dependencia();

                    //itemSup.PZVDNCY_ID = result.PZVDNCY2.PZVDNCY_ID;
                    itemSup._PZVDNCY_NAME= result.PZVDNCY2.PZVDNCY_NAME;
                    itemSup._PZVDNCY_LEVEL = result.PZVDNCY2.PZVDNCY_LEVEL;
                    itemSup._PZVDNCY_KEY = result.PZVDNCY2.PZVDNCY_KEY;
                    itemSup._PZVDNCY_SITE = result.PZVDNCY2.PZVDNCY_SITE;
                    itemSup._PZVDNCY_CODE_SUP = result.PZVDNCY2.PZVDNCY_CODE_SUP;
                    itemSup._PZVDNCY_ACTIVITY_DATE = result.PZVDNCY2.PZVDNCY_ACTIVITY_DATE;
                    itemSup._PZVDNCY_CODE = result.PZVDNCY2.PZVDNCY_CODE;
                    itemSup._PZVDNCY_DATA_ORIGIN = result.PZVDNCY2.PZVDNCY_DATA_ORIGIN;

                    item._PZVDNCY2 = itemSup;



                    re.Add(item);
                }

            }
            catch (Exception e)
            {

            }

            return Ok(re);
        }

        // GET: api/PZVDNCYs/5
        [ResponseType(typeof(Dependencia))]
        public IHttpActionResult GetPZVDNCY(String id)
        {
          
            db.PZVDNCY.Include(B => B.PZVDNCY1);
            var es = db.PZVDNCY.SqlQuery(@"select * 
                                           from PZVDNCY
                                            WHERE PZVDNCY_CODE='"+id+"' ");
            if (es == null)
            {
                return NotFound();
            }
            foreach (var result in es) { 

            Models.Dependencia item = new Models.Dependencia();

            //Dependencia
            //item.PZVDNCY_ID = result.PZVDNCY_ID;
            item._PZVDNCY_CODE = result.PZVDNCY_CODE;
            item._PZVDNCY_CODE_SUP = result.PZVDNCY_CODE_SUP;
            item._PZVDNCY_NAME = result.PZVDNCY_NAME;
            item._PZVDNCY_LEVEL = result.PZVDNCY_LEVEL;
            item._PZVDNCY_KEY = result.PZVDNCY_KEY;
            item._PZVDNCY_SITE = result.PZVDNCY_SITE;
            item._PZVDNCY_ACTIVITY_DATE = result.PZVDNCY_ACTIVITY_DATE;

            
            return Ok(item);
            }
            return BadRequest("Codigo no encontrado");
        }

        // PUT: api/PZVDNCYs/5
        /// <summary>
        /// 
        /// Modifica los Datos asociados a un departamento
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pZVDCOD"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/PZVDNCY/PutPZVDNCY/{id}/")]
        [ResponseType(typeof(Dependencia))]
        public IHttpActionResult PutPZVDNCY(String id, Dependencia pZVDCOD)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pZVDCOD._PZVDNCY_CODE)
            {
                return BadRequest();
            }

          
            try
            {
                DateTime fecha = DateTime.Today;
                pZVDCOD._PZVDNCY_ACTIVITY_DATE = fecha;

                PZVDNCY dep = new PZVDNCY();
               // dep.PZVDNCY_ID = pZVDCOD.PZVDNCY_ID;
                dep.PZVDNCY_CODE = pZVDCOD._PZVDNCY_CODE;
                dep.PZVDNCY_KEY = pZVDCOD._PZVDNCY_KEY;
                dep.PZVDNCY_LEVEL = pZVDCOD._PZVDNCY_LEVEL;
                dep.PZVDNCY_CODE_SUP = pZVDCOD._PZVDNCY_CODE_SUP;
                dep.PZVDNCY_SITE = pZVDCOD._PZVDNCY_SITE;
                dep.PZVDNCY_USER = pZVDCOD._PZVDNCY_USER;
                dep.PZVDNCY_NAME = pZVDCOD._PZVDNCY_NAME;
                dep.PZVDNCY_DATA_ORIGIN = pZVDCOD._PZVDNCY_DATA_ORIGIN;
                dep.PZVDNCY_ACTIVITY_DATE = pZVDCOD._PZVDNCY_ACTIVITY_DATE;

                db.PZVDNCY.Attach(dep);
                db.Entry(dep).State = EntityState.Modified;
                db.SaveChanges();

                return Ok("Departamento:"+dep.PZVDNCY_NAME+" Modificado exitosamente");

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PZVDNCYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PZVDNCYs
        /// <summary>
        /// 
        /// Crea una Nueva Dependencia
        /// RUTA: /api/PZVDNCYs/PostPZVDNCY
        /// 
        /// </summary>
        /// <param name="pZVDCOD"></param>
        /// <returns>CreatedAtRoute("DefaultApi", new { id = pZVDCOD.PZVDNCY_ID }, pZVDCOD)</returns>

        [HttpPost]
        [ResponseType(typeof(Dependencia))]
        public IHttpActionResult PostPZVDNCY(Dependencia pZVDCOD)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (db.PZVDNCY.Count(e => e.PZVDNCY_CODE == pZVDCOD._PZVDNCY_CODE) > 0) {

                return BadRequest("El codigo de la dependendencia ya existe.");
            }

            PZVDNCY dep = new PZVDNCY();

            DateTime fecha = DateTime.Today;


            pZVDCOD._PZVDNCY_ACTIVITY_DATE = fecha;
            dep.PZVDNCY_CODE = pZVDCOD._PZVDNCY_CODE;
            dep.PZVDNCY_KEY = pZVDCOD._PZVDNCY_KEY;
            dep.PZVDNCY_LEVEL = pZVDCOD._PZVDNCY_LEVEL;
            dep.PZVDNCY_CODE_SUP = pZVDCOD._PZVDNCY_CODE_SUP;
            dep.PZVDNCY_SITE = pZVDCOD._PZVDNCY_SITE;
            dep.PZVDNCY_USER = pZVDCOD._PZVDNCY_USER;
            dep.PZVDNCY_NAME = pZVDCOD._PZVDNCY_NAME;
            dep.PZVDNCY_DATA_ORIGIN = pZVDCOD._PZVDNCY_DATA_ORIGIN;
            dep.PZVDNCY_ACTIVITY_DATE = pZVDCOD._PZVDNCY_ACTIVITY_DATE;


            db.PZVDNCY.Add(dep);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
             
            }

            return CreatedAtRoute("DefaultApi", new { id = pZVDCOD._PZVDNCY_CODE }, pZVDCOD);
        }

        // DELETE: api/PZVDNCYs/5
        [ResponseType(typeof(PZVDNCY))]
        public IHttpActionResult DeletePZVDNCY(String id)
        {
            PZVDNCY pZVDCOD = db.PZVDNCY.Find(id);
            if (pZVDCOD == null)
            {
                return NotFound();
            }

            db.PZVDNCY.Remove(pZVDCOD);
            db.SaveChanges();

            return Ok(pZVDCOD);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZVDNCYExists(String id)
        {
            return db.PZVDNCY.Count(e => e.PZVDNCY_CODE == id) > 0;
        }
    }
}