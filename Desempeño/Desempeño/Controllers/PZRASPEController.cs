﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data;

namespace Desempeño.Controllers
{
    public class PZRASPEController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/PZRASPE

        /// <summary>
        ///  Obtener Personas con cargo y deppendencia ademas del perfil
        /// RUTA: api/PZRASPE/GetPZRASPE
        /// 
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [ResponseType(typeof(List<Models.AsignacionCargoPersona>))]
        [Route("api/PZRASPE/GetPZRASPE/")]
        public IHttpActionResult GetPZRASPE()
        {
            List<Models.AsignacionCargoPersona> result = new List<Models.AsignacionCargoPersona>();

            
            db.PZRASPE.Include(per => per.PZBPRSO)
                .Include(prf => prf.PZVPFLE)
                .Include(cardep => cardep.PZRDYPT)
                .Include(car => car.PZRDYPT.PZVPOST)
                .Include(dep => dep.PZRDYPT.PZVDNCY);
            var perper = db.PZRASPE.SqlQuery(@" SELECT * from PZRASPE ").ToList();

            foreach (var x in perper)
            {
                Models.AsignacionCargoPersona ppcd = new Models.AsignacionCargoPersona();
                //ppcd.PZRASPE_ID = x;
                ppcd._PZRASPE_POST_CODE = x.PZRASPE_POST_CODE;
                ppcd._PZRASPE_DNCY_CODE = x.PZRASPE_DNCY_CODE;
                ppcd._PZRASPE_PIDM = x.PZRASPE_PIDM;
                ppcd._PZRASPE_PFLE_CODE= x.PZRASPE_PFLE_CODE;
                ppcd._PZRASPE_USER = x.PZRASPE_USER;
                ppcd._PZRASPE_DATA_ORIGIN = x.PZRASPE_DATA_ORIGIN;
                ppcd._PZRASPE_ACTIVITY_DATE = x.PZRASPE_ACTIVITY_DATE;
                ppcd._PZRASPE_END_DATE = x.PZRASPE_END_DATE;
                ppcd._PZRASPE_START_DATE = x.PZRASPE_START_DATE;
            

                //Datos Persona
                Models.Persona per = new Models.Persona();
                per._PZBPRSO_PIDM = x.PZBPRSO.PZBPRSO_PIDM;
                per._PZBPRSO_SITE = x.PZBPRSO.PZBPRSO_SITE;
                per._PZBPRSO_USER = x.PZBPRSO.PZBPRSO_USER;
                per._PZBPRSO_PAYSHEET = x.PZBPRSO.PZBPRSO_PAYSHEET;
                per._PZBPRSO_DATA_ORIGIN = x.PZBPRSO.PZBPRSO_DATA_ORIGIN;
                per._PZBPRSO_ACTIVE = x.PZBPRSO.PZBPRSO_ACTIVE;

                ppcd._PZBPRSO = per;


                //Datos Perfil
                Models.Perfil prf = new Models.Perfil();
                prf._PZVPFLE_CODE = x.PZVPFLE.PZVPFLE_CODE;
                prf._PZVPFLE_USER = x.PZVPFLE.PZVPFLE_USER;
                prf._PZVPFLE_DATA_ORIGIN = x.PZVPFLE.PZVPFLE_DATA_ORIGIN;
                prf._PZVPFLE_NAME = x.PZVPFLE.PZVPFLE_NAME;
                prf._PZVPFLE_ACTIVITY_DATE = x.PZVPFLE.PZVPFLE_ACTIVITY_DATE;
                ppcd._PZVPFLE = prf;


                //CARGO-Dependencia
                Models.AsignacionCargoDependencia _cd = new Models.AsignacionCargoDependencia();

                //Datos de la Ternaria   
                _cd._PZRDYPT_POST_CODE = x.PZRDYPT.PZRDYPT_POST_CODE;
                _cd._PZRDYPT_DNCY_CODE = x.PZRDYPT.PZRDYPT_DNCY_CODE;
                //cd.PZRDYPT_ID = x.PZRDYPT_ID;
                _cd._PZRDYPT_USER = x.PZRDYPT.PZRDYPT_USER;
                _cd._PZRDYPT_DATA_ORIGIN = x.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                _cd._PZRDYPT_ACTIVITY_DATE = x.PZRDYPT.PZRDYPT_ACTIVITY_DATE;
                ppcd._PZRDYPT = _cd;

                //Datos Cargo
                Models.Cargo car = new Models.Cargo();

                //car.PZVPOST_ID = x.PZRDYPT.PZVPOST.PZVPOST_ID;
                car._PZVPOST_CODE = x.PZRDYPT.PZVPOST.PZVPOST_CODE;
                car._PZVPOST_NAME = x.PZRDYPT.PZVPOST.PZVPOST_NAME;
                car._PZVPOST_CODE_SUP = x.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                car._PZVPOST_USER = x.PZRDYPT.PZVPOST.PZVPOST_USER;
                car._PZVPOST_DESCRIPTION = x.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                car._PZVPOST_ACTIVITY_DATE = x.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;
                ppcd._PZRDYPT._PZVPOST = car;

                //Datos Departamento
                Models.Dependencia dep = new Models.Dependencia();
                //dep.PZVDNCY_ID = x.PZRDYPT.PZVDNCY.PZVDNCY_ID;
                dep._PZVDNCY_CODE = x.PZRDYPT.PZVDNCY.PZVDNCY_CODE;
                dep._PZVDNCY_NAME = x.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                dep._PZVDNCY_CODE_SUP = x.PZRDYPT.PZVDNCY.PZVDNCY_CODE_SUP;
                ppcd._PZRDYPT._PZVDNCY = dep;



                result.Add(ppcd);

            }

            // DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(PZRDYPT));

            //return stream1;
            return Ok(result);
        }

        // GET: api/PZRASPE/5
        [ResponseType(typeof(PZRASPE))]
        public IHttpActionResult GetPZRASPE(decimal id)
        {
            PZRASPE pZVLDEF = db.PZRASPE.Find(id);
            if (pZVLDEF == null)
            {
                return NotFound();
            }

            return Ok(pZVLDEF);
        }

        /// <summary>
        /// 
        /// Dado el PIDM de una prsona el servicio retorna los datos correspondientes a
        /// Cargo,Departamento,perfil y datos del usuario
        ///
        /// </summary>
        /// <param name="idper"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(Models.AsignacionCargoPersona))]
        [Route("api/PZRASPE/GetPZRASPEId/{idper}")]
        public IHttpActionResult GetPZRASPEId(int idper)
        {
            List<Models.AsignacionCargoPersona> result = new List<Models.AsignacionCargoPersona>();


            db.PZRASPE.Include(per => per.PZBPRSO).Include(prf => prf.PZVPFLE).Include(cardep => cardep.PZRDYPT).Include(car => car.PZRDYPT.PZVPOST).Include(dep => dep.PZRDYPT.PZVDNCY);
            var perper = db.PZRASPE.SqlQuery(@"SELECT * 
                                                FROM PZBPRSO, 
                                                     PZVPOST,
                                                     PZVPFLE,
                                                     PZRASPE,
                                                     PZRDYPT,
                                                     PZVDNCY
                                                WHERE PZRASPE.PZRASPE_PFLE_CODE = PZVPFLE.PZVPFLE_code
                                                AND pzraspe.pzraspe_pidm = PZBPRSO.PZBPRSO_PIDM 
                                                AND pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                AND pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                and PZRDYPT.PZRDYPT_POST_CODE = PZVPOST.PZVPOST_CODE
                                                and PZRDYPT.PZRDYPT_DNCY_CODE = PZVDNCY.PZVDNCY_CODE
                                                  AND PZBPRSO.PZBPRSO_PIDM = " + idper+" ").ToList();

            foreach (var x in perper)
            {
                Models.AsignacionCargoPersona ppcd = new Models.AsignacionCargoPersona();
             //   ppcd.PZRASPE_ID = x;
                ppcd._PZRASPE_DNCY_CODE = x.PZRASPE_DNCY_CODE;
                ppcd._PZRASPE_POST_CODE = x.PZRASPE_POST_CODE;
                ppcd._PZRASPE_PIDM = x.PZRASPE_PIDM;
                ppcd._PZRASPE_PFLE_CODE = x.PZRASPE_PFLE_CODE;
                ppcd._PZRASPE_END_DATE = x.PZRASPE_END_DATE;
                ppcd._PZRASPE_START_DATE = x.PZRASPE_START_DATE;
                ppcd._PZRASPE_DATA_ORIGIN = x.PZRASPE_DATA_ORIGIN;
                ppcd._PZRASPE_USER = x.PZRASPE_USER;

                //Datos Persona
                Models.Persona per = new Models.Persona();
                //per.PZBPRSO_ID = x.PZBPRSO.PZBPRSO_ID;
                //per.PZBPRSO_NAME = x.PZBPRSO.PZBPRSO_NAME;
                per._PZBPRSO_PIDM = x.PZBPRSO.PZBPRSO_PIDM;
                per._PZBPRSO_SITE = x.PZBPRSO.PZBPRSO_SITE;
                per._PZBPRSO_USER = x.PZBPRSO.PZBPRSO_USER;
                per._PZBPRSO_PAYSHEET = x.PZBPRSO.PZBPRSO_PAYSHEET;
                per._PZBPRSO_DATA_ORIGIN = x.PZBPRSO.PZBPRSO_DATA_ORIGIN;
                per._PZBPRSO_ACTIVE = x.PZBPRSO.PZBPRSO_ACTIVE;
                //per.PZBPRSO_PZVPOST = x.PZBPRSO.PZBPRSO_PZCRCOD;

                ppcd._PZBPRSO = per;


                //Datos Perfil
                Models.Perfil prf = new Models.Perfil();
                //prf.PZVPFLE_ID = x.PZVPFLE.PZVPFLE_ID;
                prf._PZVPFLE_CODE = x.PZVPFLE.PZVPFLE_CODE;
                prf._PZVPFLE_NAME = x.PZVPFLE.PZVPFLE_NAME;
                prf._PZVPFLE_USER = x.PZRASPE_USER;
                prf._PZVPFLE_DATA_ORIGIN = x.PZRASPE_DATA_ORIGIN;
                prf._PZVPFLE_ACTIVITY_DATE = x.PZVPFLE.PZVPFLE_ACTIVITY_DATE;
                ppcd._PZVPFLE = prf;
                

                Models.AsignacionCargoDependencia cd = new Models.AsignacionCargoDependencia();

                //Datos de la Ternaria   CARGO-DEPARTAMENTO
                cd._PZRDYPT_POST_CODE = x.PZRDYPT.PZRDYPT_POST_CODE;
                cd._PZRDYPT_DNCY_CODE= x.PZRDYPT.PZRDYPT_DNCY_CODE;
                //cd.PZRDYPT_ID = x.PZRDYPT.PZRDYPT_ID;
                cd._PZRDYPT_USER = x.PZRDYPT.PZRDYPT_USER;
                cd._PZRDYPT_DATA_ORIGIN = x.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                cd._PZRDYPT_ACTIVITY_DATE = x.PZRDYPT.PZRDYPT_ACTIVITY_DATE;
                ppcd._PZRDYPT = cd;

                //Datos Cargo
                Models.Cargo car = new Models.Cargo();

                //car.PZVPOST_ID = x.PZRDYPT.PZVPOST.PZVPOST_ID;
                car._PZVPOST_CODE = x.PZRDYPT.PZVPOST.PZVPOST_CODE;
                car._PZVPOST_NAME = x.PZRDYPT.PZVPOST.PZVPOST_NAME;
                car._PZVPOST_CODE_SUP = x.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                car._PZVPOST_USER = x.PZRDYPT.PZVPOST.PZVPOST_USER;
                car._PZVPOST_DESCRIPTION = x.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                car._PZVPOST_ACTIVITY_DATE = x.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;
                ppcd._PZRDYPT._PZVPOST = car;

                //Datos Departamento
                Models.Dependencia dep = new Models.Dependencia();
                //dep.PZVDNCY_ID = x.PZRDYPT.PZVDNCY.PZVDNCY_ID;
                dep._PZVDNCY_CODE = x.PZRDYPT.PZVDNCY.PZVDNCY_CODE;
                dep._PZVDNCY_NAME = x.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                dep._PZVDNCY_KEY = x.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
                dep._PZVDNCY_LEVEL = x.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
                dep._PZVDNCY_SITE = x.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
                dep._PZVDNCY_CODE_SUP = x.PZRDYPT.PZVDNCY.PZVDNCY_CODE_SUP;
                dep._PZVDNCY_USER = x.PZRDYPT.PZVDNCY.PZVDNCY_USER;
                dep._PZVDNCY_DATA_ORIGIN = x.PZRDYPT.PZVDNCY.PZVDNCY_DATA_ORIGIN;
                dep._PZVDNCY_ACTIVITY_DATE = x.PZRDYPT.PZVDNCY.PZVDNCY_ACTIVITY_DATE;
                ppcd._PZRDYPT._PZVDNCY = dep;



                result.Add(ppcd);

            }

            // DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(PZRDYPT));

            //return stream1;
            return Ok(result);
        }

        /// <summary>
        /// Carga un perfil especifico
        /// 
        /// </summary>
        /// <param name="_post"></param>
        /// <param name="_dncy"></param>
        /// <param name="_pfle"></param>
        /// <param name="_pidm"></param>
        /// <param name="pZVLDEF"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(Models.AsignacionCargoPersona))]
        [Route("api/PZRASPE/GetPZRASPEId/{_post}/{_dncy}/{_pfle}/{_pidm}/")]
        public IHttpActionResult GetPZRASPEId(String _post, String _dncy,
                                              String _pfle, String _pidm
                                              )
        {
            List<Models.AsignacionCargoPersona> result = new List<Models.AsignacionCargoPersona>();


            db.PZRASPE.Include(per => per.PZBPRSO).Include(prf => prf.PZVPFLE).Include(cardep => cardep.PZRDYPT).Include(car => car.PZRDYPT.PZVPOST).Include(dep => dep.PZRDYPT.PZVDNCY);
            var perper = db.PZRASPE.SqlQuery(@"SELECT * 
                                                FROM PZBPRSO, 
                                                     PZVPOST,
                                                     PZVPFLE,
                                                     PZRASPE,
                                                     PZRDYPT,
                                                     PZVDNCY
                                                WHERE PZRASPE.PZRASPE_PFLE_CODE = PZVPFLE.PZVPFLE_code
                                                AND pzraspe.pzraspe_pidm = PZBPRSO.PZBPRSO_PIDM 
                                                AND pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                AND pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                and PZRDYPT.PZRDYPT_POST_CODE = PZVPOST.PZVPOST_CODE
                                                and PZRDYPT.PZRDYPT_DNCY_CODE = PZVDNCY.PZVDNCY_CODE
                                                   AND PZBPRSO.PZBPRSO_PIDM = '" + _pidm + "'" +
                                                 " AND pzvpost.pzvpost_code = '"+_post+"'" +
                                                 " AND pzvdncy.pzvdncy_code = '"+_dncy+"'" +
                                                 " AND pzvpfle.PZVPFLE_CODE = '"+_pfle+"' ").ToList();

            foreach (var x in perper)
            {
                Models.AsignacionCargoPersona ppcd = new Models.AsignacionCargoPersona();
                //   ppcd.PZRASPE_ID = x;
                ppcd._PZRASPE_DNCY_CODE = x.PZRASPE_DNCY_CODE;
                ppcd._PZRASPE_POST_CODE = x.PZRASPE_POST_CODE;
                ppcd._PZRASPE_PIDM = x.PZRASPE_PIDM;
                ppcd._PZRASPE_PFLE_CODE = x.PZRASPE_PFLE_CODE;
                ppcd._PZRASPE_END_DATE = x.PZRASPE_END_DATE;
                ppcd._PZRASPE_START_DATE = x.PZRASPE_START_DATE;
                ppcd._PZRASPE_DATA_ORIGIN = x.PZRASPE_DATA_ORIGIN;
                ppcd._PZRASPE_USER = x.PZRASPE_USER;

                //Datos Persona
                Models.Persona per = new Models.Persona();
                //per.PZBPRSO_ID = x.PZBPRSO.PZBPRSO_ID;
                //per.PZBPRSO_NAME = x.PZBPRSO.PZBPRSO_NAME;
                per._PZBPRSO_PIDM = x.PZBPRSO.PZBPRSO_PIDM;
                per._PZBPRSO_SITE = x.PZBPRSO.PZBPRSO_SITE;
                per._PZBPRSO_USER = x.PZBPRSO.PZBPRSO_USER;
                per._PZBPRSO_PAYSHEET = x.PZBPRSO.PZBPRSO_PAYSHEET;
                per._PZBPRSO_DATA_ORIGIN = x.PZBPRSO.PZBPRSO_DATA_ORIGIN;
                per._PZBPRSO_ACTIVE = x.PZBPRSO.PZBPRSO_ACTIVE;
                //per.PZBPRSO_PZVPOST = x.PZBPRSO.PZBPRSO_PZCRCOD;

                ppcd._PZBPRSO = per;


                //Datos Perfil
                Models.Perfil prf = new Models.Perfil();
                //prf.PZVPFLE_ID = x.PZVPFLE.PZVPFLE_ID;
                prf._PZVPFLE_CODE = x.PZVPFLE.PZVPFLE_CODE;
                prf._PZVPFLE_NAME = x.PZVPFLE.PZVPFLE_NAME;
                prf._PZVPFLE_USER = x.PZRASPE_USER;
                prf._PZVPFLE_DATA_ORIGIN = x.PZRASPE_DATA_ORIGIN;
                prf._PZVPFLE_ACTIVITY_DATE = x.PZVPFLE.PZVPFLE_ACTIVITY_DATE;
                ppcd._PZVPFLE = prf;


                Models.AsignacionCargoDependencia cd = new Models.AsignacionCargoDependencia();

                //Datos de la Ternaria   CARGO-DEPARTAMENTO
                cd._PZRDYPT_POST_CODE = x.PZRDYPT.PZRDYPT_POST_CODE;
                cd._PZRDYPT_DNCY_CODE = x.PZRDYPT.PZRDYPT_DNCY_CODE;
                //cd.PZRDYPT_ID = x.PZRDYPT.PZRDYPT_ID;
                cd._PZRDYPT_USER = x.PZRDYPT.PZRDYPT_USER;
                cd._PZRDYPT_DATA_ORIGIN = x.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                cd._PZRDYPT_ACTIVITY_DATE = x.PZRDYPT.PZRDYPT_ACTIVITY_DATE;
                ppcd._PZRDYPT = cd;

                //Datos Cargo
                Models.Cargo car = new Models.Cargo();

                //car.PZVPOST_ID = x.PZRDYPT.PZVPOST.PZVPOST_ID;
                car._PZVPOST_CODE = x.PZRDYPT.PZVPOST.PZVPOST_CODE;
                car._PZVPOST_NAME = x.PZRDYPT.PZVPOST.PZVPOST_NAME;
                car._PZVPOST_CODE_SUP = x.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                car._PZVPOST_USER = x.PZRDYPT.PZVPOST.PZVPOST_USER;
                car._PZVPOST_DESCRIPTION = x.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                car._PZVPOST_ACTIVITY_DATE = x.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;
                ppcd._PZRDYPT._PZVPOST = car;

                //Datos Departamento
                Models.Dependencia dep = new Models.Dependencia();
                //dep.PZVDNCY_ID = x.PZRDYPT.PZVDNCY.PZVDNCY_ID;
                dep._PZVDNCY_CODE = x.PZRDYPT.PZVDNCY.PZVDNCY_CODE;
                dep._PZVDNCY_NAME = x.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                dep._PZVDNCY_KEY = x.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
                dep._PZVDNCY_LEVEL = x.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
                dep._PZVDNCY_SITE = x.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
                dep._PZVDNCY_CODE_SUP = x.PZRDYPT.PZVDNCY.PZVDNCY_CODE_SUP;
                dep._PZVDNCY_USER = x.PZRDYPT.PZVDNCY.PZVDNCY_USER;
                dep._PZVDNCY_DATA_ORIGIN = x.PZRDYPT.PZVDNCY.PZVDNCY_DATA_ORIGIN;
                dep._PZVDNCY_ACTIVITY_DATE = x.PZRDYPT.PZVDNCY.PZVDNCY_ACTIVITY_DATE;
                ppcd._PZRDYPT._PZVDNCY = dep;



                result.Add(ppcd);

            }

            // DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(PZRDYPT));

            //return stream1;
            return Ok(result);
        }
        /// <summary>
        /// 
        /// Dado el PIDM del supervisor Carga las depndencias asociadas a el mismo
        /// 
        /// </summary>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Dependencia>))]
        [Route("api/PZRASPE/GetPZRASPEDepEva/{_pidm}/")]
        public IHttpActionResult GetPZRASPEDepEva(String _pidm) {

            List<Models.Dependencia> result = new List<Models.Dependencia>();


            var perper = db.PZVDNCY.SqlQuery(@"select DISTINCT  PZVDNCY.*--LEVEL,PZVPOST.*,pzbprso.*,PZVDNCY.*,PZVPFLE.*
                                                from pzvpost 
                                                    INNER JOIN PZRDYPT ON pzvpost.pzvpost_code = pzrdypt.pzrdypt_post_code
                                                    INNER JOIN PZVDNCY ON pzrdypt.pzrdypt_dncy_code = pzvdncy.pzvdncy_code
                                                    INNER JOIN PZRASPE ON pzrdypt.pzrdypt_post_code = pzraspe.pzraspe_post_code
                                                                      AND pzrdypt.pzrdypt_dncy_code  = pzraspe.pzraspe_dncy_code
                                                    INNER JOIN PZBPRSO ON pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm
                                                    INNER JOIN PZVPFLE ON pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code    
                                                    INNER JOIN PZRPLAP ON pzraspe.pzraspe_pidm = pzrplap.pzrplap_pidm
                                                                       AND pzraspe.pzraspe_dncy_code = pzrplap.pzrplap_dncy_code
                                                                       AND pzraspe.pzraspe_post_code = pzrplap.pzrplap_post_code
                                                                       AND pzraspe.pzraspe_pfle_code = pzrplap.pzrplap_pfle_code
                                                                       and pzrplap.pzrplap_status ='P'
                                                    start WITH  pzbprso.pzbprso_pidm='"+_pidm+"'"+ 
                                                    " CONNECT BY NOCYCLE PRIOR pzvdncy.pzvdncy_code=pzvdncy.pzvdncy_code_sup "+
                                                    " ORDER BY pzvdncy.pzvdncy_code").ToList();

            foreach (var x in perper)
            {
               
                //Datos Departamento
                Models.Dependencia dep = new Models.Dependencia();
                dep._PZVDNCY_CODE = x.PZVDNCY_CODE;
                dep._PZVDNCY_NAME = x.PZVDNCY_NAME;
                dep._PZVDNCY_KEY = x.PZVDNCY_KEY;
                dep._PZVDNCY_LEVEL = x.PZVDNCY_LEVEL;
                dep._PZVDNCY_SITE = x.PZVDNCY_SITE;
                dep._PZVDNCY_CODE_SUP = x.PZVDNCY_CODE_SUP;
                dep._PZVDNCY_USER = x.PZVDNCY_USER;
                dep._PZVDNCY_DATA_ORIGIN = x.PZVDNCY_DATA_ORIGIN;
                dep._PZVDNCY_ACTIVITY_DATE = x.PZVDNCY_ACTIVITY_DATE;

                result.Add(dep);

            }


            return Ok(result);
        }


        /// <summary>
        /// 
        /// Obtiene los datos de los evaluados dado el pidm del evaluador
        /// 
        /// </summary>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.AsignacionCargoPersona>))]
        [Route("api/PZRASPE/GetPZRASPE/{_pidm}")]
        public IHttpActionResult GetPZRASPE(string _pidm)
        {
            List<Models.Persona> result = new List<Models.Persona>();


            /*     db.PZRASPE.Include(per => per.PZBPRSO)
                     .Include(prf => prf.PZVPFLE)
                     .Include(cardep => cardep.PZRDYPT)
                     .Include(car => car.PZRDYPT.PZVPOST)
                     .Include(dep => dep.PZRDYPT.PZVDNCY);*/

            /* 
             * LO MISMO PERO EN LUGAR DE PIDM CARGO
             * 
             * select PZBPRSO.*,PZVPOST.*
                                                from pzvpost
                                                    INNER JOIN PZRDYPT ON pzvpost.pzvpost_code = pzrdypt.pzrdypt_post_code
                                                    INNER JOIN PZVDNCY ON pzrdypt.pzrdypt_dncy_code = pzvdncy.pzvdncy_code
                                                    INNER JOIN PZRASPE ON pzrdypt.pzrdypt_post_code = pzraspe.pzraspe_post_code
                                                                      AND pzrdypt.pzrdypt_dncy_code = pzraspe.pzraspe_dncy_code
                                                    INNER JOIN PZBPRSO ON pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm
                                                    INNER JOIN PZVPFLE ON pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                    start WITH  pzvpost.pzvpost_code ='ADDITI'--pzbprso.pzbprso_pidm = '71285'
                                                     CONNECT BY NOCYCLE PRIOR pzvpost.pzvpost_code = pzvpost.pzvpost_code_sup 
                                                     ORDER BY pzvdncy.pzvdncy_code
             */

            var perper = db.PZBPRSO.SqlQuery(@"select PZBPRSO.*
                                                from pzvpost
                                                    INNER JOIN PZRDYPT ON pzvpost.pzvpost_code = pzrdypt.pzrdypt_post_code
                                                    INNER JOIN PZVDNCY ON pzrdypt.pzrdypt_dncy_code = pzvdncy.pzvdncy_code
                                                    INNER JOIN PZRASPE ON pzrdypt.pzrdypt_post_code = pzraspe.pzraspe_post_code
                                                                      AND pzrdypt.pzrdypt_dncy_code = pzraspe.pzraspe_dncy_code
                                                    INNER JOIN PZBPRSO ON pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm
                                                    INNER JOIN PZVPFLE ON pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                    start WITH  pzbprso.pzbprso_pidm = '71285'
                                                     CONNECT BY NOCYCLE PRIOR pzvpost.pzvpost_code = pzvpost.pzvpost_code_sup 
                                                     ORDER BY pzvdncy.pzvdncy_code").ToList();
          foreach (var x in perper)
          {



              //Datos Persona
              Models.Persona per = new Models.Persona();
              per._PZBPRSO_PIDM = x.PZBPRSO_PIDM;
              per._PZBPRSO_SITE = x.PZBPRSO_SITE;
              per._PZBPRSO_USER = x.PZBPRSO_USER;
              per._PZBPRSO_PAYSHEET = x.PZBPRSO_PAYSHEET;
              per._PZBPRSO_DATA_ORIGIN = x.PZBPRSO_DATA_ORIGIN;
              per._PZBPRSO_ACTIVE = x.PZBPRSO_ACTIVE;




              result.Add(per);

          }

          // DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(PZRDYPT));

          //return stream1;
          return Ok(result);
      }

        /// <summary>
      /// Modifica el perfil asignado a la person
      /// </summary>
      /// <param name="_post"></param>
      /// <param name="_dncy"></param>
      /// <param name="_pfle"></param>
      /// <param name="_pidm"></param>
      /// <param name="pZVLDEF"></param>
      /// <returns></returns>
        [HttpPut]
        [Route("api/PZRASPE/PutPZRASPE/{_post}/{_dncy}/{_pfle}/{_pidm}/")]
        [ResponseType(typeof(Models.AsignacionCargoPersona))]
        public IHttpActionResult PutPZRASPE(String _post,String _dncy,
                                            String _pfle,String _pidm,
                                            Models.AsignacionCargoPersona pZVLDEF)
      {
          if (!ModelState.IsValid)
          {
              return BadRequest(ModelState);
          }



          try
          {
              DateTime fecha = DateTime.Today;
              pZVLDEF._PZRASPE_ACTIVITY_DATE = fecha;

              PZRASPE per_per = new PZRASPE();


              per_per.PZRASPE_PFLE_CODE = pZVLDEF._PZRASPE_PFLE_CODE;
              per_per.PZRASPE_DNCY_CODE = pZVLDEF._PZRASPE_DNCY_CODE;
              per_per.PZRASPE_POST_CODE = pZVLDEF._PZRASPE_POST_CODE;
              per_per.PZRASPE_PIDM = pZVLDEF._PZRASPE_PIDM;
              per_per.PZRASPE_USER = pZVLDEF._PZRASPE_USER;
              per_per.PZRASPE_START_DATE = pZVLDEF._PZRASPE_START_DATE;
              per_per.PZRASPE_END_DATE = pZVLDEF._PZRASPE_END_DATE;
              per_per.PZRASPE_DATA_ORIGIN = pZVLDEF._PZRASPE_DATA_ORIGIN;
              per_per.PZRASPE_ACTIVITY_DATE = pZVLDEF._PZRASPE_ACTIVITY_DATE;

              db.PZRASPE.Attach(per_per);
              db.Entry(per_per).State = EntityState.Modified;
              db.SaveChanges();

              return Ok("Registro Modificado Exitosamente");
          }
          catch (DbUpdateConcurrencyException)
          {

          }

          return StatusCode(HttpStatusCode.NoContent);
      }

        // POST: api/PZRASPE
        [ResponseType(typeof(Models.AsignacionCargoPersona))]
        public IHttpActionResult PostPZRASPE(Models.AsignacionCargoPersona pZVLDEF)
      {
          if (!ModelState.IsValid)
          {
              return BadRequest(ModelState);
          }

          try
          {
              DateTime fecha = DateTime.Today;
              pZVLDEF._PZRASPE_ACTIVITY_DATE = fecha;

              PZRASPE per_per = new PZRASPE();


              per_per.PZRASPE_PFLE_CODE = pZVLDEF._PZRASPE_PFLE_CODE;
              per_per.PZRASPE_DNCY_CODE = pZVLDEF._PZRASPE_DNCY_CODE;
              per_per.PZRASPE_POST_CODE = pZVLDEF._PZRASPE_POST_CODE;
              per_per.PZRASPE_PIDM = pZVLDEF._PZRASPE_PIDM;
              per_per.PZRASPE_USER = pZVLDEF._PZRASPE_USER;
              per_per.PZRASPE_START_DATE = pZVLDEF._PZRASPE_START_DATE;
              per_per.PZRASPE_END_DATE = pZVLDEF._PZRASPE_END_DATE;
              per_per.PZRASPE_DATA_ORIGIN = pZVLDEF._PZRASPE_DATA_ORIGIN;
              per_per.PZRASPE_ACTIVITY_DATE = pZVLDEF._PZRASPE_ACTIVITY_DATE;

              db.PZRASPE.Add(per_per);

              db.SaveChanges();
          }
          catch (DbUpdateException)
          {
            /*  if (PZRASPEExists(pZVLDEF.PZRASPE_POST_CODE) ||)
              {
                  return Conflict();
              }
              else
              {
                  throw;
              }*/
        }

            return CreatedAtRoute("DefaultApi", new { id = pZVLDEF }, pZVLDEF);
        }

        // DELETE: api/PZRASPE/5
        [ResponseType(typeof(PZRASPE))]
        public IHttpActionResult DeletePZRASPE(decimal id)
        {
            PZRASPE pZVLDEF = db.PZRASPE.Find(id);
            if (pZVLDEF == null)
            {
                return NotFound();
            }

            db.PZRASPE.Remove(pZVLDEF);
            db.SaveChanges();

            return Ok(pZVLDEF);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZRASPEExists(decimal id)
        {
            return db.PZRASPE.Count(/*e => e.PZRASPE_ID == id*/) > 0;
        }
    }
}