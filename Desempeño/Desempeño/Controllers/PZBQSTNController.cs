﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZBQSTNController : ApiController
    {
        private Entities db = new Entities();
        private Repository.ValidatorRepository _v = new Repository.ValidatorRepository();

        /// <summary>
        /// 
        /// Lista las preguntas del sistema 
        /// RUTA: api/PZBQSTN/GetPZBQSTN
        /// 
        /// </summary>
        /// <returns></returns>
        [ResponseTypeAttribute(typeof(List<Models.Pregunta>))]
        public IHttpActionResult GetPZBQSTN()
        {
            List<Models.Pregunta> re = new List<Models.Pregunta>();

            try
            {

                var es = db.PZBQSTN.SqlQuery(@"select * FROM PZBQSTN ").ToList();

                foreach (var result in es)
                {
                    Models.Pregunta item = new Models.Pregunta();

                    item._PZBQSTN_CODE = result.PZBQSTN_CODE;
                    item._PZBQSTN_NAME = result.PZBQSTN_NAME;
                    item._PZBQSTN_USER = result.PZBQSTN_USER;
                    item._PZBQSTN_DESCRIPTION = result.PZBQSTN_DESCRIPTION;
                    item._PZBQSTN_DATA_ORIGIN = result.PZBQSTN_DATA_ORIGIN;
                    item._PZBQSTN_ACTIVITY_DATE = DateTime.Today;
                   
                    


                    re.Add(item);
                }

            }
            catch (Exception e)
            {
                return NotFound();
            }

            return Ok(re);

        }
        /// <summary>
        /// Consulta sobre la pregunta de acuerdo a su codigo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/PZBQSTN/5
        [ResponseType(typeof(PZBQSTN))]
        public IHttpActionResult GetPZBQSTNs(decimal id)
        {
            PZBQSTN pZRQDEF = db.PZBQSTN.Find(id);
            if (pZRQDEF == null)
            {
                return NotFound();
            }

            return Ok(pZRQDEF);
        }




        /// <summary>
        /// 
        /// Agrega preguntas al sistema
        /// Ruta:api/PZBQSTN/PostPZBQSTN
        /// 
        /// </summary>
        /// <param name="pZRQDEF"></param>
        /// <returns>201 Created</returns>
        [ResponseType(typeof(Models.Pregunta))]
        public IHttpActionResult PostPZBQSTN(Models.Pregunta pZRQDEF)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {

           //     Decimal es = (Decimal)db.PZBQSTN.Max(x => x.);
           //    es = es + 1;

                DateTime fecha = DateTime.Today;


                pZRQDEF._PZBQSTN_ACTIVITY_DATE = fecha;
          //      pZRQDEF.PZBQSTN_CODE = es;

                PZBQSTN objPreg = new PZBQSTN();
                objPreg.PZBQSTN_CODE = pZRQDEF._PZBQSTN_CODE;
                //objPreg.PZBQSTN_DESCRIPTION = pZRQDEF._PZBQSTN_VALUE;
                objPreg.PZBQSTN_USER = pZRQDEF._PZBQSTN_USER;
                objPreg.PZBQSTN_NAME = pZRQDEF._PZBQSTN_NAME;
                objPreg.PZBQSTN_DESCRIPTION = pZRQDEF._PZBQSTN_DESCRIPTION;
                objPreg.PZBQSTN_DATA_ORIGIN = pZRQDEF._PZBQSTN_DATA_ORIGIN;
                objPreg.PZBQSTN_ACTIVITY_DATE = pZRQDEF._PZBQSTN_ACTIVITY_DATE;
              //  objPreg.PZBQSTN_PROFICIENCY = pZRQDEF.PZBQSTN_COMPETENCIA;


                db.PZBQSTN.Add(objPreg);


                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PZBQSTNExists(pZRQDEF._PZBQSTN_CODE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pZRQDEF._PZBQSTN_CODE }, pZRQDEF);
        }

        /// <summary>
        /// Actualiza una pregunta, dado el codigo de la pregunta
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pZRQDEF"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/PZBQSTN/PutPZBQSTN/{id}/")]
        [ResponseType(typeof(Pregunta))]
        public IHttpActionResult PutPZBQSTN(string id, Pregunta pZRQDEF)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pZRQDEF._PZBQSTN_CODE)
            {
                return BadRequest();
            }



            try
            {

                String valid = _v.validarComponentesInstrumentos(id);
                if (valid == "CAMPO ASIGNADO")
                {

                    return BadRequest("Este campo no se puede editar porque ya fue asignado");

                }

                DateTime fecha = DateTime.Today;
                pZRQDEF._PZBQSTN_ACTIVITY_DATE = fecha;

                PZBQSTN objPreg = new PZBQSTN();
                objPreg.PZBQSTN_CODE = pZRQDEF._PZBQSTN_CODE;
                objPreg.PZBQSTN_NAME = pZRQDEF._PZBQSTN_NAME;
                objPreg.PZBQSTN_USER = pZRQDEF._PZBQSTN_USER;
                objPreg.PZBQSTN_DESCRIPTION = pZRQDEF._PZBQSTN_DESCRIPTION;
                objPreg.PZBQSTN_DATA_ORIGIN = pZRQDEF._PZBQSTN_DATA_ORIGIN;
                objPreg.PZBQSTN_ACTIVITY_DATE = pZRQDEF._PZBQSTN_ACTIVITY_DATE;
                //objPreg.PZBQSTN_PROFICIENCY = pZRQDEF.PZBQSTN_PROFICIENCY;


                db.PZBQSTN.Attach(objPreg);
                System.Diagnostics.Debug.WriteLine(objPreg);
                db.Entry(objPreg).State = EntityState.Modified;
                System.Diagnostics.Debug.WriteLine((db.Entry(objPreg).State = EntityState.Modified));
                db.SaveChanges();


                return Ok("Registro Modificado Exitosamente");

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PZBQSTNExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(pZRQDEF);
        }



        // DELETE: api/PZBQSTN/5
        [ResponseType(typeof(PZBQSTN))]
        public IHttpActionResult DeletePZBQSTN(decimal id)
        {
            PZBQSTN pZRQDEF = db.PZBQSTN.Find(id);
            if (pZRQDEF == null)
            {
                return NotFound();
            }

            db.PZBQSTN.Remove(pZRQDEF);
            db.SaveChanges();

            return Ok(pZRQDEF);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZBQSTNExists(String id)
        {
            return db.PZBQSTN.Count(e => e.PZBQSTN_CODE == id) > 0;
        }
    }
}