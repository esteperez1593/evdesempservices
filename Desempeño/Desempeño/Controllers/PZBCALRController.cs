﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZBCALRController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/PZBCALR
        /// <summary>
        /// 
        /// Obtiene la lista de calendarios registrados
        /// RUTA : api/PZBCALR/GetPZBCALR
        /// 
        /// </summary>
        /// <returns>List<Models.Calendario></returns
        [HttpGet]
        [Route("api/PZBCALR/GetPZBCALR/")]
        [ResponseType(typeof(List<Models.Calendario>))]
        public IHttpActionResult GetPZBCALR()
        {
            List<Models.Calendario> result = new List<Models.Calendario>();
            try
            {

                //db.PZBCALR.Include(inscal => inscal.PZRPLAP).Include(calfas => calfas.PZRCRPH);
                var res = db.PZBCALR.SqlQuery(@"Select * from PZBCALR").ToList();

                foreach (var X in res) {

                    //Datos del Calendario
                    Models.Calendario cal = new Models.Calendario();
                  //  cal.PZBCALR_ID = X.PZBCALR_ID;
                    cal._PZBCALR_CODE = X.PZBCALR_CODE;
                    cal._PZBCALR_NAME = X.PZBCALR_NAME;
                    cal._PZBCALR_USER = X.PZBCALR_USER;
                    cal._PZBCALR_TERM = X.PZBCALR_TERM;
                    cal._PZBCALR_SRL = X.PZBCALR_SRL;
                    cal._PZBCALR_END_DATE = X.PZBCALR_END_DATE;
                    cal._PZBCALR_INIT_DATE = X.PZBCALR_INIT_DATE;
                    cal._PZBCALR_DATA_ORIGIN = X.PZBCALR_DATA_ORIGIN;
                    cal._PZBCALR_ACTIVITY_DATE = X.PZBCALR_ACTIVITY_DATE;
                    //cal._PZBCALR_POLL_CODE = X.PZBCALR_POLL_CODE;
                    result.Add(cal);

                }

            }
            catch (Exception e)
            {

            }

            return Ok(result);
        }

   
        /// <summary>
        /// 
        /// Modifica los Calendarios
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pZRCDEF"></param>
        /// <returns></returns>
        // PUT: api/PZBCALR/5
        [HttpPut]
        [Route("api/PZBCALR/PutPZBCALR/{id}")]
        [ResponseType(typeof(Calendario))]
        public IHttpActionResult PutPZBCALR(string id, Calendario pZRCDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

         
            
            try
            {
                DateTime fecha = DateTime.Today;

                pZRCDEF._PZBCALR_ACTIVITY_DATE = fecha;

                PZBCALR calendario = new PZBCALR();

                //calendario.PZBCALR_ID = pZRCDEF.PZBCALR_ID;
                calendario.PZBCALR_USER = pZRCDEF._PZBCALR_USER;
                calendario.PZBCALR_CODE = pZRCDEF._PZBCALR_CODE;
               // calendario.PZBCALR_POLL_CODE = pZRCDEF.PZBCALR_POLL_CODE;
                calendario.PZBCALR_SRL = pZRCDEF._PZBCALR_SRL;
                calendario.PZBCALR_NAME = pZRCDEF._PZBCALR_NAME;
                calendario.PZBCALR_TERM = pZRCDEF._PZBCALR_TERM;
                calendario.PZBCALR_INIT_DATE = pZRCDEF._PZBCALR_INIT_DATE;
                calendario.PZBCALR_END_DATE = pZRCDEF._PZBCALR_END_DATE;
                calendario.PZBCALR_DATA_ORIGIN = pZRCDEF._PZBCALR_DATA_ORIGIN;
                calendario.PZBCALR_ACTIVITY_DATE = pZRCDEF._PZBCALR_ACTIVITY_DATE;

                db.PZBCALR.Attach(calendario);
                db.Entry(calendario).State = EntityState.Modified;
                db.SaveChanges();

                return Ok("El calendario "+calendario.PZBCALR_NAME+" del periodo "+calendario.PZBCALR_TERM+" ha sido modificado exitosamente");

            }
            catch (DbUpdateConcurrencyException)
            {
               
                    return NotFound();
          
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PZBCALR
        /// <summary>
        /// 
        /// Inserta un nuevo calendario
        /// Ruta : api/PZBCALR/PostPZBCALR
        /// 
        /// </summary>
        /// <param name="pZRCDEF"></param>
        /// <returns></returns>
        [ResponseType(typeof(Calendario))]
        public IHttpActionResult PostPZBCALR(Models.Calendario pZRCDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (db.PZBCALR.Count(e => e.PZBCALR_CODE == pZRCDEF._PZBCALR_CODE) > 0)
            {

                return BadRequest("El codigo " + pZRCDEF._PZBCALR_CODE + " ya existe.");
            }


            try
            {
               //Decimal es = (Decimal)db.PZBCALR.Max(x => x.PZBCALR_ID);


               //es = es + 1;

                DateTime fecha = DateTime.Today;

                PZBCALR cal = new PZBCALR();
               // cal.PZBCALR_ID = es;
                cal.PZBCALR_CODE = pZRCDEF._PZBCALR_CODE;
                cal.PZBCALR_NAME = pZRCDEF._PZBCALR_NAME;
                cal.PZBCALR_SRL = pZRCDEF._PZBCALR_SRL;
                cal.PZBCALR_TERM = pZRCDEF._PZBCALR_TERM;
                cal.PZBCALR_END_DATE = pZRCDEF._PZBCALR_END_DATE;
                cal.PZBCALR_INIT_DATE = pZRCDEF._PZBCALR_INIT_DATE;
                cal.PZBCALR_DATA_ORIGIN = pZRCDEF._PZBCALR_DATA_ORIGIN;
                cal.PZBCALR_ACTIVITY_DATE = fecha;
                cal.PZBCALR_USER = pZRCDEF._PZBCALR_USER;
               // cal.PZBCALR_POLL_CODE = pZRCDEF._PZBCALR_POLL_CODE;

                db.PZBCALR.Add(cal);

                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PZBCALRExists(pZRCDEF._PZBCALR_CODE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pZRCDEF._PZBCALR_CODE }, pZRCDEF);
        }

        // DELETE: api/PZBCALR/5
        [ResponseType(typeof(PZBCALR))]
        public IHttpActionResult DeletePZBCALR(decimal id)
        {
            PZBCALR pZRCDEF = db.PZBCALR.Find(id);
            if (pZRCDEF == null)
            {
                return NotFound();
            }

            db.PZBCALR.Remove(pZRCDEF);
            db.SaveChanges();

            return Ok(pZRCDEF);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZBCALRExists(String id)
        {
            return db.PZBCALR.Count(e => e.PZBCALR_CODE == id) > 0;
        }
    }
}