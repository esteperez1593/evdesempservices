﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using System.Data.Entity.Infrastructure;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZVTYAWController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/PZVTYAW
        /// <summary>
        /// 
        /// Obtiene los tipos de Respuesta Disponible
        /// Ruta api/PZVTYAW/GetPZVTYAW
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/PZVTYAW/GetPZVTYAWs/")]
        [ResponseType(typeof(TipoRespuesta))]
        public IHttpActionResult GetPZVTYAWs()
        {

            List<Models.TipoRespuesta> result = new List<Models.TipoRespuesta>();
            try
            {

                db.PZVTYAW.Include(tr => tr.PZBPLQS);
                var res = db.PZVTYAW.SqlQuery(@" SELECT * from PZVTYAW ").ToList();

                foreach (var x in res)
                {

                    //Datos del tipo de Respuesta
                    Models.TipoRespuesta tr = new Models.TipoRespuesta();
                    tr._PZVTYAW_CODE = x.PZVTYAW_CODE;
                    tr._PZVTYAW_DESCRIPTION = x.PZVTYAW_DESCRIPTION;
                    tr._PZVTYAW_ACTIVITY_DATE = x.PZVTYAW_ACTIVITY_DATE;
                    tr._PZVTYAW_DATA_ORIGIN = x.PZVTYAW_DATA_ORIGIN;
                    tr._PZVTYAW_USER = x.PZVTYAW_USER;
                    
                    result.Add(tr);

                }

                return (Ok(result));

            }

            catch (Exception e)
            {

                return (NotFound());

            }


        } 


        // PUT: api/PZVTYAW/5
        /// <summary>
        /// 
        /// Modifica los registros de los tipos de RESPUESTA
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="PZVTYAW"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/PZVTYAW/PutPZVTYAW/{id}/")]
        [ResponseType(typeof(TipoRespuesta))]
        public IHttpActionResult PutPZVTYAW(string id, TipoRespuesta _PZVTYAW)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != _PZVTYAW._PZVTYAW_CODE)
            {
                return BadRequest();
            }



            try
            {
                DateTime fecha = DateTime.Today;

                _PZVTYAW._PZVTYAW_ACTIVITY_DATE = fecha;
                    
                PZVTYAW Trespuesta = new PZVTYAW();

               // Trespuesta.PZVTYAW_ID = PZVTYAW.PZVTYAW_ID;
                Trespuesta.PZVTYAW_USER = _PZVTYAW._PZVTYAW_USER;
                Trespuesta.PZVTYAW_CODE = _PZVTYAW._PZVTYAW_CODE;
                Trespuesta.PZVTYAW_DESCRIPTION = _PZVTYAW._PZVTYAW_DESCRIPTION;
                Trespuesta.PZVTYAW_DATA_ORIGIN = _PZVTYAW._PZVTYAW_DATA_ORIGIN;
                Trespuesta.PZVTYAW_ACTIVITY_DATE = _PZVTYAW._PZVTYAW_ACTIVITY_DATE;

                db.PZVTYAW.Attach(Trespuesta);
                System.Diagnostics.Debug.WriteLine(Trespuesta);
                db.Entry(Trespuesta).State = EntityState.Modified;
                System.Diagnostics.Debug.WriteLine((db.Entry(Trespuesta).State = EntityState.Modified));
                db.SaveChanges();



                return Ok("Tipo de Respuesta: "+Trespuesta.PZVTYAW_DESCRIPTION+" Modificado");

                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PZVTYAWExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PZVTYAW
        /// <summary>
        /// 
        /// Agrega los Tipo de Respuestas Posibles
        /// RUTA: api/PZVTYAW/PostPZVTYAW
        /// 
        /// </summary>
        /// <param name="PZVTYAW"></param>
        /// <returns>Tipo de Respuesta</returns>
        [HttpPost]
        [ResponseType(typeof(Models.TipoRespuesta))]
        [Route("api/PZVTYAW/PostPZVTYAW/")]
        public IHttpActionResult PostPZVTYAW(Models.TipoRespuesta PZVTYAW)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (db.PZVTYAW.Count(e => e.PZVTYAW_CODE == PZVTYAW._PZVTYAW_CODE) > 0)
            {

                return BadRequest("El codigo " + PZVTYAW._PZVTYAW_CODE+ " ya existe.");
            }

            try
            {
               // Decimal es = (Decimal)db.PZVTYAW.Max(x => x.PZVTYAW_ID);


              //  es = es + 1;

                DateTime fecha = DateTime.Today;

                //aqui tendria que enviar el usernaame ademas de lo que esta.

                PZVTYAW._PZVTYAW_ACTIVITY_DATE = fecha;
              //  PZVTYAW.PZVTYAW_ID = es;


                PZVTYAW objTRes = new PZVTYAW();
               // objTRes.PZVTYAW_ID = PZVTYAW.PZVTYAW_ID;
                objTRes.PZVTYAW_CODE = PZVTYAW._PZVTYAW_CODE;
                objTRes.PZVTYAW_DESCRIPTION = PZVTYAW._PZVTYAW_DESCRIPTION;
                objTRes.PZVTYAW_ACTIVITY_DATE = PZVTYAW._PZVTYAW_ACTIVITY_DATE;
                objTRes.PZVTYAW_DATA_ORIGIN = PZVTYAW._PZVTYAW_DATA_ORIGIN;
                objTRes.PZVTYAW_USER = PZVTYAW._PZVTYAW_USER;

                db.PZVTYAW.Add(objTRes);

                db.SaveChanges();

            }
            catch (DbUpdateException)
            {
                if (PZVTYAWExists(PZVTYAW._PZVTYAW_CODE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = PZVTYAW._PZVTYAW_CODE }, PZVTYAW);
        }


        // DELETE: api/PZVTYAW/5
        [ResponseType(typeof(PZVTYAW))]
        public IHttpActionResult DeletePZVTYAW(decimal id)
        {
            PZVTYAW PZVTYAW = db.PZVTYAW.Find(id);
            if (PZVTYAW == null)
            {
                return NotFound();
            }

            db.PZVTYAW.Remove(PZVTYAW);
            db.SaveChanges();

            return Ok(PZVTYAW);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZVTYAWExists(string id)
        {
            return db.PZVTYAW.Count(e => e.PZVTYAW_CODE == id) > 0;
        }
    }
}