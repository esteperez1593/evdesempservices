﻿using Desempeño.DataConect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Desempeño.Controllers
{
    public class ValidatorController:ApiController
    {

        Entities db = new Entities();


        /// <summary>
        /// Funcion que valida si el instrumento ya fue respondido
        /// 
        /// </summary>
        /// <param name="_pidm"></param>
        /// <param name="_poll"></param>
        /// <param name="_calr"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/VALIDATOR/validarStatusPoll/{_pidm}/{_poll}/{_calr}/{id}")]
        public IHttpActionResult validarStatusPoll(string _pidm, string _poll, string _calr, int id)
        {

            Models.AsignacionInstrumentoPersona _plap = new Models.AsignacionInstrumentoPersona();

            string _est = "";

            try
            {

                var res = db.PZRPLAP.SqlQuery(@" Select *  from pzrplap where pzrplap_poll_code = '" + _poll + "' and pzrplap_calr_code = '" + _calr + "' and pzrplap_pidm = '" + _pidm + "'  and PZRPLAP_SEQ_NUMBER = '" + id + "'  ").ToList();

                foreach (var result in res)
                {

                    _plap._PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                    _est = _plap._PZRPLAP_STATUS;
                }

                if (_est == "F")
                {

                    return BadRequest("Este instrumento ya fue completado");

                }
                else
                {
                    return Ok(_plap._PZRPLAP_STATUS );
                }


            }
            catch (Exception e) { }



            {

            }



            return NotFound();

        }
        /// <summary>
        /// Valida si un componente de un instrumento esta asignado
        /// en caso de que el mismo ya se encuentre asignado no se podria 
        /// editar
        /// </summary>
        /// <param name="parametro"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/VALIDATOR/validarComponentesInstrumentos/{parametro}")]
        public IHttpActionResult validarComponentesInstrumentos(string parametro)
        {

            Models.AsignacionInstrumentoPregunta _plap = new Models.AsignacionInstrumentoPregunta();

            string _est = "";

            try
            {

                var res = db.PZBPLQS.SqlQuery(@"SELECT  pzbplqs.* 
                                                FROM pzbplqs,pzbarea
                                                WHERE pzbplqs.pzbplqs_area_code = pzbarea.pzbarea_code
                                                    and pzbarea_code ='" + parametro + "' ")
                         .Union(db.PZBPLQS.SqlQuery(@"SELECT   pzbplqs.* 
                                                            FROM pzbplqs,pzbcrit
                                                            WHERE pzbplqs.pzbplqs_crit_code = pzbcrit.pzbcrit_code
                                                            and pzbcrit.pzbcrit_code ='" + parametro + "'"))
                         .Union(db.PZBPLQS.SqlQuery(@"SELECT  pzbplqs.* 
                                                            FROM pzbplqs,pzbqstn
                                                            where pzbplqs.pzbplqs_qstn_code = pzbqstn.pzbqstn_code
                                                            and pzbqstn.pzbqstn_code ='" + parametro + "'"))
                         .Union(db.PZBPLQS.SqlQuery(@"SELECT  pzbplqs.* 
                                                            FROM pzbplqs,pzbcalr
                                                            where pzbplqs.pzbplqs_calr_code = pzbcalr_code
                                                            and pzbcalr.pzbcalr_code = '" + parametro + "'")).ToList()
                  .Union(db.PZBPLQS.SqlQuery(@"SELECT  pzbplqs.* 
                                                            FROM pzbplqs,pzbpoll
                                                            where pzbplqs.pzbplqs_poll_code = pzbpoll_code
                                                            and pzbpoll.pzbpoll_code = '" + parametro + "'")).ToList();
                if (res.Count() != 0)
                {

                    return BadRequest("CAMPO ASIGNADO");

                }
                else
                {
                    return Ok("OK");

                }

            }
            catch (Exception e) { }
            {

            }

            return NotFound();
        }

    }
}