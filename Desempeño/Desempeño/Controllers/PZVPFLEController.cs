﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZVPFLEController : ApiController
    {
        private Entities db = new Entities();


        /// <summary>
        /// 
        /// Carga los Perfiles Disponibles 
        /// en el sistema
        /// RUTA : api/PZVPFLE/GetPZVPFLE
        /// 
        /// </summary>
        /// <returns>OK(Lista de Perfiles)</returns>
        [HttpGet]
        [ResponseTypeAttribute(typeof(List<Models.Perfil>))]
        [Route("api/PZVPFLE/GetPZVPFLE/")]
        public IHttpActionResult GetPZVPFLE()
        {
            List<Models.Perfil> re = new List<Models.Perfil>();

            try
            {


                var es = db.PZVPFLE.SqlQuery(@"select * FROM PZVPFLE ").ToList();

                foreach (var result in es)
                {
                    Models.Perfil item = new Models.Perfil();

                    //item.PZVPFLE_ID = result.PZVPFLE_ID;
                    item._PZVPFLE_CODE = result.PZVPFLE_CODE;
                    item._PZVPFLE_NAME = result.PZVPFLE_NAME;
                    item._PZVPFLE_ACTIVITY_DATE = result.PZVPFLE_ACTIVITY_DATE;
                    item._PZVPFLE_DATA_ORIGIN = result.PZVPFLE_DATA_ORIGIN;
                    item._PZVPFLE_USER = result.PZVPFLE_USER;
                    

                    re.Add(item);
                }

            }
            catch (Exception e)
            {
                return NotFound();
            }

            return Ok(re);

        }


        // GET: api/PZVPFLE/5
        [ResponseType(typeof(PZVPFLE))]
        public IHttpActionResult GetPZVPFLE(String id)
        {
            PZVPFLE pZRPDEF = db.PZVPFLE.Find(id);
            if (pZRPDEF == null)
            {
                return NotFound();
            }

            return Ok(pZRPDEF);
        }

        // PUT: api/PZVPFLE/5
        /// <summary>
        /// 
        /// Modifica los datos de un perfil registrado
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="_pZRPDEF"></param>
        /// <returns></returns>
        
        [HttpPut]
        [Route("api/PZVPFLE/PutPZVPFLE/{id}")]
        [ResponseType(typeof(Perfil))]
        public IHttpActionResult PutPZVPFLE(String id, Perfil _pZRPDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != _pZRPDEF._PZVPFLE_CODE)
            {
                return BadRequest();
            }


            try
            {
                DateTime fecha = DateTime.Today;

                _pZRPDEF._PZVPFLE_ACTIVITY_DATE = fecha;


                PZVPFLE perfil = new PZVPFLE();

               // perfil.PZVPFLE_ID = pZRPDEF.PZVPFLE_ID;
                perfil.PZVPFLE_NAME = _pZRPDEF._PZVPFLE_NAME;
                perfil.PZVPFLE_CODE = _pZRPDEF._PZVPFLE_CODE;
                perfil.PZVPFLE_ACTIVITY_DATE = fecha;
                perfil.PZVPFLE_DATA_ORIGIN = _pZRPDEF._PZVPFLE_DATA_ORIGIN;
                perfil.PZVPFLE_USER = _pZRPDEF._PZVPFLE_USER;

                db.PZVPFLE.Attach(perfil);
                db.Entry(perfil).State = EntityState.Modified;

                db.SaveChanges();

                return Ok("Se ha modificado el perfil " + perfil.PZVPFLE_NAME + " exitosamente");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PZVPFLEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PZVPFLE
        /// <summary>
        /// 
        /// Agrega perfiles 
        /// Ruta : http://localhost:63665/api/PZVPFLE/PostPZVPFLE
        /// 
        /// </summary>
        /// <param name="_pZRPDEF"></param>
        /// <returns></returns>
        [ResponseType(typeof(List<Models.Perfil>))]
        public IHttpActionResult PostPZVPFLE(Models.Perfil _pZRPDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (db.PZVPFLE.Count(e => e.PZVPFLE_CODE == _pZRPDEF._PZVPFLE_CODE) > 0)
            {

                return BadRequest("El codigo " + _pZRPDEF._PZVPFLE_CODE + " ya existe.");
            }


            try
            {

                //Decimal es = db.PZVPFLE.Max(x => x.PZVPFLE_ID);

                //es = es + 1;

                DateTime fecha = DateTime.Today;

                _pZRPDEF._PZVPFLE_ACTIVITY_DATE = fecha;
                //pZRPDEF.PZVPFLE_ID = es;

                PZVPFLE perfil = new PZVPFLE();

                //perfil.PZVPFLE_ID = pZRPDEF.PZVPFLE_ID;
                perfil.PZVPFLE_CODE = _pZRPDEF._PZVPFLE_CODE;
                perfil.PZVPFLE_NAME = _pZRPDEF._PZVPFLE_NAME;
                perfil.PZVPFLE_ACTIVITY_DATE = fecha;
                perfil.PZVPFLE_DATA_ORIGIN = _pZRPDEF._PZVPFLE_DATA_ORIGIN;
                perfil.PZVPFLE_USER = _pZRPDEF._PZVPFLE_USER;

                db.PZVPFLE.Add(perfil);

                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PZVPFLEExists(_pZRPDEF._PZVPFLE_CODE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = _pZRPDEF._PZVPFLE_CODE }, _pZRPDEF);
        }

        // DELETE: api/PZVPFLE/5
        [ResponseType(typeof(PZVPFLE))]
        public IHttpActionResult DeletePZVPFLE(decimal id)
        {
            PZVPFLE pZRPDEF = db.PZVPFLE.Find(id);
            if (pZRPDEF == null)
            {
                return NotFound();
            }

            db.PZVPFLE.Remove(pZRPDEF);
            db.SaveChanges();

            return Ok(pZRPDEF);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZVPFLEExists(String id)
        {
            return db.PZVPFLE.Count(e => e.PZVPFLE_CODE == id) > 0;
        }
    }
}