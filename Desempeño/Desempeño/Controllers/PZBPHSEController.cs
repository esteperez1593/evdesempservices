﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZBPHSEController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/PZBPHSE

        /// <summary>
        /// 
        /// carga todas las fases registradas en el sistema
        /// Ruta:api/PZBPHSE/GetPZBPHSE
        /// 
        /// </summary>
        /// <returns>[ResponseType(typeof(List<Models.Fase>))]</returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Fase>))]
        [Route("api/PZBPHSE/GetPZBPHSEs/")]
        public IHttpActionResult GetPZBPHSEs()
        {
            List<Models.Fase> result = new List<Models.Fase>();
            try
            {

                db.PZBPHSE.Include(men => men.PZRMSSG);
                var res = db.PZBPHSE.SqlQuery(@"Select * from PZBPHSE").ToList();

                foreach (var x in res)
                {

                    //Datos de la fase del calendario
                    Models.Fase fas = new Models.Fase();
                    fas._PZBPHSE_CODE= x.PZBPHSE_CODE;
                    fas._PZBPHSE_NAME = x.PZBPHSE_NAME;
                    fas._PZBPHSE_USER = x.PZBPHSE_USER;
                    fas._PZBPHSE_OPEN_DATE = x.PZBPHSE_OPEN_DATE;
                    fas._PZBPHSE_CLOSE_DATE = x.PZBPHSE_CLOSE_DATE;
                    fas._PZBPHSE_CALR_CODE = x.PZBPHSE_CALR_CODE;
                    fas._PZBPHSE_OPEXTENSION_DATE = x.PZBPHSE_OPEXTENSION_DATE;
                    fas._PZBPHSE_CLEXTENSION_DATE = x.PZBPHSE_CLEXTENSION_DATE;
                    fas._PZBPHSE_DATA_ORIGIN = x.PZBPHSE_DATA_ORIGIN;
                    fas._PZBPHSE_ACTIVITY_DATE = x.PZBPHSE_ACTIVITY_DATE;


                    result.Add(fas);

                }

            }
            catch (Exception e)
            {

            }

            return Ok(result);
        }

        [ResponseType(typeof(List<Models.Fase>))]
        public IHttpActionResult GetFasMen()
        {
            List<Models.Fase> result = new List<Models.Fase>();
            try
            {

                db.PZBPHSE.Include(men => men.PZRMSSG);
                var res = db.PZBPHSE.SqlQuery(@"Select fas.*,men.* 
                                                from PZBPHSE fas,PZRMSSG men 
                                                WHERE men.PZRMSSG_PHSE_CODE = fas.PZBPHSE_CODE").ToList();

                foreach (var x in res)
                {

                    //Datos de la fase del calendario

                    Models.Fase fas = new Models.Fase();
                    fas._PZBPHSE_CODE = x.PZBPHSE_CODE;
                    fas._PZBPHSE_NAME = x.PZBPHSE_NAME;
                    fas._PZBPHSE_USER = x.PZBPHSE_USER;
                    fas._PZBPHSE_OPEN_DATE = x.PZBPHSE_OPEN_DATE;
                    fas._PZBPHSE_CLOSE_DATE = x.PZBPHSE_CLOSE_DATE;
                    fas._PZBPHSE_CALR_CODE = x.PZBPHSE_CALR_CODE;
                    fas._PZBPHSE_OPEXTENSION_DATE = x.PZBPHSE_OPEXTENSION_DATE;
                    fas._PZBPHSE_CLEXTENSION_DATE = x.PZBPHSE_CLEXTENSION_DATE;
                    fas._PZBPHSE_DATA_ORIGIN = x.PZBPHSE_DATA_ORIGIN;
                    fas._PZBPHSE_ACTIVITY_DATE = x.PZBPHSE_ACTIVITY_DATE;

                    result.Add(fas);

                }

            }
            catch (Exception e)
            {

            }

            return Ok(result);
        }



        // GET: api/PZBPHSE/5
        [ResponseType(typeof(PZBPHSE))]
        public IHttpActionResult GetPZBPHSE(decimal id)
        {
            PZBPHSE pZVTDEF = db.PZBPHSE.Find(id);
            if (pZVTDEF == null)
            {
                return NotFound();
            }

            return Ok(pZVTDEF);
        }
        /// <summary>
        /// 
        /// Modifica los datos de una Fase
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pZVTDEF"></param>
        /// <returns></returns>
        // PUT: api/PZBPHSE/5

        [HttpPut]
        [Route("api/PZBPHSE/PutPZBPHSE/{id}/")]
        [ResponseType(typeof(Fase))]
        public IHttpActionResult PutPZBPHSE(string id, Fase pZVTDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }



            try
            {

                DateTime fecha = DateTime.Today;
                pZVTDEF._PZBPHSE_ACTIVITY_DATE = fecha;

                PZBPHSE fas =new PZBPHSE();
                fas.PZBPHSE_CODE = pZVTDEF._PZBPHSE_CODE;
                fas.PZBPHSE_NAME = pZVTDEF._PZBPHSE_NAME;
                fas.PZBPHSE_USER = pZVTDEF._PZBPHSE_USER;
                fas.PZBPHSE_OPEN_DATE = pZVTDEF._PZBPHSE_OPEN_DATE;
                fas.PZBPHSE_CLOSE_DATE = pZVTDEF._PZBPHSE_CLOSE_DATE;
                fas.PZBPHSE_CALR_CODE = pZVTDEF._PZBPHSE_CALR_CODE;
                fas.PZBPHSE_OPEXTENSION_DATE = pZVTDEF._PZBPHSE_OPEXTENSION_DATE;
                fas.PZBPHSE_CLEXTENSION_DATE = pZVTDEF._PZBPHSE_CLEXTENSION_DATE;
                fas.PZBPHSE_DATA_ORIGIN = pZVTDEF._PZBPHSE_DATA_ORIGIN;
                fas.PZBPHSE_ACTIVITY_DATE = fecha;


                db.PZBPHSE.Attach(fas);
                db.Entry(fas).State = EntityState.Modified;
                db.SaveChanges();

                return Ok("Datos de Fase modificados exitosamente");

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PZBPHSEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        
    }


        /// <summary>
        ///
        /// Agrega fases a un calendario
        /// 
        /// </summary>
        /// <param name="pZVTDEF"></param>
        /// <returns>Models.Fase</returns>
        [ResponseType(typeof(Models.Fase))]
        public IHttpActionResult PostPZBPHSE(Models.Fase pZVTDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }



            try
            {
                //Decimal es = (Decimal)db.PZBPHSE.Max(x => x.PZBPHSE_ID);


                //es = es + 1;

                DateTime fecha = DateTime.Today;

                //Datos de la fase del calendario
                PZBPHSE fas = new PZBPHSE();
                fas.PZBPHSE_CODE = pZVTDEF._PZBPHSE_CODE;
                fas.PZBPHSE_NAME = pZVTDEF._PZBPHSE_NAME;
                fas.PZBPHSE_USER = pZVTDEF._PZBPHSE_USER;
                fas.PZBPHSE_OPEN_DATE = pZVTDEF._PZBPHSE_OPEN_DATE;
                fas.PZBPHSE_CLOSE_DATE = pZVTDEF._PZBPHSE_CLOSE_DATE;
                fas.PZBPHSE_CALR_CODE = pZVTDEF._PZBPHSE_CALR_CODE;
                fas.PZBPHSE_OPEXTENSION_DATE = pZVTDEF._PZBPHSE_OPEXTENSION_DATE;
                fas.PZBPHSE_CLEXTENSION_DATE = pZVTDEF._PZBPHSE_CLEXTENSION_DATE;
                fas.PZBPHSE_DATA_ORIGIN = pZVTDEF._PZBPHSE_DATA_ORIGIN;

                fas.PZBPHSE_ACTIVITY_DATE = fecha;

                db.PZBPHSE.Add(fas);

                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PZBPHSEExists(pZVTDEF._PZBPHSE_CODE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pZVTDEF._PZBPHSE_CODE }, pZVTDEF);
        }
        
        
        // DELETE: api/PZBPHSE/5
        [ResponseType(typeof(PZBPHSE))]
        public IHttpActionResult DeletePZBPHSE(decimal id)
        {
            PZBPHSE pZVTDEF = db.PZBPHSE.Find(id);
            if (pZVTDEF == null)
            {
                return NotFound();
            }

            db.PZBPHSE.Remove(pZVTDEF);
            db.SaveChanges();

            return Ok(pZVTDEF);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZBPHSEExists(string id)
        {
            return db.PZBPHSE.Count(e => e.PZBPHSE_CODE== id) > 0;
        }
    }
}