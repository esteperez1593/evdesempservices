﻿using Desempeño.DataConect;
using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Desempeño.Controllers
{
    public class PZVTYRSController : ApiController
    {

        private Entities db = new Entities();

        // GET: api/PZVTYRS
        /// <summary>
        /// 
        /// Obtiene los tipos de Resultado Disponible
        /// Ruta api/PZVTYRS/GetPZVTYRS
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/PZVTYRS/GetPZVTYRSs/")]
        [ResponseType(typeof(List<TipoResultado>))]
        public IHttpActionResult GetPZVTYRSs()
        {

            List<Models.TipoResultado> result = new List<Models.TipoResultado>();
            try
            {

                //db.PZVTYRS.Include(tr => tr.PZ);
                var res = db.PZVTYRS.SqlQuery(@" SELECT * from PZVTYRS ").ToList();

                foreach (var x in res)
                {

                    //Datos del tipo de Respuesta
                    Models.TipoResultado tr = new Models.TipoResultado();
                    tr._PZVTYRS_CODE = x.PZVTYRS_CODE;
                    tr._PZVTYRS_DESCRIPTION = x.PZVTYRS_DESCRIPTION;
                    tr._PZVTYRS_ACTIVITY_DATE = x.PZVTYRS_ACTIVITY_DATE;
                    tr._PZVTYRS_DATA_ORIGIN = x.PZVTYRS_DATA_ORIGIN;
                    tr._PZVTYRS_USER = x.PZVTYRS_USER;

                    result.Add(tr);

                }

                return (Ok(result));

            }

            catch (Exception e)
            {

                return (NotFound());

            }


        }


        // PUT: api/PZVTYRS/5
        /// <summary>
        /// 
        /// Modifica los registros de los tipos de Resultado
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="PZVTYRS"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/PZVTYRS/PutPZVTYRS/{id}/")]
        [ResponseType(typeof(TipoResultado))]
        public IHttpActionResult PutPZVTYRS(string id, TipoResultado _PZVTYRS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != _PZVTYRS._PZVTYRS_CODE)
            {
                return BadRequest();
            }



            try
            {
                DateTime fecha = DateTime.Today;

                _PZVTYRS._PZVTYRS_ACTIVITY_DATE = fecha;

                PZVTYRS Trespuesta = new PZVTYRS();

                // Trespuesta.PZVTYRS_ID = PZVTYRS.PZVTYRS_ID;
                Trespuesta.PZVTYRS_USER = _PZVTYRS._PZVTYRS_USER;
                Trespuesta.PZVTYRS_CODE = _PZVTYRS._PZVTYRS_CODE;
                Trespuesta.PZVTYRS_DESCRIPTION = _PZVTYRS._PZVTYRS_DESCRIPTION;
                Trespuesta.PZVTYRS_DATA_ORIGIN = _PZVTYRS._PZVTYRS_DATA_ORIGIN;
                Trespuesta.PZVTYRS_ACTIVITY_DATE = _PZVTYRS._PZVTYRS_ACTIVITY_DATE;

                db.PZVTYRS.Attach(Trespuesta);
                System.Diagnostics.Debug.WriteLine(Trespuesta);
                db.Entry(Trespuesta).State = EntityState.Modified;
                System.Diagnostics.Debug.WriteLine((db.Entry(Trespuesta).State = EntityState.Modified));
                db.SaveChanges();



                return Ok("Tipo de Respuesta: " + Trespuesta.PZVTYRS_DESCRIPTION + " Modificado");

                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PZVTYRSExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PZVTYRS
        /// <summary>
        /// 
        /// Agrega los Tipo de Resultado Posibles
        /// RUTA: api/PZVTYRS/PostPZVTYRS
        /// 
        /// </summary>
        /// <param name="PZVTYRS"></param>
        /// <returns>Tipo de Respuesta</returns>
        [HttpPost]
        [ResponseType(typeof(Models.TipoResultado))]
        [Route("api/PZVTYRS/PostPZVTYRS/")]
        public IHttpActionResult PostPZVTYRS(Models.TipoResultado _PZVTYRS)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (db.PZVTYRS.Count(e => e.PZVTYRS_CODE == _PZVTYRS._PZVTYRS_CODE) > 0)
            {

                return BadRequest("El codigo " + _PZVTYRS._PZVTYRS_CODE + " ya existe.");
            }

            try
            {
                // Decimal es = (Decimal)db.PZVTYRS.Max(x => x.PZVTYRS_ID);


                //  es = es + 1;

                DateTime fecha = DateTime.Today;


                _PZVTYRS._PZVTYRS_ACTIVITY_DATE = fecha;
                //  PZVTYRS.PZVTYRS_ID = es;


                PZVTYRS Trespuesta = new PZVTYRS();
                Trespuesta.PZVTYRS_USER = _PZVTYRS._PZVTYRS_USER;
                Trespuesta.PZVTYRS_CODE = _PZVTYRS._PZVTYRS_CODE;
                Trespuesta.PZVTYRS_DESCRIPTION = _PZVTYRS._PZVTYRS_DESCRIPTION;
                Trespuesta.PZVTYRS_DATA_ORIGIN = _PZVTYRS._PZVTYRS_DATA_ORIGIN;
                Trespuesta.PZVTYRS_ACTIVITY_DATE = _PZVTYRS._PZVTYRS_ACTIVITY_DATE;


                db.PZVTYRS.Add(Trespuesta);

                db.SaveChanges();

            }
            catch (DbUpdateException)
            {
                if (PZVTYRSExists(_PZVTYRS._PZVTYRS_CODE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = _PZVTYRS._PZVTYRS_CODE }, _PZVTYRS);
        }


        // DELETE: api/PZVTYRS/5
        [ResponseType(typeof(PZVTYRS))]
        public IHttpActionResult DeletePZVTYRS(decimal id)
        {
            PZVTYRS PZVTYRS = db.PZVTYRS.Find(id);
            if (PZVTYRS == null)
            {
                return NotFound();
            }

            db.PZVTYRS.Remove(PZVTYRS);
            db.SaveChanges();

            return Ok(PZVTYRS);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZVTYRSExists(string id)
        {
            return db.PZVTYRS.Count(e => e.PZVTYRS_CODE == id) > 0;
        }
    }
}
