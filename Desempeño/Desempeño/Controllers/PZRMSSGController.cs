﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZRMSSGController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/PZRMSSG
        /// <summary>
        /// Obtiene TODOS los mensajes registrados
        /// 
        /// 
        /// </summary>
        /// <returns></returns>

        [ResponseType(typeof(List<Models.Mensaje>))]
        public IHttpActionResult GetPZRMSSG()
        {
            List<Models.Mensaje> result = new List<Models.Mensaje>();
            try
            {

                db.PZRMSSG.Include(men => men.PZBPHSE);
                var res = db.PZRMSSG.SqlQuery(@"Select * from PZRMSSG").ToList();

                foreach (var x in res)
                {

                    //Datos del Mensaje
                    Models.Mensaje men = new Models.Mensaje();
                    men._PZRMSSG_ID = x.PZRMSSG_ID;
                    men._PZRMSSG_PHSE_CODE = x.PZRMSSG_PHSE_CODE;
                    men._PZRMSSG_USER = x.PZRMSSG_USER;
                    men._PZRMSSG_DESCRIPTION = x.PZRMSSG_DESCRIPTION;
                    men._PZRMSSG_DATA_ORIGIN = x.PZRMSSG_DATA_ORIGIN;
                    men._PZRMSSG_ACTIVITY_DATE = x.PZRMSSG_ACTIVITY_DATE;

                    result.Add(men);

                }
                 
            }
            catch (Exception e) {

            }

            return Ok(result);
        }

        /// <summary>
        /// 
        /// Carga las fases y los mensajes que tiene asociados
        /// 
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(List<Models.Mensaje>))]
        public IHttpActionResult GetPZRMSSGF()
        {
            List<Models.Mensaje> result = new List<Models.Mensaje>();
            try
            {

                db.PZRMSSG.Include(men => men.PZBPHSE);
                var res = db.PZRMSSG.SqlQuery(@"Select fas.*,men.* 
                                                from PZBPHSE fas,PZRMSSG men 
                                                WHERE men.PZRMSSG_PHSE_CODE = fas.PZBPHSE_CODE").ToList();

                foreach (var x in res)
                {

                    //Datos del Mensaje
                    Models.Mensaje men = new Models.Mensaje();
                    men._PZRMSSG_ID = x.PZRMSSG_ID;
                    men._PZRMSSG_PHSE_CODE = x.PZRMSSG_PHSE_CODE;
                    men._PZRMSSG_USER = x.PZRMSSG_USER;
                    men._PZRMSSG_DESCRIPTION = x.PZRMSSG_DESCRIPTION;
                    men._PZRMSSG_DATA_ORIGIN = x.PZRMSSG_DATA_ORIGIN;
                    men._PZRMSSG_ACTIVITY_DATE = x.PZRMSSG_ACTIVITY_DATE;

                    result.Add(men);

                }

            }
            catch (Exception e)
            {

            }

            return Ok(result);
        }

        // GET: api/PZRMSSG/5
        [ResponseType(typeof(PZRMSSG))]
        public IHttpActionResult GetPZRMSSG(decimal id)
        {
            PZRMSSG pZRMDEF = db.PZRMSSG.Find(id);
            if (pZRMDEF == null)
            {
                return NotFound();
            }

            return Ok(pZRMDEF);
        }

        // PUT: api/PZRMSSG/5
        /// <summary>
        /// 
        /// Modifica un mensaje registrado
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pZRMDEF"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/PZRMSSG/PutPZRMSSG/{id}/")]
        [ResponseType(typeof(Mensaje))]
        public IHttpActionResult PutPZRMSSG(decimal id, Mensaje pZRMDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pZRMDEF._PZRMSSG_ID)
            {
                return BadRequest();
            }

           

            try
            {

                DateTime fecha = DateTime.Today;

                PZRMSSG objMen = new PZRMSSG();
                objMen.PZRMSSG_ID = pZRMDEF._PZRMSSG_ID;
                objMen.PZRMSSG_USER = pZRMDEF._PZRMSSG_USER;
                objMen.PZRMSSG_DATA_ORIGIN = pZRMDEF._PZRMSSG_DATA_ORIGIN;
                objMen.PZRMSSG_DESCRIPTION = pZRMDEF._PZRMSSG_DESCRIPTION;
                objMen.PZRMSSG_PHSE_CODE = pZRMDEF._PZRMSSG_PHSE_CODE;
                objMen.PZRMSSG_ACTIVITY_DATE = fecha;

                db.PZRMSSG.Attach(objMen);

                db.Entry(objMen).State = EntityState.Modified;

                db.SaveChanges();

                return Ok("Se ha modificado el mensaje exitosamente");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PZRMSSGExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PZRMSSG
        /// <summary>
        ///
        /// Agrega los mensajes 
        /// 
        /// </summary>
        /// <param name="pZRMDEF"></param>
        /// <returns></returns>
        [ResponseType(typeof(Models.Mensaje))]
        public IHttpActionResult PostPZRMSSG(Models.Mensaje _pZRMDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }



            try
            {
                int es;
                int num_reg = (int)db.PZRMSSG.SqlQuery(@"select * FROM PZRMSSG").Count();

                if (num_reg == 0)
                {

                    es = 1;
                }

                else
                {
                    es = (int)db.PZRMSSG.Max(x => x.PZRMSSG_ID);


                    es = es + 1;
                }

                DateTime fecha = DateTime.Today;

                PZRMSSG objMen = new PZRMSSG();
                objMen.PZRMSSG_ID = es;
                objMen.PZRMSSG_USER = _pZRMDEF._PZRMSSG_USER;
                objMen.PZRMSSG_DATA_ORIGIN = _pZRMDEF._PZRMSSG_DATA_ORIGIN;
                objMen.PZRMSSG_DESCRIPTION = _pZRMDEF._PZRMSSG_DESCRIPTION;
                objMen.PZRMSSG_PHSE_CODE = _pZRMDEF._PZRMSSG_PHSE_CODE;
                objMen.PZRMSSG_ACTIVITY_DATE = fecha;

                db.PZRMSSG.Add(objMen);

                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PZRMSSGExists(_pZRMDEF._PZRMSSG_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = _pZRMDEF._PZRMSSG_ID }, _pZRMDEF);
        }

        // DELETE: api/PZRMSSG/5
        [ResponseType(typeof(PZRMSSG))]
        public IHttpActionResult DeletePZRMSSG(decimal id)
        {
            PZRMSSG pZRMDEF = db.PZRMSSG.Find(id);
            if (pZRMDEF == null)
            {
                return NotFound();
            }

            db.PZRMSSG.Remove(pZRMDEF);
            db.SaveChanges();

            return Ok(pZRMDEF);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZRMSSGExists(decimal id)
        {
            return db.PZRMSSG.Count(e => e.PZRMSSG_ID == id) > 0;
        }
    }
}