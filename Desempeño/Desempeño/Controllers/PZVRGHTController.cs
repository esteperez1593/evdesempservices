﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZVRGHTController : ApiController
    {
        private Entities db = new Entities();


        /// <summary>
        /// Carga todas las acciones registradas
        /// </summary>
        /// <returns></returns>
        // GET: api/PZVRGHT
        [HttpGet]
        [Route("api/PZVRGHT/GetPZVRGHTs/")]
        [ResponseType(typeof(List<Models.DefinicionPermisos>))]
        public IHttpActionResult GetPZVRGHTs()
        {
            List<Models.DefinicionPermisos> result = new List<Models.DefinicionPermisos>();
            try
            {

                var res = db.PZVRGHT.SqlQuery(@"Select * from PZVRGHT").ToList();

                foreach (var X in res)
                {

                    //Datos del Instrumento Aplicable
                    Models.DefinicionPermisos _defpER = new Models.DefinicionPermisos();
                    _defpER._PZVRGHT_CODE = X.PZVRGHT_CODE;
                    _defpER._PZVRGHT_USER = X.PZVRGHT_USER;
                    _defpER._PZVRGHT_GRADE = X.PZVRGHT_GRADE;
                    _defpER._PZVRGHT_DATA_ORIGIN = X.PZVRGHT_DATA_ORIGIN;
                    _defpER._PZVRGHT_DESCRIPTION = X.PZVRGHT_DESCRIPTION;
                    _defpER._PZVRGHT_ACTIVITY_DATE = X.PZVRGHT_ACTIVITY_DATE;
                    result.Add(_defpER);

                }

            }
            catch (Exception e)
            {

            }

            return Ok(result);
        }


        /// <summary>
        /// Dado un numero que represente el rol
        /// carga los permisos asociados
        /// </summary>
        /// <param name="_rol"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/PZVRGHT/GetPZVRGHTs/{_rol}")]
        [ResponseType(typeof(List<Models.DefinicionPermisos>))]
        public IHttpActionResult GetPZVRGHTs(int _rol )
        {
            List<Models.DefinicionPermisos> result = new List<Models.DefinicionPermisos>();
            try
            {

                var res = db.PZVRGHT.SqlQuery(@"Select * from PZVRGHT 
                                                    where pzvrght_grade >= '"+_rol+"'").ToList();

                foreach (var X in res)
                {

                    //Datos del Instrumento Aplicable
                    Models.DefinicionPermisos _defpER = new Models.DefinicionPermisos();
                    _defpER._PZVRGHT_CODE = X.PZVRGHT_CODE;
                    _defpER._PZVRGHT_USER = X.PZVRGHT_USER;
                    _defpER._PZVRGHT_GRADE = X.PZVRGHT_GRADE;
                    _defpER._PZVRGHT_DATA_ORIGIN = X.PZVRGHT_DATA_ORIGIN;
                    _defpER._PZVRGHT_DESCRIPTION = X.PZVRGHT_DESCRIPTION;
                    _defpER._PZVRGHT_ACTIVITY_DATE = X.PZVRGHT_ACTIVITY_DATE;
                    result.Add(_defpER);

                }

            }
            catch (Exception e)
            {

            }

            return Ok(result);
        }

        // GET: api/PZVRGHT/5
        [ResponseType(typeof(PZVRGHT))]
        public IHttpActionResult GetPZVRGHT(string id)
        {
            PZVRGHT pZPRFIL = db.PZVRGHT.Find(id);
            if (pZPRFIL == null)
            {
                return NotFound();
            }

            return Ok(pZPRFIL);
        }

        // PUT: api/PZVRGHT/5

        /// <summary>
        /// Modifica los permisos
        /// </summary>
        /// <param name="id"></param>
        /// <param name="_pZPRFIL"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/PZVRGHT/PutPZVRGHT/{id}")]
        [ResponseType(typeof(Models.DefinicionPermisos))]
        public IHttpActionResult PutPZVRGHT(string id, DefinicionPermisos _pZPRFIL)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            try
            {
                //  Decimal es = (Decimal)db.PZVPERM.Max(x => x.PZVPERM_ID);

                //es = es + 1;

                DateTime fecha = DateTime.Today;


                PZVRGHT _defpER = new PZVRGHT();
                _defpER.PZVRGHT_CODE = _pZPRFIL._PZVRGHT_CODE;
                _defpER.PZVRGHT_USER = _pZPRFIL._PZVRGHT_USER;
                _defpER.PZVRGHT_GRADE = _pZPRFIL._PZVRGHT_GRADE;
                _defpER.PZVRGHT_DATA_ORIGIN = _pZPRFIL._PZVRGHT_DATA_ORIGIN;
                _defpER.PZVRGHT_DESCRIPTION = _pZPRFIL._PZVRGHT_DESCRIPTION;
                _defpER.PZVRGHT_ACTIVITY_DATE = fecha;
                db.PZVRGHT.Attach(_defpER);
                db.Entry(_defpER).State = EntityState.Modified;
                db.SaveChanges();


            }
            catch (DbUpdateException)
            {
                if (PZVRGHTExists(_pZPRFIL._PZVRGHT_CODE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = _pZPRFIL._PZVRGHT_CODE}, _pZPRFIL);
        }




        // POST: api/PZVRGHT
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pZPRFIL"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/PZVRGHT/PostPZVRGHT/")]
        [ResponseType(typeof(DefinicionPermisos))]
        public IHttpActionResult PostPZVRGHT(DefinicionPermisos _pZPRFIL)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            try
            {
                //  Decimal es = (Decimal)db.PZVPERM.Max(x => x.PZVPERM_ID);

                //es = es + 1;

                DateTime fecha = DateTime.Today;

                PZVRGHT _defpER = new PZVRGHT();
                _defpER.PZVRGHT_CODE = _pZPRFIL._PZVRGHT_CODE;
                _defpER.PZVRGHT_USER = _pZPRFIL._PZVRGHT_USER;
                _defpER.PZVRGHT_GRADE = _pZPRFIL._PZVRGHT_GRADE;
                _defpER.PZVRGHT_DATA_ORIGIN = _pZPRFIL._PZVRGHT_DATA_ORIGIN;
                _defpER.PZVRGHT_DESCRIPTION = _pZPRFIL._PZVRGHT_DESCRIPTION;
                _defpER.PZVRGHT_ACTIVITY_DATE = fecha;

                db.PZVRGHT.Add(_defpER);

                db.SaveChanges();


            }
            catch (DbUpdateException)
            {
                if (PZVRGHTExists(_pZPRFIL._PZVRGHT_CODE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = _pZPRFIL._PZVRGHT_CODE }, _pZPRFIL);
        }

        // DELETE: api/PZVRGHT/5
        [ResponseType(typeof(PZVRGHT))]
        public IHttpActionResult DeletePZVRGHT(string id)
        {
            PZVRGHT pZPRFIL = db.PZVRGHT.Find(id);
            if (pZPRFIL == null)
            {
                return NotFound();
            }

            db.PZVRGHT.Remove(pZPRFIL);
            db.SaveChanges();

            return Ok(pZPRFIL);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZVRGHTExists(string id)
        {
            return db.PZVRGHT.Count(e => e.PZVRGHT_CODE == id) > 0;
        }
    }
}