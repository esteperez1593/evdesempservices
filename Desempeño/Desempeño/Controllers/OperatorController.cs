﻿using Desempeño.DataConect;
using Desempeño.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Desempeño.Controllers
{
    public class OPERATORController : ApiController
    {
        private Entities db = new Entities();


        /// <summary>
        ///  
        /// Calcula el indice de desempeño para un usuario en funcion del 
        /// valor porcentual que representa para el perfil que tiene asignado
        ///  
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <param name="_term"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetIndPerc/{_perfil}/{_calendario}/{_instrumento}/{_pidm}/{_term}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetIndPerc(string _perfil, string _calendario, string _instrumento, string _pidm, string _term)
        {


            try
            {

                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_RESULT_COMP('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "','" + _term + "') FROM DUAL");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {
                    return BadRequest("No encontrado");
                }


            }

            catch (Exception e)
            {

                return (NotFound());

            }


        }

        /// <summary>
        /// 
        /// Obtiene el porcentaje asignado a un instrumento para un
        /// oerfil dado
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_term"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetPercProf/{_perfil}/{_calendario}/{_instrumento}/{_term}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetPercProf(string _perfil, string _calendario, string _instrumento, string _term)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_PERC_EVAL('" + _perfil + "','" + _term + "','" + _instrumento + "','" + _calendario + "') FROM DUAL");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {
                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }



        }


        /// <summary>
        /// 
        /// Obtiene la cantidad de preguntas a totalizar
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetCountItem/{_perfil}/{_calendario}/{_instrumento}/{_pidm}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetCountItem(string _perfil, string _calendario, string _instrumento, string _pidm)
        {

            try
            {

                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_RESULT_COUNT('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {
                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }



        }

        /// <summary>
        ///
        /// Obtiene el valor correspondiente al diferencial 
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetDifItem/{_perfil}/{_calendario}/{_instrumento}/{_pidm}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetDifItem(string _perfil, string _calendario, string _instrumento, string _pidm)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_RESULT_DIF('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {
                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }
        }

        [HttpGet]
        [Route("api/OPERATOR/GetIndPoll/{_perfil}/{_term}/{_calendario}/{_instrumento}/{_pidm}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetIndPoll(string _perfil, string _term, string _calendario, string _instrumento, string _pidm)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_PERC_POLL('" + _perfil + "','" + _term + "','" + _instrumento + "','" + _calendario + "','" + _pidm + "') FROM DUAL");

                foreach (decimal resp in res)
                {
                    return Ok(resp);
                }
                return NotFound();
            }

            catch (Exception e)
            {

                return BadRequest();

            }
        }

        [HttpGet]
        [Route("api/OPERATOR/GetIndTot/{_perfil}/{_term}/{_calendario}/{_instrumento}/{_pidm}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetIndTot(string _perfil, string _term, string _calendario, string _instrumento, string _pidm)
        {
            FunctionsRepository _f = new FunctionsRepository();
            try
            {
                
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_TOTAL_INDEX('" + _perfil + "','" + _term + "','" + _instrumento + "','" + _calendario + "','" + _pidm + "') FROM DUAL");

                foreach (decimal resp in res)
                {
                    return Ok(resp);
                }
                return NotFound();
            }

            catch (Exception e)
            {

                return BadRequest();

            }
        }
        /// <summary>
        /// 
        /// Obtiene la media de las respuestas de un Instrumento
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetMedItem/{_perfil}/{_calendario}/{_instrumento}/{_pidm}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetMedItem(string _perfil, string _calendario, string _instrumento, string _pidm)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_RESULT_MED('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {
                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }
        }

        /// <summary>
        /// 
        /// Calculo de la sumatoria correspondiente a un instrumento
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetSumAns/{_perfil}/{_calendario}/{_instrumento}/{_pidm}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetSumAns(string _perfil, string _calendario, string _instrumento, string _pidm)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_RESULT_SUM('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {
                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }
        }

        /// <summary>
        ///
        /// Calcula el porcentaje de satisfacion en funcion de las respuestas registradas
        /// 
        /// </summary>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetPercObjT/{_instrumento}/{_pidm}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetPercObjT(string _instrumento, string _pidm,string _calr ,string _pfle)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval.F_GET_TOTAL_OBJ_PERC ('" + _pidm + "','" + _instrumento + "','"+_calr+"','"+_pfle+"') from dual ");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {
                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }
        }

        /// <summary>
        /// 
        /// Calcula el valor porcentual para un objetivo aun no registrado
        /// 
        /// </summary>
        /// <param name="_resp_value"></param>
        /// <param name="_perc_value"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetPercObj/{_resp_value}/{_perc_value}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetPercObj(decimal _resp_value, decimal _perc_value)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval.F_GET_OBJ_PERC ('" + _resp_value + "','" + _perc_value + "') from dual ");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {

                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }
        }

        /// <summary>
        /// 
        /// Calcula el valor porcentual de un instrumento
        /// 
        /// </summary>
        /// <param name="_resp_value"></param>
        /// <param name="_perc_value"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetPercPoll/{profile_code}/{term_code}/{poll_code}/{calr_code}/{pidm}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetPercPoll(String profile_code ,
                                        String term_code ,
                                        String poll_code ,
                                        String calr_code ,
                                        String pidm )
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval.F_GET_PERC_POLL ('" + profile_code + "','" + term_code + "','"+ poll_code + "','"+ calr_code + "','"+ pidm + "') from dual ");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {

                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }
        }


        /*
         * 
         * Instrumento de Extension
         * 
         */

        /// <summary>
        /// 
        /// Suma las horas correspondientes a los proyectos de extension
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetSumProyExt/{_perfil}/{_calendario}/{_instrumento}/{_pidm}/")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetSumProyExt(string _perfil, string _calendario, string _instrumento, string _pidm)
        {


            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_POLL_EXT('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL");

                if (res != null)
                {
                    return Ok(res);
                }

                else
                {
                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }


        }

        /// <summary>
        /// 
        /// Devuelve el monto actual de la UC 
        /// para el calculo de los instrumentos de extension
        ///  
        /// </summary>
        /// <param name="_term"></param>
        /// <param name="_attr"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetUCAmount/{_term}/")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetUCAmount(string _term) {

            try
            {

                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval.F_GET_AMOUNT_UC ('" + _term + "') from dual ");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {

                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }



        }

        /// <summary>
        ///  
        ///  Devuelve expresado en Unidades Credito el Valor correspondiente 
        ///  a los ingresos generados a la UCAB por concepto de asesoria y/o
        ///  Consultoria
        ///  
        /// </summary>
        /// <param name="_term"></param>
        /// <param name="_amount"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetUCValue/{_term}/{_amount}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetUCValue(string _term,string _amount)
        {
            
            try
            {

                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval.F_GET_VALUE_UC_EXT ('" + _term + "','"+_amount+"') from dual ");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {

                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }



        }

        /// <summary>
        /// 
        /// Calcula los puntos obtenidos en funcion del costo de la UC y 
        /// del monto ingresado por concepto de asesorias o consultorias
        /// 
        /// </summary>
        /// <param name="_term"></param>
        /// <param name="_amount"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetUCPoints/{_term}/{_amount}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetUCPoints(string _term, string _amount)
        {

            try
            {

                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval.F_GET_POINTS_UC_EXT ('" + _term + "','" + _amount + "') from dual ");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {

                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }



        }

        /// <summary>
        /// 
        /// Calculo de la sumatoria correspondiente a un instrumento 
        /// por areas
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetSumAns/{_perfil}/{_calendario}/{_instrumento}/{_pidm}/{_area}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetSumAns(string _perfil, string _calendario, string _instrumento, string _pidm, string _area)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_RESULT_SUM('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "','" + _area + "') FROM DUAL");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {
                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }
        }

        /*
         * 
         * Instrumento de Investigadores
         * 
         */

        /// <summary>
        /// 
        /// Calcula los puntos por publicaciones
        /// 
        /// </summary>
        /// <param name="_pregunta"></param>
        /// <param name="_cantidad"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetInvPub/{_pregunta}/{_cantidad}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetInvPub(string _pregunta, string _cantidad)
        {

            try
            {

                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval. F_CALC_INVE_PUBL('" + _pregunta + "','" + _cantidad + "') from dual ");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {

                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }



        }

        /// <summary>
        /// 
        /// Calcula los puntos por comite
        /// 
        /// </summary>
        /// <param name="_pregunta"></param>
        /// <param name="_cantidad"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetInvCom/{_pregunta}/{_cantidad}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetInvCom(string _pregunta, string _cantidad)
        {

            try
            {

                var res = db.Database.SqlQuery<decimal>(@"select bzpkeval. F_CALC_INVE_CONS('" + _pregunta + "','" + _cantidad + "') from dual ");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {

                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }



        }

        /*
         * 
         * General de funciones
         * 
         */

        /// <summary>
        ///  
        /// Obtiene el total de areas que componen un instrumento
        ///  
        /// </summary>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetAreaIns/{_calendario}/{_instrumento}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetAreaIns(string _calendario, string _instrumento)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_AREA_COUNT('" + _calendario + "','" + _instrumento + "') FROM DUAL");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {
                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }
        }

     
        /// <summary>
        /// 
        /// Devuelve el total de preguntas que componen un Area
        /// 
        /// </summary>
        /// <param name="_term"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_perfil"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetPregCont/{_term}/{_calendario}/{_instrumento}/{_perfil}")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetPregCont(string _term,string _calendario, string _instrumento, string _perfil)
        {
            try
            {
                var res = db.Database.SqlQuery<decimal>(@"SELECT BZPKEVAL.F_GET_COUNT_QSTN('" + _term + "', '" + _calendario + "','" + _instrumento + "','"+ _perfil +"') FROM DUAL");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {
                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }
        }

       
        /// <summary>
        /// 
        /// Obtiene el codigo de los instrumentos
        /// 
        /// </summary>
        /// <param name="_instrumento"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetInsCod/{_instrumento}")]
        [ResponseType(typeof(String))]
        public IHttpActionResult GetInsCod(string _instrumento)
        {
            try
            {
                var res = db.Database.SqlQuery<String>(@"SELECT BZPKEVAL.F_GET_POLL_CODE('" + _instrumento + "') FROM DUAL");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {
                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }
        }

        

        /// <summary>
        /// 
        /// Obtiene el codigo de un perfil
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetPerCod/{_perfil}")]
        [ResponseType(typeof(String))]
        public IHttpActionResult GetPerCod(string _perfil)
        {
            try
            {
                var res = db.Database.SqlQuery<String>(@"SELECT BZPKEVAL.F_GET_PFLE_CODE('" + _perfil + "') FROM DUAL");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {
                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }
        }

      
        /// <summary>
        /// 
        /// Obtiene el codigo del calendario
        /// 
        /// </summary>
        /// <param name="_calendario"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/OPERATOR/GetCalCod/{_calendario}")]
        [ResponseType(typeof(String))]
        public IHttpActionResult GetCalCod(string _calendario)
        {
            try
            {
                var res = db.Database.SqlQuery<String>(@"SELECT BZPKEVAL.F_GET_CALR_CODE('" + _calendario + "') FROM DUAL");

                if (res != null)
                {
                    return Ok(res);
                }
                else
                {
                    return BadRequest("No encontrado");
                }
            }

            catch (Exception e)
            {

                return (NotFound());

            }
        }




    }
}
