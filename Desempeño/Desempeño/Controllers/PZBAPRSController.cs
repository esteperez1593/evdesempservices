﻿using Desempeño.DataConect;
using Desempeño.Models;
using Desempeño.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
//using Desempeño.Repository.ValidatorRepository;

namespace Desempeño.Controllers
{
    public class PZBAPRSController : ApiController
    {

        private Entities db = new Entities();

        // GET: api/PZBAPRS
        /// <summary>
        /// 
        /// Obtiene la lista de _resApls registrados
        /// RUTA : api/PZBAPRS/GetPZBAPRS
        /// 
        /// </summary>
        /// <returns>List<Models.ResultadoAplicable></returns>
        [Route("api/PZBAPRS/GetPZBAPRS/")]
        [ResponseType(typeof(List<Models.ResultadoAplicable>))]
        public IHttpActionResult GetPZBAPRS()
        {
            List<Models.ResultadoAplicable> result = new List<Models.ResultadoAplicable>();
            try
            {

                //db.PZBAPRS.Include(a => a._PZBAPPL);
                var res = db.PZBAPRS.SqlQuery(@"Select * from PZBAPRS").ToList();

                foreach (var X in res)
                {

                    //Datos del Instrumento Aplicable
                    Models.ResultadoAplicable _resApl = new Models.ResultadoAplicable();
                    _resApl._PZBAPRS_CALR_CODE = X.PZBAPRS_CALR_CODE;
                    _resApl._PZBAPRS_POLL_CODE = X.PZBAPRS_POLL_CODE;
                    _resApl._PZBAPRS_SEQ_NUMBER = X.PZBAPRS_SEQ_NUMBER;
                    _resApl._PZBAPRS_USER = X.PZBAPRS_USER;
                    _resApl._PZBAPRS_DATA_ORIGIN = X.PZBAPRS_DATA_ORIGIN;
                    _resApl._PZBAPRS_ACTIVITY_DATE = X.PZBAPRS_ACTIVITY_DATE;
                    _resApl._PZBAPRS_ORDER = X.PZBAPRS_ORDER;
                    _resApl._PZBAPRS_APPL_SEQ_NUMBER = X.PZBAPRS_APPL_SEQ_NUMBER;
                    _resApl._PZBAPRS_TYRS_CODE = X.PZBAPRS_TYRS_CODE;


                    //_resApl._PZBAPRS_POLL_CODE = X.PZBAPRS_POLL_CODE;
                    result.Add(_resApl);

                }

            }
            catch (Exception e)
            {

            }

            return Ok(result);
        }


        [Route("api/PZBAPRS/GetPZBAPRS/{_term}/{_pidm}")]
        public IHttpActionResult GetPZBAPRS(string _term, string _pidm)
        {
            //  List<int> result = new List<int>();
            int perc = 0;
            try
            {

                //db.PZBAPRS.Include(a => a._PZBAPPL);
                var res = db.Database.SqlQuery<int>(@"select sum( pzbappl.pzbappl_prcnt)
                                                    from pzrplap, pzbappl, 
                                                         pzbpoll,pzvpfle,
                                                         pzbcalr,pzraspe,
                                                         pzrdypt,pzbprso,
                                                         pzvpost,pzvdncy--,
                                                        -- pzrawap,pzrrspl
                                                    where pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                      and pzbappl.pzbappl_calr_code = pzbcalr.pzbcalr_code
                                                      and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                      and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                      and pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                      and pzrplap.pzrplap_pfle_code = pzbappl.pzbappl_pfle_code
                                                      and pzbappl.pzbappl_pfle_code = pzvpfle.pzvpfle_code
                                                      and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                      and pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                      and pzrdypt.pzrdypt_dncy_code = pzvdncy.pzvdncy_code
                                                      and pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code 
                                                      and pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                      and pzrdypt.pzrdypt_post_code = pzvpost.pzvpost_code
                                                      and pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                      and pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                      and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                      and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm
                                                      and pzrplap.pzrplap_status ='F'
                                                      and pzbcalr.pzbcalr_term='"+_term+"' "+
                                                     "and pzrplap.pzrplap_pidm ='"+_pidm+"'").ToList();
                foreach (var x in res) {

                     perc = (int)x;

                }



            }
            catch (Exception e)
            {

            }

            return Ok(perc);
        }

        // GET: api/PZBAPRS/5
        [ResponseType(typeof(ResultadoAplicable))]
        public IHttpActionResult GetPZBAPRS(int id)
        {


            List<Models.ResultadoAplicable> result = new List<Models.ResultadoAplicable>();
            try
            {

                //db.PZBAPRS.Include(a => a._PZBAPPL);
                var res = db.PZBAPRS.SqlQuery(@"Select * 
                                                from PZBAPRS
                                                WHERE PZBAPRS.PZBAPRS_SEQ_NUMBER='" + id + "' ").ToList();

                foreach (var X in res)
                {

                    //Datos del Instrumento Aplicable
                    Models.ResultadoAplicable _resApl = new Models.ResultadoAplicable();
                    _resApl._PZBAPRS_CALR_CODE = X.PZBAPRS_CALR_CODE;
                    _resApl._PZBAPRS_POLL_CODE = X.PZBAPRS_POLL_CODE;
                    _resApl._PZBAPRS_SEQ_NUMBER = X.PZBAPRS_SEQ_NUMBER;
                    _resApl._PZBAPRS_USER = X.PZBAPRS_USER;
                    _resApl._PZBAPRS_DATA_ORIGIN = X.PZBAPRS_DATA_ORIGIN;
                    _resApl._PZBAPRS_ACTIVITY_DATE = X.PZBAPRS_ACTIVITY_DATE;
                    _resApl._PZBAPRS_ORDER = X.PZBAPRS_ORDER;
                    _resApl._PZBAPRS_APPL_SEQ_NUMBER = X.PZBAPRS_APPL_SEQ_NUMBER;
                    _resApl._PZBAPRS_TYRS_CODE = X.PZBAPRS_TYRS_CODE;


                    //_resApl._PZBAPRS_POLL_CODE = X.PZBAPRS_POLL_CODE;
                    result.Add(_resApl);

                }

            }
            catch (Exception e)
            {

            }

            return Ok(result);


        }


        /// <summary>
        /// 
        /// Busca las respuestas asociadas a un 
        /// instrumento de una persona 
        /// y realiza los calculos que tenga 
        /// configurados
        /// 
        /// Donde A = permite agregar nuevos resultados
        /// Donde E = permite hacer el put de un resultado
        /// 
        /// </summary>
        /// <param name="_appl"></param>
        /// <param name="_poll"></param>
        /// <param name="_calr"></param>
        /// <param name="_pidm"></param>
        /// <param name="_perfil"></param>
        /// <param name="_status"></param>
        /// <param name="_plap"></param>
        /// <param name="_type"></param>
        /// <returns></returns>
        [Route("api/PZBAPRS/GetPZBAPRS/{_appl}/{_poll}/{_calr}/{_pidm}/{_perfil}/{_status}/{_plap}/{_type}")]
      
        [ResponseType(typeof(List<Models.ResultadosParciales>))]
        public IHttpActionResult GetPZBAPRS(string _appl, string _poll,
                                            string _calr, string _pidm,
                                            string _perfil, string _status,
                                            int _plap, string _type )
        {
            List<Models.ResultadoAplicable> result = new List<Models.ResultadoAplicable>();
            List<Models.ResultadosParciales> valor = new List<Models.ResultadosParciales>();
            ValidatorRepository _validator = new ValidatorRepository();


            string   estado = _validator.validarStatusInstrumento(_pidm, _poll, _calr, _plap);

            if (estado == "F")
            {
                return BadRequest("Este instrumento ya ha sido evaluado previamente");
            }
            else
            {
                try
                {

                    db.PZBAPRS.Include(a => a.PZVTYRS)
                      .Include(b => b.PZBAPPL)
                      .Include(c => c.PZRRSPL);
                    var res = db.PZBAPRS.SqlQuery(@"Select *
                                                from PZBAPRS,pzbappl,
                                                     pzbpoll,pzbcalr,
                                                     pzvtyrs 
                                                where pzbaprs_calr_code = pzbappl_calr_code
                                                  and pzbaprs_tyrs_code = pzvtyrs_code
                                                  and pzbaprs_poll_code = pzbappl_poll_code
                                                  and pzbaprs_appl_seq_number = pzbappl_seq_number
                                                  and pzbappl_calr_code = pzbcalr_code
                                                  and pzbappl_poll_code = pzbpoll_code
                                                  and pzbappl_seq_number = '" + _appl + "'" +
                                                      " and pzbpoll_code = '" + _poll + "'" +
                                                      " and pzbcalr_code ='" + _calr + "'" +
                                                      "order by pzbaprs.pzbaprs_order asc ").ToList();

                    foreach (var X in res)
                    {

                        //Datos del Instrumento Aplicable
                        Models.ResultadoAplicable _resApl = new Models.ResultadoAplicable();
                        _resApl._PZBAPRS_CALR_CODE = X.PZBAPRS_CALR_CODE;
                        _resApl._PZBAPRS_POLL_CODE = X.PZBAPRS_POLL_CODE;
                        _resApl._PZBAPRS_SEQ_NUMBER = X.PZBAPRS_SEQ_NUMBER;
                        _resApl._PZBAPRS_USER = X.PZBAPRS_USER;
                        _resApl._PZBAPRS_DATA_ORIGIN = X.PZBAPRS_DATA_ORIGIN;
                        _resApl._PZBAPRS_ACTIVITY_DATE = X.PZBAPRS_ACTIVITY_DATE;
                        _resApl._PZBAPRS_ORDER = X.PZBAPRS_ORDER;
                        _resApl._PZBAPRS_APPL_SEQ_NUMBER = X.PZBAPRS_APPL_SEQ_NUMBER;
                        _resApl._PZBAPRS_TYRS_CODE = X.PZBAPRS_TYRS_CODE;

                        Models.TipoResultado tr = new Models.TipoResultado();
                        tr._PZVTYRS_CODE = X.PZVTYRS.PZVTYRS_CODE;
                        tr._PZVTYRS_DESCRIPTION = X.PZVTYRS.PZVTYRS_DESCRIPTION;
                        tr._PZVTYRS_ACTIVITY_DATE = X.PZVTYRS.PZVTYRS_ACTIVITY_DATE;
                        tr._PZVTYRS_DATA_ORIGIN = X.PZVTYRS.PZVTYRS_DATA_ORIGIN;
                        tr._PZVTYRS_USER = X.PZVTYRS.PZVTYRS_USER;
                        _resApl._PZVTYRS = tr;

                        Models.InstrumentoAplicable _insApl = new Models.InstrumentoAplicable();
                        _insApl._PZBAPPL_CALR_CODE = X.PZBAPPL.PZBAPPL_CALR_CODE;
                        _insApl._PZBAPPL_PFLE_CODE = X.PZBAPPL.PZBAPPL_PFLE_CODE;
                        _insApl._PZBAPPL_PRCNT = X.PZBAPPL.PZBAPPL_PRCNT;
                        _insApl._PZBAPPL_POLL_CODE = X.PZBAPPL.PZBAPPL_POLL_CODE;
                        _insApl._PZBAPPL_SEQ_NUMBER = X.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                        _insApl._PZBAPPL_USER = X.PZBAPPL.PZBAPPL_USER;
                        _insApl._PZBAPPL_DATA_ORIGIN = X.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                        _insApl._PZBAPPL_ACTIVITY_DATE = X.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                        //_insApl._PZBAPPL_POLL_CODE = X.PZBAPPL_POLL_CODE;
                        _resApl._PZBAPPL = _insApl;

                        Models.Calendario cal = new Models.Calendario();
                        cal._PZBCALR_CODE = X.PZBAPPL.PZBCALR.PZBCALR_CODE;
                        cal._PZBCALR_NAME = X.PZBAPPL.PZBCALR.PZBCALR_NAME;
                        cal._PZBCALR_USER = X.PZBAPPL.PZBCALR.PZBCALR_USER;
                        cal._PZBCALR_TERM = X.PZBAPPL.PZBCALR.PZBCALR_TERM;
                        cal._PZBCALR_SRL = X.PZBAPPL.PZBCALR.PZBCALR_SRL;
                        cal._PZBCALR_END_DATE = X.PZBAPPL.PZBCALR.PZBCALR_END_DATE;
                        cal._PZBCALR_INIT_DATE = X.PZBAPPL.PZBCALR.PZBCALR_INIT_DATE;
                        cal._PZBCALR_DATA_ORIGIN = X.PZBAPPL.PZBCALR.PZBCALR_DATA_ORIGIN;
                        cal._PZBCALR_ACTIVITY_DATE = X.PZBAPPL.PZBCALR.PZBCALR_ACTIVITY_DATE;

                        _resApl._PZBAPPL._PZBCALR = cal;

                        result.Add(_resApl);

                    }


                    int cont = result.Count;
                    decimal calculate=0;

                    for (int dimension = 0; dimension < cont; dimension++)
                    {
                        Models.ResultadosParciales item = new Models.ResultadosParciales();
                        FunctionsRepository _f = new FunctionsRepository();


                        if (result[dimension]._PZBAPRS_TYRS_CODE == "SUMOBJ")
                        {

                             calculate = _f.GetSumAns(_perfil, _calr, _poll, _pidm);

                        }

                        if (result[dimension]._PZBAPRS_TYRS_CODE == "MEDOBJ")
                        {

                            calculate = _f.GetMedItem(_perfil, _calr, _poll, _pidm);
                           
                        }

                        if (result[dimension]._PZBAPRS_TYRS_CODE == "PEROBJ")
                        {

                             calculate = _f.GetPercObjT(_perfil, _calr, _poll, _pidm);

                        }

                        if (result[dimension]._PZBAPRS_TYRS_CODE == "TOTOBJ")
                        {

                             calculate = _f.GetDifItem(_perfil, _calr, _poll, _pidm);

                        }


                        if (result[dimension]._PZBAPRS_TYRS_CODE == "SUMCOM")
                        {

                             calculate = _f.GetSumAns(_perfil, _calr, _poll, _pidm /*_area*/);


                        }

                        if (result[dimension]._PZBAPRS_TYRS_CODE == "MEDCOM")
                        {

                             calculate = _f.GetMedItem(_perfil, _calr, _poll, _pidm);

                        }

                        if (result[dimension]._PZBAPRS_TYRS_CODE == "PTOACU")
                        {

                           
                        }

                        if (result[dimension]._PZBAPRS_TYRS_CODE == "SUMPTO")
                        {

                           
                        }

                        if (result[dimension]._PZBAPRS_TYRS_CODE == "PERINS")
                        {

                             calculate = _f.GetPercProf(_perfil, _calr, _poll, result[dimension]._PZBAPPL._PZBCALR._PZBCALR_TERM);
                           
                        }
                        if (result[dimension]._PZBAPRS_TYRS_CODE == "EVAPER")
                        {

                         calculate = _f.GetPercPoll(_perfil, result[dimension]._PZBAPPL._PZBCALR._PZBCALR_TERM, _poll, _calr, _pidm);

                        }
                        if (result[dimension]._PZBAPRS_TYRS_CODE == "MEDIA")
                        {

                            calculate = _f.GetMedItem(_perfil, _calr, _poll, _pidm);

                        }

                        if (result[dimension]._PZBAPRS_TYRS_CODE == "TAKVAL")
                        {

                         //      calculate = 

                        }

                        item._PZRRSPL_NUMB_VALUE = (short)calculate;
                        item._PZRRSPL_APRS_SEQ_NUMBER = result[dimension]._PZBAPRS_SEQ_NUMBER;
                        item._PZRRSPL_ACTIVITY_DATE = DateTime.Today;
                        item._PZRRSPL_DATA_ORIGIN = "BZPKEVAL";
                        item._PZRRSPL_USER = "SYSTEM";
                        item._PZRRSPL_PLAP_SEQ_NUMBER = _plap;

                        Models.ResultadoAplicable _resApl = new Models.ResultadoAplicable();
                        _resApl._PZBAPRS_CALR_CODE = result[dimension]._PZBAPRS_CALR_CODE;
                        _resApl._PZBAPRS_POLL_CODE = result[dimension]._PZBAPRS_POLL_CODE;
                        _resApl._PZBAPRS_SEQ_NUMBER = result[dimension]._PZBAPRS_SEQ_NUMBER;
                        _resApl._PZBAPRS_USER = result[dimension]._PZBAPRS_USER;
                        _resApl._PZBAPRS_DATA_ORIGIN = result[dimension]._PZBAPRS_DATA_ORIGIN;
                        _resApl._PZBAPRS_ACTIVITY_DATE = result[dimension]._PZBAPRS_ACTIVITY_DATE;
                        _resApl._PZBAPRS_ORDER = result[dimension]._PZBAPRS_ORDER;
                        _resApl._PZBAPRS_APPL_SEQ_NUMBER = result[dimension]._PZBAPRS_APPL_SEQ_NUMBER;
                        _resApl._PZBAPRS_TYRS_CODE = result[dimension]._PZBAPRS_TYRS_CODE;


                        Models.TipoResultado tr = new Models.TipoResultado();
                        tr._PZVTYRS_CODE = result[dimension]._PZVTYRS._PZVTYRS_CODE;
                        tr._PZVTYRS_DESCRIPTION = result[dimension]._PZVTYRS._PZVTYRS_DESCRIPTION;
                        tr._PZVTYRS_ACTIVITY_DATE = result[dimension]._PZVTYRS._PZVTYRS_ACTIVITY_DATE;
                        tr._PZVTYRS_DATA_ORIGIN = result[dimension]._PZVTYRS._PZVTYRS_DATA_ORIGIN;
                        tr._PZVTYRS_USER = result[dimension]._PZVTYRS._PZVTYRS_USER;
                        _resApl._PZVTYRS = tr;
                        item._PZBAPRS = _resApl;

                        valor.Add(item);

                    }

                    // return Ok(valor);


                }
                catch (Exception e)
                {

                }
            }


            FunctionsRepository _reg = new FunctionsRepository();
            string resultParcial = "";

            if (_type == "A")
            {
                resultParcial = _reg.PostPZRRSPL(_status, _plap, valor);
            }

            else {

                if (_type == "E")
                {

                    resultParcial = _reg.PutPZRRSPL(_status, _plap, valor);

                }

            }
          

            if (resultParcial == "Error")
            {

                return BadRequest("Error en el registro de resultados");
            }
           
     

            return Ok(valor);
        }



        /// <summary>
        /// 
        /// Modifica los Calendarios
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pZRCDEF"></param>
        /// <returns></returns>
        // PUT: api/PZBAPRS/5
        [HttpPut]
        [Route("api/PZBAPRS/PutPZBAPRS/{id}")]
        [ResponseType(typeof(ResultadoAplicable))]
        public IHttpActionResult PutPZBAPRS(string id, ResultadoAplicable pZRCDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }



            try
            {
                DateTime fecha = DateTime.Today;

                pZRCDEF._PZBAPRS_ACTIVITY_DATE = fecha;

                PZBAPRS _resApl = new PZBAPRS();

                //_resApl.PZBAPRS_ID = pZRCDEF.PZBAPRS_ID;
                _resApl.PZBAPRS_CALR_CODE = pZRCDEF._PZBAPRS_CALR_CODE;
                _resApl.PZBAPRS_ORDER = pZRCDEF._PZBAPRS_ORDER;
                _resApl.PZBAPRS_TYRS_CODE = pZRCDEF._PZBAPRS_TYRS_CODE;
                _resApl.PZBAPRS_POLL_CODE = pZRCDEF._PZBAPRS_POLL_CODE;
                _resApl.PZBAPRS_SEQ_NUMBER = pZRCDEF._PZBAPRS_SEQ_NUMBER;
                _resApl.PZBAPRS_USER = pZRCDEF._PZBAPRS_USER;
                _resApl.PZBAPRS_DATA_ORIGIN = pZRCDEF._PZBAPRS_DATA_ORIGIN;
                _resApl.PZBAPRS_ACTIVITY_DATE = pZRCDEF._PZBAPRS_ACTIVITY_DATE;
                _resApl.PZBAPRS_APPL_SEQ_NUMBER = pZRCDEF._PZBAPRS_APPL_SEQ_NUMBER;


                db.PZBAPRS.Attach(_resApl);
                db.Entry(_resApl).State = EntityState.Modified;
                db.SaveChanges();

                return Ok();//"El _resApl " + _resApl.PZBAPRS_NAME + " del periodo " + _resApl.PZBAPRS_TERM + " ha sido modificado exitosamente");

            }
            catch (DbUpdateConcurrencyException)
            {

                return NotFound();

            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PZBAPRS
        /// <summary>
        /// 
        /// Inserta un nuevo _resApl
        /// Ruta : api/PZBAPRS/PostPZBAPRS
        /// 
        /// </summary>
        /// <param name="pZRCDEF"></param>
        /// <returns></returns>
        [ResponseType(typeof(ResultadoAplicable))]
        public IHttpActionResult PostPZBAPRS(Models.ResultadoAplicable pZRCDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (db.PZBAPRS.Count(e => e.PZBAPRS_SEQ_NUMBER == pZRCDEF._PZBAPRS_SEQ_NUMBER) > 0)
            {

                return BadRequest("El codigo " + pZRCDEF._PZBAPRS_SEQ_NUMBER + " ya existe.");
            }


            try
            {
                int es = (int)db.PZBAPRS.Max(x => x.PZBAPRS_SEQ_NUMBER);


                es = es + 1;

                DateTime fecha = DateTime.Today;

                PZBAPRS _resApl = new PZBAPRS();
                // _resApl.PZBAPRS_ID = es;
                _resApl.PZBAPRS_CALR_CODE = pZRCDEF._PZBAPRS_CALR_CODE;
                _resApl.PZBAPRS_ORDER = pZRCDEF._PZBAPRS_ORDER;
                _resApl.PZBAPRS_TYRS_CODE = pZRCDEF._PZBAPRS_TYRS_CODE;
                _resApl.PZBAPRS_POLL_CODE = pZRCDEF._PZBAPRS_POLL_CODE;
                _resApl.PZBAPRS_SEQ_NUMBER = pZRCDEF._PZBAPRS_SEQ_NUMBER;
                _resApl.PZBAPRS_USER = pZRCDEF._PZBAPRS_USER;
                _resApl.PZBAPRS_APPL_SEQ_NUMBER = pZRCDEF._PZBAPRS_APPL_SEQ_NUMBER;
                _resApl.PZBAPRS_DATA_ORIGIN = pZRCDEF._PZBAPRS_DATA_ORIGIN;
                _resApl.PZBAPRS_ACTIVITY_DATE = pZRCDEF._PZBAPRS_ACTIVITY_DATE;
                _resApl.PZBAPRS_ACTIVITY_DATE = fecha;
                _resApl.PZBAPRS_USER = pZRCDEF._PZBAPRS_USER;
                // _resApl.PZBAPRS_POLL_CODE = pZRCDEF._PZBAPRS_POLL_CODE;

                db.PZBAPRS.Add(_resApl);

                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PZBAPRSExists(pZRCDEF._PZBAPRS_SEQ_NUMBER))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pZRCDEF._PZBAPRS_SEQ_NUMBER }, pZRCDEF);
        }

        // DELETE: api/PZBAPRS/5
        [ResponseType(typeof(PZBAPRS))]
        public IHttpActionResult DeletePZBAPRS(decimal id)
        {
            PZBAPRS pZRCDEF = db.PZBAPRS.Find(id);
            if (pZRCDEF == null)
            {
                return NotFound();
            }

            db.PZBAPRS.Remove(pZRCDEF);
            db.SaveChanges();

            return Ok(pZRCDEF);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZBAPRSExists(int id)
        {
            return db.PZBAPRS.Count(e => e.PZBAPRS_SEQ_NUMBER == id) > 0;
        }

    }

}

