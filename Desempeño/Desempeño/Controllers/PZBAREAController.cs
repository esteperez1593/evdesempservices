﻿using Desempeño.DataConect;
using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Desempeño.Controllers
{
    public class PZBAREAController : ApiController
    {

        private Entities db = new Entities();
        private Repository.ValidatorRepository _v = new Repository.ValidatorRepository();

        // GET: api/PZBAREA
        /// <summary>
        /// Obtiene la lista de _areas registrados
        /// RUTA : api/PZBAREA/GetPZBAREA
        /// </summary>
        /// <returns>List<Models.Area></returns>
        [HttpGet]
        [Route("api/PZBAREA/GetPZBAREA/")]
        [ResponseType(typeof(List<Models.Area>))]
        public IHttpActionResult GetPZBAREA()
        {
            List<Models.Area> result = new List<Models.Area>();
            try
            {

                //db.PZBAREA.Include(ins_area => ins_area.PZRPLAP).Include(_areafas => _areafas.PZRCRPH);
                var res = db.PZBAREA.SqlQuery(@"Select * from PZBAREA").ToList();

                foreach (var X in res)
                {

                    //Datos del Area
                    Models.Area _area = new Models.Area();
                    //  _area.PZBAREA_ID = X.PZBAREA_ID;
                    _area._PZBAREA_CODE = X.PZBAREA_CODE;
                    _area._PZBAREA_NAME = X.PZBAREA_NAME;
                    _area._PZBAREA_DESCRIPTION = X.PZBAREA_DESCRIPTION;
                    _area._PZBAREA_USER = X.PZBAREA_USER;
                    _area._PZBAREA_DATA_ORIGIN = X.PZBAREA_DATA_ORIGIN;
                    _area._PZBAREA_ACTIVITY_DATE = X.PZBAREA_ACTIVITY_DATE;
                    
                    //_area._PZBAREA_POLL_CODE = X.PZBAREA_POLL_CODE;
                    result.Add(_area);

                }

            }
            catch (Exception e)
            {

            }

            return Ok(result);
        }

        // GET: api/PZBAREA/5
        /// <summary>
        /// Devuelve un area a partir de su id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(Area))]
        public IHttpActionResult GetPZBAREA(decimal id)
        {
            PZBAREA pZRCDEF = db.PZBAREA.Find(id);
            if (pZRCDEF == null)
            {
                return NotFound();
            }

            return Ok(pZRCDEF);
        }

        /// <summary>
        /// 
        /// Modifica las Areas
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pZRCDEF"></param>
        /// <returns></returns>
        // PUT: api/PZBAREA/5
        [HttpPut]
        [Route("api/PZBAREA/PutPZBAREA/{id}/")]
        [ResponseType(typeof(Area))]
        public IHttpActionResult PutPZBAREA(string id, Area pZRCDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }



            try
            {

                String valid = _v.validarComponentesInstrumentos(id);
                if (valid == "CAMPO ASIGNADO") {

                    return BadRequest("Este campo no se puede editar porque ya fue asignado");

                }

                DateTime fecha = DateTime.Today;

                pZRCDEF._PZBAREA_ACTIVITY_DATE = fecha;

                PZBAREA _area = new PZBAREA();

                //_area.PZBAREA_ID = pZRCDEF.PZBAREA_ID;
                _area.PZBAREA_USER = pZRCDEF._PZBAREA_USER;
                _area.PZBAREA_CODE = pZRCDEF._PZBAREA_CODE;
                _area.PZBAREA_NAME = pZRCDEF._PZBAREA_NAME;
                _area.PZBAREA_DESCRIPTION = pZRCDEF._PZBAREA_DESCRIPTION;
                _area.PZBAREA_DATA_ORIGIN = pZRCDEF._PZBAREA_DATA_ORIGIN;
                _area.PZBAREA_ACTIVITY_DATE = pZRCDEF._PZBAREA_ACTIVITY_DATE;

                db.PZBAREA.Attach(_area);
                db.Entry(_area).State = EntityState.Modified;
                db.SaveChanges();

                return Ok("El codigo " + _area.PZBAREA_CODE + " para el area: " + _area.PZBAREA_NAME + " ha sido modificado exitosamente");

            }
            catch (DbUpdateConcurrencyException)
            {

                return NotFound();

            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PZBAREA
        /// <summary>
        /// 
        /// Inserta un nuevo _area
        /// Ruta : api/PZBAREA/PostPZBAREA
        /// 
        /// </summary>
        /// <param name="pZRCDEF"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/PZBAREA/PostPZBAREA/")]
        [ResponseType(typeof(Area))]
        public IHttpActionResult PostPZBAREA(Models.Area pZRCDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (db.PZBAREA.Count(e => e.PZBAREA_CODE == pZRCDEF._PZBAREA_CODE) > 0)
            {

                return BadRequest("El codigo " + pZRCDEF._PZBAREA_CODE + " ya existe.");
            }


            try
            {
              
                //Decimal es = (Decimal)db.PZBAREA.Max(x => x.PZBAREA_ID);


                //es = es + 1;

                DateTime fecha = DateTime.Today;

                PZBAREA _area = new PZBAREA();
                _area.PZBAREA_USER = pZRCDEF._PZBAREA_USER;
                _area.PZBAREA_CODE = pZRCDEF._PZBAREA_CODE;
                _area.PZBAREA_NAME = pZRCDEF._PZBAREA_NAME;
                _area.PZBAREA_DESCRIPTION = pZRCDEF._PZBAREA_DESCRIPTION;
                _area.PZBAREA_DATA_ORIGIN = pZRCDEF._PZBAREA_DATA_ORIGIN;
                _area.PZBAREA_ACTIVITY_DATE = fecha;
                _area.PZBAREA_USER = pZRCDEF._PZBAREA_USER;
                // _area.PZBAREA_POLL_CODE = pZRCDEF._PZBAREA_POLL_CODE;

                db.PZBAREA.Add(_area);

                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PZBAREAExists(pZRCDEF._PZBAREA_CODE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pZRCDEF._PZBAREA_CODE }, pZRCDEF);
        }

        // DELETE: api/PZBAREA/5
        [ResponseType(typeof(PZBAREA))]
        public IHttpActionResult DeletePZBAREA(decimal id)
        {
            PZBAREA pZRCDEF = db.PZBAREA.Find(id);
            if (pZRCDEF == null)
            {
                return NotFound();
            }

            db.PZBAREA.Remove(pZRCDEF);
            db.SaveChanges();

            return Ok(pZRCDEF);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZBAREAExists(String id)
        {
            return db.PZBAREA.Count(e => e.PZBAREA_CODE == id) > 0;
        }

    }
}
