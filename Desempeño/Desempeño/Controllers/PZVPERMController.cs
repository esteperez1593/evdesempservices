﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZVPERMController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/PZVPERM
        /// <summary>
        /// 
        /// Lista los Permisos existentes
        /// 
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult GetPZVPERMs()
        {
            List<Models.Permiso> result = new List<Models.Permiso>();
            try
            {

                var res = db.PZVPERM.SqlQuery(@"Select * from PZVPERM ").ToList();

                foreach (var x in res)
                {

                    //Datos de los permisos
                    Models.Permiso per = new Models.Permiso();
                    per._PZVPERM_CODE = x.PZVPERM_CODE;
                    per._PZVPERM_PFLE_CODE = x.PZVPERM_PFLE_CODE;
                    per._PZVPERM_POST_CODE = x.PZVPERM_POST_CODE;
                    per._PZVPERM_DNCY_CODE = x.PZVPERM_DNCY_CODE;
                    per._PZVPERM_RGHT_CODE = x.PZVPERM_RGHT_CODE;
                    per._PZVPERM_ACTIVITY_DATE = x.PZVPERM_ACTIVITY_DATE;
                    per._PZVPERM_DATA_ORIGIN = x.PZVPERM_DATA_ORIGIN;
                    per._PZVPERM_USER = x.PZVPERM_USER;

                    result.Add(per);

                }
            }
            catch (Exception e) { }

            return Ok(result);

        }

        /// <summary>
        /// Permisos de Un USUARIO
        /// </summary>
        /// <param name="pidm"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/PZVPERM/GetPZVPERM/{pidm}")]
        public IHttpActionResult GetPZVPERM(int pidm)
        {
            List<Models.DefinicionPermisos> result = new List<Models.DefinicionPermisos>();
            try
            {
               
                var res = db.PZVRGHT.SqlQuery(@"select pzvrght.* 
                                                from pzvperm,pzraspe,pzvrght,pzbprso
                                                where pzvperm.pzvperm_post_code= pzraspe.pzraspe_post_code
                                                    and pzvperm.pzvperm_dncy_code = pzraspe.pzraspe_dncy_code
                                                    and pzvperm.pzvperm_rght_code = pzvrght.pzvrght_code
                                                    and pzvperm.pzvperm_pidm =pzraspe.pzraspe_pidm
                                                    and pzbprso.pzbprso_pidm= pzraspe.pzraspe_pidm
                                                    and pzbprso.pzbprso_pidm='"+pidm+"' ").ToList();

                foreach (var x in res)
                {
                    //Datos del Instrumento Aplicable
                    Models.DefinicionPermisos _defpER = new Models.DefinicionPermisos();
                    _defpER._PZVRGHT_CODE = x.PZVRGHT_CODE;
                    _defpER._PZVRGHT_USER = x.PZVRGHT_USER;
                    _defpER._PZVRGHT_GRADE = x.PZVRGHT_GRADE;
                    _defpER._PZVRGHT_DATA_ORIGIN = x.PZVRGHT_DATA_ORIGIN;
                    _defpER._PZVRGHT_DESCRIPTION = x.PZVRGHT_DESCRIPTION;
                    _defpER._PZVRGHT_ACTIVITY_DATE = x.PZVRGHT_ACTIVITY_DATE;
                    result.Add(_defpER);

                }
            }
            catch (Exception e) { }

            return Ok(result);


        }


        [HttpGet]
        [Route("api/PZVPERM/GetPZVPERMMin/{pidm}")]
        [ResponseType(typeof(int))]
        public IHttpActionResult GetPZVPERMMin(int pidm)
        {
            int d=0;

            try
            {

               
                var res = db.PZVRGHT.SqlQuery(@" select DISTINCT pzvrght.* from pzvperm,pzraspe,pzvrght,pzbprso
                                                        where pzvperm.pzvperm_post_code= pzraspe.pzraspe_post_code
                                                            and pzvperm.pzvperm_dncy_code = pzraspe.pzraspe_dncy_code
                                                            and pzvperm.pzvperm_rght_code = pzvrght.pzvrght_code
                                                            and pzvperm.pzvperm_pidm =pzraspe.pzraspe_pidm
                                                            and pzbprso.pzbprso_pidm= pzraspe.pzraspe_pidm
                                                            and pzvrght.pzvrght_grade = ( select MIN (pzvrght.PZVRGHT_GRADE) 
                                                                                            from pzvperm,pzraspe,pzvrght,pzbprso
                                                                                            where pzvperm.pzvperm_post_code= pzraspe.pzraspe_post_code
                                                                                                and pzvperm.pzvperm_dncy_code = pzraspe.pzraspe_dncy_code
                                                                                                and pzvperm.pzvperm_rght_code = pzvrght.pzvrght_code
                                                                                                and pzvperm.pzvperm_pidm =pzraspe.pzraspe_pidm
                                                                                                and pzbprso.pzbprso_pidm= pzraspe.pzraspe_pidm
                                                                                                and pzbprso.pzbprso_pidm='"+pidm+"') ").ToList();
                foreach (var X in res)
                {

                    //Datos del Instrumento Aplicable
                    DefinicionPermisos _defpER = new Models.DefinicionPermisos();
                    _defpER._PZVRGHT_CODE = X.PZVRGHT_CODE;
                    _defpER._PZVRGHT_USER = X.PZVRGHT_USER;
                    _defpER._PZVRGHT_GRADE = X.PZVRGHT_GRADE;
                    _defpER._PZVRGHT_DATA_ORIGIN = X.PZVRGHT_DATA_ORIGIN;
                    _defpER._PZVRGHT_DESCRIPTION = X.PZVRGHT_DESCRIPTION;
                    _defpER._PZVRGHT_ACTIVITY_DATE = X.PZVRGHT_ACTIVITY_DATE;

                    d = _defpER._PZVRGHT_GRADE;
                }


               

                
            }
            catch (Exception e) { }

            return Ok(d);


        }

        /// <summary>
        /// 
        /// Modifica los permisos registrados
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pZVLCOD"></param>
        /// <returns></returns>
        /// 
        // PUT: api/PZVPERM/5
        [HttpPut]
        [Route("api/PZVPERM/PutPZVPERM/{id}/")]
        [ResponseType(typeof(Permiso))]
        public IHttpActionResult PutPZVPERM(int id, Permiso _pZVLCOD)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != _pZVLCOD._PZVPERM_CODE)
            {
                return BadRequest();
            }

         

            try
            {
                DateTime fecha = DateTime.Today;
                _pZVLCOD._PZVPERM_ACTIVITY_DATE = fecha;

                PZVPERM permisos= new PZVPERM();

                //permisos.PZVPERM_ID = pZVLCOD.PZVPERM_ID;
                permisos.PZVPERM_POST_CODE = _pZVLCOD._PZVPERM_POST_CODE;
                permisos.PZVPERM_DNCY_CODE = _pZVLCOD._PZVPERM_DNCY_CODE;
                permisos.PZVPERM_PFLE_CODE = _pZVLCOD._PZVPERM_PFLE_CODE;
                permisos.PZVPERM_CODE = (int) _pZVLCOD._PZVPERM_CODE;
                
                permisos.PZVPERM_USER = _pZVLCOD._PZVPERM_USER;
                permisos.PZVPERM_DATA_ORIGIN = _pZVLCOD._PZVPERM_DATA_ORIGIN;
                permisos.PZVPERM_ACTIVITY_DATE = _pZVLCOD._PZVPERM_ACTIVITY_DATE;
                permisos.PZVPERM_PIDM = _pZVLCOD._PZVPERM_PIDM;
                permisos.PZVPERM_RGHT_CODE = _pZVLCOD._PZVPERM_RGHT_CODE;
                db.PZVPERM.Attach(permisos);
                db.Entry(permisos).State = EntityState.Modified;
                db.SaveChanges();

                return Ok("Permiso Modificado exitosamente");

            }
            catch (DbUpdateConcurrencyException)
            {

            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        [HttpPost]
        [ResponseType(typeof(List<Models.Permiso>))]
        [Route("api/PZVPERM/PostPZVPERM/")]
        public IHttpActionResult PostPZVPERM(List<Models.Permiso> _pZVLCOD)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {


                foreach (var i in _pZVLCOD)
                {

                    //Decimal es = (Decimal)db.PZVPERM.Max(x => x.PZVPERM_CODE);


                    //es = es + 1;

                    DateTime fecha = DateTime.Today;

                    //aqui tendria que enviar el usernaame ademas de lo que esta.

                    i._PZVPERM_ACTIVITY_DATE = fecha;
                    //i._PZRAWAP_SEQ_NUMBER = (int)es;



                    PZVPERM _permiso = new PZVPERM();

                    _permiso.PZVPERM_POST_CODE = i._PZVPERM_POST_CODE;
                    _permiso.PZVPERM_DNCY_CODE = i._PZVPERM_DNCY_CODE;
                    _permiso.PZVPERM_PFLE_CODE = i._PZVPERM_PFLE_CODE;
                    _permiso.PZVPERM_CODE = (int) i._PZVPERM_CODE;

                    _permiso.PZVPERM_USER = i._PZVPERM_USER;
                    _permiso.PZVPERM_DATA_ORIGIN = i._PZVPERM_DATA_ORIGIN;
                    _permiso.PZVPERM_ACTIVITY_DATE = i._PZVPERM_ACTIVITY_DATE;
                    _permiso.PZVPERM_PIDM = i._PZVPERM_PIDM;
                    _permiso.PZVPERM_RGHT_CODE =i._PZVPERM_RGHT_CODE;

                    db.PZVPERM.Add(_permiso);

                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {

            }


            return CreatedAtRoute("DefaultApi", new { id = _pZVLCOD }, _pZVLCOD);
        }
        
        // POST: api/PZVPERM

        /// <summary>
        /// 
        /// Agrega los permisos 
        /// 
        /// </summary>
        /// <param name="pZVLCOD"></param>
        /// <returns></returns>
        [ResponseType(typeof(Permiso))]
        public IHttpActionResult PostPZVPERM(Models.Permiso _pZVLCOD)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            try
            {
              //  Decimal es = (Decimal)db.PZVPERM.Max(x => x.PZVPERM_ID);

                //es = es + 1;

                DateTime fecha = DateTime.Today;

                PZVPERM objPer = new PZVPERM();
                //objPer.PZVPERM_ID = es;
                objPer.PZVPERM_CODE = (int) _pZVLCOD._PZVPERM_CODE;
                objPer.PZVPERM_PFLE_CODE = _pZVLCOD._PZVPERM_PFLE_CODE;
                objPer.PZVPERM_DNCY_CODE = _pZVLCOD._PZVPERM_DNCY_CODE;
                objPer.PZVPERM_POST_CODE = _pZVLCOD._PZVPERM_POST_CODE;
                objPer.PZVPERM_RGHT_CODE= _pZVLCOD._PZVPERM_RGHT_CODE;
                objPer.PZVPERM_USER = _pZVLCOD._PZVPERM_USER;
                objPer.PZVPERM_ACTIVITY_DATE = fecha;
                objPer.PZVPERM_DATA_ORIGIN = _pZVLCOD._PZVPERM_DATA_ORIGIN;
                objPer.PZVPERM_PIDM = (int)_pZVLCOD._PZVPERM_PIDM;

                db.PZVPERM.Add(objPer);

                db.SaveChanges();


            }
            catch (DbUpdateException)
            {
            
            }

            return CreatedAtRoute("DefaultApi", new { id = _pZVLCOD._PZVPERM_CODE }, _pZVLCOD);
        }

        // DELETE: api/PZVPERM/5
        [ResponseType(typeof(PZVPERM))]
        public IHttpActionResult DeletePZVPERM(decimal id)
        {
            PZVPERM pZVLCOD = db.PZVPERM.Find(id);
            if (pZVLCOD == null)
            {
                return NotFound();
            }

            db.PZVPERM.Remove(pZVLCOD);
            db.SaveChanges();

            return Ok(pZVLCOD);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZVPERMExists(int id)
        {
            return db.PZVPERM.Count(e => e.PZVPERM_CODE == id) > 0;
        }
    }
}