﻿using Desempeño.Models;
using Flurl;
using Flurl.Http;
using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Desempeño.Controllers
{
    public class BannerUserController : ApiController
    {
        /// <summary>
        /// Servicio que consulta a traves del parametro pidm los datos de ese usuario
        /// </summary>
        /// <param name="pidm">PIDM del usuario</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/BannerUser/GetBannerUser/{pidm}")]
        [ResponseType(typeof(BannerUser))]
        public async Task <BannerUser>GetBannerUser(long? pidm)
        {
            try
            {
                string configvalue1 = ConfigurationManager.AppSettings["apiRest_user"];
                string configvalue2 = ConfigurationManager.AppSettings["api_pass"];
                return await ConfigurationManager.AppSettings["servBanner"].AppendPathSegment("v1")
                                                                         .AppendPathSegment("persons")
                                                                         .AppendPathSegment(pidm + "/")
                                                                         .WithBasicAuth(configvalue1, configvalue2)
                                                                         .WithHeaders("Authorization: Token 988542a2d56fba15a5d4e54d4ef91fee9358d98c")
                                                                         /*  .WithOAuthBearerToken("988542a2d56fba15a5d4e54d4ef91fee9358d98c")*/
                                                                         .GetJsonAsync<BannerUser>();
            }
            catch (FlurlHttpException e)
            {
                Console.WriteLine(e);
                return null;
            }


        }
        /// <summary>
        /// Servicio que consulta los datos de un usuario a traves de su id, es decir su cedula
        /// </summary>
        /// <param name="id">Cedula del Usuario</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/BannerUser/GetBannerUserById/{id}")]
        [ResponseType(typeof(BannerUser))]
        public async Task<BannerUser> GetBannerUserById(string id)
        {
            try
            {
                string configvalue1 = ConfigurationManager.AppSettings["apiRest_user"];
                string configvalue2 = ConfigurationManager.AppSettings["api_pass"];
                return await ConfigurationManager.AppSettings["servBanner"].AppendPathSegment("v1")
                                                                         .AppendPathSegment("persons")
                                                                         .AppendPathSegment("document_id:" + id + "/")
                                                                         .WithBasicAuth(configvalue1, configvalue2)
                                                                         .WithHeaders("Authorization: Token 988542a2d56fba15a5d4e54d4ef91fee9358d98c")
                                                                         /*.WithHeaders("Authorization: Token 988542a2d56fba15a5d4e54d4ef91fee9358d98c")*/
                                                                         /*.WithOAuthBearerToken("988542a2d56fba15a5d4e54d4ef91fee9358d98c")*/
                                                                         .GetJsonAsync<BannerUser>();
            }
            catch (FlurlHttpException e)
            {
                Console.WriteLine(e);
                return null;
            }


        }
    }
}
