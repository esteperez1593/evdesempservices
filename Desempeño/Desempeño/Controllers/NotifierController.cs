﻿using Desempeño.DataConect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Desempeño.Controllers
{
    public class NotifierController : ApiController
    {
        private Entities db = new Entities();

        /// <summary>
        /// 
        /// Envia Correo a un Usuario dado el pidm 
        /// desde el sistema de evaluacion de desempeño
        /// 
        /// </summary>
        /// <param name="_pidm"></param>
        /// <param name="_titulo"></param>
        /// <param name="_contenido"></param>
        /// <returns></returns>

        [HttpGet]
        [Route("api/Notifier/GetNofifierPidm/")]
        [ResponseType(typeof(String))]
        public IHttpActionResult GetNofifierPidm(String _pidm, String _titulo, String _contenido)
        {
            /* SELECT baninst1.f_sendemail(baninst1.f_bus_email (f_bus_pidm('20913337'),NULL,'A','Y')
               , null, 'evaluacionRRHH@ucab.edu.ve', 'Prueba de Estefania', 'smtp2.ucab.edu.ve', 'Envio de Correo Banner Prueba', 'Hello World')
                --INTO :lst_email_success_reply
                FROM dual;
              */

            try
            {
                var res = db.Database.SqlQuery<String>(@" SELECT baninst1.f_sendemail(
                                                                 baninst1.f_bus_email ('" + _pidm + "',NULL,'A','Y'), null, 'evaluacionRRHH@ucab.edu.ve', 'Evaluación de Desempeño', 'smtp2.ucab.edu.ve', '" + _titulo + "', '" + _contenido + "') " +
                                                                " FROM dual"
                                                                 ).ToList();
                if (res.Equals("T - Mail Sent"))
                {

                    return (Ok("Enviado"));

                }
                else
                {
                    return BadRequest("No se pudo enviar. Intente Nuevamente. ");
                }

            }

            catch (Exception e)
            {

                return (NotFound());

            }


        }


        /// <summary>
        /// 
        /// Envia correos a un grupo de Usuarios
        /// 
        /// </summary>
        /// <param name="_pidm"></param>
        /// <param name="_titulo"></param>
        /// <param name="_contenido"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Notifier/GetNofifierGroup/")]
        [ResponseType(typeof(String))]
        public IHttpActionResult GetNofifierGroup(String[] _pidm, String _titulo, String _contenido)
        {

            try
            {
                for (int dimension = 1; dimension <= _pidm.Length; dimension++)
                {

                    var res = db.Database.SqlQuery<String>(@" SELECT baninst1.f_sendemail(
                                                                 baninst1.f_bus_email ('" + _pidm[dimension] + "',NULL,'A','Y'), null, 'evaluacionRRHH@ucab.edu.ve', 'Evaluación de Desempeño', 'smtp2.ucab.edu.ve', '" + _titulo + "', '" + _contenido + "') " +
                                                                " FROM dual"
                                                                 ).ToList();
                }

                return Ok("Enviados");

            }

            catch (Exception e)
            {

                return (NotFound());

            }


        }





    }
}
