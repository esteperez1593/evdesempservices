﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZVPOSTController : ApiController
    {
        private Entities db = new Entities();


        /// <summary>
        /// 
        /// Lista el conjunto de Cargos existentes
        /// RUTA: api/PZVPOST/GetPZVPOST
        /// 
        /// </summary>
        /// <returns>re</returns>
        [HttpGet]
        [Route("api/PZVPOST/GetPZVPOST/")]
        [ResponseType(typeof(Cargo))]
        public List<Models.Cargo> GetPZVPOST()
        { 
            List<Models.Cargo> re = new List<Models.Cargo>();
        
            try
            {
                db.PZVPOST.Include(B => B.PZVPOST1);   
                var es = db.PZVPOST.SqlQuery(@"select * FROM PZVPOST ").ToList();

                foreach (var result in es)
                {
                    Models.Cargo item = new Models.Cargo();

                 //   item.PZVPOST_ID = result.PZVPOST_ID;
                    item._PZVPOST_CODE = result.PZVPOST_CODE;
                    item._PZVPOST_NAME = result.PZVPOST_NAME;
                    item._PZVPOST_CODE_SUP = result.PZVPOST_CODE_SUP;
                    item._PZVPOST_USER = result.PZVPOST_USER;
                    item._PZVPOST_DESCRIPTION = result.PZVPOST_DESCRIPTION;
                    item._PZVPOST_ACTIVITY_DATE = result.PZVPOST_ACTIVITY_DATE;
                    

                    re.Add(item);
                }

              }
            catch(Exception e) {

            }

            return re;
        }



        /// <summary>
        /// 
        ///
        /// Cargos evaluadores
        /// RUTA: api/PZVPOST/GetPZVPOST
        /// 
        /// </summary>
        /// <returns>re</returns>
        [HttpGet]
        [Route("api/PZVPOST/GetPZVPOSTRev/{_post}")]
        [ResponseType(typeof(Cargo))]
        public List<Models.Cargo> GetPZVPOSTRev(String _post)
        {
            List<Models.Cargo> re = new List<Models.Cargo>();

            try
            {
                db.PZVPOST.Include(B => B.PZVPOST1);
                var es = db.PZVPOST.SqlQuery(@" select rev.* 
                                               FROM PZVPOST car ,PZVPOST rev
                                                WHERE   car.pzvpost_code_sup = rev.pzvpost_code
                                                and car.pzvpost_code='"+_post+"' ").ToList();


                foreach (var result in es)
                {
                   /* Models.Cargo item = new Models.Cargo();

                    //   item.PZVPOST_ID = result.PZVPOST_ID;
                    item._PZVPOST_CODE = result.PZVPOST_CODE;
                    item._PZVPOST_NAME = result.PZVPOST_NAME;
                    item._PZVPOST_USER = result.PZVPOST_USER;
                    item._PZVPOST_CODE_SUP = result.PZVPOST_CODE_SUP;
                    item._PZVPOST_DATA_ORIGIN = result.PZVPOST_DATA_ORIGIN;
                    item._PZVPOST_DESCRIPTION = result.PZVPOST_DESCRIPTION;
                    item._PZVPOST_ACTIVITY_DATE = result.PZVPOST_ACTIVITY_DATE;
                    */
                    Models.Cargo supervisor = new Models.Cargo();
                    supervisor._PZVPOST_CODE = result.PZVPOST2.PZVPOST_CODE;
                    supervisor._PZVPOST_NAME = result.PZVPOST2.PZVPOST_NAME;
                    supervisor._PZVPOST_USER = result.PZVPOST2.PZVPOST_USER;
                    supervisor._PZVPOST_CODE_SUP = result.PZVPOST2.PZVPOST_CODE_SUP;
                    supervisor._PZVPOST_DESCRIPTION = result.PZVPOST2.PZVPOST_DESCRIPTION;
                    supervisor._PZVPOST_ACTIVITY_DATE = result.PZVPOST2.PZVPOST_ACTIVITY_DATE;
                    supervisor._PZVPOST_DATA_ORIGIN = result.PZVPOST2.PZVPOST_DATA_ORIGIN;

                   // item._PZVPOST1.Add(supervisor);

                    re.Add(supervisor);
                }

            }
            catch (Exception e)
            {

            }

            return re;
        }


        [HttpGet]
        [Route("api/PZVPOST/GetPOSTPOST/")]
        [ResponseType(typeof(Cargo))]
        public List<Models.Cargo> GetPOSTPOST()
        {
            List<Models.Cargo> re = new List<Models.Cargo>();

            try
            {
                db.PZVPOST.Include(B => B.PZVPOST1);
                var es = db.PZVPOST.SqlQuery(@" select * 
                                               FROM PZVPOST car ,PZVPOST rev
                                                WHERE   car.pzvpost_code_sup = rev.pzvpost_code
                                                 ").ToList();


                foreach (var result in es)
                {
                    Models.Cargo item = new Models.Cargo();

                    //   item.PZVPOST_ID = result.PZVPOST_ID;
                    item._PZVPOST_CODE = result.PZVPOST_CODE;
                    item._PZVPOST_NAME = result.PZVPOST_NAME;
                    item._PZVPOST_USER = result.PZVPOST_USER;
                    item._PZVPOST_CODE_SUP = result.PZVPOST_CODE_SUP;
                    item._PZVPOST_DATA_ORIGIN = result.PZVPOST_DATA_ORIGIN;
                    item._PZVPOST_DESCRIPTION = result.PZVPOST_DESCRIPTION;
                    item._PZVPOST_ACTIVITY_DATE = result.PZVPOST_ACTIVITY_DATE;

                    Models.Cargo supervisor = new Models.Cargo();
                    supervisor._PZVPOST_CODE = result.PZVPOST2.PZVPOST_CODE;
                    supervisor._PZVPOST_NAME = result.PZVPOST2.PZVPOST_NAME;
                    supervisor._PZVPOST_USER = result.PZVPOST2.PZVPOST_USER;
                    supervisor._PZVPOST_CODE_SUP = result.PZVPOST2.PZVPOST_CODE_SUP;
                    supervisor._PZVPOST_DESCRIPTION = result.PZVPOST2.PZVPOST_DESCRIPTION;
                    supervisor._PZVPOST_ACTIVITY_DATE = result.PZVPOST2.PZVPOST_ACTIVITY_DATE;
                    supervisor._PZVPOST_DATA_ORIGIN = result.PZVPOST2.PZVPOST_DATA_ORIGIN;

                    item._PZVPOST1.Add(supervisor);

                    re.Add(item);
                }

            }
            catch (Exception e)
            {

            }

            return re;
        }

        /// <summary>
        /// 
        /// Dado un id devuelve el cargo
        /// RUTA: api/PZVPOST/GetPZVPOST/5
        /// 
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Cargo encontrado</returns>
        [HttpGet]
        [ResponseType(typeof(Models.Cargo))]
        public IHttpActionResult GetPZVPOST(String id)
        {
            try
            {
                PZVPOST pZCRCOD = db.PZVPOST.Find(id);
                Models.Cargo result = new Models.Cargo();

                //result.PZVPOST_ID = pZCRCOD.PZVPOST_ID;
                result._PZVPOST_CODE = pZCRCOD.PZVPOST_CODE;
                result._PZVPOST_DESCRIPTION = pZCRCOD.PZVPOST_DESCRIPTION;
                result._PZVPOST_CODE_SUP = pZCRCOD.PZVPOST_CODE_SUP;
                result._PZVPOST_NAME = pZCRCOD.PZVPOST_NAME;
                result._PZVPOST_USER = pZCRCOD.PZVPOST_USER;
                result._PZVPOST_ACTIVITY_DATE = pZCRCOD.PZVPOST_ACTIVITY_DATE;
                result._PZVPOST_DATA_ORIGIN = pZCRCOD.PZVPOST_DATA_ORIGIN;

                return Ok(result);

                if (pZCRCOD == null)
                {
                    return NotFound();
                }
            }
            catch (Exception e) {

            }

            return Ok();
           
        }
        /// <summary>
        /// 
        /// Lista los cargos con los supervisores que les corresponden
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseTypeAttribute(typeof(List<Models.Cargo>))]
        public IHttpActionResult GetCargosYSupervisores()
        {
            List<Models.Cargo> re = new List<Models.Cargo>();

            try
            {

                var es = db.PZVPOST.SqlQuery(@"select car.*,sup.*
                                                FROM PZVPOST car , PZVPOST sup 
                                                where car.PZVPOST_CODE_sup = sup.PZVPOST_CODE 
                                                ").ToList();

                foreach (var result in es)
                {
                    //Cargo

                    Models.Cargo item = new Models.Cargo();

                   // item.PZVPOST_ID = result.PZVPOST_ID;
                    item._PZVPOST_CODE = result.PZVPOST_CODE;
                    item._PZVPOST_NAME = result.PZVPOST_NAME;
                    item._PZVPOST_CODE_SUP = result.PZVPOST_CODE_SUP;
                    item._PZVPOST_USER = result.PZVPOST_USER;
                    item._PZVPOST_DESCRIPTION = result.PZVPOST_DESCRIPTION;
                    item._PZVPOST_ACTIVITY_DATE = result.PZVPOST_ACTIVITY_DATE;
                    item._PZVPOST_DATA_ORIGIN = result.PZVPOST_DATA_ORIGIN;
                    //Supervisor

                    Models.Cargo itemSup = new Models.Cargo();

                    
                   // itemSup.PZVPOST_ID = result.PZVPOST2.PZVPOST_ID;
                    itemSup._PZVPOST_CODE = result.PZVPOST2.PZVPOST_CODE;
                    itemSup._PZVPOST_NAME = result.PZVPOST2.PZVPOST_NAME;
                    itemSup._PZVPOST_CODE_SUP = result.PZVPOST2.PZVPOST_CODE_SUP;
                    itemSup._PZVPOST_USER = result.PZVPOST2.PZVPOST_USER;
                    itemSup._PZVPOST_DESCRIPTION = result.PZVPOST2.PZVPOST_DESCRIPTION;
                    itemSup._PZVPOST_ACTIVITY_DATE = result.PZVPOST2.PZVPOST_ACTIVITY_DATE;
                    itemSup._PZVPOST_DATA_ORIGIN = result.PZVPOST2.PZVPOST_DATA_ORIGIN;

                    item._PZVPOST2 = itemSup;

                    re.Add(item);
                }

            }
            catch (Exception e)
            {

            }

            return Ok(re);
        }

        // PUT: api/PZVPOST/5
        /// <summary>
        /// 
        /// Modifica los cargos registrados
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pZCRCOD"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/PZVPOST/PutPZVPOST/{id}/")]
        [ResponseType(typeof(Cargo))]

        public IHttpActionResult PutPZVPOST(String id, Cargo pZCRCOD)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pZCRCOD._PZVPOST_CODE)
            {
                return BadRequest();
            }


            try
            {
                DateTime fecha = DateTime.Today;

                pZCRCOD._PZVPOST_ACTIVITY_DATE = fecha;

                PZVPOST cargo = new PZVPOST();

          //      cargo.PZVPOST_ID = pZCRCOD.PZVPOST_ID;
                cargo.PZVPOST_CODE = pZCRCOD._PZVPOST_CODE;
                cargo.PZVPOST_USER = pZCRCOD._PZVPOST_USER;
                cargo.PZVPOST_CODE_SUP = pZCRCOD._PZVPOST_CODE_SUP;
                cargo.PZVPOST_NAME = pZCRCOD._PZVPOST_NAME;
                cargo.PZVPOST_DESCRIPTION = pZCRCOD._PZVPOST_DESCRIPTION;
                cargo.PZVPOST_DATA_ORIGIN = pZCRCOD._PZVPOST_DATA_ORIGIN;
                cargo.PZVPOST_ACTIVITY_DATE = pZCRCOD._PZVPOST_ACTIVITY_DATE;

                db.PZVPOST.Attach(cargo);
                System.Diagnostics.Debug.WriteLine(cargo);
                db.Entry(cargo).State = EntityState.Modified;
                System.Diagnostics.Debug.WriteLine(db.Entry(cargo).State = EntityState.Modified);

                db.SaveChanges();

                return Ok("El cargo "+cargo.PZVPOST_DESCRIPTION+" se ha modificado exitosamente");
            }
            catch (DbUpdateConcurrencyException)
            {

                Ok("No se pudo actualizar el registro");
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PZVPOST
        /// <summary>
        /// 
        /// Crea nuevas Dependencias
        /// 
        /// RUTA : api/PZVPOST/PostPZVPOST
        /// 
        /// </summary>
        /// <param name="pZCRCOD"></param>
        /// 
        /// <returns>  return CreatedAtRoute("DefaultApi", new { id = pZCRCOD.PZVPOST_ID }, pZCRCOD) </returns>
        [HttpPost]
        [Route("api/PZVPOST/PostPZVPOST/")]
        [ResponseType(typeof(Cargo))]
        public IHttpActionResult PostPZVPOST(Cargo pZCRCOD)
        {

            PZVPOST cargo = new PZVPOST();

            DateTime fecha = DateTime.Today;

            pZCRCOD._PZVPOST_ACTIVITY_DATE = fecha;

            if (db.PZVPOST.Count(e => e.PZVPOST_CODE == pZCRCOD._PZVPOST_CODE) > 0)
            {

                return BadRequest("El codigo de la del cargo " + pZCRCOD._PZVPOST_CODE + " ya existe.");
            }

            



            cargo.PZVPOST_USER = pZCRCOD._PZVPOST_USER;
            cargo.PZVPOST_CODE = pZCRCOD._PZVPOST_CODE;
            cargo.PZVPOST_CODE_SUP = pZCRCOD._PZVPOST_CODE_SUP;
            cargo.PZVPOST_NAME = pZCRCOD._PZVPOST_NAME;
            cargo.PZVPOST_DESCRIPTION = pZCRCOD._PZVPOST_DESCRIPTION;
            cargo.PZVPOST_DATA_ORIGIN = pZCRCOD._PZVPOST_DATA_ORIGIN;
            cargo.PZVPOST_ACTIVITY_DATE = pZCRCOD._PZVPOST_ACTIVITY_DATE;


            db.PZVPOST.Add(cargo);


            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PZVPOSTExists(pZCRCOD._PZVPOST_CODE))
                {
                    return BadRequest("El codigo Ingresado ya se encuenta registrado en el sistema");
                }
                else
                {
                    throw;
                }

            }

            return CreatedAtRoute("DefaultApi", new { id = pZCRCOD._PZVPOST_CODE }, pZCRCOD);
        }

        // DELETE: api/PZVPOST/5
        [HttpDelete]
        [ResponseType(typeof(PZVPOST))]
        public IHttpActionResult DeletePZVPOST(String id)
        {
            PZVPOST pZCRCOD = db.PZVPOST.Find(id);
            if (pZCRCOD == null)
            {
                return NotFound();
            }

            db.PZVPOST.Remove(pZCRCOD);
            db.SaveChanges();

            return Ok(pZCRCOD);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZVPOSTExists(String id)
        {
            return db.PZVPOST.Count(e => e.PZVPOST_CODE == id) > 0;
        }
    }
}