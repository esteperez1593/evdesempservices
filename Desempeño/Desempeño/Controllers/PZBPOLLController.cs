using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
//using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using System.Data.Entity.Infrastructure;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZBPOLLController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/PZBPOLL
        //RUTA: api/PZBPOLL/Getapi/PZBPOLL
        /// <summary>
        /// 
        /// Lista todos los instrumentos de evaluacion registrados 
        /// 
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.Instrumento>))]
        [Route("api/PZBPOLL/GetPZBPOLL/")]
        public IHttpActionResult GetPZBPOLL()
        {

            List<Models.Instrumento> result = new List<Models.Instrumento>();
            try
            {
                // db.PZBPOLL.Include(p => p.PZP);
                var res = db.PZBPOLL.SqlQuery(@" SELECT * from PZBPOLL ").ToList();

                foreach (var x in res)
                {
                    Models.Instrumento ins = new Models.Instrumento();

                    // ins.PZBPOLL_ID = x.PZBPOLL_ID;
                    ins._PZBPOLL_NAME = x.PZBPOLL_NAME;
                    ins._PZBPOLL_CODE = x.PZBPOLL_CODE;
                    ins._PZBPOLL_USER = x.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = x.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = x.PZBPOLL_ACTIVITY_DATE;
                    result.Add(ins);

                }

                return (Ok(result));

            }

            catch (Exception e)
            {

                return (NotFound());

            }


        }
        /// <summary>
        /// Dado el pidm del usuario muestra sus instrumentos 
        /// activos pendientes
        /// </summary>
        /// <param name="pidm"> PIDM del usuario</param>
        /// <param name="status">Estatus del Instrumento</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.InstrumentoAplicable>))]
        [Route("api/PZBPOLL/GetPZBPOLLP/{pidm}/{status}")]
        public IHttpActionResult GetPZBPOLLP(int pidm, String status)
        {

            List<Models.InstrumentoAplicable> result = new List<Models.InstrumentoAplicable>();
            try
            {
                db.PZBAPPL.Include(p => p.PZBCALR)
                    .Include(c => c.PZBPOLL)
                    .Include(a => a.PZBCALR.PZBPHSE)
                    .Include(s => s.PZBPLQS)
                    .Include(p => p.PZRPLAP);



                var res = db.PZBAPPL.SqlQuery(@"SELECT DISTINCT PZBAPPL.*,PZBPOLL.*
                                                FROM PZBPLQS,PZRPLAP,
                                                PZBCALR,
                                                PZVPFLE, 
                                                PZRASPE,
                                                PZBPRSO,
                                                PZRDYPT,
                                                PZVDNCY,
                                                PZVPOST,
                                                PZBAPPL,
                                                pzbpoll,
                                                PZBQSTN,
                                                PZBPHSE,
                                                PZVPOST PZVPOST1,
                                                PZVDNCY PZVDNCY1
                                                WHERE pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                AND   pzbappl.pzbappl_pfle_code = PZRPLAP.PZRPLAP_PFLE_CODE
                                                AND   PZRPLAP.PZRPLAP_PFLE_code = pzraspe.pzraspe_pfle_code
                                                AND   pzraspe.pzraspe_pfle_code = PZVPFLE.PZVPFLE_code
                                                AND   pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                and   pzbappl.pzbappl_calr_code = PZRPLAP.PZRPLAP_CALR_code 
                                                AND   pzbappl.pzbappl_calr_code = PZBCALR.PZBCALR_CODE
                                                AND pzbcalr.pzbcalr_code = pzbphse.pzbphse_calr_code
                                                    AND SYSDATE >= pzbphse.pzbphse_open_date
                                                    and sysdate <=pzbphse.pzbphse_close_date 
                                               
                                                AND   pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                AND   pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                AND   PZRDYPT.PZRDYPT_POST_CODE = PZVPOST.PZVPOST_CODE
                                                AND   PZVPOST.PZVPOST_CODE_sup = PZVPOST1.PZVPOST_CODE 
                                                AND   pzrplap.pzrplap_dncy_code= pzraspe.pzraspe_dncy_code
                                                AND   pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                AND   PZRDYPT.PZRDYPT_DNCY_CODE = PZVDNCY.PZVDNCY_CODE
                                                AND   PZVDNCY.PZVDNCY_CODE_sup = PZVDNCY1.PZVDNCY_CODE
                                                AND   PZRPLAP.PZRPLAP_PIDM = PZRASPE.PZRASPE_pidm 
                                                AND   PZRASPE.PZRASPE_pidm = pzbprso.pzbprso_pidm
                                                AND   PZBPLQS.PZBPLQS_QSTN_CODE = PZBQSTN.PZBQSTN_CODE
                                                AND   pzbplqs.pzbplqs_appl_seq_number = pzbappl.pzbappl_seq_number
                                                AND   PZBPLQS.PZBPLQS_POLL_CODE = pzbappl.pzbappl_poll_code
                                                AND   PZBPLQS.PZBPLQS_CALR_CODE = pzbappl.pzbappl_calr_code
                                                AND   pzvpfle.pzvpfle_code = pzbappl.pzbappl_pfle_code
                                                AND PZRPLAP.PZRPLAP_STATUS = '" + status + "' " +
                                               "and pzrplap.pzrplap_pidm = '" + pidm + "'").ToList();

                foreach (var x in res)
                {

                    Models.InstrumentoAplicable apl = new Models.InstrumentoAplicable();
                    apl._PZBAPPL_USER = x.PZBAPPL_USER;
                    apl._PZBAPPL_PRCNT = x.PZBAPPL_PRCNT;
                    apl._PZBAPPL_SEQ_NUMBER = x.PZBAPPL_SEQ_NUMBER;
                    apl._PZBAPPL_POLL_CODE = x.PZBAPPL_POLL_CODE;
                    apl._PZBAPPL_PFLE_CODE = x.PZBAPPL_PFLE_CODE;
                    apl._PZBAPPL_CALR_CODE = x.PZBAPPL_CALR_CODE;
                    apl._PZBAPPL_ACTIVITY_DATE = x.PZBAPPL_ACTIVITY_DATE;
                    apl._PZBAPPL_DATA_ORIGIN = x.PZBAPPL_DATA_ORIGIN;


                    Models.Instrumento ins = new Models.Instrumento();

                    ins._PZBPOLL_NAME = x.PZBPOLL.PZBPOLL_NAME;
                    ins._PZBPOLL_CODE = x.PZBPOLL.PZBPOLL_CODE;
                    ins._PZBPOLL_USER = x.PZBPOLL.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = x.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = x.PZBPOLL.PZBPOLL_ACTIVITY_DATE;

                    apl._PZBPOLL = ins;

                    Models.Calendario dcal = new Models.Calendario();
                    dcal._PZBCALR_CODE = x.PZBCALR.PZBCALR_CODE;
                    dcal._PZBCALR_NAME = x.PZBCALR.PZBCALR_NAME;
                    dcal._PZBCALR_USER = x.PZBCALR.PZBCALR_USER;
                    dcal._PZBCALR_TERM = x.PZBCALR.PZBCALR_TERM;
                    dcal._PZBCALR_SRL = x.PZBCALR.PZBCALR_SRL;
                    dcal._PZBCALR_END_DATE = x.PZBCALR.PZBCALR_END_DATE;
                    dcal._PZBCALR_INIT_DATE = x.PZBCALR.PZBCALR_INIT_DATE;
                    dcal._PZBCALR_DATA_ORIGIN = x.PZBCALR.PZBCALR_DATA_ORIGIN;
                    dcal._PZBCALR_ACTIVITY_DATE = x.PZBCALR.PZBCALR_ACTIVITY_DATE;
                    apl._PZBCALR = dcal;


                    Models.AsignacionInstrumentoPregunta _insPre = new Models.AsignacionInstrumentoPregunta();
                    foreach (var p in x.PZBPLQS)
                    {

                        _insPre._PZBPLQS_SEQ_NUMBER = p.PZBPLQS_SEQ_NUMBER;
                        _insPre._PZBPLQS_USER = p.PZBPLQS_USER;
                        _insPre._PZBPLQS_DATA_ORIGIN = p.PZBPLQS_DATA_ORIGIN;
                        _insPre._PZBPLQS_ACTIVITY_DATE = p.PZBPLQS_ACTIVITY_DATE;
                        _insPre._PZBPLQS_QSTN_CODE = p.PZBPLQS_QSTN_CODE;
                        _insPre._PZBPLQS_POLL_CODE = p.PZBPLQS_POLL_CODE;
                        _insPre._PZBPLQS_CALR_CODE = p.PZBPLQS_CALR_CODE;
                        _insPre._PZBPLQS_APPL_SEQ_NUMBER = p.PZBPLQS_APPL_SEQ_NUMBER;
                        _insPre._PZBPLQS_AREA_CODE = p.PZBPLQS_AREA_CODE;
                        _insPre._PZBPLQS_CRIT_CODE = p.PZBPLQS_CRIT_CODE;
                        _insPre._PZBPLQS_TYAW_CODE = p.PZBPLQS_TYAW_CODE;
                        _insPre._PZBPLQS_MAX_OVAL = p.PZBPLQS_MAX_OVAL;
                        _insPre._PZBPLQS_MAX_VALUE = p.PZBPLQS_MAX_VALUE;
                        _insPre._PZBPLQS_MIN_OVAL = p.PZBPLQS_MIN_OVAL;
                        _insPre._PZBPLQS_MIN_VALUE = p.PZBPLQS_MIN_VALUE;
                        _insPre._PZBPLQS_ORDER = p.PZBPLQS_ORDER;
                        _insPre._PZBPLQS_STATUS = p.PZBPLQS_STATUS;
                        apl._PZBPLQS.Add(_insPre);
                    }



                    var re = db.PZBPHSE.SqlQuery(@" select PZBPHSE.*
                                                    from pzrplap,pzbappl,pzbcalr,pzbpoll,PZBPHSE
                                                    where  pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                    and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                    and pzbappl.pzbappl_calr_code = pzbcalr.pzbcalr_code
                                                    AND pzbcalr.pzbcalr_code = pzbphse.pzbphse_calr_code
                                                    AND SYSDATE >= pzbphse.pzbphse_open_date
                                                    and sysdate <=pzbphse.pzbphse_close_date 
                                                    and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                    and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                    and pzbpoll.pzbpoll_code = '" + _insPre._PZBPLQS_POLL_CODE + "'" +
                                                             "and pzbcalr.pzbcalr_code = '" + _insPre._PZBPLQS_CALR_CODE + "'" +
                                                             "AND PZRPLAP.PZRPLAP_STATUS = '" + status + "' " +
                                                            "and pzrplap.pzrplap_pidm = '" + pidm + "'").ToList();

                    foreach (var cal in re)
                    {

                        Models.Fase fas = new Models.Fase();
                        fas._PZBPHSE_CODE = cal.PZBPHSE_CODE;
                        fas._PZBPHSE_NAME = cal.PZBPHSE_NAME;
                        fas._PZBPHSE_USER = cal.PZBPHSE_USER;
                        fas._PZBPHSE_OPEN_DATE = cal.PZBPHSE_OPEN_DATE;
                        fas._PZBPHSE_CLOSE_DATE = cal.PZBPHSE_CLOSE_DATE;
                        fas._PZBPHSE_CLEXTENSION_DATE = cal.PZBPHSE_CLEXTENSION_DATE;
                        fas._PZBPHSE_OPEXTENSION_DATE = cal.PZBPHSE_OPEXTENSION_DATE;
                        fas._PZBPHSE_DATA_ORIGIN = cal.PZBPHSE_DATA_ORIGIN;
                        fas._PZBPHSE_ACTIVITY_DATE = cal.PZBPHSE_ACTIVITY_DATE;
                        fas._PZBPHSE_CALR_CODE = cal.PZBPHSE_CALR_CODE;
                        apl._PZBCALR._PZBPHSE.Add(fas);

                    }

                    var e = db.PZRPLAP.SqlQuery(@" select PZRPLAP.*
                                                    from pzrplap,pzbappl,pzbcalr,pzbpoll,PZBPHSE
                                                    where  pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                    and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                    and pzbappl.pzbappl_calr_code = pzbcalr.pzbcalr_code
                                                    AND pzbcalr.pzbcalr_code = pzbphse.pzbphse_calr_code
                                                    AND SYSDATE >= pzbphse.pzbphse_open_date
                                                    and sysdate <=pzbphse.pzbphse_close_date 
                                                    and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                    and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                    and pzbpoll.pzbpoll_code = '" + _insPre._PZBPLQS_POLL_CODE + "'" +
                                           "and pzbcalr.pzbcalr_code = '" + _insPre._PZBPLQS_CALR_CODE + "'" +
                                           "AND PZRPLAP.PZRPLAP_STATUS = '" + status + "' " +
                                          "and pzrplap.pzrplap_pidm = '" + pidm + "'").ToList();
                    foreach (var j in e)
                    {
                        //Datos ICPD
                        Models.AsignacionInstrumentoPersona item = new Models.AsignacionInstrumentoPersona();
                        item._PZRPLAP_CALR_CODE = j.PZRPLAP_CALR_CODE;
                        item._PZRPLAP_POST_CODE = j.PZRPLAP_POST_CODE;
                        item._PZRPLAP_DNCY_CODE = j.PZRPLAP_DNCY_CODE;
                        item._PZRPLAP_PFLE_CODE = j.PZRPLAP_PFLE_CODE;
                        item._PZRPLAP_STATUS = j.PZRPLAP_STATUS;
                        item._PZRPLAP_DATA_ORIGIN = j.PZRPLAP_DATA_ORIGIN;
                        item._PZRPLAP_ACTIVITY_DATE = j.PZRPLAP_ACTIVITY_DATE;
                        item._PZRPLAP_PIDM = j.PZRPLAP_PIDM;
                        item._PZRPLAP_USER = j.PZRPLAP_USER;
                        item._PZRPLAP_APPL_SEQ_NUMBER = j.PZRPLAP_APPL_SEQ_NUMBER;
                        item._PZRPLAP_SEQ_NUMBER = j.PZRPLAP_SEQ_NUMBER;
                        apl._PZRPLAP.Add(item);

                    }
                    result.Add(apl);
                }

                return (Ok(result));

            }

            catch (Exception e)
            {

                return (NotFound());

            }


        }
        /// <summary>
        /// Dado el pidm del usuario muestra los instrumentos que tiene asignados
        /// </summary>
        /// <param name="pidm"> PIDM del usuario</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.InstrumentoAplicable>))]
        [Route("api/PZBPOLL/GetPZBPOLLByPidm/{_pidm}/{_pidme}")]
        public IHttpActionResult GetPZBPOLLByPidm(int _pidm,string _pidme)
        {

            List<Models.InstrumentoAplicable> result = new List<Models.InstrumentoAplicable>();
            try
            {
                db.PZBAPPL.Include(p => p.PZBCALR)
                    .Include(c => c.PZBPOLL)
                    .Include(a => a.PZBCALR.PZBPHSE)
                    .Include(s => s.PZBPLQS)
                    .Include(p => p.PZRPLAP);

                if (_pidme =="perc") {
                    _pidme = "%";
                }

                var res = db.PZBAPPL.SqlQuery(@"SELECT DISTINCT  PZBAPPL.*,PZBPOLL.*
                                                FROM PZBPLQS,PZRPLAP,
                                                PZBCALR,
                                                PZVPFLE, 
                                                PZRASPE,
                                                PZBPRSO,
                                                PZRDYPT,
                                                PZVDNCY,
                                                PZVPOST,
                                                PZBAPPL,
                                                pzbpoll,
                                                PZBQSTN,
                                                PZBPHSE,
                                                PZVPOST PZVPOST1,
                                                PZVDNCY PZVDNCY1,
                                                PZRDYPT PZRDYPT1,
                                                PZRASPE PZRASPE1,
                                                PZBPRSO PZBPRSO1
                                                WHERE pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                AND   pzbappl.pzbappl_pfle_code = PZRPLAP.PZRPLAP_PFLE_CODE
                                                AND   PZRPLAP.PZRPLAP_PFLE_code = pzraspe.pzraspe_pfle_code
                                                AND   pzraspe.pzraspe_pfle_code = PZVPFLE.PZVPFLE_code
                                                AND   pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                and   pzbappl.pzbappl_calr_code = PZRPLAP.PZRPLAP_CALR_code 
                                                AND   pzbappl.pzbappl_calr_code = PZBCALR.PZBCALR_CODE
                                                AND pzbcalr.pzbcalr_code = pzbphse.pzbphse_calr_code
                                                    AND SYSDATE >= pzbphse.pzbphse_open_date
                                                    and sysdate <=pzbphse.pzbphse_close_date 
                                               
                                                AND   pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                AND   pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                AND   PZRDYPT.PZRDYPT_POST_CODE = PZVPOST.PZVPOST_CODE
                                                AND   PZVPOST.PZVPOST_CODE_sup = PZVPOST1.PZVPOST_CODE 
                                                AND   PZVPOST1.PZVPOST_CODE = pzrdypt1.pzrdypt_post_code
                                                AND   pzrplap.pzrplap_dncy_code= pzraspe.pzraspe_dncy_code
                                                AND   pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                AND   PZRDYPT.PZRDYPT_DNCY_CODE = PZVDNCY.PZVDNCY_CODE
                                                AND   PZVDNCY.PZVDNCY_CODE_sup = PZVDNCY1.PZVDNCY_CODE
                                               -- AND   PZVDNCY1.PZVDNCY_CODE = pzrdypt1.pzrdypt_dncy_code
                                                AND   PZRPLAP.PZRPLAP_PIDM = PZRASPE.PZRASPE_pidm 
                                                AND   PZRASPE.PZRASPE_pidm = pzbprso.pzbprso_pidm
                                                AND   PZBPLQS.PZBPLQS_QSTN_CODE = PZBQSTN.PZBQSTN_CODE
                                                AND   pzbplqs.pzbplqs_appl_seq_number = pzbappl.pzbappl_seq_number
                                                AND   PZBPLQS.PZBPLQS_POLL_CODE = pzbappl.pzbappl_poll_code
                                                AND   PZBPLQS.PZBPLQS_CALR_CODE = pzbappl.pzbappl_calr_code
                                                AND   pzvpfle.pzvpfle_code = pzbappl.pzbappl_pfle_code
                                                AND   pzrdypt1.pzrdypt_post_code = PZRASPE1.PZRASPE_POST_CODE
                                                AND   PZRDYPT1.PZRDYPT_DNCY_CODE = PZRASPE1.PZRASPE_DNCY_CODE
                                                AND   PZRASPE1.PZRASPE_PIDM = PZBPRSO1.PZBPRSO_PIDM
                                                and PZBPRSO.PZBPRSO_PIDM = '"+_pidm+"'"+
                                              " and PZBPRSO1.PZBPRSO_PIDM like  '"+_pidme+"'").ToList();

                foreach (var x in res)
                {

                    Models.InstrumentoAplicable apl = new Models.InstrumentoAplicable();
                    apl._PZBAPPL_USER = x.PZBAPPL_USER;
                    apl._PZBAPPL_PRCNT = x.PZBAPPL_PRCNT;
                    apl._PZBAPPL_SEQ_NUMBER = x.PZBAPPL_SEQ_NUMBER;
                    apl._PZBAPPL_POLL_CODE = x.PZBAPPL_POLL_CODE;
                    apl._PZBAPPL_PFLE_CODE = x.PZBAPPL_PFLE_CODE;
                    apl._PZBAPPL_CALR_CODE = x.PZBAPPL_CALR_CODE;
                    apl._PZBAPPL_ACTIVITY_DATE = x.PZBAPPL_ACTIVITY_DATE;
                    apl._PZBAPPL_DATA_ORIGIN = x.PZBAPPL_DATA_ORIGIN;


                    Models.Instrumento ins = new Models.Instrumento();

                    ins._PZBPOLL_NAME = x.PZBPOLL.PZBPOLL_NAME;
                    ins._PZBPOLL_CODE = x.PZBPOLL.PZBPOLL_CODE;
                    ins._PZBPOLL_USER = x.PZBPOLL.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = x.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = x.PZBPOLL.PZBPOLL_ACTIVITY_DATE;

                    apl._PZBPOLL = ins;

                    Models.Calendario dcal = new Models.Calendario();
                    dcal._PZBCALR_CODE = x.PZBCALR.PZBCALR_CODE;
                    dcal._PZBCALR_NAME = x.PZBCALR.PZBCALR_NAME;
                    dcal._PZBCALR_USER = x.PZBCALR.PZBCALR_USER;
                    dcal._PZBCALR_TERM = x.PZBCALR.PZBCALR_TERM;
                    dcal._PZBCALR_SRL = x.PZBCALR.PZBCALR_SRL;
                    dcal._PZBCALR_END_DATE = x.PZBCALR.PZBCALR_END_DATE;
                    dcal._PZBCALR_INIT_DATE = x.PZBCALR.PZBCALR_INIT_DATE;
                    dcal._PZBCALR_DATA_ORIGIN = x.PZBCALR.PZBCALR_DATA_ORIGIN;
                    dcal._PZBCALR_ACTIVITY_DATE = x.PZBCALR.PZBCALR_ACTIVITY_DATE;
                    apl._PZBCALR = dcal;


                    Models.AsignacionInstrumentoPregunta _insPre = new Models.AsignacionInstrumentoPregunta();
                    foreach (var p in x.PZBPLQS)
                    {

                        _insPre._PZBPLQS_SEQ_NUMBER = p.PZBPLQS_SEQ_NUMBER;
                        _insPre._PZBPLQS_USER = p.PZBPLQS_USER;
                        _insPre._PZBPLQS_DATA_ORIGIN = p.PZBPLQS_DATA_ORIGIN;
                        _insPre._PZBPLQS_ACTIVITY_DATE = p.PZBPLQS_ACTIVITY_DATE;
                        _insPre._PZBPLQS_QSTN_CODE = p.PZBPLQS_QSTN_CODE;
                        _insPre._PZBPLQS_POLL_CODE = p.PZBPLQS_POLL_CODE;
                        _insPre._PZBPLQS_CALR_CODE = p.PZBPLQS_CALR_CODE;
                        _insPre._PZBPLQS_APPL_SEQ_NUMBER = p.PZBPLQS_APPL_SEQ_NUMBER;
                        _insPre._PZBPLQS_AREA_CODE = p.PZBPLQS_AREA_CODE;
                        _insPre._PZBPLQS_CRIT_CODE = p.PZBPLQS_CRIT_CODE;
                        _insPre._PZBPLQS_TYAW_CODE = p.PZBPLQS_TYAW_CODE;
                        _insPre._PZBPLQS_MAX_OVAL = p.PZBPLQS_MAX_OVAL;
                        _insPre._PZBPLQS_MAX_VALUE = p.PZBPLQS_MAX_VALUE;
                        _insPre._PZBPLQS_MIN_OVAL = p.PZBPLQS_MIN_OVAL;
                        _insPre._PZBPLQS_MIN_VALUE = p.PZBPLQS_MIN_VALUE;
                        _insPre._PZBPLQS_ORDER = p.PZBPLQS_ORDER;
                        _insPre._PZBPLQS_STATUS = p.PZBPLQS_STATUS;
                        apl._PZBPLQS.Add(_insPre);
                    }



                    var re = db.PZBPHSE.SqlQuery(@" select PZBPHSE.*
                                                    from pzrplap,pzbappl,pzbcalr,pzbpoll,PZBPHSE
                                                    where  pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                    and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                    and pzbappl.pzbappl_calr_code = pzbcalr.pzbcalr_code
                                                    AND pzbcalr.pzbcalr_code = pzbphse.pzbphse_calr_code
                                                    AND SYSDATE >= pzbphse.pzbphse_open_date
                                                    and sysdate <=pzbphse.pzbphse_close_date 
                                                    and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                    and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                    and pzbpoll.pzbpoll_code = '" + _insPre._PZBPLQS_POLL_CODE + "'" +
                                                             "and pzbcalr.pzbcalr_code = '" + _insPre._PZBPLQS_CALR_CODE + "'" +

                                                            "and pzrplap.pzrplap_pidm = '" + _pidm + "'").ToList();

                    foreach (var cal in re)
                    {

                        Models.Fase fas = new Models.Fase();
                        fas._PZBPHSE_CODE = cal.PZBPHSE_CODE;
                        fas._PZBPHSE_NAME = cal.PZBPHSE_NAME;
                        fas._PZBPHSE_USER = cal.PZBPHSE_USER;
                        fas._PZBPHSE_OPEN_DATE = cal.PZBPHSE_OPEN_DATE;
                        fas._PZBPHSE_CLOSE_DATE = cal.PZBPHSE_CLOSE_DATE;
                        fas._PZBPHSE_CLEXTENSION_DATE = cal.PZBPHSE_CLEXTENSION_DATE;
                        fas._PZBPHSE_OPEXTENSION_DATE = cal.PZBPHSE_OPEXTENSION_DATE;
                        fas._PZBPHSE_DATA_ORIGIN = cal.PZBPHSE_DATA_ORIGIN;
                        fas._PZBPHSE_ACTIVITY_DATE = cal.PZBPHSE_ACTIVITY_DATE;
                        fas._PZBPHSE_CALR_CODE = cal.PZBPHSE_CALR_CODE;
                        apl._PZBCALR._PZBPHSE.Add(fas);

                    }

                    var e = db.PZRPLAP.SqlQuery(@" select PZRPLAP.*
                                                    from pzrplap,pzbappl,pzbcalr,pzbpoll,PZBPHSE
                                                    where  pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                    and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                    and pzbappl.pzbappl_calr_code = pzbcalr.pzbcalr_code
                                                    AND pzbcalr.pzbcalr_code = pzbphse.pzbphse_calr_code
                                                    AND SYSDATE >= pzbphse.pzbphse_open_date
                                                    and sysdate <=pzbphse.pzbphse_close_date 
                                                    and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                    and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                    and pzbpoll.pzbpoll_code = '" + _insPre._PZBPLQS_POLL_CODE + "'" +
                                           "and pzbcalr.pzbcalr_code = '" + _insPre._PZBPLQS_CALR_CODE + "'" +

                                          "and pzrplap.pzrplap_pidm = '" + _pidm + "'").ToList();
                    foreach (var j in e)
                    {
                        //Datos ICPD
                        Models.AsignacionInstrumentoPersona item = new Models.AsignacionInstrumentoPersona();
                        item._PZRPLAP_CALR_CODE = j.PZRPLAP_CALR_CODE;
                        item._PZRPLAP_POST_CODE = j.PZRPLAP_POST_CODE;
                        item._PZRPLAP_DNCY_CODE = j.PZRPLAP_DNCY_CODE;
                        item._PZRPLAP_PFLE_CODE = j.PZRPLAP_PFLE_CODE;
                        item._PZRPLAP_STATUS = j.PZRPLAP_STATUS;
                        item._PZRPLAP_DATA_ORIGIN = j.PZRPLAP_DATA_ORIGIN;
                        item._PZRPLAP_ACTIVITY_DATE = j.PZRPLAP_ACTIVITY_DATE;
                        item._PZRPLAP_PIDM = j.PZRPLAP_PIDM;
                        item._PZRPLAP_USER = j.PZRPLAP_USER;
                        item._PZRPLAP_APPL_SEQ_NUMBER = j.PZRPLAP_APPL_SEQ_NUMBER;
                        item._PZRPLAP_SEQ_NUMBER = j.PZRPLAP_SEQ_NUMBER;
                        apl._PZRPLAP.Add(item);

                    }
                    result.Add(apl);
                }

                return (Ok(result));

            }

            catch (Exception e)
            {

                return (NotFound());

            }


        }


       


        // PUT: api/PZBPOLL/5
        /// <summary>
        /// 
        ///  Modifica los Datos
        ///  Instrumentos de evalucion 
        /// 
        /// </summary>
        /// <param name="id">Codigo del instrumento</param>
        /// <param name="pZRIDEF">Modelo enviado para ser modificado</param>
        /// <returns></returns>

        [HttpPut]
        [Route("api/PZBPOLL/PutPZBPOLL/{id}/")]
        [ResponseType(typeof(Instrumento))]
        public IHttpActionResult PutPZBPOLL(String id, Instrumento pZRIDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pZRIDEF._PZBPOLL_CODE)
            {
                return BadRequest();
            }



            try
            {
                DateTime fecha = DateTime.Today;

                pZRIDEF._PZBPOLL_ACTIVITY_DATE = fecha;

                PZBPOLL instrumento = new PZBPOLL();

                // instrumento.PZBPOLL_ID = pZRIDEF.PZBPOLL_ID;
                instrumento.PZBPOLL_CODE = pZRIDEF._PZBPOLL_CODE;
                instrumento.PZBPOLL_USER = pZRIDEF._PZBPOLL_USER;
                instrumento.PZBPOLL_NAME = pZRIDEF._PZBPOLL_NAME;
                instrumento.PZBPOLL_DATA_ORIGIN = pZRIDEF._PZBPOLL_DATA_ORIGIN;
                instrumento.PZBPOLL_ACTIVITY_DATE = pZRIDEF._PZBPOLL_ACTIVITY_DATE;

                db.PZBPOLL.Attach(instrumento);
                db.Entry(instrumento).State = EntityState.Modified;
                db.SaveChanges();

                return Ok("El instrumento " + instrumento.PZBPOLL_NAME + " se ha modificado exitosamente");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PZBPOLLExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        /// <summary>
        /// 
        /// Agrega Instrumentos Nuevos.
        /// RUTA : api/PZBPOLL/PostPZBPOLL
        /// 
        /// </summary>
        /// <param name="pZRIDEF"> Modelo para agregar un nuevo instrumento</param>
        /// <returns>201 Created</returns>
        [ResponseType(typeof(Instrumento))]
        public IHttpActionResult PostPZBPOLL(Models.Instrumento pZRIDEF)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (db.PZBPOLL.Count(e => e.PZBPOLL_CODE == pZRIDEF._PZBPOLL_CODE) > 0)
            {

                return BadRequest("El codigo " + pZRIDEF._PZBPOLL_CODE + " ya existe.");
            }

            try
            {
                //Decimal es = (Decimal)db.PZBPOLL.Max(x => x.PZBPOLL_ID);
                //                PZBPRSO _user = (PZBPRSO)db.PZBPRSO.Find(pZRIDEF. = pZRIDEF.PZBPRSO_ID);

                //es = es + 1;

                DateTime fecha = DateTime.Today;

                //aqui tendria que enviar el usernaame ademas de lo que esta.

                pZRIDEF._PZBPOLL_ACTIVITY_DATE = fecha;
                //pZRIDEF.PZBPOLL_ID = es;
                //pZRIDEF.PZBPRSO = _user;
                //Objeto creado para enviar a la Bd

                PZBPOLL objInst = new PZBPOLL();
                //objInst.PZBPOLL_ID = pZRIDEF.PZBPOLL_ID;
                objInst.PZBPOLL_CODE = pZRIDEF._PZBPOLL_CODE;
                objInst.PZBPOLL_NAME = pZRIDEF._PZBPOLL_NAME;
                objInst.PZBPOLL_USER = pZRIDEF._PZBPOLL_USER;
                objInst.PZBPOLL_DATA_ORIGIN = pZRIDEF._PZBPOLL_DATA_ORIGIN;
                objInst.PZBPOLL_ACTIVITY_DATE = pZRIDEF._PZBPOLL_ACTIVITY_DATE;






                db.PZBPOLL.Add(objInst);

                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PZBPOLLExists(pZRIDEF._PZBPOLL_CODE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pZRIDEF._PZBPOLL_CODE }, pZRIDEF);
        }
        /// <summary>
        /// Elimina dado un id del instrumento dicho instrumento 
        /// </summary>
        /// <param name="id">Codigo del instrumento</param>
        /// <returns></returns>
        // DELETE: api/PZBPOLL/5
        [ResponseType(typeof(PZBPOLL))]
        public IHttpActionResult DeletePZBPOLL(decimal id)
        {
            PZBPOLL pZRIDEF = db.PZBPOLL.Find(id);
            if (pZRIDEF == null)
            {
                return NotFound();
            }

            db.PZBPOLL.Remove(pZRIDEF);
            db.SaveChanges();

            return Ok(pZRIDEF);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);

        }

        private bool PZBPOLLExists(String id)
        {
            return db.PZBPOLL.Count(e => e.PZBPOLL_CODE == id) > 0;
        }
    }


}