﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using System.Runtime.Serialization.Json;
using System.IO;

namespace Desempeño.Controllers
{
    public class PZRDYPTController : ApiController
    {
        private Entities db = new Entities();





        /// <summary>
        /// 
        /// Carga el Listado de Cargos y Departamentos
        /// con la relación de por quien es evaluado         
        /// 
        /// </summary>
        /// <returns>Lista de car-dep</returns>
        [HttpGet]
        [ResponseType(typeof(List<Models.AsignacionCargoDependencia>))]
        [Route("api/PZRDYPT/GetAsignacionCargoDependencia/")]
        public IHttpActionResult  GetAsignacionCargoDependencia()
        {
             List<Models.AsignacionCargoDependencia> result = new List<Models.AsignacionCargoDependencia>();

            // DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<PZRDYPT>));

            db.PZRDYPT.Include(car => car.PZVPOST).Include(dep => dep.PZVDNCY).Include(rev => rev.PZVPOST.PZVPOST2).Include(sup => sup.PZVDNCY.PZVDNCY2);

            var cardep = db.PZRDYPT.SqlQuery(@"select car.*,dep.*,rev.*,sup.*,cardep.*
                                                from PZVPOST CAR ,PZVDNCY DEP, 
                                                     PZRDYPT CARDEP ,PZVDNCY SUP,
                                                     PZVPOST rev
                                                where cardep.PZRDYPT_post_code=car.pzvpost_code and 
                                                car.pzvpost_code_sup = rev.pzvpost_code and
                                                cardep.PZRDYPT_dncy_CODE=dep.pzvdncy_CODE and 
                                                dep.PZVDNCY_CODE_SUP = sup.pzvdncy_CODE").ToList();

                foreach (var x in cardep) {

                Models.AsignacionCargoDependencia cd = new Models.AsignacionCargoDependencia();
                
                //Datos de la Ternaria   
                cd._PZRDYPT_POST_CODE= x.PZRDYPT_POST_CODE;
                cd._PZRDYPT_DNCY_CODE = x.PZRDYPT_DNCY_CODE;
               // cd.PZRDYPT_ID = x.PZRDYPT_ID;
                cd._PZRDYPT_USER = x.PZRDYPT_USER;
                cd._PZRDYPT_DATA_ORIGIN = x.PZRDYPT_DATA_ORIGIN;
                cd._PZRDYPT_ACTIVITY_DATE = x.PZRDYPT_ACTIVITY_DATE;
                
                
                //DEPENDENCIA
                Models.Dependencia dep = new Models.Dependencia();
                //dep.PZVDNCY_ID = x.PZVDNCY.PZVDNCY_ID;
                dep._PZVDNCY_KEY = x.PZVDNCY.PZVDNCY_KEY;
                dep._PZVDNCY_SITE = x.PZVDNCY.PZVDNCY_SITE;
                dep._PZVDNCY_LEVEL = x.PZVDNCY.PZVDNCY_LEVEL;
                dep._PZVDNCY_NAME = x.PZVDNCY.PZVDNCY_NAME;
                dep._PZVDNCY_ACTIVITY_DATE = x.PZVDNCY.PZVDNCY_ACTIVITY_DATE;


                //Departamento Evaluador
                Models.Dependencia depEval = new Models.Dependencia();
               // depEval.PZVDNCY_ID = x.PZVDNCY.PZVDNCY2.PZVDNCY_ID;
                depEval._PZVDNCY_KEY = x.PZVDNCY.PZVDNCY2.PZVDNCY_KEY;
                depEval._PZVDNCY_SITE = x.PZVDNCY.PZVDNCY2.PZVDNCY_SITE;
                depEval._PZVDNCY_LEVEL = x.PZVDNCY.PZVDNCY2.PZVDNCY_LEVEL;
                depEval._PZVDNCY_NAME = x.PZVDNCY.PZVDNCY2.PZVDNCY_NAME;
                depEval._PZVDNCY_ACTIVITY_DATE = x.PZVDNCY.PZVDNCY2.PZVDNCY_ACTIVITY_DATE;

                dep._PZVDNCY2 = depEval;

                cd._PZVDNCY = dep;
                
                //Cargo
                Models.Cargo item = new Models.Cargo();

                //item.PZVPOST_ID = x.PZVPOST.PZVPOST_ID;
                item._PZVPOST_CODE = x.PZVPOST.PZVPOST_CODE;
                item._PZVPOST_NAME = x.PZVPOST.PZVPOST_NAME;
                item._PZVPOST_CODE_SUP = x.PZVPOST.PZVPOST_CODE_SUP;
                item._PZVPOST_USER = x.PZVPOST.PZVPOST_USER;
                item._PZVPOST_DESCRIPTION = x.PZVPOST.PZVPOST_DESCRIPTION;
                item._PZVPOST_ACTIVITY_DATE = x.PZVPOST.PZVPOST_ACTIVITY_DATE;
                item._PZVPOST_DATA_ORIGIN = x.PZVPOST.PZVPOST_DATA_ORIGIN;
                // Cargo Supervisor

                Models.Cargo itemSup = new Models.Cargo();

                //itemSup.PZVPOST_ID = x.PZVPOST.PZVPOST2.PZVPOST_ID;
                itemSup._PZVPOST_CODE = x.PZVPOST.PZVPOST2.PZVPOST_CODE;
                itemSup._PZVPOST_NAME = x.PZVPOST.PZVPOST2.PZVPOST_NAME;
                itemSup._PZVPOST_CODE_SUP = x.PZVPOST.PZVPOST_CODE_SUP;
                itemSup._PZVPOST_USER = x.PZVPOST.PZVPOST2.PZVPOST_USER;
                itemSup._PZVPOST_DESCRIPTION = x.PZVPOST.PZVPOST2.PZVPOST_DESCRIPTION;
                itemSup._PZVPOST_ACTIVITY_DATE = x.PZVPOST.PZVPOST2.PZVPOST_ACTIVITY_DATE;
                itemSup._PZVPOST_DATA_ORIGIN = x.PZVPOST.PZVPOST2.PZVPOST_DATA_ORIGIN;

                item._PZVPOST2 = itemSup;

                cd._PZVPOST = item;

                result.Add(cd);

            }

         

            //return stream1;
            return Ok(result);
        }

        /// <summary>
        /// 
        /// 
        /// Dado un id de cargo y de departamento
        /// Genera el listado de de cargos que 
        /// evalua esa persona
        ///
        /// 
        /// </summary>
        /// <returns>Listado de Cargos Evaluados</returns>
        /// 

        [HttpGet]
        [ResponseType(typeof(List<Models.AsignacionCargoDependencia>))]
        [Route("api/PZRDYPT/GetEvaluados/{idcar}/{iddep}")]
        public IHttpActionResult GetEvaluados(String idcar, String iddep)
        {
            List<Models.AsignacionCargoPersona> result = new List<Models.AsignacionCargoPersona>();

            // iddep = 2;
            // idcar = 2;

            db.PZRASPE.Include(car => car.PZRDYPT.PZVPOST)
                      .Include(dep => dep.PZRDYPT.PZVDNCY)
                      .Include(rev => rev.PZRDYPT.PZVPOST.PZVPOST2)
                      .Include(sup => sup.PZRDYPT.PZVDNCY.PZVDNCY2)
                      .Include(asp => asp.PZVPERM)
                      .Include(pso => pso.PZBPRSO)
                      .Include(dyp => dyp.PZRDYPT);

            var cardep = db.PZRASPE.SqlQuery(@"select *
                                                from PZVPOST CAR ,PZVDNCY DEP,
                                                     PZRDYPT CARDEP ,PZVDNCY SUP,
                                                     PZVPOST rev,
                                                     pzraspe aspe,
                                                     pzvpfle pfle,
                                                     pzbprso prso
                                                where cardep.pzrdypt_post_code=car.pzvpost_code and 
                                                      car.pzvpost_code_sup = rev.pzvpost_code and
                                                      cardep.PZRDYPT_DNCY_CODE = dep.pzvdncy_CODE and 
                                                      cardep.PZRDYPT_DNCY_CODE = aspe.pzraspe_dncy_code and
                                                      cardep.pzrdypt_post_code = aspe.pzraspe_post_code and
                                                      pfle.pzvpfle_code = aspe.pzraspe_pfle_code and
                                                      prso.pzbprso_pidm = aspe.pzraspe_pidm and
                                                      sup.pzvdncy_CODE = '"+ iddep + "' and "+
                                                      "rev.pzvpost_code = '"+idcar+"'").ToList();
            foreach (var x in cardep)
            {

               
                Models.AsignacionCargoPersona ppcd = new Models.AsignacionCargoPersona();
                //   ppcd.PZRASPE_ID = x;
                ppcd._PZRASPE_DNCY_CODE = x.PZRASPE_DNCY_CODE;
                ppcd._PZRASPE_POST_CODE = x.PZRASPE_POST_CODE;
                ppcd._PZRASPE_PIDM = x.PZRASPE_PIDM;
                ppcd._PZRASPE_PFLE_CODE = x.PZRASPE_PFLE_CODE;
                ppcd._PZRASPE_END_DATE = x.PZRASPE_END_DATE;
                ppcd._PZRASPE_START_DATE = x.PZRASPE_START_DATE;
                ppcd._PZRASPE_DATA_ORIGIN = x.PZRASPE_DATA_ORIGIN;
                ppcd._PZRASPE_USER = x.PZRASPE_USER;

                Models.AsignacionCargoDependencia cd = new Models.AsignacionCargoDependencia();

                //Datos de la Ternaria   
                cd._PZRDYPT_POST_CODE = x.PZRDYPT.PZRDYPT_POST_CODE;
                cd._PZRDYPT_DNCY_CODE = x.PZRDYPT.PZRDYPT_DNCY_CODE;
                cd._PZRDYPT_USER = x.PZRDYPT.PZRDYPT_USER;
                cd._PZRDYPT_DATA_ORIGIN = x.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                cd._PZRDYPT_ACTIVITY_DATE = x.PZRDYPT.PZRDYPT_ACTIVITY_DATE;
                ppcd._PZRDYPT = cd;

                //DEPENDENCIA
                Models.Dependencia dep = new Models.Dependencia();
                //dep.PZVDNCY_ID = x.PZVDNCY.PZVDNCY_ID;
                dep._PZVDNCY_KEY = x.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
                dep._PZVDNCY_SITE = x.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
                dep._PZVDNCY_LEVEL = x.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
                dep._PZVDNCY_NAME = x.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                dep._PZVDNCY_ACTIVITY_DATE = x.PZRDYPT.PZVDNCY.PZVDNCY_ACTIVITY_DATE;
                dep._PZVDNCY_USER = x.PZRDYPT.PZVDNCY.PZVDNCY_USER;
                dep._PZVDNCY_DATA_ORIGIN = x.PZRDYPT.PZVDNCY.PZVDNCY_DATA_ORIGIN;

                ppcd._PZRDYPT._PZVDNCY = dep;

                Models.Persona per = new Models.Persona();
                per._PZBPRSO_PIDM = x.PZBPRSO.PZBPRSO_PIDM;
                per._PZBPRSO_SITE = x.PZBPRSO.PZBPRSO_SITE;
                per._PZBPRSO_USER = x.PZBPRSO.PZBPRSO_USER;
                per._PZBPRSO_PAYSHEET = x.PZBPRSO.PZBPRSO_PAYSHEET;
                per._PZBPRSO_DATA_ORIGIN = x.PZBPRSO.PZBPRSO_DATA_ORIGIN;
                per._PZBPRSO_ACTIVE = x.PZBPRSO.PZBPRSO_ACTIVE;
                //per.PZBPRSO_PZVPOST = x.PZBPRSO.PZBPRSO_PZCRCOD;

                ppcd._PZBPRSO = per;


                //Departamento Evaluador
                Models.Dependencia depEval = new Models.Dependencia();
                //depEval.PZVDNCY_ID = x.PZVDNCY.PZVDNCY2.PZVDNCY_ID;
                depEval._PZVDNCY_CODE = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_CODE;
                depEval._PZVDNCY_KEY = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_KEY;
                depEval._PZVDNCY_SITE = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_SITE;
                depEval._PZVDNCY_LEVEL = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_LEVEL;
                depEval._PZVDNCY_NAME = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_NAME;
                depEval._PZVDNCY_USER = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_USER;
                depEval._PZVDNCY_ACTIVITY_DATE = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_ACTIVITY_DATE;
                depEval._PZVDNCY_DATA_ORIGIN = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_DATA_ORIGIN;

                ppcd._PZRDYPT._PZVDNCY._PZVDNCY2 = depEval;
                dep._PZVDNCY2 = depEval;

                cd._PZVDNCY = dep;

                //Cargo
                Models.Cargo item = new Models.Cargo();

                //item.PZVPOST_ID = x.PZVPOST.PZVPOST_ID;
                item._PZVPOST_NAME = x.PZRDYPT.PZVPOST.PZVPOST_NAME;
                item._PZVPOST_CODE_SUP = x.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                item._PZVPOST_USER = x.PZRDYPT.PZVPOST.PZVPOST_USER;
                item._PZVPOST_DESCRIPTION = x.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                item._PZVPOST_ACTIVITY_DATE = x.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;
                item._PZVPOST_DATA_ORIGIN = x.PZRDYPT.PZVPOST.PZVPOST_DATA_ORIGIN;

                ppcd._PZRDYPT._PZVPOST = item;
                // Cargo Supervisor

                Models.Cargo itemSup = new Models.Cargo();

                //itemSup.PZVPOST_ID = x.PZVPOST.PZVPOST2.PZVPOST_ID;
                itemSup._PZVPOST_CODE = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_CODE;
                itemSup._PZVPOST_NAME = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_NAME;
                itemSup._PZVPOST_CODE_SUP = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_CODE_SUP;
                itemSup._PZVPOST_USER = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_USER;
                itemSup._PZVPOST_DESCRIPTION = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_DESCRIPTION;
                itemSup._PZVPOST_ACTIVITY_DATE = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_ACTIVITY_DATE;
                itemSup._PZVPOST_DATA_ORIGIN = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_DATA_ORIGIN;

                ppcd._PZRDYPT._PZVPOST._PZVPOST2 = itemSup;
                item._PZVPOST2 = itemSup;

                cd._PZVPOST = item;

                result.Add(ppcd);

            }

            // DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(PZRDYPT));

            //return stream1;
            return Ok(result);
        }


        [HttpGet]
        [ResponseType(typeof(List<Models.AsignacionCargoDependencia>))]
        [Route("api/PZRDYPT/GetMisEvaluados/{idcar}/{iddep}")]
        public IHttpActionResult GetMisEvaluados(String idcar, String iddep)
        {
            List<Models.AsignacionCargoPersona> result = new List<Models.AsignacionCargoPersona>();

            // iddep = 2;
            // idcar = 2;
            //db.PZRASPE.Include(car => car.PZRDYPT.)
            //        .Include(dep => dep.PZRDYPT.PZVDNCY)
            //      //  .Include(rev => rev.PZRDYPT.PZVPOST.PZVPOST2)
            //       // .Include(sup => sup.PZRDYPT.PZVDNCY.PZVDNCY2)
            //        //.Include(asp => asp.PZVPERM)
            //        //.Include(pso => pso.PZBPRSO)
            //        .Include(dyp => dyp.PZRDYPT);

            var cardep = db.PZRASPE.SqlQuery(@" (SELECT DYPT.PZRDYPT_POST_CODE AS DYPT_POST, DYPT.PZRDYPT_DNCY_CODE AS DYPT_DNCY, ASPE.PZRASPE_PIDM ASPE_PIDM FROM PZRDYPT DYPT, (SELECT PST.PZVPOST_CODE from PZVPOST PST 
WHERE PST.PZVPOST_CODE_SUP='ADDITI') POST, PZRASPE ASPE
WHERE POST.PZVPOST_CODE = DYPT.PZRDYPT_POST_CODE
AND DYPT.PZRDYPT_DNCY_CODE= ASPE.PZRASPE_DNCY_CODE
AND DYPT.PZRDYPT_POST_CODE =ASPE.PZRASPE_POST_CODE)");
            //db.PZRASPE.Include(car => car.PZRDYPT.PZVPOST)
            //          .Include(dep => dep.PZRDYPT.PZVDNCY)
            //          .Include(rev => rev.PZRDYPT.PZVPOST.PZVPOST2)
            //          .Include(sup => sup.PZRDYPT.PZVDNCY.PZVDNCY2)
            //          .Include(asp => asp.PZVPERM)
            //          .Include(pso => pso.PZBPRSO)
            //          .Include(dyp => dyp.PZRDYPT);

            //var cardep = db.PZRASPE.SqlQuery(@"select *
            //                                    from PZVPOST CAR ,PZVDNCY DEP,
            //                                         PZRDYPT CARDEP ,PZVDNCY SUP,
            //                                         PZVPOST rev,
            //                                         pzraspe aspe,
            //                                         pzvpfle pfle,
            //                                         pzbprso prso
            //                                    where cardep.pzrdypt_post_code=car.pzvpost_code and 
            //                                          car.pzvpost_code_sup = rev.pzvpost_code and
            //                                          cardep.PZRDYPT_DNCY_CODE = dep.pzvdncy_CODE and 
            //                                          cardep.PZRDYPT_DNCY_CODE = aspe.pzraspe_dncy_code and
            //                                          cardep.pzrdypt_post_code = aspe.pzraspe_post_code and
            //                                          pfle.pzvpfle_code = aspe.pzraspe_pfle_code and
            //                                          prso.pzbprso_pidm = aspe.pzraspe_pidm and
            //                                          dep.PZVDNCY_CODE_SUP = sup.pzvdncy_CODE and
            //                                          sup.pzvdncy_CODE = '" + iddep + "' and " +
            //                                          "rev.pzvpost_code = '" + idcar + "'").ToList();
            //foreach (var x in cardep)
            //{


            //    Models.AsignacionCargoPersona ppcd = new Models.AsignacionCargoPersona();
            //    //   ppcd.PZRASPE_ID = x;
            //    ppcd._PZRASPE_DNCY_CODE = x.PZRASPE_DNCY_CODE;
            //    ppcd._PZRASPE_POST_CODE = x.PZRASPE_POST_CODE;
            //    ppcd._PZRASPE_PIDM = x.PZRASPE_PIDM;
            //    ppcd._PZRASPE_PFLE_CODE = x.PZRASPE_PFLE_CODE;
            //    ppcd._PZRASPE_END_DATE = x.PZRASPE_END_DATE;
            //    ppcd._PZRASPE_START_DATE = x.PZRASPE_START_DATE;
            //    ppcd._PZRASPE_DATA_ORIGIN = x.PZRASPE_DATA_ORIGIN;
            //    ppcd._PZRASPE_USER = x.PZRASPE_USER;

            //    Models.AsignacionCargoDependencia cd = new Models.AsignacionCargoDependencia();

            //    //Datos de la Ternaria   
            //    cd._PZRDYPT_POST_CODE = x.PZRDYPT.PZRDYPT_POST_CODE;
            //    cd._PZRDYPT_DNCY_CODE = x.PZRDYPT.PZRDYPT_DNCY_CODE;
            //    cd._PZRDYPT_USER = x.PZRDYPT.PZRDYPT_USER;
            //    cd._PZRDYPT_DATA_ORIGIN = x.PZRDYPT.PZRDYPT_DATA_ORIGIN;
            //    cd._PZRDYPT_ACTIVITY_DATE = x.PZRDYPT.PZRDYPT_ACTIVITY_DATE;
            //    ppcd._PZRDYPT = cd;

            //    //DEPENDENCIA
            //    Models.Dependencia dep = new Models.Dependencia();
            //    //dep.PZVDNCY_ID = x.PZVDNCY.PZVDNCY_ID;
            //    dep._PZVDNCY_KEY = x.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
            //    dep._PZVDNCY_SITE = x.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
            //    dep._PZVDNCY_LEVEL = x.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
            //    dep._PZVDNCY_NAME = x.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
            //    dep._PZVDNCY_ACTIVITY_DATE = x.PZRDYPT.PZVDNCY.PZVDNCY_ACTIVITY_DATE;
            //    dep._PZVDNCY_USER = x.PZRDYPT.PZVDNCY.PZVDNCY_USER;
            //    dep._PZVDNCY_DATA_ORIGIN = x.PZRDYPT.PZVDNCY.PZVDNCY_DATA_ORIGIN;

            //    ppcd._PZRDYPT._PZVDNCY = dep;

            //    Models.Persona per = new Models.Persona();
            //    per._PZBPRSO_PIDM = x.PZBPRSO.PZBPRSO_PIDM;
            //    per._PZBPRSO_SITE = x.PZBPRSO.PZBPRSO_SITE;
            //    per._PZBPRSO_USER = x.PZBPRSO.PZBPRSO_USER;
            //    per._PZBPRSO_PAYSHEET = x.PZBPRSO.PZBPRSO_PAYSHEET;
            //    per._PZBPRSO_DATA_ORIGIN = x.PZBPRSO.PZBPRSO_DATA_ORIGIN;
            //    per._PZBPRSO_ACTIVE = x.PZBPRSO.PZBPRSO_ACTIVE;
            //    //per.PZBPRSO_PZVPOST = x.PZBPRSO.PZBPRSO_PZCRCOD;

            //    ppcd._PZBPRSO = per;


            //    //Departamento Evaluador
            //    Models.Dependencia depEval = new Models.Dependencia();
            //    //depEval.PZVDNCY_ID = x.PZVDNCY.PZVDNCY2.PZVDNCY_ID;
            //    depEval._PZVDNCY_CODE = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_CODE;
            //    depEval._PZVDNCY_KEY = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_KEY;
            //    depEval._PZVDNCY_SITE = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_SITE;
            //    depEval._PZVDNCY_LEVEL = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_LEVEL;
            //    depEval._PZVDNCY_NAME = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_NAME;
            //    depEval._PZVDNCY_USER = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_USER;
            //    depEval._PZVDNCY_ACTIVITY_DATE = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_ACTIVITY_DATE;
            //    depEval._PZVDNCY_DATA_ORIGIN = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_DATA_ORIGIN;

            //    ppcd._PZRDYPT._PZVDNCY._PZVDNCY2 = depEval;
            //    dep._PZVDNCY2 = depEval;

            //    cd._PZVDNCY = dep;

            //    //Cargo
            //    Models.Cargo item = new Models.Cargo();

            //    //item.PZVPOST_ID = x.PZVPOST.PZVPOST_ID;
            //    item._PZVPOST_NAME = x.PZRDYPT.PZVPOST.PZVPOST_NAME;
            //    item._PZVPOST_CODE_SUP = x.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
            //    item._PZVPOST_USER = x.PZRDYPT.PZVPOST.PZVPOST_USER;
            //    item._PZVPOST_DESCRIPTION = x.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
            //    item._PZVPOST_ACTIVITY_DATE = x.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;
            //    item._PZVPOST_DATA_ORIGIN = x.PZRDYPT.PZVPOST.PZVPOST_DATA_ORIGIN;

            //    ppcd._PZRDYPT._PZVPOST = item;
            //    // Cargo Supervisor

            //    Models.Cargo itemSup = new Models.Cargo();

            //    //itemSup.PZVPOST_ID = x.PZVPOST.PZVPOST2.PZVPOST_ID;
            //    itemSup._PZVPOST_CODE = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_CODE;
            //    itemSup._PZVPOST_NAME = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_NAME;
            //    itemSup._PZVPOST_CODE_SUP = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_CODE_SUP;
            //    itemSup._PZVPOST_USER = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_USER;
            //    itemSup._PZVPOST_DESCRIPTION = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_DESCRIPTION;
            //    itemSup._PZVPOST_ACTIVITY_DATE = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_ACTIVITY_DATE;
            //    itemSup._PZVPOST_DATA_ORIGIN = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_DATA_ORIGIN;

            //    ppcd._PZRDYPT._PZVPOST._PZVPOST2 = itemSup;
            //    item._PZVPOST2 = itemSup;

            //    cd._PZVPOST = item;

            //    result.Add(ppcd);

            //}

            // DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(PZRDYPT));

            //return stream1;
            return Ok(cardep);
        }

        // GET: api/PZRDYPT/5
        [ResponseType(typeof(PZRDYPT))]
        public IHttpActionResult GetPZRDYPT(String id)
        {
            PZRDYPT pZVCDEF = db.PZRDYPT.Find(id);
            if (pZVCDEF == null)
            {
                return NotFound();
            }

            return Ok(pZVCDEF);
        }
        /// <summary>
        /// 
        /// Modifica los cargos asociados a un departamento
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pZVCDEF"></param>
        /// <returns></returns>
        // PUT: api/PZRDYPT/5
        [HttpPut]
        [Route("api/PZRDYPT/PutPZRDYPT/{id}/{idc}")]
        [ResponseType(typeof(Models.AsignacionCargoDependencia))]
        public IHttpActionResult PutPZRDYPT(String id,String idc, Models.AsignacionCargoDependencia pZVCDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

        

            try
            {
                DateTime fecha = DateTime.Today;
                pZVCDEF._PZRDYPT_ACTIVITY_DATE = fecha;

                PZRDYPT car_dep = new PZRDYPT();

                //car_dep.PZRDYPT_ID = pZVCDEF.PZRDYPT_ID;
                car_dep.PZRDYPT_POST_CODE = pZVCDEF._PZRDYPT_POST_CODE;
                car_dep.PZRDYPT_DNCY_CODE = pZVCDEF._PZRDYPT_DNCY_CODE;
                car_dep.PZRDYPT_USER = pZVCDEF._PZRDYPT_USER;
                car_dep.PZRDYPT_DATA_ORIGIN = pZVCDEF._PZRDYPT_DATA_ORIGIN;
                car_dep.PZRDYPT_ACTIVITY_DATE = pZVCDEF._PZRDYPT_ACTIVITY_DATE;
                
                db.PZRDYPT.Attach(car_dep);
                db.Entry(car_dep).State = EntityState.Modified;
                db.SaveChanges();

                return Ok("Registro Modificado exitosamente");
            }
            catch (DbUpdateConcurrencyException)
            {
                
                    return NotFound();
        
            }

            return StatusCode(HttpStatusCode.NoContent);
        }



        /// <summary>
        /// 
        /// Agrega los datos correspondientes a la relacion del
        /// cargo y el departamento
        /// RUTA: api/PZRDYPT/PostPZRDYPT
        /// 
        /// </summary>
        /// <param name="pZVCDEF"></param>
        /// <returns>Objeto Agregado</returns>
        [ResponseType(typeof(Models.AsignacionCargoDependencia))]
        public IHttpActionResult PostPZRDYPT(Models.AsignacionCargoDependencia pZVCDEF)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

         


            try
            {
                PZRDYPT cd = new PZRDYPT();

                //Decimal es = (Decimal)db.PZRDYPT.Max(x => x.);
                DateTime fecha = DateTime.Today;
                pZVCDEF._PZRDYPT_ACTIVITY_DATE = fecha;
                //es = es + 1;


                cd.PZRDYPT_POST_CODE = pZVCDEF._PZRDYPT_POST_CODE;
                cd.PZRDYPT_DNCY_CODE = pZVCDEF._PZRDYPT_DNCY_CODE;
                // cd.PZRDYPT_ID = x.PZRDYPT_ID;
                cd.PZRDYPT_USER = pZVCDEF._PZRDYPT_USER;
                cd.PZRDYPT_DATA_ORIGIN = pZVCDEF._PZRDYPT_DATA_ORIGIN;
                cd.PZRDYPT_ACTIVITY_DATE = pZVCDEF._PZRDYPT_ACTIVITY_DATE;


                //pZVCDEF.PZRDYPT_ID = es;


                db.PZRDYPT.Add(cd);
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
             
                    return Conflict();
       

            }

            return CreatedAtRoute("DefaultApi", new { id = pZVCDEF }, pZVCDEF);
        }

        // DELETE: api/PZRDYPT/5
        [ResponseType(typeof(PZRDYPT))]
        public IHttpActionResult DeletePZRDYPT(String id)
        {
            PZRDYPT pZVCDEF = db.PZRDYPT.Find(id);
            if (pZVCDEF == null)
            {
                return NotFound();
            }

            db.PZRDYPT.Remove(pZVCDEF);
            db.SaveChanges();

            return Ok(pZVCDEF);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

      
    }
}