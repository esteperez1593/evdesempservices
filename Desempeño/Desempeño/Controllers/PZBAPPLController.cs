﻿using Desempeño.DataConect;
using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Desempeño.Controllers
{
    public class PZBAPPLController : ApiController
    {

        private Entities db = new Entities();
        //GET: api/PZBAPPL
        /// <summary>
        /// Obtiene la lista de instrumentos aplicables (_insApls) registrados
        /// RUTA : api/PZBAPPL/GetPZBAPPL
        /// </summary>
        /// <returns> Retorna una lista de Instrumentos aplicables</returns>
        [HttpGet]
        [Route("api/PZBAPPL/GetPZBAPPL/")]
        [ResponseType(typeof(List<Models.InstrumentoAplicable>))]
        public IHttpActionResult GetPZBAPPL()
        {
            List<Models.InstrumentoAplicable> result = new List<Models.InstrumentoAplicable>();
            try
            {

                //db.PZBAPPL.Include(ins_insApl => ins_insApl).Include(_insAplfas => _insAplfas.PZRCRPH);
                var res = db.PZBAPPL.SqlQuery(@"Select * from PZBAPPL").ToList();

                foreach (var X in res)
                {

                    //Datos del Instrumento Aplicable
                    Models.InstrumentoAplicable _insApl = new Models.InstrumentoAplicable();
                    _insApl._PZBAPPL_CALR_CODE = X.PZBAPPL_CALR_CODE;
                    _insApl._PZBAPPL_PFLE_CODE = X.PZBAPPL_PFLE_CODE;
                    _insApl._PZBAPPL_PRCNT = X.PZBAPPL_PRCNT;
                    _insApl._PZBAPPL_POLL_CODE = X.PZBAPPL_POLL_CODE;
                    _insApl._PZBAPPL_SEQ_NUMBER = X.PZBAPPL_SEQ_NUMBER;
                    _insApl._PZBAPPL_USER = X.PZBAPPL_USER;               
                    _insApl._PZBAPPL_DATA_ORIGIN = X.PZBAPPL_DATA_ORIGIN;
                    _insApl._PZBAPPL_ACTIVITY_DATE = X.PZBAPPL_ACTIVITY_DATE;
                    //_insApl._PZBAPPL_POLL_CODE = X.PZBAPPL_POLL_CODE;
                    result.Add(_insApl);

                }

            }
            catch (Exception e)
            {

            }

            return Ok(result);
        }

        // GET: api/PZBAPPL/5
        [ResponseType(typeof(PZBAPPL))]
        public IHttpActionResult GetPZBAPPL(decimal id)
        {
            PZBAPPL pZRCDEF = db.PZBAPPL.Find(id);
            if (pZRCDEF == null)
            {
                return NotFound();
            }

            return Ok(pZRCDEF);
        }

        /// <summary>
        /// 
        /// Modifica los Calendarios
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pZRCDEF"></param>
        /// <returns></returns>
        // PUT: api/PZBAPPL/5
        [HttpPut]
        [Route("api/PZBAPPL/PutPZBAPPL/{id}")]
        [ResponseType(typeof(InstrumentoAplicable))]
        public IHttpActionResult PutPZBAPPL(string id, InstrumentoAplicable pZRCDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }



            try
            {
                DateTime fecha = DateTime.Today;

                pZRCDEF._PZBAPPL_ACTIVITY_DATE = fecha;

                PZBAPPL _insApl = new PZBAPPL();

                //_insApl.PZBAPPL_ID = pZRCDEF.PZBAPPL_ID;
                _insApl.PZBAPPL_CALR_CODE = pZRCDEF._PZBAPPL_CALR_CODE;
                _insApl.PZBAPPL_PFLE_CODE = pZRCDEF._PZBAPPL_PFLE_CODE;
                _insApl.PZBAPPL_PRCNT = pZRCDEF._PZBAPPL_PRCNT;
                _insApl.PZBAPPL_POLL_CODE = pZRCDEF._PZBAPPL_POLL_CODE;
                _insApl.PZBAPPL_SEQ_NUMBER = pZRCDEF._PZBAPPL_SEQ_NUMBER;
                _insApl.PZBAPPL_USER = pZRCDEF._PZBAPPL_USER;
                _insApl.PZBAPPL_DATA_ORIGIN = pZRCDEF._PZBAPPL_DATA_ORIGIN;
                _insApl.PZBAPPL_DATA_ORIGIN = pZRCDEF._PZBAPPL_DATA_ORIGIN;
                _insApl.PZBAPPL_ACTIVITY_DATE = pZRCDEF._PZBAPPL_ACTIVITY_DATE;

                db.PZBAPPL.Attach(_insApl);
                db.Entry(_insApl).State = EntityState.Modified;
                db.SaveChanges();

                return Ok();//"El _insApl " + _insApl.PZBAPPL_NAME + " del periodo " + _insApl.PZBAPPL_TERM + " ha sido modificado exitosamente");

            }
            catch (DbUpdateConcurrencyException)
            {

                return NotFound();

            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PZBAPPL
        /// <summary>
        /// 
        /// Inserta un nuevo _insApl
        /// Ruta : api/PZBAPPL/PostPZBAPPL
        /// 
        /// </summary>
        /// <param name="pZRCDEF"></param>
        /// <returns></returns>
        [ResponseType(typeof(InstrumentoAplicable))]
        public IHttpActionResult PostPZBAPPL(Models.InstrumentoAplicable pZRCDEF)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (db.PZBAPPL.Count(e => e.PZBAPPL_SEQ_NUMBER == pZRCDEF._PZBAPPL_SEQ_NUMBER) > 0)
            {

                return BadRequest("El codigo " + pZRCDEF._PZBAPPL_SEQ_NUMBER + " ya existe.");
            }


            try
            {
                int es = (int)db.PZBAPPL.Max(x => x.PZBAPPL_SEQ_NUMBER);


                es = es + 1;

                DateTime fecha = DateTime.Today;

                PZBAPPL _insApl = new PZBAPPL();
                // _insApl.PZBAPPL_ID = es;
                _insApl.PZBAPPL_CALR_CODE = pZRCDEF._PZBAPPL_CALR_CODE;
                _insApl.PZBAPPL_PFLE_CODE = pZRCDEF._PZBAPPL_PFLE_CODE;
                _insApl.PZBAPPL_PRCNT = pZRCDEF._PZBAPPL_PRCNT;
                _insApl.PZBAPPL_POLL_CODE = pZRCDEF._PZBAPPL_POLL_CODE;
                _insApl.PZBAPPL_SEQ_NUMBER = pZRCDEF._PZBAPPL_SEQ_NUMBER;
                _insApl.PZBAPPL_USER = pZRCDEF._PZBAPPL_USER;
                _insApl.PZBAPPL_DATA_ORIGIN = pZRCDEF._PZBAPPL_DATA_ORIGIN;
                _insApl.PZBAPPL_DATA_ORIGIN = pZRCDEF._PZBAPPL_DATA_ORIGIN;
                _insApl.PZBAPPL_ACTIVITY_DATE = pZRCDEF._PZBAPPL_ACTIVITY_DATE;
                _insApl.PZBAPPL_ACTIVITY_DATE = fecha;
                _insApl.PZBAPPL_USER = pZRCDEF._PZBAPPL_USER;
                // _insApl.PZBAPPL_POLL_CODE = pZRCDEF._PZBAPPL_POLL_CODE;

                db.PZBAPPL.Add(_insApl);

                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PZBAPPLExists(pZRCDEF._PZBAPPL_SEQ_NUMBER))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = pZRCDEF._PZBAPPL_SEQ_NUMBER }, pZRCDEF);
        }

        // DELETE: api/PZBAPPL/5
        [ResponseType(typeof(PZBAPPL))]
        public IHttpActionResult DeletePZBAPPL(decimal id)
        {
            PZBAPPL pZRCDEF = db.PZBAPPL.Find(id);
            if (pZRCDEF == null)
            {
                return NotFound();
            }

            db.PZBAPPL.Remove(pZRCDEF);
            db.SaveChanges();

            return Ok(pZRCDEF);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZBAPPLExists(int id)
        {
            return db.PZBAPPL.Count(e => e.PZBAPPL_SEQ_NUMBER == id) > 0;
        }

    }
}
