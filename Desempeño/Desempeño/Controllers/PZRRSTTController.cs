﻿using Desempeño.DataConect;
using Desempeño.Models;
using Desempeño.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Desempeño.Controllers
{
    public class PZRRSTTController : ApiController
    {
        private Entities db = new Entities();
        private FunctionsRepository _f = new FunctionsRepository();

        // GET: api/PZRRSTT
        /// <summary>
        /// 
        /// Obtiene los tipos de Resultado Disponible
        /// Ruta api/PZRRSTT/GetPZRRSTT
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/PZRRSTT/GetPZRRSTTs/")]
        [ResponseType(typeof(List<ResultadosTotales>))]
        public IHttpActionResult GetPZRRSTTs()
        {

            List<Models.ResultadosTotales> result = new List<Models.ResultadosTotales>();
            try
            {

                //db.PZRRSTT.Include(tr => tr.PZ);
                var res = db.PZRRSTT.SqlQuery(@" SELECT * from PZRRSTT ").ToList();

                foreach (var x in res)
                {

                    //Datos del tipo de Respuesta
                    Models.ResultadosTotales tr = new Models.ResultadosTotales();
                    tr._PZRRSTT_ID = x.PZRRSTT_ID;
                    tr._PZRRSTT_DESCRIPTION = x.PZRRSTT_DESCRIPTION;
                    tr._PZRRSTT_ACTIVITY_DATE = x.PZRRSTT_ACTIVITY_DATE;
                    tr._PZRRSTT_DATA_ORIGIN = x.PZRRSTT_DATA_ORIGIN;
                    tr._PZRRSTT_USER = x.PZRRSTT_USER;
                    tr._PZRRSTT_PLAP_SEQ_NUMBER = x.PZRRSTT_PLAP_SEQ_NUMBER;
                    tr._PZRRSTT_RESULT_VALUE = x.PZRRSTT_RESULT_VALUE;
            
                    result.Add(tr);

                }

                return (Ok(result));

            }

            catch (Exception e)
            {

                return (NotFound());

            }


        }

        [HttpGet]
        [Route("api/PZRRSTT/GetPZRRSTT/{_perfil}/{_calendario}/{_instrumento}/{_pidm}/")]
        [ResponseType(typeof(Decimal))]
        public IHttpActionResult GetPZRRSTT(string _perfil,string _calendario, string _instrumento, string _pidm)
        {

          
            try
            {
                return Ok(db.Database.SqlQuery<decimal>(@"SELECT AGET_POLL_EXT('"+_perfil+"','"+_calendario+"','"+_instrumento+"','"+_pidm+"') FROM DUAL"));

            }

            catch (Exception e)
            {

                return (NotFound());

            }


        }

        // PUT: api/PZRRSTT/5
        /// <summary>
        /// 
        /// Modifica los registros de los tipos de Resultado
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="PZRRSTT"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/PZRRSTT/PutPZRRSTT/{id}/")]
        [ResponseType(typeof(ResultadosTotales))]
        public IHttpActionResult PutPZRRSTT(int id, ResultadosTotales _PZRRSTT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != _PZRRSTT._PZRRSTT_ID)
            {
                return BadRequest();
            }



            try
            {
                DateTime fecha = DateTime.Today;

                _PZRRSTT._PZRRSTT_ACTIVITY_DATE = fecha;


                PZRRSTT tr = new PZRRSTT();
                tr.PZRRSTT_ID = _PZRRSTT._PZRRSTT_ID;
                tr.PZRRSTT_DESCRIPTION = _PZRRSTT._PZRRSTT_DESCRIPTION;
                tr.PZRRSTT_ACTIVITY_DATE = _PZRRSTT._PZRRSTT_ACTIVITY_DATE;
                tr.PZRRSTT_DATA_ORIGIN = _PZRRSTT._PZRRSTT_DATA_ORIGIN;
                tr.PZRRSTT_USER = _PZRRSTT._PZRRSTT_USER;
                tr.PZRRSTT_PLAP_SEQ_NUMBER = _PZRRSTT._PZRRSTT_PLAP_SEQ_NUMBER;
                tr.PZRRSTT_RESULT_VALUE = _PZRRSTT._PZRRSTT_RESULT_VALUE;

                db.PZRRSTT.Attach(tr);
                System.Diagnostics.Debug.WriteLine(tr);
                db.Entry(tr).State = EntityState.Modified;
                System.Diagnostics.Debug.WriteLine((db.Entry(tr).State = EntityState.Modified));
                db.SaveChanges();



                return Ok("Tipo de Respuesta: " + tr.PZRRSTT_DESCRIPTION + " Modificado");

                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PZRRSTTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PZRRSTT
        /// <summary>
        /// 
        /// Agrega los Tipo de Resultado Posibles
        /// RUTA: api/PZRRSTT/PostPZRRSTT
        /// 
        /// </summary>
        /// <param name="PZRRSTT"></param>
        /// <returns>Tipo de Respuesta</returns>
        [HttpPost]
        [ResponseType(typeof(Models.ResultadosTotales))]
        [Route("api/PZRRSTT/PostPZRRSTT/{_perfil}/{_term}/{_calendario}/{_instrumento}/{_pidm}/")]
        public IHttpActionResult PostPZRRSTT(string _perfil, string _term, string _calendario, string _instrumento, string _pidm,Models.ResultadosTotales _PZRRSTT)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (db.PZRRSTT.Count(e => e.PZRRSTT_ID == _PZRRSTT._PZRRSTT_ID) > 0)
            {

                return BadRequest("El codigo " + _PZRRSTT._PZRRSTT_ID + " ya existe.");
            }

            try
            {

                decimal value=_f.GetIndPoll (_perfil, _term, _calendario, _instrumento, _pidm);
                // Decimal es = (Decimal)db.PZRRSTT.Max(x => x.PZRRSTT_ID);


                //  es = es + 1;

                DateTime fecha = DateTime.Today;


                _PZRRSTT._PZRRSTT_ACTIVITY_DATE = fecha;
                //  PZRRSTT.PZRRSTT_ID = es;


                PZRRSTT tr = new PZRRSTT();
                tr.PZRRSTT_ID = _PZRRSTT._PZRRSTT_ID;
                tr.PZRRSTT_DESCRIPTION = _PZRRSTT._PZRRSTT_DESCRIPTION;
                tr.PZRRSTT_ACTIVITY_DATE = _PZRRSTT._PZRRSTT_ACTIVITY_DATE;
                tr.PZRRSTT_DATA_ORIGIN = _PZRRSTT._PZRRSTT_DATA_ORIGIN;
                tr.PZRRSTT_USER = _PZRRSTT._PZRRSTT_USER;
                tr.PZRRSTT_PLAP_SEQ_NUMBER = _PZRRSTT._PZRRSTT_PLAP_SEQ_NUMBER;
                tr.PZRRSTT_RESULT_VALUE =(int) value;


                db.PZRRSTT.Add(tr);

                db.SaveChanges();

            }
            catch (DbUpdateException)
            {
                if (PZRRSTTExists(_PZRRSTT._PZRRSTT_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = _PZRRSTT._PZRRSTT_ID }, _PZRRSTT);
        }


        // DELETE: api/PZRRSTT/5
        [ResponseType(typeof(PZRRSTT))]
        public IHttpActionResult DeletePZRRSTT(decimal id)
        {
            PZRRSTT PZRRSTT = db.PZRRSTT.Find(id);
            if (PZRRSTT == null)
            {
                return NotFound();
            }

            db.PZRRSTT.Remove(PZRRSTT);
            db.SaveChanges();

            return Ok(PZRRSTT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZRRSTTExists(int id)
        {
            return db.PZRRSTT.Count(e => e.PZRRSTT_ID == id) > 0;
        }
    }
}
