﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Desempeño.DataConect;
using Desempeño.Models;

namespace Desempeño.Controllers
{
    public class PZBCRITController : ApiController
    {
        private Entities db = new Entities();
        private Repository.ValidatorRepository _v = new Repository.ValidatorRepository();


        /// <summary>
        /// 
        /// Trae todos los criterio de evalucion disponibles.
        /// RUTA : api/PZBCRIT/GetPZBCRIT
        /// 
        /// </summary>
        /// <returns>200</returns>
        [ResponseType(typeof(List<Models.Criterio>))]
        public IHttpActionResult GetPZBCRIT()
        {
            List<Models.Criterio> result = new List<Models.Criterio>();
            try
            {

                var cri = db.PZBCRIT.SqlQuery(@"select * from PZBCRIT").ToList();

               foreach (var x in cri) {

                    Models.Criterio res = new Models.Criterio();
                    res._PZBCRIT_CODE = x.PZBCRIT_CODE;
                    res._PZBCRIT_USER = x.PZBCRIT_USER;
                    res._PZBCRIT_DESCRIPTION = x.PZBCRIT_DESCRIPTION;
                    res._PZBCRIT_DATA_ORIGIN = x.PZBCRIT_DATA_ORIGIN;
                    res._PZBCRIT_ACTIVITY_DATE = x.PZBCRIT_ACTIVITY_DATE;
                    result.Add(res);

                }

            return Ok(result);

            }
            catch(Exception e) {

                return NotFound();
                
                  }

        }




        // GET: api/PZBCRIT/5
        [ResponseType(typeof(PZBCRIT))]
        public IHttpActionResult GetPZBCRITs(decimal id)
        {
            PZBCRIT PZBCRIT = db.PZBCRIT.Find(id);
            if (PZBCRIT == null)
            {
                return NotFound();
            }

            return Ok(PZBCRIT);
        }

        // PUT: api/PZBCRIT/5
        /// <summary>
        /// 
        /// Modifica un Criterio Registrado
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="PZBCRIT"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/PZBCRIT/PutPZBCRIT/{id}/")]
        [ResponseType(typeof(Criterio))]
        public IHttpActionResult PutPZBCRIT(string id, Criterio PZBCRIT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != PZBCRIT._PZBCRIT_CODE)
            {
                return BadRequest();
            }


            try
            {
                String valid = _v.validarComponentesInstrumentos(id);
                if (valid == "CAMPO ASIGNADO")
                {
                    return BadRequest("Este campo no se puede editar porque ya fue asignado");

                }

                DateTime fecha = DateTime.Today;

                PZBCRIT._PZBCRIT_ACTIVITY_DATE = fecha;

                PZBCRIT criterio  = new PZBCRIT();

                criterio.PZBCRIT_CODE = PZBCRIT._PZBCRIT_CODE;
                criterio.PZBCRIT_USER = PZBCRIT._PZBCRIT_USER;
                criterio.PZBCRIT_DESCRIPTION = PZBCRIT._PZBCRIT_DESCRIPTION;
                criterio.PZBCRIT_DATA_ORIGIN = PZBCRIT._PZBCRIT_DATA_ORIGIN;
                criterio.PZBCRIT_ACTIVITY_DATE = PZBCRIT._PZBCRIT_ACTIVITY_DATE;

                db.PZBCRIT.Attach(criterio);
                db.Entry(criterio).State = EntityState.Modified;

                db.SaveChanges();

                return Ok("Se ha modificado el criterio exitosamente");

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PZBCRITExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        /// <summary>
        /// 
        /// Agrega Criterios al Listado de criterios disponibles
        /// RUTA : api/PZBCRIT/PostPZBCRIT
        /// 
        /// </summary>
        /// <param name="PZBCRIT"></param>
        /// <returns>201 Created</returns>
        [ResponseType(typeof(Criterio))]
        public IHttpActionResult PostPZBCRIT(Models.Criterio PZBCRIT)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try {



                DateTime fecha = DateTime.Today;


                PZBCRIT._PZBCRIT_ACTIVITY_DATE = fecha;
//                PZBCRIT.PZBCRIT_ID = es;

                PZBCRIT objCrit = new PZBCRIT();
                objCrit.PZBCRIT_CODE = PZBCRIT._PZBCRIT_CODE;
                objCrit.PZBCRIT_DESCRIPTION = PZBCRIT._PZBCRIT_DESCRIPTION;
                objCrit.PZBCRIT_DATA_ORIGIN = PZBCRIT._PZBCRIT_DATA_ORIGIN;
                objCrit.PZBCRIT_ACTIVITY_DATE = PZBCRIT._PZBCRIT_ACTIVITY_DATE;
                objCrit.PZBCRIT_USER = PZBCRIT._PZBCRIT_USER;
               
                

                db.PZBCRIT.Add(objCrit);

 
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PZBCRITExists(PZBCRIT._PZBCRIT_CODE))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = PZBCRIT._PZBCRIT_CODE }, PZBCRIT);
        }

        // DELETE: api/PZBCRIT/5
        [ResponseType(typeof(PZBCRIT))]
        public IHttpActionResult DeletePZBCRIT(decimal id)
        {
            PZBCRIT PZBCRIT = db.PZBCRIT.Find(id);
            if (PZBCRIT == null)
            {
                return NotFound();
            }

            db.PZBCRIT.Remove(PZBCRIT);
            db.SaveChanges();

            return Ok(PZBCRIT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PZBCRITExists(string id)
        {
            return db.PZBCRIT.Count(e => e.PZBCRIT_CODE == id) > 0;
        }
    }
}