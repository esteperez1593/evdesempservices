﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class Mensaje
    {

        public int _PZRMSSG_ID { get; set; }
        public string _PZRMSSG_DESCRIPTION { get; set; }
        public string _PZRMSSG_PHSE_CODE { get; set; }
        public string _PZRMSSG_USER { get; set; }
        public System.DateTime _PZRMSSG_ACTIVITY_DATE { get; set; }
        public string _PZRMSSG_DATA_ORIGIN { get; set; }

        public  Fase _PZBPHSE { get; set; }

    }
}