﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class ResultadosParciales
    {

        public int _PZRRSPL_SEQ_NUMBER { get; set; }
        public int _PZRRSPL_PLAP_SEQ_NUMBER { get; set; }
        public int _PZRRSPL_APRS_SEQ_NUMBER { get; set; }
        public Nullable<decimal> _PZRRSPL_NUMB_VALUE { get; set; }
        public string _PZRRSPL_STR_VALUE { get; set; }
        public string _PZRRSPL_USER { get; set; }
        public System.DateTime _PZRRSPL_ACTIVITY_DATE { get; set; }
        public string _PZRRSPL_DATA_ORIGIN { get; set; }


        public ResultadoAplicable _PZBAPRS { get; set; }
        public AsignacionInstrumentoPersona _PZRPLAP { get; set; }

    }
}