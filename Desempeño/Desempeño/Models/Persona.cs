﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class Persona
    {

        public Persona()
        {
            this._PZRASPE = new HashSet<AsignacionCargoPersona>();
        }

        public int _PZBPRSO_PIDM { get; set; }
        public string _PZBPRSO_PAYSHEET { get; set; }
        public string _PZBPRSO_ACTIVE { get; set; }
        public string _PZBPRSO_SITE { get; set; }
        public string _PZBPRSO_USER { get; set; }
        public System.DateTime _PZBPRSO_ACTIVITY_DATE { get; set; }
        public string _PZBPRSO_DATA_ORIGIN { get; set; }

        public ICollection<AsignacionCargoPersona> _PZRASPE { get; set; }

    }
}