﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class Permiso
    {

        public decimal _PZVPERM_CODE { get; set; }
        public string _PZVPERM_POST_CODE { get; set; }
        public string _PZVPERM_DNCY_CODE { get; set; }
        public string _PZVPERM_PFLE_CODE { get; set; }
        public Nullable<int> _PZVPERM_PIDM { get; set; }
        public string _PZVPERM_RGHT_CODE { get; set; }
        public string _PZVPERM_USER { get; set; }
        public System.DateTime _PZVPERM_ACTIVITY_DATE { get; set; }
        public string _PZVPERM_DATA_ORIGIN { get; set; }

        public AsignacionCargoPersona  _PZRASPE { get; set; }
        public Perfil _PZVPFLE { get; set; }
        public DefinicionPermisos _PZVRGHT { get; set; }

    }
}