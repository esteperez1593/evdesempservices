﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class Fase
    {
        public Fase()
        {
            this._PZRMSSG = new HashSet<Mensaje>();
        }

        public string _PZBPHSE_CODE { get; set; }
        public string _PZBPHSE_NAME { get; set; }
        public string _PZBPHSE_CALR_CODE { get; set; }
        public System.DateTime _PZBPHSE_OPEN_DATE { get; set; }
        public System.DateTime _PZBPHSE_CLOSE_DATE { get; set; }
        public Nullable<System.DateTime> _PZBPHSE_OPEXTENSION_DATE { get; set; }
        public Nullable<System.DateTime> _PZBPHSE_CLEXTENSION_DATE { get; set; }
        public string _PZBPHSE_USER { get; set; }
        public System.DateTime _PZBPHSE_ACTIVITY_DATE { get; set; }
        public string _PZBPHSE_DATA_ORIGIN { get; set; }

        public Calendario _PZBCALR { get; set; }
        public ICollection<Mensaje> _PZRMSSG { get; set; }
    }
}