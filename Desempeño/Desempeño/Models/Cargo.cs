﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class Cargo
    {

        public Cargo()
        {
            this._PZRDYPT = new HashSet<AsignacionCargoDependencia>();
            this._PZVPOST1 = new HashSet<Cargo>();
        }

        public string _PZVPOST_CODE { get; set; }
        public string _PZVPOST_NAME { get; set; }
        public string _PZVPOST_DESCRIPTION { get; set; }
        public string _PZVPOST_CODE_SUP { get; set; }
        public string _PZVPOST_USER { get; set; }
        public System.DateTime _PZVPOST_ACTIVITY_DATE { get; set; }
        public string _PZVPOST_DATA_ORIGIN { get; set; }

        public ICollection<AsignacionCargoDependencia> _PZRDYPT { get; set; }
        public ICollection<Cargo> _PZVPOST1 { get; set; }
        public Cargo _PZVPOST2 { get; set; }

    }
}