﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class Perfil
    {

        public Perfil()
        {
            this._PZBAPPL = new HashSet<InstrumentoAplicable>();
            this._PZRASPE = new HashSet<AsignacionCargoPersona>();
            this._PZVPERM = new HashSet<Permiso>();
        }

        public string _PZVPFLE_CODE { get; set; }
        public string _PZVPFLE_NAME { get; set; }
        public string _PZVPFLE_USER { get; set; }
        public System.DateTime _PZVPFLE_ACTIVITY_DATE { get; set; }
        public string _PZVPFLE_DATA_ORIGIN { get; set; }

        public ICollection<InstrumentoAplicable> _PZBAPPL { get; set; }
        public ICollection<AsignacionCargoPersona> _PZRASPE { get; set; }
        public ICollection<Permiso> _PZVPERM { get; set; }

    }
}