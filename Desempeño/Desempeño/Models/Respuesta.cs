﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class Respuesta
    {
        public int _PZRAWAP_SEQ_NUMBER { get; set; }
        public int _PZRAWAP_PLAP_SEQ_NUMBER { get; set; }
        public int _PZRAWAP_PLQS_SEQ_NUMBER { get; set; }
        public string _PZRAWAP_DESCRIPTION { get; set; }
        public Nullable<short> _PZRAWAP_PERC_IMP { get; set; }
        public short _PZRAWAP_VALUE { get; set; }
        public string _PZRAWAP_USER { get; set; }
        public System.DateTime _PZRAWAP_ACTIVITY_DATE { get; set; }
        public string _PZRAWAP_DATA_ORIGIN { get; set; }

        public AsignacionInstrumentoPregunta _PZBPLQS { get; set; }
        public AsignacionInstrumentoPersona _PZRPLAP { get; set; }
    }
}