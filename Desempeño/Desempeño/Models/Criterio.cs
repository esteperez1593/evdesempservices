﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class Criterio
    {

        public Criterio()
        {
            this._PZBPLQS = new HashSet<AsignacionInstrumentoPregunta>();
        }

        public string _PZBCRIT_CODE { get; set; }
        public string _PZBCRIT_DESCRIPTION { get; set; }
        public string _PZBCRIT_USER { get; set; }
        public System.DateTime _PZBCRIT_ACTIVITY_DATE { get; set; }
        public string _PZBCRIT_DATA_ORIGIN { get; set; }

        public  ICollection<AsignacionInstrumentoPregunta> _PZBPLQS { get; set; }

    }
}