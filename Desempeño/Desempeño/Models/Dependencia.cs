﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class Dependencia
    {

        public Dependencia()
        {
            this._PZRDYPT = new HashSet<AsignacionCargoDependencia>();
            this._PZVDNCY1 = new HashSet<Dependencia>();
        }

        public string _PZVDNCY_CODE { get; set; }
        public string _PZVDNCY_NAME { get; set; }
        public string _PZVDNCY_CODE_SUP { get; set; }
        public string _PZVDNCY_SITE { get; set; }
        public int _PZVDNCY_LEVEL { get; set; }
        public string _PZVDNCY_KEY { get; set; }
        public string _PZVDNCY_USER { get; set; }
        public System.DateTime _PZVDNCY_ACTIVITY_DATE { get; set; }
        public string _PZVDNCY_DATA_ORIGIN { get; set; }

        public  ICollection<AsignacionCargoDependencia> _PZRDYPT { get; set; }
        public ICollection<Dependencia> _PZVDNCY1 { get; set; }
        public Dependencia _PZVDNCY2 { get; set; }

    }
}