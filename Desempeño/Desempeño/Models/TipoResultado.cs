﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class TipoResultado
    {

        public TipoResultado()
        {
            this._PZBAPRS = new HashSet<ResultadoAplicable>();
        }

        public string _PZVTYRS_CODE { get; set; }
        public string _PZVTYRS_DESCRIPTION { get; set; }
        public string _PZVTYRS_USER { get; set; }
        public System.DateTime _PZVTYRS_ACTIVITY_DATE { get; set; }
        public string _PZVTYRS_DATA_ORIGIN { get; set; }

        public  ICollection<ResultadoAplicable> _PZBAPRS { get; set; }

    }
}