﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class TipoRespuesta
    {
      public  TipoRespuesta()
        {
            this._PZBPLQS = new HashSet<AsignacionInstrumentoPregunta>();
        }

        public string _PZVTYAW_CODE { get; set; }
        public string _PZVTYAW_DESCRIPTION { get; set; }
        public string _PZVTYAW_USER { get; set; }
        public System.DateTime _PZVTYAW_ACTIVITY_DATE { get; set; }
        public string _PZVTYAW_DATA_ORIGIN { get; set; }

        public  ICollection<AsignacionInstrumentoPregunta> _PZBPLQS { get; set; }
    }
}