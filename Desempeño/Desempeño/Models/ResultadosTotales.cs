﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class ResultadosTotales
    {

        public int _PZRRSTT_ID { get; set; }
        public int _PZRRSTT_PLAP_SEQ_NUMBER { get; set; }
        public string _PZRRSTT_DESCRIPTION { get; set; }
        public Nullable<int> _PZRRSTT_RESULT_VALUE { get; set; }
        public string _PZRRSTT_USER { get; set; }
        public System.DateTime _PZRRSTT_ACTIVITY_DATE { get; set; }
        public string _PZRRSTT_DATA_ORIGIN { get; set; }

        public AsignacionInstrumentoPersona _PZRPLAP { get; set; }

    }
}