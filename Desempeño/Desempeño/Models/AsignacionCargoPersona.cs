﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class AsignacionCargoPersona
    {

        public AsignacionCargoPersona()
        {
            this._PZRPLAP = new HashSet<AsignacionInstrumentoPersona>();
            this._PZVPERM = new HashSet<Permiso>();
        }

        public string _PZRASPE_POST_CODE { get; set; }
        public string _PZRASPE_DNCY_CODE { get; set; }
        public string _PZRASPE_PFLE_CODE { get; set; }
        public int _PZRASPE_PIDM { get; set; }
        public System.DateTime _PZRASPE_START_DATE { get; set; }
        public Nullable<System.DateTime> _PZRASPE_END_DATE { get; set; }
        public string _PZRASPE_USER { get; set; }
        public System.DateTime _PZRASPE_ACTIVITY_DATE { get; set; }
        public string _PZRASPE_DATA_ORIGIN { get; set; }

        public Persona _PZBPRSO { get; set; }
        public ICollection<AsignacionInstrumentoPersona> _PZRPLAP { get; set; }
        public ICollection<Permiso> _PZVPERM { get; set; }
        public AsignacionCargoDependencia _PZRDYPT { get; set; }
        public Perfil _PZVPFLE { get; set; }

    }
}