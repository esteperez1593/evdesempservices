﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class AsignacionInstrumentoPregunta
    {
        public AsignacionInstrumentoPregunta()
        {
            this._PZRAWAP = new HashSet<Respuesta>();
        }

        public int _PZBPLQS_SEQ_NUMBER { get; set; }
        public int _PZBPLQS_APPL_SEQ_NUMBER { get; set; }
        public string _PZBPLQS_POLL_CODE { get; set; }
        public string _PZBPLQS_CALR_CODE { get; set; }
        public string _PZBPLQS_QSTN_CODE { get; set; }
        public string _PZBPLQS_AREA_CODE { get; set; }
        public string _PZBPLQS_TYAW_CODE { get; set; }
        public string _PZBPLQS_CRIT_CODE { get; set; }
        public Nullable<short> _PZBPLQS_MIN_VALUE { get; set; }
        public Nullable<short> _PZBPLQS_MAX_VALUE { get; set; }
        public Nullable<short> _PZBPLQS_MIN_OVAL { get; set; }
        public Nullable<short> _PZBPLQS_MAX_OVAL { get; set; }
        public short _PZBPLQS_ORDER { get; set; }
        public string _PZBPLQS_STATUS { get; set; }
        public string _PZBPLQS_USER { get; set; }
        public System.DateTime _PZBPLQS_ACTIVITY_DATE { get; set; }
        public string _PZBPLQS_DATA_ORIGIN { get; set; }

        public  InstrumentoAplicable _PZBAPPL { get; set; }
        public Area _PZBAREA { get; set; }
        public Criterio _PZBCRIT { get; set; }
        public ICollection<Respuesta> _PZRAWAP { get; set; }
        public Pregunta _PZBQSTN { get; set; }
        public TipoRespuesta _PZVTYAW { get; set; }
    }
}