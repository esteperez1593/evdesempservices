﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class Calendario
    {

        public Calendario()
        {
            this._PZBAPPL = new HashSet<InstrumentoAplicable>();
            this._PZBPHSE = new HashSet<Fase>();
        }

        public string _PZBCALR_CODE { get; set; }
        public string _PZBCALR_NAME { get; set; }
        public int _PZBCALR_SRL { get; set; }
        public System.DateTime _PZBCALR_INIT_DATE { get; set; }
        public System.DateTime _PZBCALR_END_DATE { get; set; }
        public string _PZBCALR_TERM { get; set; }
        public string _PZBCALR_USER { get; set; }
        public System.DateTime _PZBCALR_ACTIVITY_DATE { get; set; }
        public string _PZBCALR_DATA_ORIGIN { get; set; }

        public  ICollection<InstrumentoAplicable> _PZBAPPL { get; set; }
        public  ICollection<Fase> _PZBPHSE { get; set; }

    }
}