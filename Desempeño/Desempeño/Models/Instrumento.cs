﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class Instrumento
    {

        public Instrumento()
        {
            this._PZBAPPL = new HashSet<InstrumentoAplicable>();
        }

        public Instrumento(string _PZBPOLL_NAME)
        {
            this._PZBPOLL_NAME = _PZBPOLL_NAME;
            this._PZBAPPL = new HashSet<InstrumentoAplicable>();
        }

        public string _PZBPOLL_CODE { get; set; }
        public string _PZBPOLL_NAME { get; set; }
        public string _PZBPOLL_USER { get; set; }
        public System.DateTime _PZBPOLL_ACTIVITY_DATE { get; set; }
        public string _PZBPOLL_DATA_ORIGIN { get; set; }

        public ICollection<InstrumentoAplicable> _PZBAPPL { get; set; }

    }
}