﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class DefinicionPermisos
    {

        public DefinicionPermisos()
        {
            this._PZVPERM = new HashSet<Permiso>();
        }

        public string _PZVRGHT_CODE { get; set; }
        public string _PZVRGHT_DESCRIPTION { get; set; }
        public int _PZVRGHT_GRADE { get; set; }
        public string _PZVRGHT_USER { get; set; }
        public System.DateTime _PZVRGHT_ACTIVITY_DATE { get; set; }
        public string _PZVRGHT_DATA_ORIGIN { get; set; }

        public  ICollection<Permiso> _PZVPERM { get; set; }

    }
}