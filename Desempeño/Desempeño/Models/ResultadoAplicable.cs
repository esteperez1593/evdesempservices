﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class ResultadoAplicable
    {
        public ResultadoAplicable()
        {
            this._PZRRSPL = new HashSet<ResultadosParciales>();
        }

        public int _PZBAPRS_SEQ_NUMBER { get; set; }
        public int _PZBAPRS_APPL_SEQ_NUMBER { get; set; }
        public string _PZBAPRS_POLL_CODE { get; set; }
        public string _PZBAPRS_CALR_CODE { get; set; }
        public string _PZBAPRS_TYRS_CODE { get; set; }
        public int _PZBAPRS_ORDER { get; set; }
        public string _PZBAPRS_USER { get; set; }
        public System.DateTime _PZBAPRS_ACTIVITY_DATE { get; set; }
        public string _PZBAPRS_DATA_ORIGIN { get; set; }

        public InstrumentoAplicable _PZBAPPL { get; set; }
        public ICollection<ResultadosParciales> _PZRRSPL { get; set; }
        public TipoResultado _PZVTYRS
        {
            get; set;
        }
    }
    }