﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class Pregunta
    {
        public Pregunta()
        {
            this._PZBPLQS = new HashSet<AsignacionInstrumentoPregunta>();
        }

        public string _PZBQSTN_CODE { get; set; }
        public string _PZBQSTN_NAME { get; set; }
        public string _PZBQSTN_DESCRIPTION { get; set; }
        public string _PZBQSTN_USER { get; set; }
        public System.DateTime _PZBQSTN_ACTIVITY_DATE { get; set; }
        public string _PZBQSTN_DATA_ORIGIN { get; set; }

        public  ICollection<AsignacionInstrumentoPregunta> _PZBPLQS { get; set; }
    }
}