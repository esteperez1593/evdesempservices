﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class Area
    {

        public Area()
        {
            this._PZBPLQS = new HashSet<AsignacionInstrumentoPersona>();
        }

        public string _PZBAREA_CODE { get; set; }
        public string _PZBAREA_NAME { get; set; }
        public string _PZBAREA_DESCRIPTION { get; set; }
        public string _PZBAREA_USER { get; set; }
        public System.DateTime _PZBAREA_ACTIVITY_DATE { get; set; }
        public string _PZBAREA_DATA_ORIGIN { get; set; }

        public  ICollection<AsignacionInstrumentoPersona> _PZBPLQS { get; set; }
    }
}