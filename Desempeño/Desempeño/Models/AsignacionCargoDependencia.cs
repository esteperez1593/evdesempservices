﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class AsignacionCargoDependencia
    {
        public AsignacionCargoDependencia()
        {
            this._PZRASPE = new HashSet<AsignacionCargoPersona>();
        }

        public string _PZRDYPT_POST_CODE { get; set; }
        public string _PZRDYPT_DNCY_CODE { get; set; }
        public string _PZRDYPT_USER { get; set; }
        public System.DateTime _PZRDYPT_ACTIVITY_DATE { get; set; }
        public string _PZRDYPT_DATA_ORIGIN { get; set; }

        public  ICollection<AsignacionCargoPersona> _PZRASPE { get; set; }
        public  Dependencia _PZVDNCY { get; set; }
        public  Cargo _PZVPOST { get; set; }
    }
}