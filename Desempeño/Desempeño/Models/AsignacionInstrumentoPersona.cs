﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class AsignacionInstrumentoPersona
    {

        public AsignacionInstrumentoPersona()
        {
            this._PZRAWAP = new HashSet<Respuesta>();
            this._PZRRSPL = new HashSet<ResultadosParciales>();
            this._PZRRSTT = new HashSet<ResultadosTotales>();
        }

        public int _PZRPLAP_SEQ_NUMBER { get; set; }
        public int _PZRPLAP_APPL_SEQ_NUMBER { get; set; }
        public string _PZRPLAP_POLL_CODE { get; set; }
        public string _PZRPLAP_CALR_CODE { get; set; }
        public int _PZRPLAP_PIDM { get; set; }
        public string _PZRPLAP_DNCY_CODE { get; set; }
        public string _PZRPLAP_POST_CODE { get; set; }
        public string _PZRPLAP_PFLE_CODE { get; set; }
        public string _PZRPLAP_STATUS { get; set; }
        public Nullable<System.DateTime> _PZRPLAP_DATE { get; set; }
        public string _PZRPLAP_USER { get; set; }
        public System.DateTime _PZRPLAP_ACTIVITY_DATE { get; set; }
        public string _PZRPLAP_DATA_ORIGIN { get; set; }

        public InstrumentoAplicable _PZBAPPL { get; set; }
        public AsignacionCargoPersona _PZRASPE { get; set; }
        public ICollection<Respuesta> _PZRAWAP { get; set; }
        public ICollection<ResultadosParciales> _PZRRSPL { get; set; }
        public ICollection<ResultadosTotales> _PZRRSTT { get; set; }

    }
}