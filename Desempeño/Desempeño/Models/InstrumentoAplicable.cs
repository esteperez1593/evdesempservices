﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{
    public class InstrumentoAplicable
    {
        public InstrumentoAplicable()
        {
            this._PZBAPRS = new HashSet<ResultadoAplicable>();
            this._PZBPLQS = new HashSet<AsignacionInstrumentoPregunta>();
            this._PZRPLAP = new HashSet<AsignacionInstrumentoPersona>();
        }

        public int _PZBAPPL_SEQ_NUMBER { get; set; }
        public string _PZBAPPL_POLL_CODE { get; set; }
        public string _PZBAPPL_CALR_CODE { get; set; }
        public string _PZBAPPL_PFLE_CODE { get; set; }
        public int _PZBAPPL_PRCNT { get; set; }
        public string _PZBAPPL_USER { get; set; }
        public System.DateTime _PZBAPPL_ACTIVITY_DATE { get; set; }
        public string _PZBAPPL_DATA_ORIGIN { get; set; }

        public ICollection<ResultadoAplicable> _PZBAPRS { get; set; }
        public ICollection<AsignacionInstrumentoPregunta> _PZBPLQS { get; set; }
        public ICollection<AsignacionInstrumentoPersona> _PZRPLAP { get; set; }
        public Calendario _PZBCALR { get; set; }
        public Instrumento _PZBPOLL { get; set; }
        public Perfil _PZVPFLE { get; set; }


    }
}