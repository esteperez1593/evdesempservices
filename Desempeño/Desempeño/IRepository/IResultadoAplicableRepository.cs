﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IResultadoAplicableRepository
    {
        List<ResultadoAplicable> GetPZBAPRS();
        ResultadoAplicable GetPZBAPRS(int id);
        ResultadoAplicable PutPZBAPRS(string id, 
                                      ResultadoAplicable pZRCDEF);
        ResultadoAplicable PostPZBAPRS(ResultadoAplicable pZRCDEF);
    }
}