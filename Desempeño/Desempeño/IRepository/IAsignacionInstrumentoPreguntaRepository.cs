﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IAsignacionInstrumentoPreguntaRepository
    {
        List<AsignacionInstrumentoPregunta> GetPZBPLQSs();

        List<Pregunta> GetInsPre(String idins);

        List<Pregunta> GetInsPre(String idins, String idcal);

        AsignacionInstrumentoPregunta PutPZBPLQS(int id, 
                                                 AsignacionInstrumentoPregunta pZBPLQS);
        AsignacionInstrumentoPregunta PostPZBPLQS(AsignacionInstrumentoPregunta _pZBPLQS);

    }
}