﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IMensajeRepository
    {
        List<Mensaje> GetPZRMSSG();
        List<Mensaje> GetPZRMSSGF();
        Mensaje PutPZRMSSG(decimal id,
                           Mensaje pZRMDEF);
        Mensaje PostPZRMSSG(Mensaje _pZRMDEF);

    }
}