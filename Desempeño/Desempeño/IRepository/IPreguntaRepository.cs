﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IPreguntaRepository
    {
        List<Pregunta> GetPZBQSTN();
        Pregunta PostPZBQSTN(Pregunta pZRQDEF);
        Pregunta PutPZBQSTN(string id, Pregunta pZRQDEF);

    }
}