﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IPerfilRepository
    {
        List<Perfil> GetPZVPFLE();
        Perfil PutPZVPFLE(String id, 
                          Perfil _pZRPDEF);
        Perfil PostPZVPFLE(Perfil _pZRPDEF);
    }
}