﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IPermisoRepository
    {
        List<Permiso> GetPZVPERMs();
        List<DefinicionPermisos> GetPZVPERM(int pidm);
        int GetPZVPERMMin(int pidm);
        Permiso PutPZVPERM(string id, 
                           Permiso _pZVLCOD);
        Permiso PostPZVPERM(List<Permiso> _pZVLCOD);
        Permiso PostPZVPERM(Permiso _pZVLCOD);
    }
}