﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IFaseRepository
    {
        List<Fase> GetPZBPHSEs();
        List<Fase> GetFasMen();
        Fase PutPZBPHSE(string id, Fase pZVTDEF);

        Fase PostPZBPHSE(Fase pZVTDEF);

    }
}