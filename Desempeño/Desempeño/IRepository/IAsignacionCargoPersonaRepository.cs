﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IAsignacionCargoPersonaRepository
    {
        List<AsignacionCargoPersona> GetPZRASPE();

        AsignacionCargoPersona GetPZRASPEId(int idper);

        AsignacionCargoPersona GetPZRASPEId(String _post, 
                                            String _dncy,
                                            String _pfle, 
                                            String _pidm);
        AsignacionCargoPersona PutPZRASPE(String _post, 
                                          String _dncy,
                                          String _pfle,
                                          String _pidm,
                                          AsignacionCargoPersona pZVLDEF);

        AsignacionCargoPersona PostPZRASPE(AsignacionCargoPersona pZVLDEF);


    }
}