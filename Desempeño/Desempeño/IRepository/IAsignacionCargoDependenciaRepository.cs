﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IAsignacionCargoDependenciaRepository
    {
        List<AsignacionCargoDependencia> GetAsignacionCargoDependencia();
        List<AsignacionCargoDependencia> GetEvaluados(String idcar, String iddep);
        AsignacionCargoDependencia GetPZRDYPT(String id);
        AsignacionCargoDependencia PutPZRDYPT(String id, String idc, AsignacionCargoDependencia pZVCDEF);
        AsignacionCargoDependencia GetById(decimal id);
        AsignacionCargoDependencia GetByName(String name);
        AsignacionCargoDependencia PostPZRDYPT(AsignacionCargoDependencia pZVCDEF);

    }
}