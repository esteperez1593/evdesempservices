﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IAsignacionInstrumentoPersonaRepository
    {
        List<AsignacionInstrumentoPersona> GetPZRPLAP();
        List<AsignacionInstrumentoPersona> GetPZRPLAPCalendar();
        AsignacionInstrumentoPersona GetPZRPLAPId(String id);
        decimal GetPZRPLAPCalendarCount(String id_calendario,
                                        String status);
        List<Persona> GetPZRPLAPCalendarBusqueda(String id_calendario,
                                                 String sede,
                                                 String depto);
        List<Persona> GetPZRPLAPCalendarSiteDep(String id_calendario,
                                                String sede,
                                                String depto);
        List<Persona> GetPZRPLAPCalendarCI(String id_calendario, 
                                           String ci);
        List<Persona> GetPZRPLAPCalendarSede(String id_calendario, 
                                             String sede);
        List<Persona> GetPZRPLAPCalendarSiteDep2(String id_calendario, 
                                                 String sede,
                                                 String depto);
        List<Persona> GetPZRPLAPCalendarP(String id_calendario);
        List<Persona> GetPZRPLAPCalendarC(String id_calendario);

        List<AsignacionInstrumentoPersona> GetPZRPLAPIdE(String id);

        List<AsignacionInstrumentoPersona> GetPZRPLAPEvaPen(String id);

        List<AsignacionInstrumentoPersona> GetPZRPLAPEvaPenDep(String id);

        List<AsignacionInstrumentoPersona> GetPZRPLAPIdI(String id);

        List<AsignacionInstrumentoPersona> GetPZRPLAPIdC(String id,
                                                         String idi);

        List<AsignacionInstrumentoPersona> GetPZRPLAPIdI(String ide,
                                                         String idi);

        List<AsignacionInstrumentoPersona> GetPZRPLAPAll();

        AsignacionInstrumentoPersona PutPZRPLAP(String id,
                                                AsignacionInstrumentoPersona _PZRPLAP);

        AsignacionInstrumentoPersona PostPZRPLAP(AsignacionInstrumentoPersona _PZRPLAP);

    }
}