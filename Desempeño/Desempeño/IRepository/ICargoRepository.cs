﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desempeño.Models
{

    interface ICargoRepository
    {
        List<Models.Cargo> GetPZVPOST();

        List<Models.Cargo> GetPZVPOST(String id);

        List<Models.Cargo> GetCargosYSupervisores();

        Models.Cargo PutPZVPOST(String id, Cargo pZCRCOD);

        Models.Cargo PostPVPOST(Cargo pZCRCOD);



    }
}
