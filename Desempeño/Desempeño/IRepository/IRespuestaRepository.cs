﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IRespuestaRepository
    {
        List<Respuesta> GetPZRAWAP();
        List<Respuesta> GetPZRAWAP(int id, 
                             string idins, 
                             string idcal);
        List<Respuesta> RespuestaGetPZRAWAP(int id, 
                                            String idins);
        Respuesta PutPZRAWAP(decimal id, Respuesta _pZVADEF);

        List<Respuesta> PostPZRAWAP(List<Respuesta> __pZVADEF);

        Respuesta PostPZRAWAPO(Respuesta _pZVADEF);

        Respuesta PutPZRPLAP(int id,
                             string user,
                             string status);

    }
}