﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface ICalendarioRepository
    {
        List<Calendario> GetPZBCALR();
        Calendario PutPZBCALR(string id, Calendario pZRCDEF);
        Calendario PostPZBCALR(Calendario pZRCDEF);

    }
}