﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IDependenciaRepository
    {
        List<Dependencia> GetPZVDNCYs();
        Dependencia GetPZVDNCY(String id);
        Dependencia PutPZVDNCY(String id,
                               Dependencia pZVDCOD);
        Dependencia PostPZVDNCY(Dependencia pZVDCOD);

    }
}