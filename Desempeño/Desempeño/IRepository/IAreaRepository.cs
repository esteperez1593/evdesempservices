﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IAreaRepository
    {
        List<Area> GetPZVAREA();

        Area GetPZBAREA(decimal id);
        Area GetByName(String name);
        Area PutPZBAREA(string id, Area pZRCDEF);
         Area PostPZBAREA(Area pZRCDEF);
    }
}