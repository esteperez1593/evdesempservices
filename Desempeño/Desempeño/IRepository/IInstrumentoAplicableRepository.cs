﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IInstrumentoAplicableRepository
    {
        List<InstrumentoAplicable>  GetPZBAPPL();
        InstrumentoAplicable PutPZBAPPL(string id, 
                                        InstrumentoAplicable pZRCDEF);
        InstrumentoAplicable GetByName(String name);

        InstrumentoAplicable PostPZBAPPL(InstrumentoAplicable pZRCDEF);
    }
}