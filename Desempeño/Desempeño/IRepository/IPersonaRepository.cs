﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IPersonaRepository
    {
        List<Persona> GetPZBPRSO();
        Persona PutPZBPRSO(decimal id, Persona pZPERSO);
        Persona PostPZBPRSO(Persona pZPERSO);

    }
}