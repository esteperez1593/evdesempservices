﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IInstrumentoRepository
    {
        List<Instrumento> GetPZBPOLL();
        List<Instrumento> GetPZBPOLLP(int pidm, String status);
        Instrumento PutPZBPOLL(String id, Instrumento pZRIDEF);
        Instrumento PostPZBPOLL(Instrumento pZRIDEF);

    }
}