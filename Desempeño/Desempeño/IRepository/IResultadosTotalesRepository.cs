﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IResultadosTotalesRepository
    {
            List<ResultadosTotales> GetPZRRSTTs();    
            ResultadosTotales PutPZRRSTT(int id,
                                         ResultadosTotales _PZRRSTT);
            ResultadosTotales PostPZRRSTT(ResultadosTotales _PZRRSTT);

        }
    }
