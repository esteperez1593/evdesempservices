﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IResultadosParcialesRepository
    {

        List<ResultadosParciales> GetPZRRSPLs();

        ResultadosParciales PutPZRRSPL(int id, 
                                       ResultadosParciales _PZRRSPL);

        ResultadosParciales PostPZRRSPL(ResultadosParciales _PZRRSPL);

    }
}