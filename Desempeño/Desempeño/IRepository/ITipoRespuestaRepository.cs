﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface ITipoRespuestaRepository
    {
        List<TipoRespuesta> GetPZVTYAWs();
        TipoRespuesta PutPZVTYAW(string id, 
                                 TipoRespuesta _PZVTYAW);
        TipoRespuesta PostPZVTYAW(TipoRespuesta PZVTYAW);

    }
}