﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface ICriterioRepository
    {
        Criterio GetPZBCRIT();
        
        Criterio PutPZBCRIT(string id,
                            Criterio PZBCRIT);
        Criterio PostPZBCRIT(Criterio PZBCRIT);

    }
}