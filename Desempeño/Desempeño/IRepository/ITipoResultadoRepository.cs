﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface ITipoResultadoRepository
    {
        List<TipoResultado> GetPZVTYRSs();
        TipoResultado PutPZVTYRS(string id, 
                                 TipoResultado _PZVTYRS);
        TipoResultado PostPZVTYRS(TipoResultado _PZVTYRS);
    }
}