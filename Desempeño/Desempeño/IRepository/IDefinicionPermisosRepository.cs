﻿using Desempeño.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IDefinicionPermisosRepository
    {
        List<DefinicionPermisos> GetPZVRGHTs();
        List<DefinicionPermisos> GetPZVRGHTs(int _rol);
        DefinicionPermisos PutPZVRGHT(string id, 
                                      DefinicionPermisos _pZPRFIL);
        DefinicionPermisos PostPZVRGHT(DefinicionPermisos _pZPRFIL);

    }
}